# ![Musa](http://www.tut.fi/musa-project/wp-content/uploads/2015/07/cropped-MUSA-logo-square1.png "Musa") MUSA-DEPLOYER

This is the source code for musa deployer. 

> Note that you may need request code and credentials for accessing a specs server. See below.

## Integrate into IDE:

- clone project
- get SPECS project dependencies (musa-deployer-broker and data-model at https://bitbucket.org/account/user/cerict/projects/MUSA), Ask CERICT team for internal dependencies and credentials.
- configure specs projects (user, password, etc) provided by CERICT
- [only if debugging broker]  in broker/src/main/resources/credentials, rename files as shown in /home/ubuntu/workspaceMUSA/musa-deployer-broker/src/main/java/eu/specs/project/enforcement/contextlistener/ContextListener.java
- add camel parser and jar dependencies to /lib folder
- **REMOVE guava 15 files at camel.jar since guava 16 is required by jclouds. (open JAR and remove com.google.common folder)**
- import project as maven project
- add /lib folder to build path
- maven update
- [only if running from ide] add start parameters (port and frameworkSessionAPI url) in the run configuration file
- [only if debugging broker] create run configuration and add SPECS environment variables (see specs-broker.sh)

## Usage:
- check startup variables (see above)
- add to /etc/hosts the following line: <internal_ip_of_chef_server>      MUSAControl
- if deployed: go to http://localhost to see documentation
- if not deployed: check /src/main/resources/index.md

This app is REST based:
- /plans/{id} to create implementation plan from camel definition
- /deployments/{id} to execute full deployment from a complete implementation plan
- /camels/{id} only to help deployment tests
- /agents/{IP} to get configuration for enforcement agents

More info at [MUSA Home](https://musa-project.eu/) | [Deployer video](https://youtu.be/TTK3Rn572Ss) | [Deployer public deliverable](https://musa-project.eu/documents2/d34-final-secure-multi-cloud-deployment-mechanisms-and-tools).

