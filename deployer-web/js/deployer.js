/**
 * Deployer generic utils
 * Usage:
 *  const deployer = window.createDeployer()
 */
window.createDeployer = function() {

  const LITERAL_MUSA_TOKEN = 'musa.sso.token'

  function getQueryStringParameter(key) {
    const params = (new URL(document.location)).searchParams
    const value = params.get(key)
    if (!value) alert(`Error: the URL must contain the "${key}" parameter`)
    return value
  }

  /**
   * Deployer utils Object
   * This is the object that a developer shoud use after invoking window.createDeployer()
   * @type {Object}
   */
  const deployer = {
    id: getQueryStringParameter('appId'),
    name: getQueryStringParameter('appName'),
    get url() { return window.location.origin },
    get securityHeaders() {
      let token = localStorage.getItem(LITERAL_MUSA_TOKEN)
      if (token && token.startsWith("\"")) token = token.substring(1, token.length - 1)
      if (token) return { "Authorization": token }
      else return { "Authorization": "dummy_token" }
    }
  }

  return deployer
}
