package eu.musa.deployer;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import eu.musa.deployer.broker.BrokerProxy;
import eu.musa.deployer.parsers.Deployment;
import eu.musa.deployer.persistence.Persistence;

/**
 * Main entry point for MUSA multicloud deployment plans
 * <p>Typically,  a deployment is started when the KANBAN module has all the info for deployment components
 *  into different multicloud providers </p>
 *  <ul>
 *  <li>inputs: camel (provider independent model) , SLAs, 
 *  and Cloud Providers fulfilling the deployment requirements for each component </li>
 *  <li>output: deployment plan in SPECS format, and command SPECS to deploy the components</li>
 *  </ul>
 */
public class Deployer {
	
	public static ConcurrentHashMap<String,Deployer> DEPLOYMENTS_IN_PROGRESS=new ConcurrentHashMap<String,Deployer>();
	
	private final String id;
	private final Logger logger;
	private BrokerProxy broker;
	private String token;
	
	public static Deployer getInstance(String id, String token){
		if(DEPLOYMENTS_IN_PROGRESS.containsKey(id)) return DEPLOYMENTS_IN_PROGRESS.get(id);
		else return new Deployer(id, token);
	}
	
	private Deployer (String id, String token){
		this.id=id;
		this.token = token;
		logger=Logger.getLogger(this.getClass().getName());
	}
	
	public void deploy() throws Exception{
		try {
			if(DEPLOYMENTS_IN_PROGRESS.containsKey(id)) {
				throw new Exception("Deployment "+id+" is currently running");
			}else{
				DEPLOYMENTS_IN_PROGRESS.put(id,this);
			}
			logger.info("Starting deploying "+id);
			checkSPECSBroker();
			BrokerProxy.ACTION action = Persistence.exists(Deployment.getURI(id)) ? BrokerProxy.ACTION.REDEPLOY : BrokerProxy.ACTION.DEPLOY;
			broker = new BrokerProxy(id, action, token);
			broker.start();
		} catch (Exception e) {
			logger.severe(e.getMessage());
			DEPLOYMENTS_IN_PROGRESS.remove(id);
			throw e;
		}
	}
	
	public void undeploy() throws Exception{
		try {
			logger.info("Starting undeployment of  "+id);
			checkSPECSBroker();
			// if deployment still running, stop deployment
			// stopBroker();
			if(DEPLOYMENTS_IN_PROGRESS.containsKey(id)) {
				throw new Exception("Process"+id+" is currently running");
			}else{
				DEPLOYMENTS_IN_PROGRESS.put(id,this);
			}
			broker = new BrokerProxy(id, BrokerProxy.ACTION.UNDEPLOY, token);
			broker.start();
		} catch (Exception e) {
			logger.severe(e.getMessage());
			DEPLOYMENTS_IN_PROGRESS.remove(id);
			throw e;
		}
	}
	
	private void stopBroker(){
		if (broker!=null  && broker.isAlive()){
			broker.interrupt();
			try{Thread.sleep(2000);}catch(Exception e){;};
		}
	}	
	   
	private static void checkSPECSBroker() throws Exception{
		if (System.getenv("PROVIDER_USERNAME")==null) throw new Exception("SPECS CLIENT IS NOT CONFIGURED PROPERLY");
	}

	
}
