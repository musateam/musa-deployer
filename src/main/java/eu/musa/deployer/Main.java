package eu.musa.deployer;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import eu.musa.deployer.framework.ClientSession;
import eu.musa.deployer.web.WebServer;

/**
 * Main entry point of deployer app.
 * 
 * <ul> Notes:
 * 	<li>some SPECS env variables must be set for valid deployment credentials </li>
 * 	<li>This class name must be set at entry point in pom.xml file for building to production</li>
 * </ul>
 */
public class Main {
	
	public static void main(String[] args) throws Exception {
		int port ;
		if (args.length != 2) {
			port = Integer.valueOf(args[0]);
			ClientSession.URL_SERVICE_SESSION = args[1];
			ClientSession.URL_SERVICE_DEPLOYER = args[2];
		}else{
			throw new Exception("Start java app with right  parameters!!!");
		}
		configureLogger();
		WebServer webServer=new WebServer(port);
	}
	
	private static void  configureLogger() throws SecurityException, IOException{
		FileInputStream fis =  new FileInputStream("java.util.logging.properties");
	    LogManager.getLogManager().readConfiguration(fis);
	    System.setProperty("java.util.logging.FileHandler.formatter","java.util.logging.SimpleFormatter");
	}
	
	private static void configureLoggerDevelopment(){
		Logger logger=Logger.getLogger("eu.musa");
		logger.setLevel(Level.ALL);
		for (Handler handler:logger.getParent().getHandlers()){
			if (handler instanceof ConsoleHandler) handler.setLevel(Level.ALL);
		}
	}
	
}
