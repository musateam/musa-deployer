package eu.musa.deployer.broker;

import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.ThreadContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.musa.deployer.Deployer;
import eu.musa.deployer.framework.ClientSession;
import eu.musa.deployer.parsers.Plan;
import eu.musa.deployer.persistence.Persistence;
import eu.musa.deployer.web.rest.RESTClient;
import eu.specs.datamodel.broker.ProviderCredential;
import eu.specs.datamodel.broker.ProviderCredentialsManager;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Csp;
import eu.specs.project.enforcement.broker.provisioning.Provisioner;

/**
 * Access SPECS BROKER while hiding its functionality.
 * <p>This class is a handy way to call broker operations, but exposing only as a high level methods and hiding environment variables, access configuration, Loggers and so on. </p>
 * <p>To use this class, just add broker as a Maven dependency, configure the broker log4j2.xml to use ThreadContext Routes and run the proxy class a thread to allow concurrent broker executions without messing the logs</p>
 * <p>If just needed for test, no log conf is required, and just start as normal class instead of Thread<p>
 */
public class BrokerProxy extends Thread{
	
	private String id;
	public static enum ACTION{DEPLOY,REDEPLOY,UNDEPLOY,DELETEVMs};
	private ACTION action;
	/**
	 * The PRIVATE logger, these logs are for developers and administrators ONLY
	 */
	private Logger logger;
	/**
	 * The PUBLIC logger, these logs are for generating the deployment report
	 */
	private org.apache.logging.log4j.Logger report;
	private String executionToken;
	private String authorizationToken;
	
	/**
	 * Constructor
	 * @param id typically the multi-cloud deployment application id
	 */
	public BrokerProxy(String id, ACTION action, String authorizationToken){
		this.id=id;
		this.authorizationToken =  authorizationToken;
		logger=Logger.getLogger(this.getClass().getName()+"#"+id);
		this.action=action;
		this.executionToken="appID:"+id;
		report=LogManager.getLogger(Provisioner.class.getName());
		//addCustomAppender(logger2);
	}
	
	
	@Override
	public void run() {
		// add id to Logger Context
		String[] data = executionToken.split(":");
		ThreadContext.put(data[0].trim(),data[1].trim());
		report.info("\n\n");
		report.info("------------------------------");
		report.info("Starting action "+action.toString());
		report.info("------------------------------");
		switch(action){
			case DEPLOY:
				deployPlan();
				break;
			case REDEPLOY:
				deployPlan();
				break;
			case UNDEPLOY:
				undeployPlan();
				break;
			default:
				logger.severe("The action "+action.toString()+" is not implemented. This is probably a developer error");
		}
	}
	
	private void deployPlan(){
		logger.info("Starting deploying "+id);
		report.info("Starting deploying "+id);
		try {
			ImplementationPlanTwoProviders impPlan=Plan.getInstance(id).getPlan();
			ProviderCredentialsManager providerCredentialsManager=prepareAuthenticacion(impPlan);
			impPlan.setId(UUID.randomUUID().toString());
			Provisioner provisioner=new Provisioner(providerCredentialsManager, executionToken);
			// redeploy will perform a hard redeployment (delete ALL vms,. and create them again from plan)
			if ( action == ACTION.REDEPLOY) provisioner.redeployVms(impPlan) ; else  provisioner.deployVms(impPlan);
			// once deployment is OK, replace the deployer plan with an updated plan from SPECS
			try{
				store_SPECS_Plan(impPlan.getSlaId());
			}catch (Exception e){
				report.warn("The deployment was successful but an error ocurred when updating the plan with the real VM IPs. Check your Cloud resources for these real IPs");
			}
			//callSecAP( id, true);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error when deploying " +id + ". " + e.getMessage(), e);
			report.error("Error when deploying "+id + ". " + e.getMessage(), e );
			String msg = "Finished (with ERRORS) deploying "+id;
			logger.info(msg);
			report.info(msg);
		}finally{
			// end of deployment process
			String msg = "Finished deploying " + id + " (check log above)";
			logger.info(msg);
			report.info(msg);
			logger.info("------------------------------");
			report.info("------------------------------");
			Deployer.DEPLOYMENTS_IN_PROGRESS.remove(id);
		}
	}
	
	/**
	 * The SPECS plan contains the IPs for the required machines
	 * @throws Exception 
	 */
	private void store_SPECS_Plan(String sla_id) throws Exception{
		logger.fine("Store the SPECS plan into the current one");
		ImplementationPlanTwoProviders specsPlan =  Provisioner.getImplementationPlanFromDatabagItem(sla_id);
		if(specsPlan == null) throw new NullPointerException("Plan sla=" + sla_id+ " at remote Chef server was not found");
		Plan plan =  Plan.getInstance(id, specsPlan);
		logger.finest("THE SPECS (EXTERNAL) IMP PLAN AFTER DEPLOYMENT IS: ");
		logger.finest(plan.toString()); 
		System.out.println(plan.toString());
		Persistence.update(Plan.getURI(id), plan.toString());
	}
	
	private void undeployPlan() {
		logger.info("Starting undeploying "+id);
		report.info("Starting undeploying "+id);
		try {
			ImplementationPlanTwoProviders impPlan=Plan.getInstance(id).getPlan();
			ImplementationPlanTwoProviders remoteImpPlan=Provisioner.getImplementationPlanFromDatabagItem(impPlan.getSlaId());
			if (remoteImpPlan == null) {
				String msg="The implementation plan with sla_id=\"" + impPlan.getSlaId() + "\" was not found in the Chef server.  Undeployment with the the local plan will be tried instead. If errors, a hard undeployment can be done manually by removing the vms and the security group at the VM management application (i.e: at Horizon)";
				logger.warning(msg);
				report.warn(msg);
				//try undeploying with the local plan
				remoteImpPlan=impPlan;
			}
			ProviderCredentialsManager validProviderCredentialsManager=prepareAuthenticacion(remoteImpPlan);
			Provisioner validProvisioner=new Provisioner(validProviderCredentialsManager, executionToken);
			validProvisioner.deleteAllVms(remoteImpPlan);
			// Persistence.delete(Deployment.getURI(id));//WARNING cannot delete deploy file because next deployment doesn't trace to file since it is lost by log4j2 existing objects
	//		callSecAP( id, false);
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Error when undeploying "+id+ ". " + e.getMessage(), e);
			report.error("Error when undeploying "+id + ". " + e.getMessage(), e);
			String msg = "Finished (with ERRORS) undeploying "+id;
			logger.info(msg);
			report.info(msg);
		} finally {
			Deployer.DEPLOYMENTS_IN_PROGRESS.remove(id);
			String msg = "Finished undeploying " + id + " (check log above)";
			logger.info(msg);
			report.info(msg);
			logger.info("------------------------------");
			report.info("------------------------------");
			Deployer.DEPLOYMENTS_IN_PROGRESS.remove(id);
		}
	}
	
	private static ProviderCredentialsManager prepareAuthenticacion(ImplementationPlanTwoProviders impPlan){
		ProviderCredential openStack = new ProviderCredential(System.getenv("PROVIDER_USERNAME"), System.getenv("PROVIDER_PASSWORD"), System.getenv("PROVIDER_ENDPOINT"));
		ProviderCredential amazon = new ProviderCredential(System.getenv("PROVIDER_USERNAME2"), System.getenv("PROVIDER_PASSWORD2"), System.getenv("PROVIDER_ENDPOINT2"));
		ProviderCredentialsManager credentials = new ProviderCredentialsManager();
		for (Csp csp : impPlan.getCsps()){	
			String cloud = csp.getIaas().getProvider();
			if(cloud.equals("ec2"))  credentials.add(csp, amazon) ; else credentials.add(csp, openStack);	
		}
		return credentials;
	}
	
	/**
	 * Call the SecAP platform once the deployment/undeployment has finished.
	 * If error, just log without throwing exceptions (the deployment was ok although the SecAP process had an error)  
	 * @param id
	 * @param mustStart
	 */
//	private void callSecAP(String id, boolean mustStart) {
//		String url = "";
//		try {
//			try{
//				 url = ClientSession.getValue(id, ClientSession.KEY_startMonitoringUrl, authorizationToken);
//			} catch (IOException ioe){
//				String msg = "Error when requesting session " + id +"." +ioe.getMessage() ;
//				logger.info(msg);
//				report.info(msg);
//				throw ioe;
//			}
//			url = mustStart ? url : url.replace("start=1", "start=0");
//			RESTClient.GET(url, null);
//		} catch (Exception e) {
//			String msg = "Error when calling secAP url: '" + url + "'. " + e.getMessage() ;
//			logger.warning(msg);
//			report.warn(msg);
//		}
//	}
	
}

