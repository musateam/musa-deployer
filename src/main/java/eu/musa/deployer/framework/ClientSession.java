package eu.musa.deployer.framework;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.musa.deployer.web.rest.RESTClient;

/**
 * Session object from framework (KANBAN)
 * 
 *  * <p>Session example:</p>
 * <pre>
 * {
	  "applicationId": 3,
	  "applicationName": "New application",
	  "ModelTS": "2017-05-02 08:05:12",
	  "RiskTS": "",
	  "DstTS": "",
	  "SlaTemplateTS": "",
	  "PlannerTS": "",
	  "SlaTS": "",
	  "DeployerTS": "",
	  "components": [
	    {
	      "cid": "5deb22a3-0b46-457f-8b61-b0db5ffc1eb5",
	      "name": "Component",
	      "column": "Model"
	    },
	    {
	      "cid": "5deb22a3-0b46-457f-8b61-b0db5ffc1eb6",
	      "name": "Component2",
	      "column": "Model"
	    }
	  ],
	  "modelUrl": "http://localhost:883/model/3",
	  "getModelUrl": "http://localhost:883/model/3",
	  "riskUrl": "http://localhost:8888/app/3",
	  "getRiskUrl": "http://localhost:8888/app/3",
	  "slaUrl": "http://localhost:8888/slaTool/3",
	  "getSlaUrl": "http://localhost:8888/slaTool/3",
	  "getSlaTemplateUrl": "http://localhost:8888/slaTool/3",
	  "dstUrl": "",
	  "getDstUrl": "",
	  "plannerUrl": "",
	  "deployerUrl": "",
	  "getDeployerLogUrl": ""
	}
 * </pre>
 */
public class ClientSession {
	
	public static final String KEY_PlannerTS = "PlannerTS";
	public static final String KEY_DeployerTS = "DeployerTS";
	public static final String KEY_SecurityHeader ="Authorization";
	public static final String KEY_startMonitoringUrl ="startMonitoringUrl"; 
	public static final String KEY_getSlaUrl="getSlaUrl";
	
	public static String URL_SERVICE_SESSION;
	public static String URL_SERVICE_DEPLOYER;
	
	
	public static String getValue(String id, String key, String token) throws IOException {
		Map<String, Object> session = getSession(id, token);
		if (session.containsKey(key) == false) throw new IOException("Session object does not contain key '" + key + "'");
		else return session.get(key).toString();
	}
	
	public static void setValue(String id, String key, String value, String token) throws IOException {
		Map<String, Object> session = getSession(id, token);
		session.put(key, value);
		ObjectMapper mapper = new ObjectMapper();
		String payload = mapper.writeValueAsString(session);
		Map<String,String> params = new HashMap<>();
		params.put("", payload);
		RESTClient.REQUEST(URL_SERVICE_SESSION + id, "put", getHeaders(token), params);
	}
	
	public static void updateTimestamp(String id, String key, String token) throws IOException {
		setValue(id, key, Instant.now().toString(), token);
	}
	
	public static String getSessionRAW(String id, String token) throws IOException{
		return RESTClient.GET(URL_SERVICE_SESSION + id, getHeaders(token));
	}
	
	public static Map<String, Object> getSession(String id, String token) throws IOException {
		String data = RESTClient.GET(URL_SERVICE_SESSION + id, getHeaders(token));
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> session =  (Map<String, Object>) mapper.readValue(data, new TypeReference<HashMap<String, Object>>(){});
		session = checkDeployerUrls(id, session, token);
		return session;
	}
	
	public  static Map<String, String> getHeaders(String token) {
		Map<String,String> headers = new HashMap<>();
		if (token !=null) headers.put(ClientSession.KEY_SecurityHeader, token);
		return headers;
	}
	
	/**
	 * If session has empty deployer urls, insert them.
	 * @param id
	 * @param session
	 * @param token
	 * @return
	 * @throws IOException
	 */
	private static Map<String, Object>   checkDeployerUrls(String id, Map<String, Object> session, String token) throws IOException {
		if (session.get("getPlannerUrl") == null || session.get("getPlannerUrl").toString().equals("") ) {
			// add urls
			String plan = URL_SERVICE_DEPLOYER + "plans/" + id;
			String deployment = URL_SERVICE_DEPLOYER + "deployments/" + id;
			session.put("getPlannerUrl", plan);
			//session.put("deployerUrl", URL_SERVICE_DEPLOYER + "deploy.html?appId=" + id + "&appName=" + session.get("applicationName"));
			session.put("getDeployerLogUrl", deployment);
			session.put("getDeployedPlanUrl", plan);
			//  update session service
			ObjectMapper mapper = new ObjectMapper();
			String payload = mapper.writeValueAsString(session);
			Map<String,String> params = new HashMap<>();
			params.put("", payload);
			RESTClient.REQUEST(URL_SERVICE_SESSION + id, "put", getHeaders(token), params);
		}
		return session;
	}
}
