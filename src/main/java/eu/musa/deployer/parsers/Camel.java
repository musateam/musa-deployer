package eu.musa.deployer.parsers;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.camel_dsl.dsl.CamelDslStandaloneSetup;
import org.camel_dsl.dsl.camelDsl.impl.CamelPlusModelImpl;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.util.StringInputStream;

import com.google.inject.Injector;

import eu.musa.deployer.web.rest.RESTClient;

public class Camel {
	
	public static final String FOLDER="db/camels/";
	private final String id;
	private final CamelPlusModelImpl model;
	
	private Camel(String id){
		this.id=id;
		String path=getURI(id);
		CamelDslStandaloneSetup.doSetup();
		CamelDslStandaloneSetup setup=new CamelDslStandaloneSetup();
		Injector injector = setup.createInjectorAndDoEMFRegistration();
		XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);
		Resource resource = resourceSet.getResource(URI.createFileURI(path),true);// load a resource by URI, in this case from the file system
		model=(CamelPlusModelImpl)resource.getContents().get(0);
	}
	
	private Camel(String id, String data) throws IOException{
		this.id=id;
		CamelDslStandaloneSetup.doSetup();
		CamelDslStandaloneSetup setup=new CamelDslStandaloneSetup();
		Injector injector = setup.createInjectorAndDoEMFRegistration();
		XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		Resource resource = resourceSet.createResource(URI.createURI("dummy:/"+id+".camel")); 
		resource.load(new StringInputStream(data), null);
		model=(CamelPlusModelImpl)resource.getContents().get(0); 
	}
	
	public static Camel getInstance(String id, String urlCamel) throws FileNotFoundException, IOException{
		String data = RESTClient.GET(urlCamel, null);
		return new Camel(id, data);
	}
	
	public static Camel newInstance(String id, String data) throws FileNotFoundException, IOException{
		return new Camel(id,data);
	}
	
	
	public static String getURI(String id){
		return FOLDER+id;
	}
	
	public String getId() {
		return id;
	}

	public CamelPlusModelImpl getModel() {
		return model;
	}

}
