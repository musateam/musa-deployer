package eu.musa.deployer.parsers;

public class Deployment {
	
	public static final String FOLDER="db/deployments/";
	
	public static String getURI(String id){
		return FOLDER+id;
	}
	
}
