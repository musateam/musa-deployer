package eu.musa.deployer.parsers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.camel_dsl.deployment.Configuration;
import org.camel_dsl.deployment.ConfiguratorCHEF;
import org.camel_dsl.deployment.DeploymentModel;
import org.camel_dsl.deployment.Hosting;
import org.camel_dsl.deployment.InternalComponent;
import org.camel_dsl.deployment.MUSAContainer;
import org.camel_dsl.deployment.ProvidedCommunication;
import org.camel_dsl.deployment.RequiredCommunication;
import org.w3c.dom.Element;

import eu.musa.deployer.framework.ClientSession;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Component;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Csp;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Iaas;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Paas;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Pool;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Vm;

public class Parser {
	
	private  String id;
	private  String token;
	private Camel camel;
	private Plan plan;
	private Map<String, Box> locator;
	private Map<String, String[]> definitions;
	private Map<String,Vm> tempVms = new HashMap<>();
	
	public Parser(String id, String token) throws Exception {
		this.id = id;
		this.token = token;
		String urlModel;
		try {
			urlModel = ClientSession.getValue(id, "getModelUrl", token);
		} catch (IOException e) {
			throw new IOException("Problems with session " + id + ". " + e.getMessage());
		}
		try {
			this.camel = Camel.getInstance(id,urlModel);
		} catch (Exception e) {
			throw new IOException("Problems with camel  " + id + "  at " + urlModel + ". " + e.getMessage() , e);
		}
		try{
			this.plan = Plan.newInstance(id);
		} catch (Exception e) {
			throw new Exception("Error when creating Implementation plan", e);
		}
	}
	
	public Plan parse() throws Exception{
		parseGeneralData(plan);
		locator = parseCSPs();
		insertCamelComponents();
		addVMSeqNumber();
		insertMonitoringAgents();
		patch22();
		return plan;
	}
	
	private void parseGeneralData(Plan plan) throws IOException{
		String dName = ClientSession.getValue(id, "applicationName", token);
		String name;
		String timestamp = Long.toString(System.currentTimeMillis());
		if (dName  != null && dName.length() > 2  &&  dName.toLowerCase().contains("new application") == false) {
			name = id + "-" +dName; 
			name =  name.toLowerCase().replaceAll("[^A-Za-z0-9]", "");  // specs sla does not allow some characters
		} else {
			 name = "deployer-"+timestamp;
		}
		plan.getPlan().setSlaId(name);
		plan.getPlan().setCreationTime(new Date());
	}
	
	private Map<String, Box>  parseCSPs() throws Exception{
		definitions = getComponentsFromSession( id, token);
		return fillCSPs(definitions );
	}
	
	/**
	 * Get a list of <componentName, [cid, slaUrl]> from session service,
	 * @param id
	 * @param token
	 * @return
	 * @throws IOException
	 * @throws NoSuchFieldException 
	 */
	private Map<String, String[]> getComponentsFromSession(String id, String token) throws IOException, NoSuchFieldException {
		Map<String, Object> session = ClientSession.getSession(id, token);
		Map<String, String[]> components = new HashMap(); 
		List<Map<String,Object>> columns = (List<Map<String,Object>>) session.get("columns");
		for (Map<String,Object> column : columns) {
			boolean isValidColumn = column.get("name").equals("Plan Deployment") || column.get("name").equals("Deploy");
			if (!isValidColumn) continue; //process only items allocated in the deployment columns
			List<Map<String,Object>> items = (List<Map<String,Object>> ) column.get("items");
			for (Map<String,Object> item : items) {
				String name = item.get("text").toString();
				String cid= item.get("text").toString();
				String slaUrl = "";
				try { 
					slaUrl=  item.get(ClientSession.KEY_getSlaUrl).toString(); 
				} catch (Exception e) {
					if (!Slat.mustSiMULATE) throw new NoSuchFieldException(ClientSession.KEY_getSlaUrl +  " was not found for component Id " + cid);  
				}
				components.put(name, new String[] { cid, slaUrl });
			}
		}
		return components;
	}
	
	/**
	 * Create all the containers (CSP and VMs) where the components will be inserted into.
	 * @param componentDefinitions the component name and [componentId, slaUrl]
	 * @return the componentName <-> Container(csp and vm)
	 * @throws Exception 
	 */
	private 	Map<String, Box>  fillCSPs(Map<String, String[]> componentDefinitions ) throws Exception {
		Map<String, Box> componentLocator = new HashMap<>();
		Map<String, Csp> csps = new HashMap<>();
		for(String name : componentDefinitions.keySet()) {
			String cid = componentDefinitions.get(name)[0];
			String slaUrl = componentDefinitions.get(name)[1];
			Slat slat = new Slat(cid, slaUrl, token);
			
			// create the Csp that will host the component
			Iaas iaas = parseIaaS(slat.getIaaS());
			Csp csp;
			String network = iaas.getNetwork();
			if (csps.containsKey(network)) {
				csp = csps.get(network);
			} else {
				csp = new Csp();
				csps.put(network, csp);
				csp.setIaas(iaas);
				Pool pool = new Pool();
				pool.setPoolSeqNum(0);
				csp.getPools().add(pool);
				plan.getPlan().getCsps().add(csp);
			}
			

			// add csp and  vm-skeleton, to link component name with the container to be inserted into
			Vm vm = parseVM(slat.getVM());
			componentLocator.put(name, new Box(csp, vm));
		}
		return componentLocator;
	}
	
	private Iaas parseIaaS(Element el) {
		Iaas iaas=new Iaas();
		iaas.setProvider(el.getAttribute("name"));
		iaas.setZone(el.getAttribute("zone"));
		iaas.setUser(el.getAttribute("user"));
		iaas.setNetwork(el.getAttribute("network"));
		return iaas;
	}
	
	private Vm  parseVM( Element el) {
		Vm vm = new Vm();
		// vm.setVmSeqNum(r); this value will be set later
		vm.setAppliance(el.getAttribute("appliance"));
		vm.setHardware(el.getAttribute("hardware"));
		return vm;
	}
	
	private Paas parsePaaS(InternalComponent camelComponent)  {
		List<MUSAContainer> camelContainers = camel.getModel().getDeploymentModels().get(0).getContainers();
		MUSAContainer container = null;
		for(MUSAContainer c : camelContainers) {
			if (c.getName().contentEquals(camelComponent.getRequiredContainer().getName())){
				container = c;
				break;
			}
		}
		Paas paas = new Paas();
		paas.setAllocationStrategy(container.getAllocationStrategyEnum().getName());
		paas.setPaasAgent(container.getType().getName());
		return paas;
	}
	
	private void insertMonitoringAgents() {
		List<Csp> csps = plan.getPlan().getCsps();
		for(Csp csp :csps) {
			for (Pool pool : csp.getPools()) {
				for(Vm vm : pool.getVms()) addMonitoringAgents(vm);
			}
		}
	}
	
	private void insertCamelComponents() throws Exception {
		Comparator<InternalComponent> byOrder = (a, b) -> Integer.compare(a.getOrder(), b.getOrder()); 
		for (DeploymentModel dm:camel.getModel().getDeploymentModels()){
			List<InternalComponent> arr= dm.getInternalComponents().stream().sorted(byOrder).collect(Collectors.toList());
			for (InternalComponent item : arr)parseInternalComponent(item);
		}
	}
	
	/**
	 * If docker, return the  PaaS box where docker is installed.
	 * Otherwise return the VM box where component will be deployed.
	 * @param camelComponent
	 * @param csp
	 * @return
	 * @throws Exception 
	 */
	private List<Component> getComponentWrapper(InternalComponent camelComponent) throws Exception {
		Box box = locator.get(camelComponent.getName());
		if (box == null) box =guessMUSAAgentsBox(camelComponent); // MUSA agents are in camel but not in cid session
		boolean isDocker = camelComponent.getRequiredContainer() != null ? true : false;
		if (isDocker) {
			if (box.csp.getPaas() == null) box.csp.setPaas(parsePaaS(camelComponent));
			return box.csp.getPaas().getComponents();
		} else {
			String host = camelComponent.getRequiredHost().getName();
			if(tempVms.containsKey(host)== false) tempVms.put(host, box.vm);
			List<Vm> vms =box.csp.getPools().get(0).getVms();
			Vm realVM=tempVms.get(host);
			if(vms.contains(realVM)== false) vms.add(realVM);
			return realVM.getComponents();
		}
	}
	
	private Box guessMUSAAgentsBox(InternalComponent camelComponent) throws Exception{
		String name= camelComponent.getName();
		String host=camelComponent.getRequiredHost().getName();
		Optional<String> optional=	camel.getModel().getDeploymentModels().stream().map(dm->{
			 List<Hosting> names = dm.getHostings().stream().filter(el->el.getRequiredHost().getName().equals(host)?true:false).collect(Collectors.toList());
			 Optional<Hosting> hosting=names.stream().findFirst();
			return (hosting.isPresent()) ? hosting.get().getProvidedHost().getName().replace("Host",""):"";
		}).filter(el->el.equals("")==false).findFirst();
		if(!optional.isPresent()) throw new Exception("Cannot find container VM for component "+name);
		return locator.get(optional.get());
	}
	
	private void parseInternalComponent(InternalComponent internalComponent) throws Exception { 
		for (Configuration configuration:internalComponent.getConfigurations()){
			ConfiguratorCHEF configCHEF=configuration.getConfManager();
			if (configCHEF!=null){
				List<Component> container = getComponentWrapper(internalComponent);
				Component component = Plan.createComponent();
				component.setImplementationStep(container.size()); // impl. step [0 = first step...n]
				component.setComponentId(internalComponent.getName());
				component.setCookbook(configCHEF.getCookbook().replace("'",""));
				component.setRecipe(configCHEF.getRecipe().replace("'",""));
				if (internalComponent.isIPpublic()) component.setAcquirePublicIp(true);
				component.setPrivateIps(new ArrayList<String>());
				for (ProvidedCommunication comm:internalComponent.getProvidedCommunications()) {
					component.getFirewall().getIncoming().getProto().get(0).getPort_list().add(Integer.toString(comm.getPortNumber())) ;
				}
				for (RequiredCommunication comm:internalComponent.getRequiredCommunications()) {
					component.getFirewall().getOutcoming().getProto().get(0).getPort_list().add(Integer.toString(comm.getPortNumber())) ;
				}
				container.add(component);
			}
		}
	}
	
	class Box {
		public Csp csp;
		public Vm vm;
		
		public Box(Csp csp, Vm vm) {
			this.csp = csp;
			this.vm = vm;
		}
	}
	

	private String listToString(Collection<String> list ){
		String[] arr=list.toArray(new String[]{});
		StringBuilder sb=new StringBuilder();
		sb.append("[");
		for(int r=0;r<arr.length;r++){
			if (r!=0) sb.append(",");
			sb.append("\"");sb.append(arr[r]);sb.append("\"");
		}
		sb.append("]");
		return sb.toString();
	}
	
	/*
	 *[{
  "component_id" : "SAP-install",
  "cookbook" : "install-mmt-network",
  "recipe" : "default",
  "implementation_step" : 0,
  "acquire_public_ip" : false,
  "firewall" : {
    "incoming" : {
      "source_ips" : [ ],
      "source_nodes" : [ ],
      "interface" : "private:1",
      "proto" : [ {
        "port_list" : [],
        "type" : "TCP"
      } ]
    },
    "outcoming" : {
      "destination_ips" : [ ],
      "destination_nodes" : [ ],
      "interface" : "private:1",
      "proto" : [ {
        "port_list" : [ ],
        "type" : "TCP"
      } ]
    }
  },
  "private_ips" : [ ]
},
{
  "component_id" : "SAP-start",
  "cookbook" : "start-mmt-network",
  "recipe" : "default",
  "implementation_step" : 1,
  "acquire_public_ip" : false,
  "firewall" : {
    "incoming" : {
      "source_ips" : [ ],
      "source_nodes" : [ ],
      "interface" : "private:1",
      "proto" : [ {
        "port_list" : [],
        "type" : "TCP"
      } ]
    },
    "outcoming" : {
      "destination_ips" : [ ],
      "destination_nodes" : [ ],
      "interface" : "private:1",
      "proto" : [ {
        "port_list" : [ ],
        "type" : "TCP"
      } ]
    }
  },
  "private_ips" : [ ]
}]
	 */
	
	/**
	 * Every VM should have the monitoring agents installed and started.
	 * @param vm
	 */
	private void  addMonitoringAgents(Vm vm) {
		List<Component> list= new ArrayList<>();
		
		Component c1 = Plan.createComponent();
		c1.setImplementationStep(0);
		c1.setComponentId("secAP-install");
		c1.setCookbook("install-mmt-network");
		c1.setRecipe("default");
		list.add(c1);
		
		Component c2 = Plan.createComponent();
		c2.setImplementationStep(1);
		c2.setComponentId("secAP-start");
		c2.setCookbook("start-mmt-network");
		c2.setRecipe("default");
		list.add(c2);
		
		vm.getComponents().stream().forEach(el->{
			el.setImplementationStep(el.getImplementationStep()+2);
			list.add(el);
		});
	
		vm.setComponents(list);
	}
	
	/**
	 * always open 22 port on every machine since Chef server must access to it
	 */
	private void patch22(){
		plan.getPlan().getCsps().stream().forEach(csp->{
			csp.getPools().stream().forEach(pool->{
				pool.getVms().stream().forEach(vm->{
					List<Component> c= vm.getComponents();
					if (c!=null && c.size()>0) c.get(0).getFirewall().getIncoming().getProto().get(0).getPort_list().add("22");
				});
			});
		});
	}
	
	private void addVMSeqNumber(){
		plan.getPlan().getCsps().stream().forEach(csp->{
			csp.getPools().stream().forEach(pool->{
				int r=0;
				for(Vm vm:pool.getVms()) vm.setVmSeqNum(r++);
			});
		});
	}
}
