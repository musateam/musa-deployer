package eu.musa.deployer.parsers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.musa.deployer.persistence.Persistence;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Component;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Firewall;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Incoming;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Outcoming;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Proto;

/*
 * SPECS implementation plan model & utils
 */
public class Plan {
	
	public static final String FOLDER="db/plans/";
	private ImplementationPlanTwoProviders plan;
	private final String id;
	
	private Plan(String id) throws JsonParseException, JsonMappingException,FileNotFoundException, IOException{
		this(id,null,true);
	}
	
	private Plan(String id,String data) throws JsonParseException, JsonMappingException, IOException{
		this(id,data,true);
	}
	
	private Plan(String id,String data, boolean isNew) throws JsonParseException, JsonMappingException, IOException{
		this.id=id;
		String uri=getURI(id);
		if (isNew){
			if (Persistence.exists(uri)) throw new IOException("Resource already exists");
			if (data==null || data=="") data="{}";
			//TODO temporary, load some data from imp plan (once Cloud Providers data is available, vms and other things can be loaded)
			//data=Persistence.read(getURI("1475414197597"));
		}else{
			data=Persistence.read(uri);
		}
		ObjectMapper mapper = new ObjectMapper();
		plan=mapper.readValue(data, ImplementationPlanTwoProviders.class);
	}
	
	private Plan(String id, ImplementationPlanTwoProviders impPlan, String real_SLA_ID) {
		this.id = id;
		this.plan =impPlan;
	}
	
	public static Plan getInstance(String id) throws Exception{
		return new Plan(id,null,false);
	}
	
	public static Plan newInstance(String id) throws Exception{
		return new Plan(id,null);
	}
	
	public static Plan newInstance(String id, String data) throws Exception{
		return new Plan(id,data);
	}

	public static Plan newInstance(String id, ImplementationPlanTwoProviders implPlan) throws Exception{
		Plan plan = new Plan(id);
		plan.setPlan(implPlan);
		return plan;
	}
	
	public static Plan getInstance(String id, ImplementationPlanTwoProviders implPlan) throws Exception{
		return new Plan(id,implPlan,implPlan.getSlaId());
	}
	
	public static boolean  checkValidPlan(String data) throws Exception{
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.readValue(data, ImplementationPlanTwoProviders.class); return true;
		} catch (Exception e) {
			throw new Exception("Unable to parse plan");
		}
	}
	
	public static String getURI(String id){
		return FOLDER+id;
	}
	
	public ImplementationPlanTwoProviders getPlan() {
		return plan;
	}
	
	private void setPlan(ImplementationPlanTwoProviders implPlan) {
		this.plan =  implPlan;
	}

	public String getId() {
		return id;
	}
	
	/**
	 * Serialize the impl. plan into a JSON text
	 */
	@Override
	public String toString(){
		try{
			ObjectMapper objectMapper = new ObjectMapper();
			//objectMapper.setSerializationInclusion(Include.NON_NULL);
			return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(plan);
		}catch(Exception e){
			return e.getMessage();
		}
	}
	
	/**
	 * Create an empty NON-NULLABLE implPlan component object
	 * @return
	 */
	public  static  Component createComponent() { 
				Component component=new Component();
				Firewall firewall=new Firewall();
				component.setFirewall(firewall);
				
				Incoming incoming=new Incoming(); 
				incoming.setSourceIps(new ArrayList<Object>());
				incoming.setSourceNodes(new ArrayList<Object>());
				incoming.setInterface("private:1");//TODO
				// Add TCP ports
				List<Proto> incomingProtocols=new ArrayList<Proto>();
				Proto incomingProtoTcp = new Proto();
				incomingProtoTcp.setPort_list(new ArrayList<>(new TreeSet<>()));
				incomingProtoTcp.setType("TCP");
				incomingProtocols.add(incomingProtoTcp);
				incoming.setProto(incomingProtocols);
				firewall.setIncoming(incoming);
				
				Outcoming outcoming=new Outcoming();
				outcoming.setDestinationIps(new ArrayList<Object>());//TODO
				outcoming.setDestinationNodes(new ArrayList<Object>());//TODO
				outcoming.setInterface("private:1");//TODO
				// Add TCP ports
				List<Proto> outcomingProtocols=new ArrayList<Proto>();
				Proto outcomingProtoTcp = new Proto();
				outcomingProtoTcp.setPort_list(new ArrayList<>(new TreeSet<>()));
				outcomingProtoTcp.setType("TCP");
				outcomingProtocols.add(outcomingProtoTcp);
				outcoming.setProto(outcomingProtocols);
				firewall.setOutcoming(outcoming);
				
				return component;
	}
}