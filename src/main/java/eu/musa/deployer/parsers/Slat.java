package eu.musa.deployer.parsers;

import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import eu.musa.deployer.framework.ClientSession;
import eu.musa.deployer.web.rest.RESTClient;

/**
 * Manage MUSA  SLA Templates
 * 
 * Usage: 
 * <pre>
 *  (new Slat(componentID, token)).getIaaS()
 * </pre>
 */
public class Slat {
	
	public  static  boolean mustSiMULATE = (System.getenv("MUSA_SIMULATE") != null && System.getenv("MUSA_SIMULATE").toLowerCase().equals("true") ) ? true : false;
	private static Pattern PATTERN = Pattern.compile("<.+:serviceResources>[\\s\\S]*?<\\/.+:serviceResources>", Pattern.MULTILINE);
	
	private Document document;
	private String cid; //component ID
	private String namespace;
	
	public Slat(String cid, String url, String token) throws Exception {
		
		this.cid = cid;
		// extract/clean relevant data from SLAT service
		String data = mustSiMULATE ? SLA_EXAMPLE : RESTClient.GET(url, ClientSession.getHeaders(token));
		Matcher m = PATTERN.matcher(data);
		if (!m.find()) throw new Exception ("The SLAT  at " + url + " does not contain 'serviceResources' tag"); 
		String text = m.group();
		namespace=getNamespace(text);
		// convert to XML
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    InputSource is = new InputSource(new StringReader(text));
	    document = builder.parse(is);
	}
	
	public Element getIaaS() throws Exception{
		NodeList nodes = document.getElementsByTagName( namespace+":resourcesProvider");
	    if (nodes.getLength() != 1) throw new Exception("Slat for component " + cid + " must contain 1 IaaS (and only 1) but there were retrieved " + nodes.getLength() +" IaaS");
	    return (Element) nodes.item(0);
	}
	
	public Element  getVM() throws Exception {
		Element iaas = getIaaS();
	    NodeList nodes = iaas.getElementsByTagName( namespace+":VM"); 
	    if (nodes.getLength() != 1) throw new Exception("Slat for component " + cid + " must contain 1 VM (and only 1) but there were retrieved " + nodes.getLength() +" VMs");
	    return (Element) nodes.item(0);
	}
	
	/**
	 * ...getElementsByTagNameNS requires uri, search only in childs (no deep), useless Node class ("\") vs useful Element class  etc.. The DOM XML implementation in any language is a disservice to inteligence
	 * Get the namespace name 8and not URI from a xml snipppet root element
	 */
	private String getNamespace(String text){
		return text.substring(1,text.indexOf(":"));
	}
	
	public static final String SLA_EXAMPLE2 = "                    \n" + 
				"                    <specs:serviceResources>\n" + 
				"                        <specs:resourcesProvider id=\"openstack\" name=\"OpenStack\" zone=\"RegionOne\" description=\"\" user=\"ubuntu\" maxAllowedVMs=\"20\" network=\"38a20d88-71a0-4880-9436-43b025a8b18d\" label=\"\">\n" + 
				"                            <specs:VM appliance=\"RegionOne/0afc5035-e48b-4d0a-85f8-d6486be818dd\" hardware=\"RegionOne/888ecb92-e0b5-4b98-a9bc-a08dcbac6f29\" description=\"\" />\n" + 
				"                        </specs:resourcesProvider>\n" + 
				"                    </specs:serviceResources>";

	public static final String SLA_EXAMPLE = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" + 
			"<ns1:AgreementOffer xmlns:ns5=\"http://www.specs-project.eu/resources/schemas/xml/control_frameworks/ccm\" xmlns:ns1=\"http://schemas.ggf.org/graap/2007/03/ws-agreement\" xmlns:ns4=\"http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist\" xmlns:ns3=\"http://www.specs-project.eu/resources/schemas/xml/SLAtemplate\">\n" + 
			"    <ns1:Name>TSMEngine_assessed_slat</ns1:Name>\n" + 
			"    <ns1:Context>\n" + 
			"        <ns1:AgreementInitiator>CUSTOMER</ns1:AgreementInitiator>\n" + 
			"        <ns1:AgreementResponder>PROVIDER</ns1:AgreementResponder>\n" + 
			"        <ns1:ServiceProvider>AgreementResponder</ns1:ServiceProvider>\n" + 
			"        <ns1:TemplateName>ASSESSED_SLA_TEMPLATE</ns1:TemplateName>\n" + 
			"    </ns1:Context>\n" + 
			"    <ns1:Terms>\n" + 
			"        <ns1:All>\n" + 
			"            <ns1:ServiceDescriptionTerm ns1:Name=\"TSMEngine_assessed_slat\" ns1:ServiceName=\"CUSTOM WEB APP\">\n" + 
			"                <ns3:serviceDescription>\n" + 
			"                    <ns3:serviceResources>\n" + 
			"                        <ns3:resourcesProvider id=\"openstack\" name=\"OpenStack\" zone=\"RegionOne\" maxAllowedVMs=\"20\" description=\"\" label=\"\" user=\"ubuntu\" network=\"38a20d88-71a0-4880-9436-43b025a8b18d\">\n" + 
			"                            <ns3:VM appliance=\"RegionOne/0afc5035-e48b-4d0a-85f8-d6486be818dd\" hardware=\"RegionOne/896a5000-e140-440d-bce8-97c8cb832b9e\" description=\"\"/>\n" + 
			"                        </ns3:resourcesProvider>\n" + 
			"                    </ns3:serviceResources>\n" + 
			"                    <ns3:capabilities>\n" + 
			"                        <ns3:capability mandatory=\"false\">\n" + 
			"                            <ns3:controlFramework id=\"NIST_800_53_r4\" frameworkName=\"NIST Control framework 800-53 rev. 4\"/>\n" + 
			"                        </ns3:capability>\n" + 
			"                    </ns3:capabilities>\n" + 
			"                    <ns3:security_metrics>\n" + 
			"                        <ns3:Metric name=\"Level of confidentiality\" referenceId=\"CONFID_LEVEL\">\n" + 
			"                            <MetricDefinition>\n" + 
			"                                <expression></expression>\n" + 
			"                                <definition>This metric indicates the level of confidentiality achieved by a system regarding client data independently of the means used to achieve this objective.</definition>\n" + 
			"                            </MetricDefinition>\n" + 
			"                        </ns3:Metric>\n" + 
			"                        <ns3:Metric name=\"Data availability\" referenceId=\"DATA_AV\">\n" + 
			"                            <MetricDefinition>\n" + 
			"                                <expression>Output=uptime / (uptime+downtime)</expression>\n" + 
			"                                <definition>Percentage of time in which data access is available to data owners</definition>\n" + 
			"                            </MetricDefinition>\n" + 
			"                        </ns3:Metric>\n" + 
			"                    </ns3:security_metrics>\n" + 
			"                </ns3:serviceDescription>\n" + 
			"            </ns1:ServiceDescriptionTerm>\n" + 
			"            <ns1:GuaranteeTerm ns1:Name=\"TSMEngine\">\n" + 
			"                <ns1:QualifyingCondition>false</ns1:QualifyingCondition>\n" + 
			"                <ns1:ServiceLevelObjective>\n" + 
			"                    <ns1:CustomServiceLevel>\n" + 
			"                        <ns3:objectiveList>\n" + 
			"                            <ns3:SLO SLO_ID=\"slo0\">\n" + 
			"                                <ns3:MetricREF>CONFID_LEVEL</ns3:MetricREF>\n" + 
			"                                <ns3:SLOexpression>\n" + 
			"                                    <ns3:oneOpExpression>\n" + 
			"                                        <ns3:operator>geq</ns3:operator>\n" + 
			"                                        <ns3:operand>2</ns3:operand>\n" + 
			"                                    </ns3:oneOpExpression>\n" + 
			"                                </ns3:SLOexpression>\n" + 
			"                            </ns3:SLO>\n" + 
			"                            <ns3:SLO SLO_ID=\"slo1\">\n" + 
			"                                <ns3:MetricREF>DATA_AV</ns3:MetricREF>\n" + 
			"                                <ns3:SLOexpression>\n" + 
			"                                    <ns3:oneOpExpression>\n" + 
			"                                        <ns3:operator>geq</ns3:operator>\n" + 
			"                                        <ns3:operand>99</ns3:operand>\n" + 
			"                                    </ns3:oneOpExpression>\n" + 
			"                                </ns3:SLOexpression>\n" + 
			"                            </ns3:SLO>\n" + 
			"                        </ns3:objectiveList>\n" + 
			"                    </ns1:CustomServiceLevel>\n" + 
			"                </ns1:ServiceLevelObjective>\n" + 
			"            </ns1:GuaranteeTerm>\n" + 
			"        </ns1:All>\n" + 
			"    </ns1:Terms>\n" + 
			"</ns1:AgreementOffer>";




}





