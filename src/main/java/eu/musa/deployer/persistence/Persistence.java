package eu.musa.deployer.persistence;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;


public  class Persistence {
	
	private static PersistenceFileSystem persistence;
	
	public  static void  create(String uri, String data) throws FileAlreadyExistsException,IOException{
		persistence.create(uri, data);
	}
	
	public  static  String  read(String uri) throws FileNotFoundException,IOException{
		return persistence.read(uri);
	}
	
	public  static  void update(String uri, String data) throws FileNotFoundException,IOException{
		persistence.update(uri, data);
	}
	
	public  static  void update(String uri, String data, boolean canCreate) throws IOException{
		persistence.update(uri, data, canCreate);
	}
	
	public   static void  delete(String uri) throws FileNotFoundException,IOException{
		persistence.delete(uri);
	}
	
	public  static boolean  exists(String uri) throws FileNotFoundException,IOException{
		return persistence.exists(uri);
	}
}
