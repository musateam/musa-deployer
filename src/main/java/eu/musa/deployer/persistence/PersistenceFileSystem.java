package eu.musa.deployer.persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.emf.cdo.util.InvalidURIException;

public class PersistenceFileSystem {
	
	private static final SimpleDateFormat DF= new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
	private static final String ROOT_FOLDER="db/";
	
	public static void  create(String uri, String data) throws FileAlreadyExistsException,IOException{
		checkSecurity(uri);
		String parent="";
		int index=uri.lastIndexOf("/");
		if (index>-1) parent=uri.substring(0,index);
		File file=new File(uri);
		File folder=new File(parent);
		if (! folder.exists())  folder.mkdirs();
		if(file.exists()) throw new FileAlreadyExistsException(uri);
		FileWriter writer=new FileWriter(file);
		writer.write(data);
		writer.close();
		backup(uri,data);
	}
	
	public static  String  read(String uri) throws FileNotFoundException,IOException{
		checkSecurity(uri);
		File file=new File(uri);
		if (! file.exists()) throw new FileNotFoundException();
		return new String(Files.readAllBytes(Paths.get(uri)));
	}
	
	public static  void update(String uri, String data) throws FileNotFoundException,IOException{
		checkSecurity(uri);
		update(uri,data,false);
	}
	
	public static void  update(String uri, String data, boolean allowCreate) throws FileNotFoundException,IOException{
		checkSecurity(uri);
		String parent="";
		int index=uri.lastIndexOf("/");
		if (index>-1) parent=uri.substring(0,index);
		File file=new File(uri);
		File folder=new File(parent);
		if (! folder.exists())  folder.mkdirs();
		if (! allowCreate){
			if(!file.exists()) throw new FileNotFoundException(uri);
		}
		FileWriter writer=new FileWriter(file);
		writer.write(data);
		writer.close();
		backup(uri,data);
	}
	
	public static  void  delete(String uri) throws FileNotFoundException,IOException{
		checkSecurity(uri);
		File file=new File(uri);
		if (! file.exists()) throw new FileNotFoundException();
		if (file.isDirectory()) throw new IOException("For security reasons, deleting a folder is not allowed");
		
		Files.delete(Paths.get(uri));
	}
	
	public  static boolean  exists(String uri) throws FileNotFoundException,IOException{
		checkSecurity(uri);
		File file=new File(uri);
		if (file.exists()) return true; else return false;
	}
	
	private static void backup(String uri, String data) throws FileNotFoundException, IOException{
		uri="db/backups/"+uri+"/" +DF.format(new Date())+" "+System.currentTimeMillis();
		String parent="";
		int index=uri.lastIndexOf("/");
		if (index>-1) parent=uri.substring(0,index);
		File file=new File(uri);
		File folder=new File(parent);
		if (! folder.exists())  folder.mkdirs();
		FileWriter writer=new FileWriter(file);
		writer.write(data);
		writer.close();
	}

	/**
	 * Prevent accessing files outside the database (ie: ../../keys.txt)
	 * @param uri
	 * @throws IOException
	 */
	private static void checkSecurity(String uri) throws IOException{
		if (uri.startsWith(ROOT_FOLDER)==false) throw new IOException();
	}
}
