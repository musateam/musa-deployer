package eu.musa.deployer.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.pegdown.Extensions;
import org.pegdown.PegDownProcessor;

public class MarkdownDispatcher {
	
	private static PegDownProcessor markdown;
	
	public static void process (HttpServletRequest request, HttpServletResponse response) throws IOException{
		response.setContentType("text/html;charset=utf-8");
		String uri=request.getPathInfo();System.out.println(uri);
		if (uri==null || uri.contentEquals("") || uri.equals("/") || uri.equals("/index.html")) uri="index";
		if (uri.startsWith("/")) uri = uri.substring(1);
		if (!uri.endsWith(".md")) uri=uri+".md";
		if (markdown==null) markdown=new PegDownProcessor(Extensions.ALL);
		InputStream is=MarkdownDispatcher.class.getClassLoader().getResourceAsStream(uri);
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb=new StringBuilder();
        reader.lines().forEach(line -> sb.append(line+"\n"));
		response.getWriter().write(getHTML(sb.toString()));
	}
	
	private static String  getHTML(String content){
		String style="";
		//String style="<link rel='stylesheet' href='http://kevinburke.bitbucket.org/markdowncss/markdown.css'></link>";
		//String style="<link rel='stylesheet' href='https://cdn.jsdelivr.net/github-markdown-css/2.4.1/github-markdown.css'></link>";
		String style2="<style>"+
			"body{width: 800px;margin: 0 auto;}h2{margin-top:1em}body{font-family:sans-serif} table{ color:darkslategray; border-collapse: collapse;border-spacing: 0;} table td ,table th{border:1px solid #ccc;;margin:0;padding:5px}"+
			"blockquote{margin: 0;padding: 10px;border: 1px solid; border-radius:5px}  "+
		"</style>";
		return 	"<html>"+
						"<head>"+
							"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" + 
								style+
								style2+
						"</head>"+
						"<body>"+
							markdown.markdownToHtml(content)+
							"<br/>"+
						"</body>"+
					"</html>";
	}
	
}
