package eu.musa.deployer.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;

import eu.musa.deployer.web.rest.ServiceAgent;
import eu.musa.deployer.web.rest.ServiceCamel;
import eu.musa.deployer.web.rest.ServiceDeploy;
import eu.musa.deployer.web.rest.ServicePlan;
import eu.musa.deployer.web.rest.ServiceSession;

public class WebServer {
	
	private final Logger logger;
	private static boolean isPRODUCTIONSERVER = false;
	
    public WebServer(int port) throws Exception    {
		logger=Logger.getLogger(this.getClass().getName());
		if (System.getenv().containsKey("isProduction") && System.getenv().get("NODE_ENV").contentEquals("1")) isPRODUCTIONSERVER = true;
        Server server = new Server(port);
        //server.setHandler(new WebHandler());
        staticAndynamicServer(server);
        server.start();
        logger.info("\n\n----------------------------------------\n\nServer started at http://localhost:"+port+"\n\n----------------------------------------");
        server.join();
    }
	
    private void staticAndynamicServer(Server server) {
        ResourceHandler resource_handler = new ResourceHandler();
        resource_handler.setDirectoriesListed(true);
        resource_handler.setWelcomeFiles(new String[]{ "index.html" });
        resource_handler.setResourceBase("./deployer-web");
        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[] { resource_handler, new WebHandler() });
        server.setHandler(handlers);
    }
    
    class WebHandler extends AbstractHandler{
    
    	public void handle(String target, Request baseRequest,HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
    		String logMessage=prepareLog(request,response);
        	response.setHeader("Access-Control-Allow-Origin", "*");
    		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE"); 
    		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
    		if (isPRODUCTIONSERVER == false)  response.setHeader("Cache-Control", "no-store"); // prevent caching 
    		try {
    			if (target.contains("..")){
    				throw new SecurityException();
    			}else if (target.startsWith("/test")){
		      		response.setStatus(200);
		    		response.getWriter().write("hello world");
				}else if (target.startsWith("/camels")){
					ServiceCamel.process(request, response);
				}else if (target.startsWith("/plans")){
					ServicePlan.process(request, response);
				}else if (target.startsWith("/deployments")){
					ServiceDeploy.process(request, response);
				}else if (target.startsWith("/agents")){
					ServiceAgent.process(request, response);
				}else if (request.getPathInfo().endsWith(".ico")){
					//
				}else if (target.startsWith("/session")){
					ServiceSession.process(request, response);
				}else{
					MarkdownDispatcher.process(request, response);
				}
        	} catch (SecurityException e) {
        		response.setHeader("Content-Type", "text/plain");
        		response.setStatus(403);
	    		response.getWriter().write(e.getMessage());
	    		//response.sendError(400,"YOU ARE NOT ALLOWED TO CALL THE MUSA-DEPLOYER. Contact musa-deployer administrators at Tecnalia.");
    		} catch (Exception e) {
    			response.setHeader("Content-Type", "text/plain");
        		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        		String message=e.getMessage();
        		if(message==null) message="";
        		message=e.getClass().getSimpleName()+": "+message;
        		response.getWriter().write(message);
    	        logger.log(Level.FINE,e.getMessage(),e);
    		} finally{
        		baseRequest.setHandled(true);
        		logger.info(logMessage+" "+response.getStatus());
        	}
        }
    	
    	private synchronized String prepareLog(HttpServletRequest request, HttpServletResponse response){
			String queryString=request.getQueryString();
			if (queryString==null) queryString="";else queryString="?"+queryString;
			return getRealUser(request)+" "+request.getMethod()+" "+request.getRequestURL().toString()+queryString;
    	}
    }
     
    private String getRealUser(HttpServletRequest request){
		String clientAddr;
		if(request.getHeader("X-Real-IP")!=null) clientAddr=request.getHeader("X-Real-IP");
		else clientAddr=request.getRemoteAddr(); 
		return clientAddr;
    }
     
    private static List<String> getFileLines(String fileName) throws IOException {
    	List<String> lines=new ArrayList<>(); 
    	FileReader fr=new FileReader(fileName);
    	BufferedReader reader = new BufferedReader(fr);
        reader.lines().forEach(line -> {if (line.trim().equals("")==false) lines.add(line);});
        reader.close();
        return lines;
    }
    
}
