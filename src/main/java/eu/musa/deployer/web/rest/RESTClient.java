package eu.musa.deployer.web.rest;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Basic REST client with no external dependencies and very easy to use.
 */
public class RESTClient {
	
	private static Logger LOGGER = Logger.getLogger(RESTClient.class.getName()); 
	 
    public static String GET(String url, Map<String,String> headers) throws IOException{
    	return REQUEST(url,"GET",headers,null);
    }
    
	/**
     * Perform an HTTP POST against a URL. 
     * <p>Please Note that the encoding charset for request-params is UTF-8<p>
     * @param url
     * @param headers
     * @param params key,value set. In case of json or plain data use ["",json], where key=""
     * @return
     * @throws IOException
     */
    public static String POST(String url, Map<String,String> headers, Map<String,String> params) throws IOException{
    	return REQUEST(url,"POST",headers,params);
    }
    
	 /**
     * Perform an HTTP POST against a URL. 
     * <p>Please Note that the encoding charset for request-params is UTF-8<p>
     * @param url
     * @param headers
     * @param parameters in JSON notation
     * @return
     * @throws IOException
     */
    public static String POST(String url, Map<String,String> headers, String jsonParams) throws IOException{
    	HashMap<String,String> params=new HashMap<>();
    	params.put("", jsonParams);
    	return REQUEST(url,"POST",headers,params);
    }
   
    public static String REQUEST(String url, String method, Map<String,String> headers, Map<String,String> params) throws IOException{
    	LOGGER.finest("calling " + method.toUpperCase() + " " + url);
    	HttpURLConnection conn=null;
    	PrintStream out=null;
    	InputStreamReader isr=null;
    	BufferedReader br=null;
    	try{
    		method=method.toUpperCase();
            URL addr=new URL(url);
            conn = (HttpURLConnection) addr.openConnection();
            if (headers!=null) {for(String key:headers.keySet()) conn.setRequestProperty(key, headers.get(key));}
            if(method.contentEquals("GET")){
            	;
            }else{
            	conn.setRequestMethod(method);
                conn.setDoOutput(true);
                out=new PrintStream(conn.getOutputStream());
                //if no key, params in json format
                if (params!=null){
                	if (params.size()==1 && params.containsKey("")){
                    	String param=params.get("");
                    	out.print(param);
                    }
                    // normal params key=value
                    else if(params.size()>0){
                    	StringBuilder sbout=new StringBuilder();
                        for(String key:params.keySet()){
                            String value=URLEncoder.encode(params.get(key), "UTF-8");   
                            if (sbout.length()>0) sbout.append("&");
                            sbout.append(key);sbout.append("=");sbout.append(value);
                        }
                        out.print(sbout.toString());
                    }
                }
                out.flush();
            }
            
            isr=new InputStreamReader(conn.getInputStream());
            br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null){sb.append(line);sb.append("\n");}
            return sb.toString(); 
    	}catch(IOException ioe){
    		throw new IOException(ioe.getMessage(),ioe);
    		//throw new IOException(ioe.getMessage() + ". " + url);
    	}finally{
    		if (br!=null)br.close();
    		if (isr!=null)isr.close();
    		if (out!=null)out.close();
    		if (conn!=null)conn.disconnect();
    	}
 
    }
    
    public static void main(String[] args) throws IOException {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6Im1hbmFnZXIxQG11c2EtcHJvamVjdC5ldSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhcHBfbWV0YWRhdGEiOnsicm9sZXMiOiJtYW5hZ2VyIiwiZ3JvdXBzIjpbInRlc3RpbmciLCJmbGlnaHRfc2NoZWR1bGluZyIsInNtYXJ0X21vYmlsaXR5Il19LCJpc3MiOiJodHRwczovL211c2EtcHJvamVjdC5ldS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTk1MjYzNmIzYmU1MzU0YjZhNzVmZjQ1IiwiYXVkIjoiUVV2Z2Rhak53VEJYdzlaeWF5VFJLU2VsR2lpdWRvMEoiLCJleHAiOjE1MDYwMTg4OTEsImlhdCI6MTUwNTk4Mjg5MX0.jtcZoI3nwMtv2Nod1ODjZVB3RX336koRhKsFwwSzaPk");
		System.out.println(REQUEST("http://framework.musa-project.eu/session/120", "get", headers, null));
	}
    
}