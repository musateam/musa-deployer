package eu.musa.deployer.web.rest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.musa.deployer.Deployer;
import eu.musa.deployer.framework.ClientSession;
import eu.musa.deployer.parsers.Deployment;
import eu.musa.deployer.parsers.Plan;
import eu.musa.deployer.persistence.Persistence;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Component;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Csp;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Pool;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Vm;

/**
 * REST service for enforcement agents configuration data.

 */
public class ServiceAgent{
	
	private static Pattern PATTERN = Pattern.compile("/agents(/?)(\\S*)");
	
	public static void process(HttpServletRequest request, HttpServletResponse response) throws Exception{
		response.setContentType("text/plain;charset=utf-8");
		Matcher m = PATTERN.matcher(request.getPathInfo()); 
		if (m.matches()){
			String id=m.group(m.groupCount());
			String method=request.getMethod().toLowerCase();
			if (method.equals("get")){
				if (id.contentEquals("")) get(request,response);
				else	get(id,request,response);
			}else if (method.equals("options")){
				response.setHeader("Allow"," HEAD,GET,OPTIONS");
				response.setStatus(HttpServletResponse.SC_OK);
			}else{
				response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
			}
		}else{
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
	
	private static void get(HttpServletRequest request, HttpServletResponse response) throws IOException{
		response.getWriter().write("Usage: http://deployer.musa-project.eu/agents/37.22.55.88");
	}
	
	
	private static void get(String ip, HttpServletRequest request, HttpServletResponse response) throws Exception{
		File dir = new File(Plan.FOLDER);
		String id = findPlan(ip, dir.listFiles());
		if (id == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		if (Deployer.DEPLOYMENTS_IN_PROGRESS.contains(id)) {
			response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
			return;
		}
		Map<String,List<String>> config = new HashMap<>();
		Plan plan = Plan.getInstance(id);
		AgentResponse payload = new AgentResponse();
		payload.id = id;
		payload.vm = ip;
		for (Csp csp : plan.getPlan().getCsps()) {
			for (Pool pool : csp.getPools()) {
				for (Vm vm : pool.getVms()) {
					for (Component component : vm.getComponents()) {
						String name = component.getComponentId();
						List <String> ips = component.getPrivateIps();
						if (name.contains("MUSAAgent") && ips.contains(ip)) {
							String recipe =  component.getRecipe();
							List<String> ports = component.getFirewall().getIncoming().getProto().get(0).getPort_list();
							payload.addAgent(name, recipe,  ports);
						}
					}
				}
			}
		}
		ObjectMapper objectMapper = new ObjectMapper();
		String result = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(payload);
		response.setContentType("application/json;charset=utf-8");
		response.getWriter().write(result);
	}
	
	public static class AgentResponse {
		
		public String id ;
		public String vm ;
		public List<Object> agents; 
		
		public void addAgent(String name, String recipe,  List<String> portList) {
			if (agents == null)  agents = new ArrayList<>();
			Map<String, Object> map = new HashMap<>();
			map.put("name", name);
			map.put("recipe", recipe);
			map.put("ports", portList);
			agents.add(map);
		}
	}
	
	private static String findPlan(String ip, File[] files) throws IOException{
		String result = null;
		Collection<File> sortedFiles = sortFilesTopLow(files); // sort files to prevent finding old deployments with same ip than new deployments 
		for (File file : sortedFiles) {
			Path path = Paths.get(file.getAbsolutePath());
			Stream<String> lines = Files.lines(path);
			Optional<String> optional = lines.parallel().filter(line -> line.contains(ip)).findAny();
			lines.close();
			if (optional.isPresent()){
				result = file.getName();
				break;
			}
		}
		return result;
	}
	
	private static Collection<File> sortFilesTopLow(File[] files) {
		Map<String, File> results = new TreeMap<String, File>((a, b) -> (Long.valueOf(a) <  Long.valueOf(b))? 1 : -1);
		for (File file : files) {
			if (file.isFile())
				results.put(file.getName(),file);
		}
		return results.values();
	}
	
}
