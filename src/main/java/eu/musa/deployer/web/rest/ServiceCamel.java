package eu.musa.deployer.web.rest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileAlreadyExistsException;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.musa.deployer.parsers.Camel;
import eu.musa.deployer.persistence.Persistence;

public class ServiceCamel  {
	
	private static Pattern PATTERN = Pattern.compile("/camels(/?)(\\d*)");
		
	public static void process(HttpServletRequest request, HttpServletResponse response) throws Exception{
		response.setContentType("application/json;charset=utf-8");
		Matcher m = PATTERN.matcher(request.getPathInfo()); 
		if (m.matches()){
			System.out.println("match");
			String id=m.group(m.groupCount());
			String method=request.getMethod().toLowerCase();
			if (method.equals("get")){
				if (id.contentEquals("")) get(request,response);
				else	get(id,request,response);
			}else if (method.equals("post")){
				if (id.equals("")) post(request,response);
				else post(id,request,response);
			}else if (method.equals("put")){
				if (id.equals("")==true) response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				else put(id,request,response);
			}else if (method.equals("options")){
				response.setHeader("Allow"," HEAD,GET,PUT,DELETE,OPTIONS");
				response.setStatus(HttpServletResponse.SC_OK);
			}else{
				response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
			}
		}else{
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
	
	private static void get(HttpServletRequest request, HttpServletResponse response) throws IOException{
		try{
			File dir=new File(Camel.FOLDER);
			File[] files=dir.listFiles();
			if (files==null) files=new File[]{};
			Set<String> ids=new TreeSet<String>((a,b)->{Long x=Long.valueOf(a); Long y=Long.valueOf(b);if (x>y) return 1;else if (x<y) return -1; else return 0;});
			for(File file:files){
				if (file.isFile())	ids.add(file.getName());
			}
			response.getWriter().write((new ObjectMapper()).writeValueAsString(ids));
		}catch(FileNotFoundException e){
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
	}
	

	private static void get(String id, HttpServletRequest request, HttpServletResponse response) throws IOException{
		if (id==null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}else{
			try{
				String resource=Persistence.read(Camel.getURI(id));
				response.getWriter().write(resource);
			}catch(FileNotFoundException e){
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}
		}
	}
	
	
	private static void post(HttpServletRequest request, HttpServletResponse response) throws Exception{
		post(Long.toString(System.currentTimeMillis()), request, response);
	
	}
	
	private static void post(String id,HttpServletRequest request, HttpServletResponse response) throws Exception{
		try {
			BufferedReader buffer = new BufferedReader(new InputStreamReader(request.getInputStream()));
			String sCamel=buffer.lines().collect(Collectors.joining("\n"));
			Camel.newInstance(id,sCamel); //check is syntactically correct
			try{
				String uri=Camel.getURI(id);
				Persistence.create(uri, sCamel);
				response.setHeader("Location", uri.replace("db/", ""));
				response.setStatus(HttpServletResponse.SC_CREATED);
			}catch(FileAlreadyExistsException e){
				response.setStatus(HttpServletResponse.SC_CONFLICT);
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	private static void put(String id,HttpServletRequest request, HttpServletResponse response) throws Exception{
		try {
			BufferedReader buffer = new BufferedReader(new InputStreamReader(request.getInputStream()));
			String sCamel=buffer.lines().collect(Collectors.joining("\n"));
			Camel.newInstance(id,sCamel); //check is syntactically correct
			try{
				String uri=Camel.getURI(id);
				Persistence.update(uri, sCamel);
				response.setHeader("Location", uri);
			}catch(FileNotFoundException e){
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
}
