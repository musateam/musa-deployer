package eu.musa.deployer.web.rest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.musa.deployer.Deployer;
import eu.musa.deployer.framework.ClientSession;
import eu.musa.deployer.parsers.Deployment;
import eu.musa.deployer.persistence.Persistence;

/**
 * REST service for real deployments.
 * <p>POST to execute a deployment and GET to retrieve the report of the deployment</p>
 */
public class ServiceDeploy {
	
	private static Pattern PATTERN = Pattern.compile("/deployments(/?)(\\d*)");
	
	public static void process(HttpServletRequest request, HttpServletResponse response) throws Exception{
		response.setContentType("text/plain;charset=utf-8");
		Matcher m = PATTERN.matcher(request.getPathInfo()); 
		if (m.matches()){
			String id=m.group(m.groupCount());
			String method=request.getMethod().toLowerCase();
			if (method.equals("get")){
				if (id.contentEquals("")) get(request,response);
				else	get(id,request,response);
			}else if (method.equals("post")){
				if (id.equals(""))  response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				else post(id,request,response);
			}else if (method.equals("put")){
				if (id.equals(""))  response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				else put(id,request,response);
			}else if (method.equals("delete")){
				if (id.equals(""))  response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				else delete(id,request,response);
			}else if (method.equals("options")){
				response.setHeader("Allow"," HEAD,GET,PUT,DELETE,OPTIONS");
				response.setStatus(HttpServletResponse.SC_OK);
			}else{
				response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
			}
		}else{
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
	
	private static void get(HttpServletRequest request, HttpServletResponse response) throws IOException{
		try{
			File dir=new File(Deployment.FOLDER);
			File[] files=dir.listFiles();
			if (files==null) files=new File[]{};
			Set<String> ids=new TreeSet<String>((a,b)->{Long x=Long.valueOf(a); Long y=Long.valueOf(b);if (x>y) return 1;else if (x<y) return -1; else return 0;});
			for(File file:files){
				if (file.isFile())	ids.add(file.getName());
			}
			response.setContentType("application/json;charset=utf-8");
			response.getWriter().write((new ObjectMapper()).writeValueAsString(ids));
		}catch(FileNotFoundException e){
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
	}
	
	/**
	 * Get the impl plan RESULTS for a given id, if results not available yet, return 404. If available, the deployment result is sent.
	 * @param id
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private static void get(String id, HttpServletRequest request, HttpServletResponse response) throws Exception{
		if (id==null){
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}else{
			String uri=Deployment.getURI(id);
			if (Persistence.exists(uri)){
				if(Deployer.DEPLOYMENTS_IN_PROGRESS.containsKey(id)) {
					response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT); //The deployment is still in progress
				}
				response.getWriter().write(Persistence.read(Deployment.getURI(id)));
			}
			else response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
	}

	private static void post(String id, HttpServletRequest request, HttpServletResponse response) throws Exception{
		try{
			String uri=Deployment.getURI(id);
			if (Persistence.exists(uri)){
				response.setStatus(HttpServletResponse.SC_CONFLICT);
				return;
			}
			String token = request.getHeader(ClientSession.KEY_SecurityHeader);
			if (token == null) {
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return;
			}
			Deployer deployer=Deployer.getInstance(id, token);
			deployer.deploy(); //async process, returns OK (while the process is still executing)
			//Persistence.create(uri, "");//No error when starting->create an empty resource->the process has started but not finished yet
			response.setHeader("Location", uri.replace("db/", ""));
			response.setStatus(HttpServletResponse.SC_CREATED);
			ClientSession.updateTimestamp(id, ClientSession.KEY_DeployerTS, token);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Only for re-deployment (if deployment, use post)
	 */ 
	private static void put(String id, HttpServletRequest request, HttpServletResponse response) throws Exception{
		try{
			String uri=Deployment.getURI(id);
			if (!Persistence.exists(uri)){
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				return;
			}
			String token = request.getHeader(ClientSession.KEY_SecurityHeader);
			if (token == null) {
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return;
			}
			Deployer deployer=Deployer.getInstance(id, token);
			deployer.deploy(); //async process, returns OK (while the process is still executing)
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			ClientSession.updateTimestamp(id, ClientSession.KEY_DeployerTS, token);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Delete deployment. If deployment is running, cancel deployment and delete.
	 */
	private static void delete(String id, HttpServletRequest request, HttpServletResponse response) throws Exception{
		String token = request.getHeader(ClientSession.KEY_SecurityHeader);
		if (token == null) {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		Deployer deployer=Deployer.getInstance(id, token);
		deployer.undeploy();
		response.setStatus(HttpServletResponse.SC_ACCEPTED);
		ClientSession.updateTimestamp(id, ClientSession.KEY_DeployerTS, token);
	}
	
}
