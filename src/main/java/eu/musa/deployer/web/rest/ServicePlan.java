package eu.musa.deployer.web.rest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.musa.deployer.framework.ClientSession;
import eu.musa.deployer.parsers.Parser;
import eu.musa.deployer.parsers.Plan;
import eu.musa.deployer.persistence.Persistence;

/**
 * REST service for getting and creating impl plans
 */
public class ServicePlan {

	private static Pattern PATTERN = Pattern.compile("/plans(/?)(\\d*)");

	public static void process(HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setContentType("application/json;charset=utf-8");
		Matcher m = PATTERN.matcher(request.getPathInfo());
		if (m.matches()) {
			String id = m.group(m.groupCount());
			String method = request.getMethod().toLowerCase();
			if (method.equals("get")) {
				if (id.contentEquals(""))
					get(request, response);
				else
					get(id, request, response);
			} else if (method.equals("post")) {
				if (id.equals(""))
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				else
					post(id, request, response);
			} else if (method.equals("put")) {
				if (id.equals("") == true)
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				else
					put(id, request, response);
			}else if (method.equals("delete")){
				if (id.equals(""))  response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				else delete(id,request,response);
			} else if (method.equals("options")) {
				response.setHeader("Allow", " HEAD,GET,PUT,DELETE,OPTIONS");
				response.setStatus(HttpServletResponse.SC_OK);
			} else {
				response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
			}
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	/**
	 * Get List of existing impl. plans
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private static void get(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			File dir = new File(Plan.FOLDER);
			File[] files = dir.listFiles();
			if (files == null)
				files = new File[] {};
			Set<String> ids = new TreeSet<String>((a, b) -> {
				Long x = Long.valueOf(a);
				Long y = Long.valueOf(b);
				if (x > y)
					return 1;
				else if (x < y)
					return -1;
				else
					return 0;
			});
			for (File file : files) {
				if (file.isFile())
					ids.add(file.getName());
			}
			response.getWriter().write((new ObjectMapper()).writeValueAsString(ids));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
	}

	/**
	 * Get impl plan for a given id, otherwise return error.
	 * <p>
	 * If no impl plan, but camel exists, a fresh impl plan is created from
	 * camel.
	 * </p>
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private static void get(String id, HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
				if (id == null) {
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					return;
				}
				String uri = Plan.getURI(id);
				if (Persistence.exists(uri) == false) {
					// first time requesting plan -> create it
					String token = request.getHeader(ClientSession.KEY_SecurityHeader);
					if (token == null) {
						response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
						return;
					}
					createPlanFromCamel(id, token);
					response.setStatus(HttpServletResponse.SC_CREATED);
					ClientSession.updateTimestamp(id, ClientSession.KEY_PlannerTS, token);
				}
				String resource = Persistence.read(Plan.getURI(id));
				response.getWriter().write(resource);
			} catch (FileNotFoundException e) {
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}
	}

	/**
	 * Create a new Implementation plan, given the id of the camel resource
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	private static void post(String id, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			String uri = Plan.getURI(id);
			if (Persistence.exists(uri)) {
				response.setStatus(HttpServletResponse.SC_CONFLICT);
				return;
			}
			String token = request.getHeader(ClientSession.KEY_SecurityHeader);
			if (token == null) {
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return;
			}
			Parser parser = new Parser(id, token);
			Plan plan = parser.parse();
			Persistence.create(uri, plan.toString());
			response.setHeader("Location", uri.replace("db/", ""));
			response.setStatus(HttpServletResponse.SC_CREATED);
			ClientSession.updateTimestamp(id, ClientSession.KEY_PlannerTS , token);
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Update an existing implementation plan. Input: the impl, plan in the
	 * request body.
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	private static void put(String id, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			String uri = Plan.getURI(id);
			if (Persistence.exists(uri) == false) {
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			} else {
				BufferedReader buffer = new BufferedReader(new InputStreamReader(request.getInputStream()));
				String data = buffer.lines().collect(Collectors.joining("\n"));
				if (Plan.newInstance("temp",data).getPlan().getSlaId() == null) throw new Exception("invalid format");//just to check that plan is parseable
				Persistence.update(uri, data);
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				ClientSession.updateTimestamp(id, ClientSession.KEY_PlannerTS , request.getHeader(ClientSession.KEY_SecurityHeader));
			}
		} catch (Exception e) {
			throw e;
		}
	}

	private static void delete(String id, HttpServletRequest request, HttpServletResponse response) throws Exception{
		String token = request.getHeader(ClientSession.KEY_SecurityHeader);
		if (token == null) {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		Persistence.delete(Plan.getURI(id));
		response.setStatus(HttpServletResponse.SC_ACCEPTED);
		ClientSession.updateTimestamp(id, ClientSession.KEY_DeployerTS, token);
	}
	
	
	private static void createPlanFromCamel(String id, String token) throws IOException {
		Plan plan;
		try {
			Parser parser = new Parser(id, token);
			plan = parser.parse();
		} catch (Exception e) {
			throw new IOException("Error when creating Implementation plan id=" + id + ". Cause: " + e.getMessage(), e);
		}
		Persistence.create(Plan.getURI(id), plan.toString());
	}

}
