package eu.musa.deployer.web.rest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.musa.deployer.persistence.Persistence;



/**
 * EMULATOR OF REST service for managing the DASHBOARD SESSION service
 * 
 * <p>Session example:</p>
 * <pre>
 * {
	  "applicationId": 3,
	  "applicationName": "New application",
	  "ModelTS": "2017-05-02 08:05:12",
	  "RiskTS": "",
	  "DstTS": "",
	  "SlaTemplateTS": "",
	  "PlannerTS": "",
	  "SlaTS": "",
	  "DeployerTS": "",
	  "components": [
	    {
	      "cid": "5deb22a3-0b46-457f-8b61-b0db5ffc1eb5",
	      "name": "Component",
	      "column": "Model"
	    },
	    {
	      "cid": "5deb22a3-0b46-457f-8b61-b0db5ffc1eb6",
	      "name": "Component2",
	      "column": "Model"
	    }
	  ],
	  "modelUrl": "http://localhost:883/model/3",
	  "getModelUrl": "http://localhost:883/model/3",
	  "riskUrl": "http://localhost:8888/app/3",
	  "getRiskUrl": "http://localhost:8888/app/3",
	  "slaUrl": "http://localhost:8888/slaTool/3",
	  "getSlaUrl": "http://localhost:8888/slaTool/3",
	  "getSlaTemplateUrl": "http://localhost:8888/slaTool/3",
	  "dstUrl": "",
	  "getDstUrl": "",
	  "plannerUrl": "",
	  "deployerUrl": "",
	  "getDeployerLogUrl": ""
	}
 * </pre>
 */
public class ServiceSession {
	
	private static Pattern PATTERN = Pattern.compile("/session(/?)(\\d*)");  

	public static void process(HttpServletRequest request, HttpServletResponse response) throws Exception{
		response.setContentType("application/json;charset=utf-8");
		Matcher m = PATTERN.matcher(request.getPathInfo()); 
		if (m.matches()){
			String id=m.group(m.groupCount());
			String method=request.getMethod().toLowerCase();
			if (method.equals("get")){
				if (id.contentEquals("")) get(request,response); 
				else	get(id,request,response); 
			}else if (method.equals("post")){
				if (id.equals("")) post(request,response); 
				else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else if (method.equals("put")){
				if (id.equals("")==true) response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				else put(id,request,response);
			}else if (method.equals("options")){
				response.setHeader("Allow"," HEAD,GET,PUT,DELETE,OPTIONS");
				response.setStatus(HttpServletResponse.SC_OK);
			}else{
				response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
			}
		}else{
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
		
	
	private static void get(HttpServletRequest request, HttpServletResponse response) throws IOException{
		try{
			File dir=new File(Session.FOLDER);
			File[] files=dir.listFiles();
			if (files==null) files=new File[]{};
			Set<Integer> ids=new TreeSet<Integer>((a,b)->{Long x=Long.valueOf(a); Long y=Long.valueOf(b);if (x>y) return 1;else if (x<y) return -1; else return 0;});
			for(File file:files){
				if (file.isFile())	ids.add(Integer.valueOf(file.getName()));
			}
			response.getWriter().write((new ObjectMapper()).writeValueAsString(ids));
		}catch(FileNotFoundException e){
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
	}
	

	private static void get(String id, HttpServletRequest request, HttpServletResponse response) throws IOException{
		if (id==null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}else{
			try{
				String resource=Persistence.read(Session.getURI(id));
				response.getWriter().write(resource);
			}catch(FileNotFoundException e){
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}
		}
	}
	
	/**
	 * Only for development purposes since the session object should be created by dashboard already
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	private static void post(HttpServletRequest request, HttpServletResponse response) throws Exception{
		try{	
			File dir=new File(Session.FOLDER);
			File[] files=dir.listFiles();
			if (files==null) files=new File[]{};
			String id=Integer.toString(files.length);
			Persistence.create(Session.getURI(id), "{\"applicationId\":"+id+",\"applicationName\":\"New Application\",\"columns\":[{\"name\":\"Model\",\"items\":[]},{\"name\":\"Risk\",\"items\":[]},{\"name\":\"Service Selection\",\"items\":[]},{\"name\":\"Plan Deployment\",\"items\":[]},{\"name\":\"SLA\",\"items\":[]},{\"name\":\"Execute\",\"items\":[]}]}");
			response.getWriter().write(id);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	private static void put(String id, HttpServletRequest request, HttpServletResponse response) throws Exception{
		try{	
			String uri=Session.getURI(id);
			if (Persistence.exists(uri)==false){
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}else{
				BufferedReader buffer = new BufferedReader(new InputStreamReader(request.getInputStream()));
				String data=buffer.lines().collect(Collectors.joining("\n"));
				Persistence.update(uri, data);
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	static class Session{
		public static final String FOLDER="db/sessions/";
		public static String getURI(String id){return FOLDER+id;}
	}
	
}
