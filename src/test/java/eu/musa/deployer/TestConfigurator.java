package eu.musa.deployer;

import eu.musa.deployer.framework.ClientSession;

public class TestConfigurator {
	
	public static String configure() {
		if (ClientSession.URL_SERVICE_SESSION == null) ClientSession.URL_SERVICE_SESSION = System.getenv("MUSA_URL_SESSION");
		String token = System.getenv("MUSA_TOKEN");
		return token;
	}
}
