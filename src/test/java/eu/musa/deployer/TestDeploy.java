package eu.musa.deployer;

import com.google.common.io.ByteSource;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestDeploy {
	
	/**
	 * Test if loader is google guava 15+, to check if Bytesource.wrap exists
	 */
	@Test
	public void testGoogleGuava(){
		boolean isTrue=false;
		try{
			ByteSource.wrap(new byte[0]);
			isTrue=true;
		}catch(Exception e){
			;
		}
		assertTrue("Guava 15+ must be installed ",isTrue);
	}
	
}
