package eu.musa.deployer.broker;

import static org.junit.Assert.*;

import org.junit.Test;

import eu.musa.deployer.parsers.Plan;
import eu.musa.deployer.persistence.Persistence;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders;
import eu.specs.project.enforcement.broker.provisioning.Provisioner;

public class TestBroker {
	
	@Test
	public void testGetImplementationPlanFromSPECS() {
		try {
			String id = "300";
			ImplementationPlanTwoProviders impPlan=Plan.getInstance(id).getPlan();
			ImplementationPlanTwoProviders specsPlan =  Provisioner.getImplementationPlanFromDatabagItem(impPlan.getSlaId());
			Plan plan =  Plan.getInstance(id, specsPlan);
			assertNotNull(plan);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
