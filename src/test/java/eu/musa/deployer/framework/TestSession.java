package eu.musa.deployer.framework;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import eu.musa.deployer.framework.ClientSession;

public class TestSession {
	
	public String serviceURL;
	
	public TestSession() {
		this.serviceURL = ClientSession.URL_SERVICE_SESSION;
		if (serviceURL == null) serviceURL = "http://localhost:8080/session/"; 
	}
	
	@Test
	public void getSession() throws IOException {
		String session = ClientSession.getSessionRAW("200", "ADD VALID TOKEN HERE");
		assertNotNull(session);
	}
}
