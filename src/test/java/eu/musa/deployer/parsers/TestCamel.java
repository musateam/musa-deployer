package eu.musa.deployer.parsers;

import org.junit.Test;

import eu.musa.deployer.framework.ClientSession;

import static org.junit.Assert.assertTrue;

public class TestCamel {
	
	public static String URL_SERVICE_MODELLER = "http://modeller.musa-project.eu/eu.musa.modeller.ws/webresources/camelfilews/getCamel/";
	
	@Test
	public void testLocal() throws Exception{
		Camel camel= Camel.newInstance("15",data);
		testGeneric("15", "LOCAL_CAMEL_SAMPLE",camel);
	}
	
	@Test
	public void testRemote() throws Exception{
		String id = "120";
		Camel camel=Camel.getInstance(id,URL_SERVICE_MODELLER + id+ "/CAMEL");
		testGeneric(id,URL_SERVICE_MODELLER, camel);
	}
	
	private static void testGeneric(String id, String url, Camel camel){
		String message="Camel "+id+" from URL: "+ url+"should be parseable";
		try{
			checkvalidCamel(camel);
			assertTrue(message,true);
		}catch(Exception e){
			assertTrue(message+"Error: "+e.getMessage(),false);
		}
	}
	
	private static void checkvalidCamel(Camel camel) throws Exception{
		if (camel.getModel().getDeploymentModels()==null || camel.getModel().getDeploymentModels().size()<1) throw new Exception("Not parsed or no content. Check if camel id="+camel.getId()+" is correct");
	}
	
	public static String data = "camel model TSMModel {\n" + 
			"	 security model SEC {\n" + 
			"	 	security capability CAP1 {\n" + 
			"\n" + 
			"	 	}\n" + 
			"	 	\n" + 
			"	 	security capability CAP2 {\n" + 
			"	 		controls [MUSASEC.AC-11(1), MUSASEC.AU-13(2), MUSASEC.AC-17(6)]\n" + 
			"	 	}\n" + 
			"	 	\n" + 
			"	 	security capability CAP_ENF_AC {\n" + 
			"	 		controls [MUSASEC.AC-11(1), MUSASEC.AU-13(2), MUSASEC.AC-17(6)]\n" + 
			"	 	}\n" + 
			"	 	\n" + 
			"	 }\n" + 
			"\n" + 
			"\n" + 
			"location model TSMLocation {\n" + 
			"	region EU {\n" + 
			"		name: Europe \n" + 
			"	}\n" + 
			"\n" + 
			"	country DE {\n" + 
			"		name: Germany\n" + 
			"		// parentRegions is not a compulsory property \n" + 
			"		// parent regions [TSMLocation.EU]\n" + 
			"	}\n" + 
			"\n" + 
			"	country UK {\n" + 
			"		name: UnitedKingdom\n" + 
			"		parent regions [TSMLocation.EU]\n" + 
			"	}\n" + 
			"\n" + 
			"	country FI {\n" + 
			"		name: Finland\n" + 
			"		parent regions [TSMLocation.EU]\n" + 
			"	}\n" + 
			"\n" + 
			"} \n" + 
			"\n" + 
			"requirement model TSMRequirement {\n" + 
			"	// example of QuantitativeHardwareRequirement only applicable to a specific Component \n" + 
			"	quantitative hardware JourneyPlanner {\n" + 
			"		ram: 1024..   // always in MEGABYTES\n" + 
			"		storage: 10.. // always in GIGABYTES // TODO: Is it required to link with range values and units???\n" + 
			"	}\n" + 
			"\n" + 
			"	/**TUT: maybe 32 cores is a little bit too much if we are talking about microservices **/\n" + 
			"	quantitative hardware CoreIntensive {\n" + 
			"		core: 4..16\n" + 
			"		ram: 1024..8192\n" + 
			"	}\n" + 
			"\n" + 
			"	quantitative hardware CPUIntensive {\n" + 
			"		core: 1..       // min and max number of CPU cores\n" + 
			"		ram: 1024..8192 // size of RAM \n" + 
			"		cpu: 1.0..      // min and max CPU frequency\n" + 
			"	}\n" + 
			"\n" + 
			"	quantitative hardware StorageIntensive {\n" + 
			"		storage: 1000..\n" + 
			"	}\n" + 
			"\n" + 
			"	os Ubuntu {os: Ubuntu 64os}\n" + 
			"\n" + 
			"	location requirement GermanyReq {\n" + 
			"		locations [TSMLocation.DE]\n" + 
			"	}\n" + 
			"\n" + 
			"	location requirement UKReq {\n" + 
			"		locations [TSMLocation.UK]\n" + 
			"	}\n" + 
			"	\n" + 
			"	location requirement FinlandReq {\n" + 
			"		locations [TSMLocation.FI]\n" + 
			"	}\n" + 
			"	\n" + 
			"} // requirement model TSMRequirement\n" + 
			"\n" + 
			"type model TUTType {\n" + 
			"	enumeration VMTypeEnum {\n" + 
			"		values [ 'M1.MICRO' : 0,\n" + 
			"		'M1.TINY' : 1,\n" + 
			"		'M1.SMALL' : 2,\n" + 
			"		'M1.MEDIUM' : 3,\n" + 
			"		'M1.LARGE' : 4,\n" + 
			"		'M1.XLARGE' : 5,\n" + 
			"		'M1.XXLARGE' : 6,\n" + 
			"		'M2.SMALL' : 7,\n" + 
			"		'M2.MEDIUM' : 8,\n" + 
			"		'M2.LARGE' : 9,\n" + 
			"		'M2.XLARGE' : 10,\n" + 
			"		'C1.SMALL' : 11,\n" + 
			"		'C1.MEDIUM' : 12,\n" + 
			"		'C1.LARGE' : 13,\n" + 
			"		'C1.XLARGE' : 14,\n" + 
			"		'C1.XXLARGE' : 15 ]\n" + 
			"	}\n" + 
			"	range MemoryRange {\n" + 
			"		primitive type: IntType\n" + 
			"		lower limit {\n" + 
			"			int value 256 included\n" + 
			"		}\n" + 
			"		upper limit {\n" + 
			"			int value 32768 included\n" + 
			"		}\n" + 
			"	}\n" + 
			"	range StorageRange {\n" + 
			"		primitive type: IntType\n" + 
			"		lower limit {\n" + 
			"			int value 0 included\n" + 
			"		}\n" + 
			"		upper limit {\n" + 
			"			int value 160 included\n" + 
			"		}\n" + 
			"	}\n" + 
			"	range CoresRange {\n" + 
			"		primitive type: IntType\n" + 
			"		lower limit {\n" + 
			"			int value 1 included\n" + 
			"		}\n" + 
			"		upper limit {\n" + 
			"			int value 16 included\n" + 
			"		}\n" + 
			"	}\n" + 
			"	string value type StringValueType {\n" + 
			"		primitive type: StringType\n" + 
			"	}\n" + 
			"	\n" + 
			"	list StorageList {\n" + 
			"		values [ int value 0,\n" + 
			"		int value 20,\n" + 
			"		int value 40,\n" + 
			"		int value 80,\n" + 
			"		int value 160 ]\n" + 
			"	}\n" + 
			"	\n" + 
			"	list MemoryList {\n" + 
			"		values [ int value 256,\n" + 
			"		int value 512,\n" + 
			"		int value 2048,\n" + 
			"		int value 4096,\n" + 
			"		int value 8192,\n" + 
			"		int value 16384,\n" + 
			"		int value 32768 ]\n" + 
			"	}\n" + 
			"	\n" + 
			"	list CoresList {\n" + 
			"		values [ int value 1,\n" + 
			"		int value 2,\n" + 
			"		int value 4,\n" + 
			"		int value 8,\n" + 
			"		int value 16 ]\n" + 
			"	}\n" + 
			"	\n" + 
			"	range Range_0_100 {\n" + 
			"		primitive type: IntType\n" + 
			"		lower limit {int value 0 included}\n" + 
			"		upper limit {int value 100}\n" + 
			"	}\n" + 
			"	\n" + 
			"	range Range_0_10000 {\n" + 
			"		primitive type: IntType\n" + 
			"		lower limit {int value 0}\n" + 
			"		upper limit {int value 10000 included}\n" + 
			"	}\n" + 
			"	\n" + 
			"	range DoubleRange_0_100 {\n" + 
			"		primitive type: DoubleType\n" + 
			"		lower limit {double value 0.0 included}\n" + 
			"		upper limit {double value 100.0 included}\n" + 
			"	}	\n" + 
			"} // type model TUTType\n" + 
			"\n" + 
			"unit model TSMUnit {\n" + 
			"	storage unit { StorageUnit: GIGABYTES }\n" + 
			"	\n" + 
			"	time interval unit {minutes: MINUTES}\n" + 
			"	\n" + 
			"	time interval unit {seconds: SECONDS}\n" + 
			"	\n" + 
			"	/* \n" + 
			"	 * TODO: Is it required in MUSA??? \n" + 
			"	memory unit { MemoryUnit: MEGABYTES }\n" + 
			"	*/\n" + 
			"	\n" + 
			"	/* some examples... */\n" + 
			"    monetary unit {Euro: EUROS}\n" + 
			"	throughput unit {SimulationsPerSecondUnit: TRANSACTIONS_PER_SECOND}\n" + 
			"	time interval unit {ResponseTimeUnit: MILLISECONDS}\n" + 
			"	time interval unit {ExperimentMakespanInSecondsUnit: SECONDS}\n" + 
			"	transaction unit {NumberOfSimulationsLeftInExperimentUnit: TRANSACTIONS}\n" + 
			"	dimensionless {AvailabilityUnit: PERCENTAGE}\n" + 
			"	dimensionless {CPUUnit: PERCENTAGE}\n" + 
			"} // unit model TUTUnit\n" + 
			"\n" + 
			"\n" + 
			"application TSMApplication {\n" + 
			"	version: 'v1.0'\n" + 
			"	owner: TUTOrganisation.test_user1\n" + 
			"	deployment models [TSMModel.TSMDeployment]\n" + 
			"} // application TSMApplication\n" + 
			"\n" + 
			"\n" + 
			"\n" + 
			"organisation model TUTOrganisation {\n" + 
			"	organisation TUT {\n" + 
			"		www: 'http://www.tut.fi/en/home'\n" + 
			"		postal address: 'Korkeakoulunkatu 10, 33720 Tampere, Finland'\n" + 
			"		email: 'test.test@tut.fi'\n" + 
			"	}\n" + 
			"\n" + 
			"	user test_user1 {\n" + 
			"		first name: test_name\n" + 
			"		last name: test_surname\n" + 
			"		email: 'test_name.test_surname@tut.fi'\n" + 
			"		musa credentials {\n" + 
			"		    end time: 2017-11-01\n" + 
			"			username: 'mcp'\n" + 
			"			password: 'test_name_surname'\n" + 
			"			}\n" + 
			"	}\n" + 
			"	\n" + 
			"	user test_user2 {\n" + 
			"	first name: user2_name\n" + 
			"	last name: user2_surname\n" + 
			"	email: 'its email'\n" + 
			"	musa credentials {\n" + 
			"	    username: 'user2'\n" + 
			"	    password: 'user2_passw'\n" + 
			"	    }\n" + 
			"	}\n" + 
			"	\n" + 
			"	user group test_group {\n" + 
			"	users [TUTOrganisation.test_user1, TUTOrganisation.test_user2]\n" + 
			"	}\n" + 
			"	\n" + 
			"	\n" + 
			"	role devop\n" + 
			"	\n" + 
			"    role assignment test_nameDevop {\n" + 
			"        start: 2016-02-26\n" + 
			"        end: 2017-02-26\n" + 
			"        assigned on: 2016-02-25\n" + 
			"        users: [TUTOrganisation.test_user1, TUTOrganisation.test_user2]\n" + 
			"        role: devop\n" + 
			"    }\n" + 
			"    \n" + 
			"    role assignment test_groupDevop {\n" + 
			"        start: 2016-02-01\n" + 
			"        end: 2017-02-26\n" + 
			"        assigned on:  2016-02-25\n" + 
			"        role: devop\n" + 
			"        user groups: [TUTOrganisation.test_group]\n" + 
			"    }\n" + 
			"\n" + 
			"	security level: HIGH\n" + 
			"} // organisation model TUTOrganisation\n" + 
			"\n" + 
			"\n" + 
			"deployment model TSMDeployment {\n" + 
			"// example of VMRequirementSet only applicable to a specific Component \n" + 
			"	requirement set JourneyPlannerHostRS {\n" + 
			"		os: TSMRequirement.Ubuntu\n" + 
			"		quantitative hardware: TSMRequirement.JourneyPlanner\n" + 
			"		location: TSMRequirement.UKReq\n" + 
			"	}\n" + 
			"\n" + 
			"	requirement set CoreIntensiveUbuntuFinlandRS {\n" + 
			"		os: TSMRequirement.Ubuntu\n" + 
			"		quantitative hardware: TSMRequirement.CoreIntensive\n" + 
			"		location: TSMRequirement.FinlandReq\n" + 
			"	}\n" + 
			"\n" + 
			"	requirement set CPUIntensiveUbuntuFinlandRS {\n" + 
			"		os: TSMRequirement.Ubuntu\n" + 
			"		quantitative hardware: TSMRequirement.CPUIntensive\n" + 
			"		location: TSMRequirement.FinlandReq\n" + 
			"	}\n" + 
			"\n" + 
			"	requirement set CPUIntensiveUbuntuUKRS {\n" + 
			"		os: TSMRequirement.Ubuntu\n" + 
			"		quantitative hardware: TSMRequirement.CPUIntensive\n" + 
			"		location: TSMRequirement.UKReq\n" + 
			"	}\n" + 
			"	\n" + 
			"	requirement set StorageIntensiveUbuntuFinlandRS {\n" + 
			"		os: TSMRequirement.Ubuntu\n" + 
			"		quantitative hardware: TSMRequirement.StorageIntensive\n" + 
			"		location: TSMRequirement.FinlandReq\n" + 
			"	}\n" + 
			"\n" + 
			"    /*\n" + 
			"	vm JourneyPlannerVM {\n" + 
			"		requirement set JourneyPlannerHostRS\n" + 
			"		provided host JourneyPlannerHost\n" + 
			"	}*/\n" + 
			"\n" + 
			"\n" + 
			"	vm CoreIntensiveUbuntuFinlandVM {\n" + 
			"		requirement set CoreIntensiveUbuntuFinlandRS\n" + 
			"		provided host CoreIntensiveUbuntuFinlandHost\n" + 
			"	}\n" + 
			"\n" + 
			"	vm CPUIntensiveUbuntuFinlandVM {\n" + 
			"		requirement set CPUIntensiveUbuntuFinlandRS\n" + 
			"		provided host CPUIntensiveUbuntuFinlandHost\n" + 
			"	}\n" + 
			"\n" + 
			"	vm StorageIntensiveUbuntuFinlandVM {\n" + 
			"		requirement set StorageIntensiveUbuntuFinlandRS\n" + 
			"		provided host StorageIntensiveUbuntuFinlandHost\n" + 
			"	}\n" + 
			"\n" + 
			"	vm CPUIntensiveUbuntuUKVM {\n" + 
			"		requirement set CPUIntensiveUbuntuUKRS\n" + 
			"		provided host CPUIntensiveUbuntuUKHost\n" + 
			"	}\n" + 
			"\n" + 
			"    pool poolTSM{\n" + 
			"        type: VM\n" + 
			"        policy: round-robin\n" + 
			"        required host CPUIntensiveUbuntuUKVM.CPUIntensiveUbuntuUKHost\n" + 
			"        required host CPUIntensiveUbuntuFinlandVM.CPUIntensiveUbuntuFinlandHost\n" + 
			"        \n" + 
			"        provided host hostPoolTSM\n" + 
			"    }\n" + 
			"    \n" + 
			"    container containerTSM{\n" + 
			"        type: docker_swarm\n" + 
			"        allocationStrategy: custom\n" + 
			"        required host poolTSM {\n" + 
			"            manager: CPUIntensiveUbuntuUKVM.CPUIntensiveUbuntuUKHost\n" + 
			"        }\n" + 
			"    }\n" + 
			"\n" + 
			"    internal component MUSAAgentAC1 {\n" + 
			"        type: AGENT.IDC_AC\n" + 
			"                        \n" + 
			"        IP public: true\n" + 
			"                        \n" + 
			"        provided security capability CAP_AC{\n" + 
			"            security capability SEC.CAP_ENF_AC\n" + 
			"        }\n" + 
			"            \n" + 
			"        required communication MessageBrokerPortReq {port: 3000 mandatory}\n" + 
			"                        \n" + 
			"        required host CPUIntensiveUbuntuFinlandHostReq\n" + 
			"                        \n" + 
			"        configuration MUSAAgentACConfigurationCHEF {\n" + 
			"            CHEF configuration manager C1 { //Configuration Management tool\n" + 
			"                cookbook: 'musa'\n" + 
			"                recipe: 'musa_enforcement_agent_ac'\n" + 
			"            }\n" + 
			"        }                                   \n" + 
			"    }\n" + 
			"    \n" + 
			"    internal component MUSAAgentAC2 {\n" + 
			"        type: AGENT.IDC_AC\n" + 
			"                        \n" + 
			"        IP public: true\n" + 
			"                        \n" + 
			"        provided security capability CAP_AC{\n" + 
			"            security capability SEC.CAP_ENF_AC\n" + 
			"        }\n" + 
			"            \n" + 
			"        required communication MessageBrokerPortReq {port: 3000 mandatory}\n" + 
			"                        \n" + 
			"        required host CPUIntensiveUbuntuUKHostReq\n" + 
			"                        \n" + 
			"        configuration MUSAAgentACConfigurationCHEF {\n" + 
			"            CHEF configuration manager C1 { //Configuration Management tool\n" + 
			"                cookbook: 'musa'\n" + 
			"                recipe: 'musa_enforcement_agent_ac'\n" + 
			"            }\n" + 
			"        }                                   \n" + 
			"    }\n" + 
			"\n" + 
			"\n" + 
			"	internal component TSMEngine {\n" + 
			"		type: COTS.IDM\n" + 
			"		order:  3\n" + 
			"		IP public:  false\n" + 
			"		\n" + 
			"		required security capability TSMEngineCapReq\n" + 
			"\n" + 
			"		provided communication TSMEnginePort { port: 8185 }\n" + 
			"		provided communication TSMEngineRESTPort { port: 443 }	\n" + 
			"		required communication IDManagerPortReq {port: 3000 mandatory}\n" + 
			"		required communication ConsumptionEstimatorPortReq {port: 9090 mandatory}\n" + 
			"		required communication JourneyPlannerPortReq {port: 8085  mandatory}\n" + 
			"		required communication DatabasePortReq {port: 3306 mandatory}\n" + 
			"		\n" + 
			"		required container containerTSM {host: CPUIntensiveUbuntuFinlandVM.CPUIntensiveUbuntuFinlandHost}\n" + 
			"		\n" + 
			"		configuration TSMEngineConfigurationCHEF {\n" + 
			"			CHEF configuration manager C1 { //Configuration Management tool\n" + 
			"				cookbook: 'tut'\n" + 
			"				recipe: 'musa_tsme'\n" + 
			"			}\n" + 
			"		}			\n" + 
			"	}\n" + 
			"\n" + 
			"	internal component JourneyPlanner {\n" + 
			"	    type: SERVICE.Firewall\n" + 
			"			order: 4\n" + 
			"		provided security capability JourneyPlannerCap {\n" + 
			"            security capability SEC.CAP1\n" + 
			"		}\n" + 
			"		required security capability JourneyPlannerCapReq\n" + 
			"\n" + 
			"		provided communication JourneyPlannerPort {port: 8085}\n" + 
			"		required communication ConsumptionEstimatorPortReq {port: 9090 mandatory}\n" + 
			"		required host JourneyPlannerHostReq\n" + 
			"		configuration JourneyPlannerManualConfiguration{\n" + 
			"			CHEF configuration manager C1 {\n" + 
			"				cookbook: 'tut'\n" + 
			"				recipe: 'musa_mjp'\n" + 
			"			}\n" + 
			"		}\n" + 
			"	}\n" + 
			"\n" + 
			"	internal component ConsumptionEstimator {\n" + 
			"	    IP public: false\n" + 
			"	    provided security capability ccc {\n" + 
			"	    security capability SEC.CAP2 }\n" + 
			"	\n" + 
			"		provided communication ConsumptionEstimatorPort {port: 9090}\n" + 
			"		required host CPUIntensiveUbuntuUKHostReq\n" + 
			"		configuration ConsumptionManualEstimatorConfiguration{\n" + 
			"			CHEF configuration manager C1 {\n" + 
			"				cookbook: 'tut'\n" + 
			"				recipe: 'musa_cec'\n" + 
			"			}\n" + 
			"		}\n" + 
			"	}\n" + 
			"\n" + 
			"	internal component IDMAM {\n" + 
			"		provided communication IDManagerPort {port: 3000}\n" + 
			"		provided communication MongoDBPort {port: 27017}\n" + 
			"		required host CoreIntensiveUbuntuFinlandHostReq\n" + 
			"		configuration IDManagerManualConfiguration{\n" + 
			"			CHEF configuration manager C1 { //Configuration Management tool\n" + 
			"				cookbook: 'tut'\n" + 
			"				recipe: 'musa_idm'  // IDM database installed within the same recipe\n" + 
			"			}\n" + 
			"		}\n" + 
			"	}\n" + 
			"	\n" + 
			"	internal component JourneyDatabase {\n" + 
			"		provided communication DatabasePort {port: 3306}\n" + 
			"		required host StorageIntensiveUbuntuFinlandHostReq\n" + 
			"		configuration DatabaseManualConfiguration{\n" + 
			"			CHEF configuration manager C1 { //Configuration Management tool\n" + 
			"				cookbook: 'tut'\n" + 
			"				recipe: 'musa_db'\n" + 
			"			}\n" + 
			"		}\n" + 
			"	}\n" + 
			"\n" + 
			"\n" + 
			"	hosting JourneyPlannerToPool {\n" + 
			"		from JourneyPlanner.JourneyPlannerHostReq to pool poolTSM.hostPoolTSM\n" + 
			"	}\n" + 
			"\n" + 
			"	hosting ConsumptionEstimatorToCPUIntensiveUbuntuUK {\n" + 
			"		from ConsumptionEstimator.CPUIntensiveUbuntuUKHostReq to CPUIntensiveUbuntuUKVM.CPUIntensiveUbuntuUKHost\n" + 
			"	}\n" + 
			"\n" + 
			"	hosting DatabaseToStorageIntensiveUbuntuFinland {\n" + 
			"		from JourneyDatabase.StorageIntensiveUbuntuFinlandHostReq to StorageIntensiveUbuntuFinlandVM.StorageIntensiveUbuntuFinlandHost\n" + 
			"	}\n" + 
			"	\n" + 
			"	hosting IDMAMToCoreIntensiveUbuntuFinland {\n" + 
			"	    from IDMAM.CoreIntensiveUbuntuFinlandHostReq to CoreIntensiveUbuntuFinlandVM.CoreIntensiveUbuntuFinlandHost\n" + 
			"	}\n" + 
			"	\n" + 
			"	hosting AC1toCPUIntensiveUbuntuFinland{\n" + 
			"	    from MUSAAgentAC1.CPUIntensiveUbuntuFinlandHostReq to CPUIntensiveUbuntuFinlandVM.CPUIntensiveUbuntuFinlandHost\n" + 
			"	}\n" + 
			"	\n" + 
			"	hosting AC2toCPUIntensiveUbuntuUK {\n" + 
			"	    from MUSAAgentAC2.CPUIntensiveUbuntuUKHostReq to CPUIntensiveUbuntuUKVM.CPUIntensiveUbuntuUKHost\n" + 
			"	}\n" + 
			"\n" + 
			"	communication TSMEngineToDatabase {\n" + 
			"		type: REMOTE\n" + 
			"		from TSMEngine.DatabasePortReq to IDMAM.MongoDBPort\n" + 
			"		protocol MYSQL\n" + 
			"	}\n" + 
			"\n" + 
			"	communication TSMEngineToIDManager {\n" + 
			"		type: REMOTE		\n" + 
			"		from TSMEngine.IDManagerPortReq to IDMAM.IDManagerPort\n" + 
			"	}\n" + 
			"\n" + 
			"	communication TSMEngineToJourneyPlanner {\n" + 
			"		type: REMOTE\n" + 
			"		from TSMEngine.JourneyPlannerPortReq to JourneyPlanner.JourneyPlannerPort\n" + 
			"	}\n" + 
			"\n" + 
			"	communication JourneyPlannerToConsumptionEstimator {\n" + 
			"		type: REMOTE		\n" + 
			"		from JourneyPlanner.ConsumptionEstimatorPortReq to ConsumptionEstimator.ConsumptionEstimatorPort\n" + 
			"	}\n" + 
			"	\n" + 
			"	capability match TSMEngineToMUSAAgentAC1 {\n" + 
			"        from TSMEngine.TSMEngineCapReq to  MUSAAgentAC1.CAP_AC\n" + 
			"	}\n" + 
			"	\n" + 
			"	capability match JourneyPlannerToMUSAAgentAC2 {\n" + 
			"        from JourneyPlanner.JourneyPlannerCapReq to MUSAAgentAC2.CAP_AC\n" + 
			"	}\n" + 
			"\n" + 
			"	\n" + 
			"} // deployment model TSMDeployment\n" + 
			"\n" + 
			"\n" + 
			"\n" + 
			"\n" + 
			"\n" + 
			"//Metric model for TSM App\n" + 
			"metric model TSMMetric {\n" + 
			"	window Win5Min {\n" + 
			"		window type: SLIDING\n" + 
			"		size type: TIME_ONLY\n" + 
			"		time size: 5\n" + 
			"		unit: TSMModel.TSMUnit.minutes\n" + 
			"	}\n" + 
			"\n" + 
			"	window Win1Min {\n" + 
			"		window type: SLIDING\n" + 
			"		size type: TIME_ONLY\n" + 
			"		time size: 1\n" + 
			"		unit: TSMModel.TSMUnit.minutes\n" + 
			"	}\n" + 
			"\n" + 
			"	schedule Schedule1Min {\n" + 
			"		type: FIXED_RATE\n" + 
			"		interval: 1\n" + 
			"		unit: TSMModel.TSMUnit.minutes\n" + 
			"	}\n" + 
			"\n" + 
			"	schedule Schedule1Sec {\n" + 
			"		type: FIXED_RATE\n" + 
			"		interval: 1\n" + 
			"		unit: TSMModel.TSMUnit.seconds\n" + 
			"	}\n" + 
			"\n" + 
			"	property AvailabilityProperty {\n" + 
			"		type: MEASURABLE\n" + 
			"		sensors [TSMMetric.AvailabilitySensor]\n" + 
			"	}\n" + 
			"\n" + 
			"	property CPUProperty {\n" + 
			"		type: MEASURABLE\n" + 
			"		sensors [TSMMetric.CPUSensor]\n" + 
			"	}\n" + 
			"\n" + 
			"	property ResponseTimeProperty {\n" + 
			"		type: MEASURABLE\n" + 
			"		sensors [TSMMetric.ResponseTimeSensor]\n" + 
			"	}\n" + 
			"\n" + 
			"	property FrequencyOfVulnerabilityScanningProperty {\n" + 
			"		type: MEASURABLE\n" + 
			"		sensors [TSMMetric.FreqOfVulnScanSensor]\n" + 
			"	}\n" + 
			"\n" + 
			"	sensor AvailabilitySensor {\n" + 
			"		configuration: 'MMTAgent.Availability'\n" + 
			"		push\n" + 
			"	}\n" + 
			"\n" + 
			"	sensor CPUSensor {\n" + 
			"		configuration: 'MMTAgent.CPU'\n" + 
			"		push\n" + 
			"	}\n" + 
			"\n" + 
			"	sensor ResponseTimeSensor {\n" + 
			"		push\n" + 
			"	}\n" + 
			"\n" + 
			"	sensor FreqOfVulnScanSensor {\n" + 
			"		configuration: 'MMTAgent.FreqOfVulnScan'\n" + 
			"		push\n" + 
			"	}\n" + 
			"	raw metric AvailabilityMetric {\n" + 
			"		value direction: 1\n" + 
			"		layer: SaaS\n" + 
			"		property: TSMModel.TSMMetric.AvailabilityProperty\n" + 
			"		unit: TSMModel.TSMUnit.AvailabilityUnit\n" + 
			"		value type: TSMModel.TUTType.DoubleRange_0_100\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric CPUMetric {\n" + 
			"		value direction: 0\n" + 
			"		layer: IaaS\n" + 
			"		property: TSMModel.TSMMetric.CPUProperty\n" + 
			"		unit: TSMModel.TSMUnit.CPUUnit\n" + 
			"		value type: TSMModel.TUTType.Range_0_100\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric ResponseTimeMetric {\n" + 
			"		value direction: 0\n" + 
			"		layer: SaaS\n" + 
			"		property: TSMModel.TSMMetric.ResponseTimeProperty\n" + 
			"		unit: TSMModel.TSMUnit.ResponseTimeUnit\n" + 
			"		value type: TSMModel.TUTType.Range_0_10000\n" + 
			"	}\n" + 
			"\n" + 
			"	composite metric MeanValueOfResponseTimeOfAllTSMEngineMetric {\n" + 
			"		value direction: 0\n" + 
			"		layer: SaaS\n" + 
			"		property: TSMModel.TSMMetric.ResponseTimeProperty\n" + 
			"		unit: TSMModel.TSMUnit.ResponseTimeUnit\n" + 
			"\n" + 
			"		metric formula MeanValueOfResponseTimeOfAllTSMEngineFormula {\n" + 
			"			function arity: UNARY\n" + 
			"			function pattern: MAP\n" + 
			"			MEAN(TSMModel.TSMMetric.ResponseTimeMetric)\n" + 
			"		}\n" + 
			"	}\n" + 
			"\n" + 
			"	composite metric CPUAverage {\n" + 
			"		description: \"Average usage of the CPU\"\n" + 
			"		value direction: 1\n" + 
			"		layer: PaaS\n" + 
			"		property: TSMModel.TSMMetric.CPUProperty\n" + 
			"		unit: TSMModel.TSMUnit.CPUUnit\n" + 
			"\n" + 
			"		metric formula Formula_Average {\n" + 
			"			function arity: UNARY\n" + 
			"			function pattern: REDUCE\n" + 
			"			MEAN( TSMModel.TSMMetric.CPUMetric )\n" + 
			"		}\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric context TSMEngineAvailabilityContext {\n" + 
			"		metric: TSMModel.TSMMetric.AvailabilityMetric\n" + 
			"		sensor: TSMMetric.AvailabilitySensor\n" + 
			"		component: TSMModel.TSMDeployment.TSMEngine\n" + 
			"		quantifier: ANY\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric context CPUMetricConditionContext {\n" + 
			"		metric: TSMModel.TSMMetric.CPUMetric\n" + 
			"		sensor: TSMMetric.CPUSensor\n" + 
			"		component: TSMModel.TSMDeployment.TSMEngine\n" + 
			"		quantifier: ANY\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric context TSMEngineResponseTimeContext {\n" + 
			"		metric: TSMModel.TSMMetric.ResponseTimeMetric\n" + 
			"		sensor: TSMMetric.ResponseTimeSensor\n" + 
			"		component: TSMModel.TSMDeployment.TSMEngine\n" + 
			"		quantifier: ANY\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric context JourneyPlannerResponseTimeContext {\n" + 
			"		metric: TSMModel.TSMMetric.ResponseTimeMetric\n" + 
			"		sensor: TSMMetric.ResponseTimeSensor\n" + 
			"		component: TSMModel.TSMDeployment.JourneyPlanner\n" + 
			"		quantifier: ANY\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric context CPURawMetricContext {\n" + 
			"		metric: TSMModel.TSMMetric.CPUMetric\n" + 
			"		sensor: TSMMetric.CPUSensor\n" + 
			"		component: TSMModel.TSMDeployment.TSMEngine\n" + 
			"		schedule: TSMModel.TSMMetric.Schedule1Sec\n" + 
			"		quantifier: ALL\n" + 
			"	}\n" + 
			"\n" + 
			"	composite metric context CPUAvgMetricContextAll {\n" + 
			"		metric: TSMModel.TSMMetric.CPUAverage\n" + 
			"		component: TSMModel.TSMDeployment.TSMEngine\n" + 
			"		window: TSMModel.TSMMetric.Win5Min\n" + 
			"		schedule: TSMModel.TSMMetric.Schedule1Min\n" + 
			"		composing metric contexts [TSMModel.TSMMetric.CPURawMetricContext]\n" + 
			"		quantifier: ALL\n" + 
			"	}\n" + 
			"\n" + 
			"	composite metric context CPUAvgMetricContextAny {\n" + 
			"		metric: TSMModel.TSMMetric.CPUAverage\n" + 
			"		component: TSMModel.TSMDeployment.TSMEngine\n" + 
			"		window: TSMModel.TSMMetric.Win1Min\n" + 
			"		schedule: TSMModel.TSMMetric.Schedule1Min\n" + 
			"		composing metric contexts [TSMModel.TSMMetric.CPURawMetricContext]\n" + 
			"		quantifier: ANY\n" + 
			"	}\n" + 
			"\n" + 
			"	metric condition TSMEngineAvailabilityCondition {\n" + 
			"		context: TSMModel.TSMMetric.TSMEngineAvailabilityContext\n" + 
			"		threshold: 99.0\n" + 
			"		comparison operator: >\n" + 
			"	}\n" + 
			"\n" + 
			"	metric condition CPUMetricCondition {\n" + 
			"		context: TSMModel.TSMMetric.CPUMetricConditionContext\n" + 
			"		threshold: 80.0\n" + 
			"		comparison operator: >\n" + 
			"	}\n" + 
			"\n" + 
			"	metric condition TSMEngineResponseTimeCondition {\n" + 
			"		context: TSMModel.TSMMetric.TSMEngineResponseTimeContext\n" + 
			"		threshold: 0.3\n" + 
			"		comparison operator: <\n" + 
			"	}\n" + 
			"\n" + 
			"	metric condition JourneyPlannerResponseTimeCondition {\n" + 
			"		context: TSMModel.TSMMetric.JourneyPlannerResponseTimeContext\n" + 
			"		threshold: 700.0\n" + 
			"		comparison operator: >\n" + 
			"	}\n" + 
			"\n" + 
			"	metric condition CPUAvgMetricConditionAll {\n" + 
			"		context: TSMModel.TSMMetric.CPUAvgMetricContextAll\n" + 
			"		threshold: 50.0\n" + 
			"		comparison operator: >\n" + 
			"	}\n" + 
			"\n" + 
			"	metric condition CPUAvgMetricConditionAny {\n" + 
			"		context: TSMModel.TSMMetric.CPUAvgMetricContextAny\n" + 
			"		threshold: 80.0\n" + 
			"		comparison operator: >\n" + 
			"	}\n" + 
			"} // metric model TSMMetric {\n" + 
			"\n" + 
			"\n" + 
			"\n" + 
			"\n" + 
			"	internal component type COTS.Web\n" + 
			"	internal component type COTS.Storage\n" + 
			"	internal component type COTS.IDM \n" + 
			"	internal component type COTS.Firewall \n" + 
			"\n" + 
			"\n" + 
			"	internal component type SERVICE.Web\n" + 
			"	internal component type SERVICE.Storage\n" + 
			"	internal component type SERVICE.IDM \n" + 
			"	internal component type SERVICE.Firewall \n" + 
			"\n" + 
			"\n" + 
			"	internal component type AGENT.IDM_SPECS\n" + 
			"	internal component type AGENT.IDC_AC\n" + 
			"	internal component type AGENT.IDS_MONTIMAGE\n" + 
			"	internal component type AGENT.Test1\n" + 
			"	internal component type AGENT.Test2\n" + 
			"	\n" + 
			"		security model MUSASEC {\n" + 
			"\n" + 
			"  domain AC { name: \"ACCESS CONTROL\" } \n" + 
			"  domain AT { name: \"AWARENESS AND TRAINING\" } \n" + 
			"  domain AU { name: \"AUDIT AND ACCOUNTABILITY\" } \n" + 
			"  domain CA { name: \"SECURITY ASSESSMENT AND AUTHORIZATION\" } \n" + 
			"  domain CM { name: \"CONFIGURATION MANAGEMENT\" } \n" + 
			"  domain CP { name: \"CONTIGENCY PLANNING\" } \n" + 
			"  domain IA { name: \"IDENTIFICATION AND AUTHENTICATION\" } \n" + 
			"  domain IR { name: \"INCIDENT RESPONSE\" } \n" + 
			"  domain MA { name: \"MAINTENANCE\" } \n" + 
			"  domain MP { name: \"MEDIA PROTECTION\" } \n" + 
			"  domain PE { name: \"PHYSICAL AND ENVIRONMENTAL PROTECTION\" } \n" + 
			"  domain PL { name: \"PLANNING\" } \n" + 
			"  domain PS { name: \"PERSONNEL SECURITY\" } \n" + 
			"  domain RA { name: \"RISK ASSESSMENT\" } \n" + 
			"  domain SA { name: \"SYSTEM AND SERVICES ACQUISITION\" } \n" + 
			"  domain SC { name: \"SYSTEM AND COMMUNICATIONS PROTECTION\" } \n" + 
			"  domain SI { name: \"SYSTEM AND INFORMATION INTEGRITY\" } \n" + 
			"  security control AC-1 { \n" + 
			"  name: \"ACCESS CONTROL POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. An access control policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the access control policy and associated access controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Access control policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Access control procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2 { \n" + 
			"  name: \"ACCOUNT MANAGEMENT \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Identifies and selects the following types of information system accounts to support organizational missions/business functions: [Assignment: organization-defined information system account types];\n" + 
			"			b. Assigns account managers for information system accounts;\n" + 
			"			c. Establishes conditions for group and role membership;\n" + 
			"			d. Specifies authorized users of the information system, group and role membership, and access authorizations (i.e., privileges) and other attributes (as required) for each account;\n" + 
			"			e. Requires approvals by [Assignment: organization-defined personnel or roles] for requests to create information system accounts;\n" + 
			"			f. Creates, enables, modifies, disables, and removes information system accounts in accordance with [Assignment: organization-defined procedures or conditions];\n" + 
			"			g. Monitors the use of information system accounts;\n" + 
			"			h. Notifies account managers:\n" + 
			"				1. When accounts are no longer required;\n" + 
			"				2. When users are terminated or transferred; and\n" + 
			"				3. When individual information system usage or need-to-know changes;\n" + 
			"			i. Authorizes access to the information system based on:\n" + 
			"				1. A valid access authorization;\n" + 
			"				2. Intended system usage; and\n" + 
			"				3. Other attributes as required by the organization or associated missions/business functions;\n" + 
			"			j. Reviews accounts for compliance with account management requirements [Assignment: organization-defined frequency]; and\n" + 
			"			k. Establishes a process for reissuing shared/group account credentials (if deployed) when individuals are removed from the group.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(1) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | AUTOMATED SYSTEM ACCOUNT MANAGEMENT \"\n" + 
			"  specification: \"The organization employs automated mechanisms to support the management of information system accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(2) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | REMOVAL OF TEMPORARY / EMERGENCY ACCOUNTS \"\n" + 
			"  specification: \"The information system automatically [Selection: removes; disables] temporary and emergency accounts after [Assignment: organization-defined time period for each type of account].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(3) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | DISABLE INACTIVE ACCOUNTS \"\n" + 
			"  specification: \"The information system automatically disables inactive accounts after [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(4) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | AUTOMATED AUDIT ACTIONS \"\n" + 
			"  specification: \"The information system automatically audits account creation, modification, enabling, disabling, and removal actions, and notifies [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(5) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | INACTIVITY LOGOUT \"\n" + 
			"  specification: \"The organization requires that users log out when [Assignment: organization-defined time-period of expected inactivity or description of when to log out].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(6) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | DYNAMIC PRIVILEGE MANAGEMENT \"\n" + 
			"  specification: \"The information system implements the following dynamic privilege management capabilities: [Assignment: organization-defined list of dynamic privilege management capabilities].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(7) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | ROLE-BASED SCHEMES \"\n" + 
			"  specification: \"The organization: (a) Establishes and administers privileged user accounts in accordance with a role-based access scheme that organizes allowed information system access and privileges into roles; (b) Monitors privileged role assignments; and (c) Takes [Assignment: organization-defined actions] when privileged role assignments are no longer appropriate.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(8) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | DYNAMIC ACCOUNT CREATION \"\n" + 
			"  specification: \"The information system creates [Assignment: organization-defined information system accounts] dynamically.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(9) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | RESTRICTIONS ON USE OF SHARED / GROUP ACCOUNTS \"\n" + 
			"  specification: \"The organization only permits the use of shared/group accounts that meet [Assignment: organization-defined conditions for establishing shared/group accounts].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(10) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | SHARED / GROUP ACCOUNT CREDENTIAL TERMINATION \"\n" + 
			"  specification: \"The information system terminates shared/group account credentials when members leave the group.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(11) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | USAGE CONDITIONS \"\n" + 
			"  specification: \"The information system enforces [Assignment: organization-defined circumstances and/or usage conditions] for [Assignment: organization-defined information system accounts].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(12) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | ACCOUNT MONITORING / ATYPICAL USAGE \"\n" + 
			"  specification: \"The organization:(a) Monitors information system accounts for [Assignment: organization-defined atypical usage]; and (b) Reports atypical usage of information system accounts to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(13) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | DISABLE ACCOUNTS FOR HIGH-RISK INDIVIDUALS \"\n" + 
			"  specification: \"The organization disables accounts of users posing a significant risk within [Assignment: organization-defined time period] of discovery of the risk.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3 { \n" + 
			"  name: \"ACCESS ENFORCEMENT \"\n" + 
			"  specification: \"The information system enforces approved authorizations for logical access to information and system resources in accordance with applicable access control policies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(1) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | RESTRICTED ACCESS TO PRIVILEGED FUNCTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(2) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | DUAL AUTHORIZATION \"\n" + 
			"  specification: \"The information system enforces dual authorization for [Assignment: organization-defined privileged commands and/or other organization-defined actions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(3) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | MANDATORY ACCESS CONTROL \"\n" + 
			"  specification: \"The information system enforces [Assignment: organization-defined mandatory access control policy] over all subjects and objects where the policy:\n" + 
			"			(a) Is uniformly enforced across all subjects and objects within the boundary of the information system;\n" + 
			"			(b) Specifies that a subject that has been granted access to information is constrained from doing any of the following;\n" + 
			"				(1) Passing the information to unauthorized subjects or objects;\n" + 
			"				(2) Granting its privileges to other subjects;\n" + 
			"				(3) Changing one or more security attributes on subjects, objects, the information system, or information system components;\n" + 
			"				(4) Choosing the security attributes and attribute values to be associated with newly created or modified objects; or\n" + 
			"				(5) Changing the rules governing access control; and\n" + 
			"			(c) Specifies that [Assignment: organization-defined subjects] may explicitly be granted [Assignment: organization-defined privileges (i.e., they are trusted subjects)] such that they are not limited by some or all of the above constraints.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(4) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | DISCRETIONARY ACCESS CONTROL \"\n" + 
			"  specification: \"The information system enforces [Assignment: organization-defined discretionary access control policy] over defined subjects and objects where the policy specifies that a subject that has been granted access to information can do one or more of the following:\n" + 
			"			(a) Pass the information to any other subjects or objects;\n" + 
			"			(b) Grant its privileges to other subjects;\n" + 
			"			(c) Change security attributes on subjects, objects, the information system, or the information system’s components;\n" + 
			"			(d) Choose the security attributes to be associated with newly created or revised objects; or\n" + 
			"			(e) Change the rules governing access control.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(5) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | SECURITY-RELEVANT INFORMATION \"\n" + 
			"  specification: \"The information system prevents access to [Assignment: organization-defined security-relevant information] except during secure, non-operable system states.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(6) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | PROTECTION OF USER AND SYSTEM INFORMATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-4 and SC-28.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(7) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | ROLE-BASED ACCESS CONTROL \"\n" + 
			"  specification: \"The information system enforces a role-based access control policy over defined subjects and objects and controls access based upon [Assignment: organization-defined roles and users authorized to assume such roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(8) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | REVOCATION OF ACCESS AUTHORIZATIONS \"\n" + 
			"  specification: \"The information system enforces the revocation of access authorizations resulting from changes to the security attributes of subjects and objects based on [Assignment: organization-defined rules governing the timing of revocations of access authorizations].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(9) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | CONTROLLED RELEASE \"\n" + 
			"  specification: \"The information system does not release information outside of the established system boundary unless: (a) The receiving [Assignment: organization-defined information system or system component] provides [Assignment: organization-defined security safeguards]; and (b) [Assignment: organization-defined security safeguards] are used to validate the appropriateness of the information designated for release.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(10) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | AUDITED OVERRIDE OF ACCESS CONTROL MECHANISMS \"\n" + 
			"  specification: \"The organization employs an audited override of automated access control mechanisms under [Assignment: organization-defined conditions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4 { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT \"\n" + 
			"  specification: \"The information system enforces approved authorizations for controlling the flow of information within the system and between interconnected systems based on [Assignment: organization-defined information flow control policies].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(1) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | OBJECT SECURITY ATTRIBUTES \"\n" + 
			"  specification: \"The information system uses [Assignment: organization-defined security attributes] associated with [Assignment: organization-defined information, source, and destination objects] to enforce [Assignment: organization-defined information flow control policies] as a basis for flow control decisions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(2) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | PROCESSING DOMAINS \"\n" + 
			"  specification: \"The information system uses protected processing domains to enforce [Assignment: organization-defined information flow control policies] as a basis for flow control decisions\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(3) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | DYNAMIC INFORMATION FLOW CONTROL \"\n" + 
			"  specification: \"The information system enforces dynamic information flow control based on [Assignment: organization-defined policies].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(4) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | CONTENT CHECK ENCRYPTED INFORMATION \"\n" + 
			"  specification: \"The information system prevents encrypted information from bypassing content-checking mechanisms by [Selection (one or more): decrypting the information; blocking the flow of the encrypted information; terminating communications sessions attempting to pass encrypted information; [Assignment: organization-defined procedure or method]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(5) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | EMBEDDED DATA TYPES \"\n" + 
			"  specification: \"The information system enforces [Assignment: organization-defined limitations] on embedding data types within other data types.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(6) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | METADATA \"\n" + 
			"  specification: \"The information system enforces information flow control based on [Assignment: organization-defined metadata].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(7) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | ONE-WAY FLOW MECHANISMS \"\n" + 
			"  specification: \"The information system enforces [Assignment: organization-defined one-way information flows] using hardware mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(8) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | SECURITY POLICY FILTERS \"\n" + 
			"  specification: \"The information system enforces information flow control using [Assignment: organization-defined security policy filters] as a basis for flow control decisions for [Assignment: organization-defined information flows].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(9) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | HUMAN REVIEWS \"\n" + 
			"  specification: \"The information system enforces the use of human reviews for [Assignment: organization-defined information flows] under the following conditions: [Assignment: organization-defined conditions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(10) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | ENABLE / DISABLE SECURITY POLICY FILTERS \"\n" + 
			"  specification: \"The information system provides the capability for privileged administrators to enable/disable [Assignment: organization-defined security policy filters] under the following conditions: [Assignment: organization-defined conditions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(11) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | CONFIGURATION OF SECURITY POLICY FILTERS \"\n" + 
			"  specification: \"The information system provides the capability for privileged administrators to configure [Assignment: organization-defined security policy filters] to support different security policies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(12) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | DATA TYPE IDENTIFIERS \"\n" + 
			"  specification: \"The information system, when transferring information between different security domains, uses [Assignment: organization-defined data type identifiers] to validate data essential for information flow decisions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(13) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | DECOMPOSITION INTO POLICY-RELEVANT SUBCOMPONENTS \"\n" + 
			"  specification: \"The information system, when transferring information between different security domains, decomposes information into [Assignment: organization-defined policy-relevant subcomponents] for submission to policy enforcement mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(14) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | SECURITY POLICY FILTER CONSTRAINTS \"\n" + 
			"  specification: \"The information system, when transferring information between different security domains, implements [Assignment: organization-defined security policy filters] requiring fully enumerated formats that restrict data structure and content.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(15) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | DETECTION OF UNSANCTIONED INFORMATION \"\n" + 
			"  specification: \"The information system, when transferring information between different security domains, examines the information for the presence of [Assignment: organized-defined unsanctioned information] and prohibits the transfer of such information in accordance with the [Assignment: organization-defined security policy].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(16) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | INFORMATION TRANSFERS ON INTERCONNECTED SYSTEMS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(17) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | | DOMAIN AUTHENTICATION \"\n" + 
			"  specification: \"The information system uniquely identifies and authenticates source and destination points by [Selection (one or more): organization, system, application, individual] for information transfer.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(18) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | SECURITY ATTRIBUTE BINDING \"\n" + 
			"  specification: \"The information system binds security attributes to information using [Assignment: organization-defined binding techniques] to facilitate information flow policy enforcement.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(19) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | VALIDATION OF METADATA \"\n" + 
			"  specification: \"The information system, when transferring information between different security domains, applies the same security policy filtering to metadata as it applies to data payloads.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(20) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | APPROVED SOLUTIONS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined solutions in approved configurations] to control the flow of [Assignment: organization-defined information] across security domains.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(21) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | PHYSICAL / LOGICAL SEPARATION OF INFORMATION FLOWS \"\n" + 
			"  specification: \"The information system separates information flows logically or physically using [Assignment: organization-defined mechanisms and/or techniques] to accomplish [Assignment: organization-defined required separations by types of information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(22) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | ACCESS ONLY \"\n" + 
			"  specification: \"The information system provides access from a single device to computing platforms, applications, or data residing on multiple different security domains, while preventing any information flow between the different security domains.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-5 { \n" + 
			"  name: \"SEPARATION OF DUTIES \"\n" + 
			"  specification: \"The organization: a. Separates [Assignment: organization-defined duties of individuals]; b. Documents separation of duties of individuals; and c. Defines information system access authorizations to support separation of duties.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6 { \n" + 
			"  name: \"SEPARATION OF DUTIES \"\n" + 
			"  specification: \"The organization: a. Separates [Assignment: organization-defined duties of individuals]; b. Documents separation of duties of individuals; and c. Defines information system access authorizations to support separation of duties.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(1) { \n" + 
			"  name: \"LEAST PRIVILEGE | AUTHORIZE ACCESS TO SECURITY FUNCTIONS \"\n" + 
			"  specification: \"The organization explicitly authorizes access to [Assignment: organization-defined security functions (deployed in hardware, software, and firmware) and security-relevant information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(2) { \n" + 
			"  name: \"LEAST PRIVILEGE | NON-PRIVILEGED ACCESS FOR NONSECURITY FUNCTIONS \"\n" + 
			"  specification: \"The organization requires that users of information system accounts, or roles, with access to [Assignment: organization-defined security functions or security-relevant information], use non-privileged accounts or roles, when accessing nonsecurity functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(3) { \n" + 
			"  name: \"LEAST PRIVILEGE | NETWORK ACCESS TO PRIVILEGED COMMANDS \"\n" + 
			"  specification: \"The organization authorizes network access to [Assignment: organization-defined privileged commands] only for [Assignment: organization-defined compelling operational needs] and documents the rationale for such access in the security plan for the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(4) { \n" + 
			"  name: \"LEAST PRIVILEGE | SEPARATE PROCESSING DOMAINS \"\n" + 
			"  specification: \"The information system provides separate processing domains to enable finer-grained allocation of user privileges.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(5) { \n" + 
			"  name: \"LEAST PRIVILEGE | PRIVILEGED ACCOUNTS \"\n" + 
			"  specification: \"The organization restricts privileged accounts on the information system to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(6) { \n" + 
			"  name: \"LEAST PRIVILEGE | PRIVILEGED ACCESS BY NON-ORGANIZATIONAL USERS \"\n" + 
			"  specification: \"The organization prohibits privileged access to the information system by non-organizational users.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(7) { \n" + 
			"  name: \"LEAST PRIVILEGE | REVIEW OF USER PRIVILEGES \"\n" + 
			"  specification: \"The organization: (a) Reviews [Assignment: organization-defined frequency] the privileges assigned to [Assignment: organization-defined roles or classes of users] to validate the need for such privileges; and (b) Reassigns or removes privileges, if necessary, to correctly reflect organizational mission/business needs.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(8) { \n" + 
			"  name: \"LEAST PRIVILEGE | PRIVILEGE LEVELS FOR CODE EXECUTION \"\n" + 
			"  specification: \"The information system prevents [Assignment: organization-defined software] from executing at higher privilege levels than users executing the software\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(9) { \n" + 
			"  name: \"LEAST PRIVILEGE | AUDITING USE OF PRIVILEGED FUNCTIONS \"\n" + 
			"  specification: \"The information system audits the execution of privileged functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(10) { \n" + 
			"  name: \"LEAST PRIVILEGE | PROHIBIT NON-PRIVILEGED USERS FROM EXECUTING PRIVILEGED FUNCTIONS \"\n" + 
			"  specification: \"The information system prevents non-privileged users from executing privileged functions to include disabling, circumventing, or altering implemented security safeguards/countermeasures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-7 { \n" + 
			"  name: \"UNSUCCESSFUL LOGON ATTEMPTS \"\n" + 
			"  specification: \"The information system: a. Enforces a limit of [Assignment: organization-defined number] consecutive invalid logon attempts by a user during a [Assignment: organization-defined time period]; and b. Automatically [Selection: locks the account/node for an [Assignment: organization-defined time period]; locks the account/node until released by an administrator; delays next logon prompt according to [Assignment: organization-defined delay algorithm]] when the maximum number of unsuccessful attempts is exceeded.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-7(1) { \n" + 
			"  name: \"UNSUCCESSFUL LOGON ATTEMPTS | AUTOMATIC ACCOUNT LOCK \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-7(2) { \n" + 
			"  name: \"UNSUCCESSFUL LOGON ATTEMPTS | PURGE / WIPE MOBILE DEVICE \"\n" + 
			"  specification: \"The information system purges/wipes information from [Assignment: organization-defined mobile devices] based on [Assignment: organization-defined purging/wiping requirements/techniques] after [Assignment: organization-defined number] consecutive, unsuccessful device logon attempts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-8 { \n" + 
			"  name: \"SYSTEM USE NOTIFICATION \"\n" + 
			"  specification: \"The information system:\n" + 
			"			a. Displays to users [Assignment: organization-defined system use notification message or banner] before granting access to the system that provides privacy and security notices consistent with applicable federal laws, Executive Orders, directives, policies, regulations, standards, and guidance and states that:\n" + 
			"				1. Users are accessing a U.S. Government information system;\n" + 
			"				2. Information system usage may be monitored, recorded, and subject to audit;\n" + 
			"				3. Unauthorized use of the information system is prohibited and subject to criminal and civil penalties; and\n" + 
			"				4. Use of the information system indicates consent to monitoring and recording;\n" + 
			"			b. Retains the notification message or banner on the screen until users acknowledge the usage conditions and take explicit actions to log on to or further access the information system; and\n" + 
			"			c. For publicly accessible systems:\n" + 
			"				1. Displays system use information [Assignment: organization-defined conditions], before granting further access;\n" + 
			"				2. Displays references, if any, to monitoring, recording, or auditing that are consistent with privacy accommodations for such systems that generally prohibit those activities; and\n" + 
			"				3. Includes a description of the authorized uses of the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-9 { \n" + 
			"  name: \"PREVIOUS LOGON (ACCESS) NOTIFICATION \"\n" + 
			"  specification: \"The information system notifies the user, upon successful logon (access) to the system, of the date and time of the last logon (access).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-9(1) { \n" + 
			"  name: \"PREVIOUS LOGON NOTIFICATION | UNSUCCESSFUL LOGONS \"\n" + 
			"  specification: \"The information system notifies the user, upon successful logon/access, of the number of unsuccessful logon/access attempts since the last successful logon/access.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-9(2) { \n" + 
			"  name: \"PREVIOUS LOGON NOTIFICATION | SUCCESSFUL / UNSUCCESSFUL LOGONS \"\n" + 
			"  specification: \"The information system notifies the user of the number of [Selection: successful logons/accesses; unsuccessful logon/access attempts; both] during [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-9(3) { \n" + 
			"  name: \"PREVIOUS LOGON NOTIFICATION | NOTIFICATION OF ACCOUNT CHANGES \"\n" + 
			"  specification: \"The information system notifies the user of changes to [Assignment: organization-defined security-related characteristics/parameters of the user’s account] during [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-9(4) { \n" + 
			"  name: \"PREVIOUS LOGON NOTIFICATION | ADDITIONAL LOGON INFORMATION \"\n" + 
			"  specification: \"The information system notifies the user, upon successful logon (access), of the following additional information: [Assignment: organization-defined information to be included in addition to the date and time of the last logon (access)].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-10 { \n" + 
			"  name: \"CONCURRENT SESSION CONTROL \"\n" + 
			"  specification: \"The information system limits the number of concurrent sessions for each [Assignment: organization-defined account and/or account type] to [Assignment: organization-defined number].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-11 { \n" + 
			"  name: \"SESSION LOCK \"\n" + 
			"  specification: \"The information system: a. Prevents further access to the system by initiating a session lock after [Assignment: organization-defined time period] of inactivity or upon receiving a request from a user; and b. Retains the session lock until the user reestablishes access using established identification and authentication procedures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-11(1) { \n" + 
			"  name: \"SESSION LOCK | PATTERN-HIDING DISPLAYS \"\n" + 
			"  specification: \"The information system conceals, via the session lock, information previously visible on the display with a publicly viewable image.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-12 { \n" + 
			"  name: \"SESSION TERMINATION \"\n" + 
			"  specification: \"The information system automatically terminates a user session after [Assignment: organization-defined conditions or trigger events requiring session disconnect].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-12(1) { \n" + 
			"  name: \"SESSION TERMINATION | USER-INITIATED LOGOUTS / MESSAGE DISPLAYS \"\n" + 
			"  specification: \"The information system: (a) Provides a logout capability for user-initiated communications sessions whenever authentication is used to gain access to [Assignment: organization-defined information resources]; and (b) Displays an explicit logout message to users indicating the reliable termination of authenticated communications sessions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-13 { \n" + 
			"  name: \"SUPERVISION AND REVIEW — ACCESS CONTROL \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-2 and AU-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-14 { \n" + 
			"  name: \"PERMITTED ACTIONS WITHOUT IDENTIFICATION OR AUTHENTICATION \"\n" + 
			"  specification: \"The organization: a. Identifies [Assignment: organization-defined user actions] that can be performed on the information system without identification or authentication consistent with organizational missions/business functions; and b. Documents and provides supporting rationale in the security plan for the information system, user actions not requiring identification or authentication.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-14(1) { \n" + 
			"  name: \"PERMITTED ACTIONS WITHOUT IDENTIFICATION OR AUTHENTICATION | NECESSARY USES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-14.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-15 { \n" + 
			"  name: \"AUTOMATED MARKING \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-3.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16 { \n" + 
			"  name: \"SECURITY ATTRIBUTES \"\n" + 
			"  specification: \"The organization: a. Provides the means to associate [Assignment: organization-defined types of security attributes] having [Assignment: organization-defined security attribute values] with information in storage, in process, and/or in transmission; b. Ensures that the security attribute associations are made and retained with the information; c. Establishes the permitted [Assignment: organization-defined security attributes] for [Assignment: organization-defined information systems]; and d. Determines the permitted [Assignment: organization-defined values or ranges] for each of the established security attributes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(1) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | DYNAMIC ATTRIBUTE ASSOCIATION \"\n" + 
			"  specification: \"The information system dynamically associates security attributes with [Assignment: organization-defined subjects and objects] in accordance with [Assignment: organization-defined security policies] as information is created and combined.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(2) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | ATTRIBUTE VALUE CHANGES BY AUTHORIZED INDIVIDUALS \"\n" + 
			"  specification: \"The information system provides authorized individuals (or processes acting on behalf of individuals) the capability to define or change the value of associated security attributes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(3) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | MAINTENANCE OF ATTRIBUTE ASSOCIATIONS BY INFORMATION SYSTEM \"\n" + 
			"  specification: \"The information system maintains the association and integrity of [Assignment: organization-defined security attributes] to [Assignment: organization-defined subjects and objects].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(4) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | ASSOCIATION OF ATTRIBUTES BY AUTHORIZED INDIVIDUALS \"\n" + 
			"  specification: \"The information system supports the association of [Assignment: organization-defined security attributes] with [Assignment: organization-defined subjects and objects] by authorized individuals (or processes acting on behalf of individuals).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(5) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | ATTRIBUTE DISPLAYS FOR OUTPUT DEVICES \"\n" + 
			"  specification: \"The information system displays security attributes in human-readable form on each object that the system transmits to output devices to identify [Assignment: organization-identified special dissemination, handling, or distribution instructions] using [Assignment: organization-identified human-readable, standard naming conventions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(6) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | MAINTENANCE OF ATTRIBUTE ASSOCIATION BY ORGANIZATION \"\n" + 
			"  specification: \"The organization allows personnel to associate, and maintain the association of [Assignment: organization-defined security attributes] with [Assignment: organization-defined subjects and objects] in accordance with [Assignment: organization-defined security policies].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(7) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | CONSISTENT ATTRIBUTE INTERPRETATION \"\n" + 
			"  specification: \"The organization provides a consistent interpretation of security attributes transmitted between distributed information system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(8) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | ASSOCIATION TECHNIQUES / TECHNOLOGIES \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined techniques or technologies] with [Assignment: organization-defined level of assurance] in associating security attributes to information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(9) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | ATTRIBUTE REASSIGNMENT \"\n" + 
			"  specification: \"The organization ensures that security attributes associated with information are reassigned only via re-grading\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(10) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | ATTRIBUTE CONFIGURATION BY AUTHORIZED INDIVIDUALS \"\n" + 
			"  specification: \"The information system provides authorized individuals the capability to define or change the type and value of security attributes available for association with subjects and objects.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17 { \n" + 
			"  name: \"REMOTE ACCESS \"\n" + 
			"  specification: \"The organization: a. Establishes and documents usage restrictions, configuration/connection requirements, and implementation guidance for each type of remote access allowed; and b. Authorizes remote access to the information system prior to allowing such connections.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(1) { \n" + 
			"  name: \"REMOTE ACCESS | AUTOMATED MONITORING / CONTROL \"\n" + 
			"  specification: \"The information system monitors and controls remote access methods.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(2) { \n" + 
			"  name: \"REMOTE ACCESS | PROTECTION OF CONFIDENTIALITY / INTEGRITY USING ENCRYPTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to protect the confidentiality and integrity of remote access sessions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(3) { \n" + 
			"  name: \"REMOTE ACCESS | MANAGED ACCESS CONTROL POINTS \"\n" + 
			"  specification: \"The information system routes all remote accesses through [Assignment: organization-defined number] managed network access control points.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(4) { \n" + 
			"  name: \"REMOTE ACCESS | PRIVILEGED COMMANDS / ACCESS \"\n" + 
			"  specification: \"The organization: (a) Authorizes the execution of privileged commands and access to security-relevant information via remote access only for [Assignment: organization-defined needs]; and (b) Documents the rationale for such access in the security plan for the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(5) { \n" + 
			"  name: \"REMOTE ACCESS | MONITORING FOR UNAUTHORIZED CONNECTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(6) { \n" + 
			"  name: \"REMOTE ACCESS | PROTECTION OF INFORMATION \"\n" + 
			"  specification: \"The organization ensures that users protect information about remote access mechanisms from unauthorized use and disclosure.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(7) { \n" + 
			"  name: \"REMOTE ACCESS | ADDITIONAL PROTECTION FOR SECURITY FUNCTION ACCESS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-3 (10).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(8) { \n" + 
			"  name: \"REMOTE ACCESS | DISABLE NONSECURE NETWORK PROTOCOLS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(9) { \n" + 
			"  name: \"REMOTE ACCESS | DISCONNECT / DISABLE ACCESS \"\n" + 
			"  specification: \"The organization provides the capability to expeditiously disconnect or disable remote access to the information system within [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-18 { \n" + 
			"  name: \"WIRELESS ACCESS \"\n" + 
			"  specification: \"The organization: a. Establishes usage restrictions, configuration/connection requirements, and implementation guidance for wireless access; and b. Authorizes wireless access to the information system prior to allowing such connections.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-18(1) { \n" + 
			"  name: \"WIRELESS ACCESS | AUTHENTICATION AND ENCRYPTION \"\n" + 
			"  specification: \"The information system protects wireless access to the system using authentication of [Selection (one or more): users; devices] and encryption.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-18(2) { \n" + 
			"  name: \"WIRELESS ACCESS | MONITORING UNAUTHORIZED CONNECTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-18(3) { \n" + 
			"  name: \"WIRELESS ACCESS | DISABLE WIRELESS NETWORKING \"\n" + 
			"  specification: \"The organization disables, when not intended for use, wireless networking capabilities internally embedded within information system components prior to issuance and deployment.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-18(4) { \n" + 
			"  name: \"WIRELESS ACCESS | RESTRICT CONFIGURATIONS BY USERS \"\n" + 
			"  specification: \"The organization identifies and explicitly authorizes users allowed to independently configure wireless networking capabilities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-18(5) { \n" + 
			"  name: \"WIRELESS ACCESS | ANTENNAS / TRANSMISSION POWER LEVELS \"\n" + 
			"  specification: \"The organization selects radio antennas and calibrates transmission power levels to reduce the probability that usable signals can be received outside of organization-controlled boundaries.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-19 { \n" + 
			"  name: \"ACCESS CONTROL FOR MOBILE DEVICES \"\n" + 
			"  specification: \"The organization: a. Establishes usage restrictions, configuration requirements, connection requirements, and implementation guidance for organization-controlled mobile devices; and b. Authorizes the connection of mobile devices to organizational information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-19(1) { \n" + 
			"  name: \"ACCESS CONTROL FOR MOBILE DEVICES | USE OF WRITABLE / PORTABLE STORAGE DEVICES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-19(2) { \n" + 
			"  name: \"ACCESS CONTROL FOR MOBILE DEVICES | USE OF PERSONALLY OWNED PORTABLE STORAGE DEVICES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-19(3) { \n" + 
			"  name: \"ACCESS CONTROL FOR MOBILE DEVICES | USE OF PORTABLE STORAGE DEVICES WITH NO IDENTIFIABLE OWNER \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-19(4) { \n" + 
			"  name: \"ACCESS CONTROL FOR MOBILE DEVICES | RESTRICTIONS FOR CLASSIFIED INFORMATION \"\n" + 
			"  specification: \"The organization:\n" + 
			"			(a) Prohibits the use of unclassified mobile devices in facilities containing information systems processing, storing, or transmitting classified information unless specifically permitted by the authorizing official; and\n" + 
			"			(b) Enforces the following restrictions on individuals permitted by the authorizing official to use unclassified mobile devices in facilities containing information systems processing, storing, or transmitting classified information:\n" + 
			"				(1) Connection of unclassified mobile devices to classified information systems is prohibited;\n" + 
			"				(2) Connection of unclassified mobile devices to unclassified information systems requires approval from the authorizing official;\n" + 
			"				(3) Use of internal or external modems or wireless interfaces within the unclassified mobile devices is prohibited; and\n" + 
			"				(4) Unclassified mobile devices and the information stored on those devices are subject to random reviews and inspections by [Assignment: organization-defined security officials], and if classified information is found, the incident handling policy is followed.\n" + 
			"			(c) Restricts the connection of classified mobile devices to classified information systems in accordance with [Assignment: organization-defined security policies].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-19(5) { \n" + 
			"  name: \"ACCESS CONTROL FOR MOBILE DEVICES | FULL DEVICE / CONTAINER-BASED ENCRYPTION \"\n" + 
			"  specification: \"The organization employs [Selection: full-device encryption; container encryption] to protect the confidentiality and integrity of information on [Assignment: organization-defined mobile devices].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-20 { \n" + 
			"  name: \"USE OF EXTERNAL INFORMATION SYSTEMS \"\n" + 
			"  specification: \"The organization establishes terms and conditions, consistent with any trust relationships established with other organizations owning, operating, and/or maintaining external information systems, allowing authorized individuals to: a. Access the information system from external information systems; and b. Process, store, or transmit organization-controlled information using external information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-20(1) { \n" + 
			"  name: \"USE OF EXTERNAL INFORMATION SYSTEMS | LIMITS ON AUTHORIZED USE \"\n" + 
			"  specification: \"The organization permits authorized individuals to use an external information system to access the information system or to process, store, or transmit organization-controlled information only when the organization: (a) Verifies the implementation of required security controls on the external system as specified in the organization’s information security policy and security plan; or (b) Retains approved information system connection or processing agreements with the organizational entity hosting the external information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-20(2) { \n" + 
			"  name: \"USE OF EXTERNAL INFORMATION SYSTEMS | PORTABLE STORAGE DEVICES \"\n" + 
			"  specification: \"The organization [Selection: restricts; prohibits] the use of organization-controlled portable storage devices by authorized individuals on external information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-20(3) { \n" + 
			"  name: \"USE OF EXTERNAL INFORMATION SYSTEMS | NON-ORGANIZATIONALLY OWNED SYSTEMS / COMPONENTS / DEVICES \"\n" + 
			"  specification: \"The organization [Selection: restricts; prohibits] the use of non-organizationally owned information systems, system components, or devices to process, store, or transmit organizational information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-20(4) { \n" + 
			"  name: \"USE OF EXTERNAL INFORMATION SYSTEMS | NETWORK ACCESSIBLE STORAGE DEVICES \"\n" + 
			"  specification: \"The organization prohibits the use of [Assignment: organization-defined network accessible storage devices] in external information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-21 { \n" + 
			"  name: \"INFORMATION SHARING \"\n" + 
			"  specification: \"The organization: a. Facilitates information sharing by enabling authorized users to determine whether access authorizations assigned to the sharing partner match the access restrictions on the information for [Assignment: organization-defined information sharing circumstances where user discretion is required]; and b. Employs [Assignment: organization-defined automated mechanisms or manual processes] to assist users in making information sharing/collaboration decisions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-21(1) { \n" + 
			"  name: \"INFORMATION SHARING | AUTOMATED DECISION SUPPORT \"\n" + 
			"  specification: \"The information system enforces information-sharing decisions by authorized users based on access authorizations of sharing partners and access restrictions on information to be shared.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-21(2) { \n" + 
			"  name: \"INFORMATION SHARING | INFORMATION SEARCH AND RETRIEVAL \"\n" + 
			"  specification: \"The information system implements information search and retrieval services that enforce [Assignment: organization-defined information sharing restrictions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-22 { \n" + 
			"  name: \"PUBLICLY ACCESSIBLE CONTENT \"\n" + 
			"  specification: \"The organization: a. Designates individuals authorized to post information onto a publicly accessible information system; b. Trains authorized individuals to ensure that publicly accessible information does not contain nonpublic information; c. Reviews the proposed content of information prior to posting onto the publicly accessible information system to ensure that nonpublic information is not included; and d. Reviews the content on the publicly accessible information system for nonpublic information [Assignment: organization-defined frequency] and removes such information, if discovered.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-23 { \n" + 
			"  name: \"DATA MINING PROTECTION \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined data mining prevention and detection techniques] for [Assignment: organization-defined data storage objects] to adequately detect and protect against data mining.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-24 { \n" + 
			"  name: \"ACCESS CONTROL DECISIONS \"\n" + 
			"  specification: \"The organization establishes procedures to ensure [Assignment: organization-defined access control decisions] are applied to each access request prior to access enforcement.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-24(1) { \n" + 
			"  name: \"ACCESS CONTROL DECISIONS | TRANSMIT ACCESS AUTHORIZATION INFORMATION \"\n" + 
			"  specification: \"The information system transmits [Assignment: organization-defined access authorization information] using [Assignment: organization-defined security safeguards] to [Assignment: organization-defined information systems] that enforce access control decisions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-24(2) { \n" + 
			"  name: \"ACCESS CONTROL DECISIONS | NO USER OR PROCESS IDENTITY \"\n" + 
			"  specification: \"The information system enforces access control decisions based on [Assignment: organization-defined security attributes] that do not include the identity of the user or process acting on behalf of the user.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-25 { \n" + 
			"  name: \"REFERENCE MONITOR \"\n" + 
			"  specification: \"The information system implements a reference monitor for [Assignment: organization-defined access control policies] that is tamperproof, always invoked, and small enough to be subject to analysis and testing, the completeness of which can be assured.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AT-1 { \n" + 
			"  name: \"SECURITY AWARENESS AND TRAINING POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The information system implements a reference monitor for [Assignment: organization-defined access control policies] that is tamperproof, always invoked, and small enough to be subject to analysis and testing, the completeness of which can be assured.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-2 { \n" + 
			"  name: \"SECURITY AWARENESS AND TRAINING POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization provides basic security awareness training to information system users (including managers, senior executives, and contractors): a. As part of initial training for new users; b. When required by information system changes; and c. [Assignment: organization-defined frequency] thereafter.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-2(1) { \n" + 
			"  name: \"SECURITY AWARENESS | PRACTICAL EXERCISES \"\n" + 
			"  specification: \"The organization includes practical exercises in security awareness training that simulate actual cyber attacks.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-2(2) { \n" + 
			"  name: \"SECURITY AWARENESS | INSIDER THREAT \"\n" + 
			"  specification: \"The organization includes security awareness training on recognizing and reporting potential indicators of insider threat.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-3 { \n" + 
			"  name: \"ROLE-BASED SECURITY TRAINING \"\n" + 
			"  specification: \"The organization provides role-based security training to personnel with assigned security roles and responsibilities: a. Before authorizing access to the information system or performing assigned duties; b. When required by information system changes; and c. [Assignment: organization-defined frequency] thereafter.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-3(1) { \n" + 
			"  name: \"ROLE-BASED SECURITY TRAINING | ENVIRONMENTAL CONTROLS \"\n" + 
			"  specification: \"The organization provides [Assignment: organization-defined personnel or roles] with initial and [Assignment: organization-defined frequency] training in the employment and operation of environmental controls.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-3(2) { \n" + 
			"  name: \"ROLE-BASED SECURITY TRAINING | PHYSICAL SECURITY CONTROLS \"\n" + 
			"  specification: \"The organization provides [Assignment: organization-defined personnel or roles] with initial and [Assignment: organization-defined frequency] training in the employment and operation of physical security controls.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-3(3) { \n" + 
			"  name: \"ROLE-BASED SECURITY TRAINING | PRACTICAL EXERCISES \"\n" + 
			"  specification: \"The organization includes practical exercises in security training that reinforce training objectives.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-3(4) { \n" + 
			"  name: \"ROLE-BASED SECURITY TRAINING | SUSPICIOUS COMMUNICATIONS AND ANOMALOUS SYSTEM BEHAVIOR \"\n" + 
			"  specification: \"The organization provides training to its personnel on [Assignment: organization-defined indicators of malicious code] to recognize suspicious communications and anomalous behavior in organizational information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-4 { \n" + 
			"  name: \"SECURITY TRAINING RECORDS \"\n" + 
			"  specification: \"The organization: a. Documents and monitors individual information system security training activities including basic security awareness training and specific information system security training; and b. Retains individual training records for [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-5 { \n" + 
			"  name: \"CONTACTS WITH SECURITY GROUPS AND ASSOCIATIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PM-15.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AU-1 { \n" + 
			"  name: \"AUDIT AND ACCOUNTABILITY POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. An audit and accountability policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the audit and accountability policy and associated audit and accountability controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Audit and accountability policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Audit and accountability procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-2 { \n" + 
			"  name: \"AUDIT EVENTS \"\n" + 
			"  specification: \" The organization:\n" + 
			"			a. Determines that the information system is capable of auditing the following events: [Assignment: organization-defined auditable events];\n" + 
			"			b. Coordinates the security audit function with other organizational entities requiring audit-related information to enhance mutual support and to help guide the selection of auditable events;\n" + 
			"			c. Provides a rationale for why the auditable events are deemed to be adequate to support after-the-fact investigations of security incidents; and\n" + 
			"			d. Determines that the following events are to be audited within the information system: [Assignment: organization-defined audited events (the subset of the auditable events defined in AU-2 a.) along with the frequency of (or situation requiring) auditing for each identified event].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-2(1) { \n" + 
			"  name: \"AUDIT EVENTS | COMPILATION OF AUDIT RECORDS FROM MULTIPLE SOURCES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AU-12.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-2(2) { \n" + 
			"  name: \"AUDIT EVENTS | SELECTION OF AUDIT EVENTS BY COMPONENT \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AU-12.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-2(3) { \n" + 
			"  name: \"AUDIT EVENTS | REVIEWS AND UPDATES \"\n" + 
			"  specification: \"The organization reviews and updates the audited events [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-2(4) { \n" + 
			"  name: \"AUDIT EVENTS | PRIVILEGED FUNCTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-6 (9).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-3 { \n" + 
			"  name: \"CONTENT OF AUDIT RECORDS \"\n" + 
			"  specification: \"The information system generates audit records containing information that establishes what type of event occurred, when the event occurred, where the event occurred, the source of the event, the outcome of the event, and the identity of any individuals or subjects associated with the event.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-3(1) { \n" + 
			"  name: \"CONTENT OF AUDIT RECORDS | ADDITIONAL AUDIT INFORMATION \"\n" + 
			"  specification: \"The information system generates audit records containing the following additional information: [Assignment: organization-defined additional, more detailed information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-3(2) { \n" + 
			"  name: \"CONTENT OF AUDIT RECORDS | CENTRALIZED MANAGEMENT OF PLANNED AUDIT RECORD CONTENT \"\n" + 
			"  specification: \"The information system provides centralized management and configuration of the content to be captured in audit records generated by [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-4 { \n" + 
			"  name: \"AUDIT STORAGE CAPACITY \"\n" + 
			"  specification: \"The organization allocates audit record storage capacity in accordance with [Assignment: organization-defined audit record storage requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-4(1) { \n" + 
			"  name: \"AUDIT STORAGE CAPACITY | TRANSFER TO ALTERNATE STORAGE \"\n" + 
			"  specification: \"The information system off-loads audit records [Assignment: organization-defined frequency] onto a different system or media than the system being audited.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-5 { \n" + 
			"  name: \"RESPONSE TO AUDIT PROCESSING FAILURES \"\n" + 
			"  specification: \"The information system: a. Alerts [Assignment: organization-defined personnel or roles] in the event of an audit processing failure; and b. Takes the following additional actions: [Assignment: organization-defined actions to be taken (e.g., shut down information system, overwrite oldest audit records, stop generating audit records)].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-5(1) { \n" + 
			"  name: \"RESPONSE TO AUDIT PROCESSING FAILURES | AUDIT STORAGE CAPACITY \"\n" + 
			"  specification: \"The information system provides a warning to [Assignment: organization-defined personnel, roles, and/or locations] within [Assignment: organization-defined time period] when allocated audit record storage volume reaches [Assignment: organization-defined percentage] of repository maximum audit record storage capacity.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-5(2) { \n" + 
			"  name: \"RESPONSE TO AUDIT PROCESSING FAILURES | REAL-TIME ALERTS \"\n" + 
			"  specification: \"The information system provides an alert in [Assignment: organization-defined real-time period] to [Assignment: organization-defined personnel, roles, and/or locations] when the following audit failure events occur: [Assignment: organization-defined audit failure events requiring real-time alerts].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-5(3) { \n" + 
			"  name: \"RESPONSE TO AUDIT PROCESSING FAILURES | CONFIGURABLE TRAFFIC VOLUME THRESHOLDS \"\n" + 
			"  specification: \"The information system enforces configurable network communications traffic volume thresholds reflecting limits on auditing capacity and [Selection: rejects; delays] network traffic above those thresholds.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-5(4) { \n" + 
			"  name: \"RESPONSE TO AUDIT PROCESSING FAILURES | SHUTDOWN ON FAILURE \"\n" + 
			"  specification: \"The information system invokes a [Selection: full system shutdown; partial system shutdown; degraded operational mode with limited mission/business functionality available] in the event of [Assignment: organization-defined audit failures], unless an alternate audit capability exists.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6 { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING \"\n" + 
			"  specification: \"The organization: a. Reviews and analyzes information system audit records [Assignment: organization-defined frequency] for indications of [Assignment: organization-defined inappropriate or unusual activity]; and b. Reports findings to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(1) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | PROCESS INTEGRATION \"\n" + 
			"  specification: \"The organization employs automated mechanisms to integrate audit review, analysis, and reporting processes to support organizational processes for investigation and response to suspicious activities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(2) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | AUTOMATED SECURITY ALERTS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(3) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | CORRELATE AUDIT REPOSITORIES \"\n" + 
			"  specification: \"The organization analyzes and correlates audit records across different repositories to gain organization-wide situational awareness.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(4) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | CENTRAL REVIEW AND ANALYSIS \"\n" + 
			"  specification: \"The information system provides the capability to centrally review and analyze audit records from multiple components within the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(5) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | INTEGRATION / SCANNING AND MONITORING CAPABILITIES \"\n" + 
			"  specification: \"The organization integrates analysis of audit records with analysis of [Selection (one or more): vulnerability scanning information; performance data; information system monitoring information; [Assignment: organization-defined data/information collected from other sources]] to further enhance the ability to identify inappropriate or unusual activity.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(6) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | CORRELATION WITH PHYSICAL MONITORING \"\n" + 
			"  specification: \"The organization correlates information from audit records with information obtained from monitoring physical access to further enhance the ability to identify suspicious, inappropriate, unusual, or malevolent activity.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(7) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | PERMITTED ACTIONS \"\n" + 
			"  specification: \"The organization specifies the permitted actions for each [Selection (one or more): information system process; role; user] associated with the review, analysis, and reporting of audit information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(8) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | FULL TEXT ANALYSIS OF PRIVILEGED COMMANDS \"\n" + 
			"  specification: \"The organization performs a full text analysis of audited privileged commands in a physically distinct component or subsystem of the information system, or other information system that is dedicated to that analysis.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(9) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | CORRELATION WITH INFORMATION FROM NONTECHNICAL SOURCES \"\n" + 
			"  specification: \"The organization correlates information from nontechnical sources with audit information to enhance organization-wide situational awareness.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(10) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | AUDIT LEVEL ADJUSTMENT \"\n" + 
			"  specification: \"The organization adjusts the level of audit review, analysis, and reporting within the information system when there is a change in risk based on law enforcement information, intelligence information, or other credible sources of information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-7 { \n" + 
			"  name: \"AUDIT REDUCTION AND REPORT GENERATION \"\n" + 
			"  specification: \"The information system provides an audit reduction and report generation capability that: a. Supports on-demand audit review, analysis, and reporting requirements and after-the-fact investigations of security incidents; and b. Does not alter the original content or time ordering of audit records.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-7(1) { \n" + 
			"  name: \"AUDIT REDUCTION AND REPORT GENERATION | AUTOMATIC PROCESSING \"\n" + 
			"  specification: \"The information system provides the capability to process audit records for events of interest based on [Assignment: organization-defined audit fields within audit records].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-7(2) { \n" + 
			"  name: \"AUDIT REDUCTION AND REPORT GENERATION | AUTOMATIC SORT AND SEARCH \"\n" + 
			"  specification: \"The information system provides the capability to sort and search audit records for events of interest based on the content of [Assignment: organization-defined audit fields within audit records].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-8 { \n" + 
			"  name: \"TIME STAMPS \"\n" + 
			"  specification: \"The information system: a. Uses internal system clocks to generate time stamps for audit records; and b. Records time stamps for audit records that can be mapped to Coordinated Universal Time (UTC) or Greenwich Mean Time (GMT) and meets [Assignment: organization-defined granularity of time measurement].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-8(1) { \n" + 
			"  name: \"TIME STAMPS | SYNCHRONIZATION WITH AUTHORITATIVE TIME SOURCE \"\n" + 
			"  specification: \"The information system: (a) Compares the internal information system clocks [Assignment: organization-defined frequency] with [Assignment: organization-defined authoritative time source]; and (b) Synchronizes the internal system clocks to the authoritative time source when the time difference is greater than [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-8(2) { \n" + 
			"  name: \"TIME STAMPS | SECONDARY AUTHORITATIVE TIME SOURCE \"\n" + 
			"  specification: \"The information system identifies a secondary authoritative time source that is located in a different geographic region than the primary authoritative time source.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9 { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION \"\n" + 
			"  specification: \"The information system protects audit information and audit tools from unauthorized access, modification, and deletion.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9(1) { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION | HARDWARE WRITE-ONCE MEDIA \"\n" + 
			"  specification: \"The information system writes audit trails to hardware-enforced, write-once media.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9(2) { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION | AUDIT BACKUP ON SEPARATE PHYSICAL SYSTEMS / COMPONENTS \"\n" + 
			"  specification: \"The information system backs up audit records [Assignment: organization-defined frequency] onto a physically different system or system component than the system or component being audited.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9(3) { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to protect the integrity of audit information and audit tools.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9(4) { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION | ACCESS BY SUBSET OF PRIVILEGED USERS \"\n" + 
			"  specification: \"The organization authorizes access to management of audit functionality to only [Assignment: organization-defined subset of privileged users].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9(5) { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION | DUAL AUTHORIZATION \"\n" + 
			"  specification: \"The organization enforces dual authorization for [Selection (one or more): movement; deletion] of [Assignment: organization-defined audit information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9(6) { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION | READ ONLY ACCESS \"\n" + 
			"  specification: \"The organization authorizes read-only access to audit information to [Assignment: organization-defined subset of privileged users].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-10 { \n" + 
			"  name: \"NON-REPUDIATION \"\n" + 
			"  specification: \"The information system protects against an individual (or process acting on behalf of an individual) falsely denying having performed [Assignment: organization-defined actions to be covered by non-repudiation].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-10(1) { \n" + 
			"  name: \"NON-REPUDIATION | ASSOCIATION OF IDENTITIES \"\n" + 
			"  specification: \"The information system: (a) Binds the identity of the information producer with the information to [Assignment: organization-defined strength of binding]; and (b) Provides the means for authorized individuals to determine the identity of the producer of the information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-10(2) { \n" + 
			"  name: \"NON-REPUDIATION | VALIDATE BINDING OF INFORMATION PRODUCER IDENTITY \"\n" + 
			"  specification: \"The information system: (a) Validates the binding of the information producer identity to the information at [Assignment: organization-defined frequency]; and (b) Performs [Assignment: organization-defined actions] in the event of a validation error.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-10(3) { \n" + 
			"  name: \"NON-REPUDIATION | CHAIN OF CUSTODY \"\n" + 
			"  specification: \"The information system maintains reviewer/releaser identity and credentials within the established chain of custody for all information reviewed or released.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-10(4) { \n" + 
			"  name: \"NON-REPUDIATION | VALIDATE BINDING OF INFORMATION REVIEWER IDENTITY \"\n" + 
			"  specification: \"The information system: (a) Validates the binding of the information reviewer identity to the information at the transfer or release points prior to release/transfer between [Assignment: organization-defined security domains]; and (b) Performs [Assignment: organization-defined actions] in the event of a validation error.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-10(5) { \n" + 
			"  name: \"NON-REPUDIATION | DIGITAL SIGNATURES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-11 { \n" + 
			"  name: \"AUDIT RECORD RETENTION \"\n" + 
			"  specification: \"The organization retains audit records for [Assignment: organization-defined time period consistent with records retention policy] to provide support for after-the-fact investigations of security incidents and to meet regulatory and organizational information retention requirements.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-11(1) { \n" + 
			"  name: \"AUDIT RECORD RETENTION | LONG-TERM RETRIEVAL CAPABILITY \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined measures] to ensure that long-term audit records generated by the information system can be retrieved.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-12 { \n" + 
			"  name: \"AUDIT GENERATION \"\n" + 
			"  specification: \"The information system: a. Provides audit record generation capability for the auditable events defined in AU-2 a. at [Assignment: organization-defined information system components];b. Allows [Assignment: organization-defined personnel or roles] to select which auditable events are to be audited by specific components of the information system; and c. Generates audit records for the events defined in AU-2 d. with the content defined in AU-3.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-12(1) { \n" + 
			"  name: \"AUDIT GENERATION | SYSTEM-WIDE / TIME-CORRELATED AUDIT TRAIL \"\n" + 
			"  specification: \"The information system compiles audit records from [Assignment: organization-defined information system components] into a system-wide (logical or physical) audit trail that is time-correlated to within [Assignment: organization-defined level of tolerance for the relationship between time stamps of individual records in the audit trail].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-12(2) { \n" + 
			"  name: \"AUDIT GENERATION | STANDARDIZED FORMATS \"\n" + 
			"  specification: \"The information system produces a system-wide (logical or physical) audit trail composed of audit records in a standardized format.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-12(3) { \n" + 
			"  name: \"AUDIT GENERATION | CHANGES BY AUTHORIZED INDIVIDUALS \"\n" + 
			"  specification: \"The information system provides the capability for [Assignment: organization-defined individuals or roles] to change the auditing to be performed on [Assignment: organization-defined information system components] based on [Assignment: organization-defined selectable event criteria] within [Assignment: organization-defined time thresholds].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-13 { \n" + 
			"  name: \"MONITORING FOR INFORMATION DISCLOSURE \"\n" + 
			"  specification: \"The organization monitors [Assignment: organization-defined open source information and/or information sites] [Assignment: organization-defined frequency] for evidence of unauthorized disclosure of organizational information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-13(1) { \n" + 
			"  name: \"MONITORING FOR INFORMATION DISCLOSURE | USE OF AUTOMATED TOOLS \"\n" + 
			"  specification: \"The organization employs automated mechanisms to determine if organizational information has been disclosed in an unauthorized manner.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-13(2) { \n" + 
			"  name: \"MONITORING FOR INFORMATION DISCLOSURE | REVIEW OF MONITORED SITES \"\n" + 
			"  specification: \"The organization reviews the open source information sites being monitored [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-14 { \n" + 
			"  name: \"SESSION AUDIT \"\n" + 
			"  specification: \"The information system provides the capability for authorized users to select a user session to capture/record or view/hear.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-14(1) { \n" + 
			"  name: \"SESSION AUDIT | SYSTEM START-UP \"\n" + 
			"  specification: \"The information system initiates session audits at system start-up.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-14(2) { \n" + 
			"  name: \"SESSION AUDIT | CAPTURE/RECORD AND LOG CONTENT \"\n" + 
			"  specification: \"The information system provides the capability for authorized users to capture/record and log content related to a user session.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-14(3) { \n" + 
			"  name: \"SESSION AUDIT | REMOTE VIEWING / LISTENING \"\n" + 
			"  specification: \"The information system provides the capability for authorized users to remotely view/hear all content related to an established user session in real time.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-15 { \n" + 
			"  name: \"ALTERNATE AUDIT CAPABILITY \"\n" + 
			"  specification: \"The organization provides an alternate audit capability in the event of a failure in primary audit capability that provides [Assignment: organization-defined alternate audit functionality].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-16 { \n" + 
			"  name: \"CROSS-ORGANIZATIONAL AUDITING \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined methods] for coordinating [Assignment: organization-defined audit information] among external organizations when audit information is transmitted across organizational boundaries.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-16(1) { \n" + 
			"  name: \"CROSS-ORGANIZATIONAL AUDITING | IDENTITY PRESERVATION \"\n" + 
			"  specification: \"The organization requires that the identity of individuals be preserved in cross-organizational audit trails.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-16(2) { \n" + 
			"  name: \"CROSS-ORGANIZATIONAL AUDITING | SHARING OF AUDIT INFORMATION \"\n" + 
			"  specification: \"The organization provides cross-organizational audit information to [Assignment: organization-defined organizations] based on [Assignment: organization-defined cross-organizational sharing agreements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control CA-1 { \n" + 
			"  name: \"SECURITY ASSESSMENT AND AUTHORIZATION POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A security assessment and authorization policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the security assessment and authorization policy and associated security assessment and authorization controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Security assessment and authorization policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Security assessment and authorization procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-2 { \n" + 
			"  name: \"SECURITY ASSESSMENTS \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops a security assessment plan that describes the scope of the assessment including:\n" + 
			"				1. Security controls and control enhancements under assessment;\n" + 
			"				2. Assessment procedures to be used to determine security control effectiveness; and\n" + 
			"				3. Assessment environment, assessment team, and assessment roles and responsibilities;\n" + 
			"			b. Assesses the security controls in the information system and its environment of operation [Assignment: organization-defined frequency] to determine the extent to which the controls are implemented correctly, operating as intended, and producing the desired outcome with respect to meeting established security requirements;\n" + 
			"			c. Produces a security assessment report that documents the results of the assessment; and\n" + 
			"			d. Provides the results of the security control assessment to [Assignment: organization-defined individuals or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-2(1) { \n" + 
			"  name: \"SECURITY ASSESSMENTS | INDEPENDENT ASSESSORS \"\n" + 
			"  specification: \"The organization employs assessors or assessment teams with [Assignment: organization-defined level of independence] to conduct security control assessments.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-2(2) { \n" + 
			"  name: \"SECURITY ASSESSMENTS | SPECIALIZED ASSESSMENTS \"\n" + 
			"  specification: \"The organization includes as part of security control assessments, [Assignment: organization-defined frequency], [Selection: announced; unannounced], [Selection (one or more): in-depth monitoring; vulnerability scanning; malicious user testing; insider threat assessment; performance/load testing; [Assignment: organization-defined other forms of security assessment]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-2(3) { \n" + 
			"  name: \"SECURITY ASSESSMENTS | EXTERNAL ORGANIZATIONS \"\n" + 
			"  specification: \"The organization accepts the results of an assessment of [Assignment: organization-defined information system] performed by [Assignment: organization-defined external organization] when the assessment meets [Assignment: organization-defined requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-3 { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS \"\n" + 
			"  specification: \"The organization: a. Authorizes connections from the information system to other information systems through the use of Interconnection Security Agreements; b. Documents, for each interconnection, the interface characteristics, security requirements, and the nature of the information communicated; and c. Reviews and updates Interconnection Security Agreements [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-3(1) { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS | UNCLASSIFIED NATIONAL SECURITY SYSTEM CONNECTIONS \"\n" + 
			"  specification: \"The organization prohibits the direct connection of an [Assignment: organization-defined unclassified, national security system] to an external network without the use of [Assignment: organization-defined boundary protection device].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-3(2) { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS | CLASSIFIED NATIONAL SECURITY SYSTEM CONNECTIONS \"\n" + 
			"  specification: \"The organization prohibits the direct connection of a classified, national security system to an external network without the use of [Assignment: organization-defined boundary protection device].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-3(3) { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS | UNCLASSIFIED NON-NATIONAL SECURITY SYSTEM CONNECTIONS \"\n" + 
			"  specification: \"The organization prohibits the direct connection of an [Assignment: organization-defined unclassified, non-national security system] to an external network without the use of [Assignment; organization-defined boundary protection device].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-3(4) { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS | CONNECTIONS TO PUBLIC NETWORKS \"\n" + 
			"  specification: \"The organization prohibits the direct connection of an [Assignment: organization-defined information system] to a public network.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-3(5) { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS | RESTRICTIONS ON EXTERNAL SYSTEM CONNECTIONS \"\n" + 
			"  specification: \"The organization employs [Selection: allow-all, deny-by-exception; deny-all, permit-by-exception] policy for allowing [Assignment: organization-defined information systems] to connect to external information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-4 { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CA-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-5 { \n" + 
			"  name: \"PLAN OF ACTION AND MILESTONES \"\n" + 
			"  specification: \"The organization: a. Develops a plan of action and milestones for the information system to document the organization’s planned remedial actions to correct weaknesses or deficiencies noted during the assessment of the security controls and to reduce or eliminate known vulnerabilities in the system; and b. Updates existing plan of action and milestones [Assignment: organization-defined frequency] based on the findings from security controls assessments, security impact analyses, and continuous monitoring activities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-5(1) { \n" + 
			"  name: \"PLAN OF ACTION AND MILESTONES | AUTOMATION SUPPORT FOR ACCURACY / CURRENCY \"\n" + 
			"  specification: \"The organization employs automated mechanisms to help ensure that the plan of action and milestones for the information system is accurate, up to date, and readily available.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-6 { \n" + 
			"  name: \"SECURITY AUTHORIZATION \"\n" + 
			"  specification: \" The organization: a. Assigns a senior-level executive or manager as the authorizing official for the information system; b. Ensures that the authorizing official authorizes the information system for processing before commencing operations; and c. Updates the security authorization [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-7 { \n" + 
			"  name: \"CONTINUOUS MONITORING \"\n" + 
			"  specification: \"The organization develops a continuous monitoring strategy and implements a continuous monitoring program that includes:\n" + 
			"			a. Establishment of [Assignment: organization-defined metrics] to be monitored;\n" + 
			"			b. Establishment of [Assignment: organization-defined frequencies] for monitoring and [Assignment: organization-defined frequencies] for assessments supporting such monitoring;\n" + 
			"			c. Ongoing security control assessments in accordance with the organizational continuous monitoring strategy;\n" + 
			"			d. Ongoing security status monitoring of organization-defined metrics in accordance with the organizational continuous monitoring strategy;\n" + 
			"			e. Correlation and analysis of security-related information generated by assessments and monitoring;\n" + 
			"			f. Response actions to address results of the analysis of security-related information; and\n" + 
			"			g. Reporting the security status of organization and the information system to [Assignment: organization-defined personnel or roles] [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-7(1) { \n" + 
			"  name: \"CONTINUOUS MONITORING | INDEPENDENT ASSESSMENT \"\n" + 
			"  specification: \"The organization employs assessors or assessment teams with [Assignment: organization-defined level of independence] to monitor the security controls in the information system on an ongoing basis.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-7(2) { \n" + 
			"  name: \"CONTINUOUS MONITORING | TYPES OF ASSESSMENTS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CA-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-7(3) { \n" + 
			"  name: \"CONTINUOUS MONITORING | TREND ANALYSES \"\n" + 
			"  specification: \"The organization employs trend analyses to determine if security control implementations, the frequency of continuous monitoring activities, and/or the types of activities used in the continuous monitoring process need to be modified based on empirical data.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-8 { \n" + 
			"  name: \"PENETRATION TESTING \"\n" + 
			"  specification: \" \n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-8(1) { \n" + 
			"  name: \"PENETRATION TESTING | INDEPENDENT PENETRATION AGENT OR TEAM \"\n" + 
			"  specification: \"The organization employs an independent penetration agent or penetration team to perform penetration testing on the information system or system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-8(2) { \n" + 
			"  name: \"PENETRATION TESTING | RED TEAM EXERCISES \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined red team exercises] to simulate attempts by adversaries to compromise organizational information systems in accordance with [Assignment: organization-defined rules of engagement].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-9 { \n" + 
			"  name: \"INTERNAL SYSTEM CONNECTIONS \"\n" + 
			"  specification: \"The organization: a. Authorizes internal connections of [Assignment: organization-defined information system components or classes of components] to the information system; and b. Documents, for each internal connection, the interface characteristics, security requirements, and the nature of the information communicated.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-9(1) { \n" + 
			"  name: \"INTERNAL SYSTEM CONNECTIONS | SECURITY COMPLIANCE CHECKS \"\n" + 
			"  specification: \"The information system performs security compliance checks on constituent system components prior to the establishment of the internal connection.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CM-1 { \n" + 
			"  name: \"CONFIGURATION MANAGEMENT POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A configuration management policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the configuration management policy and associated configuration management controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Configuration management policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Configuration management procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2 { \n" + 
			"  name: \"BASELINE CONFIGURATION \"\n" + 
			"  specification: \"The organization develops, documents, and maintains under configuration control, a current baseline configuration of the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(1) { \n" + 
			"  name: \"BASELINE CONFIGURATION | REVIEWS AND UPDATES \"\n" + 
			"  specification: \"The organization reviews and updates the baseline configuration of the information system: (a) [Assignment: organization-defined frequency]; (b) When required due to [Assignment organization-defined circumstances]; and (c) As an integral part of information system component installations and upgrades.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(2) { \n" + 
			"  name: \"BASELINE CONFIGURATION | AUTOMATION SUPPORT FOR ACCURACY / CURRENCY \"\n" + 
			"  specification: \"The organization employs automated mechanisms to maintain an up-to-date, complete, accurate, and readily available baseline configuration of the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(3) { \n" + 
			"  name: \"BASELINE CONFIGURATION | RETENTION OF PREVIOUS CONFIGURATIONS \"\n" + 
			"  specification: \"The organization retains [Assignment: organization-defined previous versions of baseline configurations of the information system] to support rollback.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(4) { \n" + 
			"  name: \"BASELINE CONFIGURATION | UNAUTHORIZED SOFTWARE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(5) { \n" + 
			"  name: \"BASELINE CONFIGURATION | AUTHORIZED SOFTWARE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(6) { \n" + 
			"  name: \"BASELINE CONFIGURATION | DEVELOPMENT AND TEST ENVIRONMENTS \"\n" + 
			"  specification: \"The organization maintains a baseline configuration for information system development and test environments that is managed separately from the operational baseline configuration.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(7) { \n" + 
			"  name: \"BASELINE CONFIGURATION | CONFIGURE SYSTEMS, COMPONENTS, OR DEVICES FOR HIGH-RISK AREAS \"\n" + 
			"  specification: \"The organization: (a) Issues [Assignment: organization-defined information systems, system components, or devices] with [Assignment: organization-defined configurations] to individuals traveling to locations that the organization deems to be of significant risk; and (b) Applies [Assignment: organization-defined security safeguards] to the devices when the individuals return.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3 { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Determines the types of changes to the information system that are configuration-controlled;\n" + 
			"			b. Reviews proposed configuration-controlled changes to the information system and approves or disapproves such changes with explicit consideration for security impact analyses;\n" + 
			"			c. Documents configuration change decisions associated with the information system;\n" + 
			"			d. Implements approved configuration-controlled changes to the information system;\n" + 
			"			e. Retains records of configuration-controlled changes to the information system for [Assignment: organization-defined time period];\n" + 
			"			f. Audits and reviews activities associated with configuration-controlled changes to the information system; and\n" + 
			"			g. Coordinates and provides oversight for configuration change control activities through [Assignment: organization-defined configuration change control element (e.g., committee, board)] that convenes [Selection (one or more): [Assignment: organization-defined frequency]; [Assignment: organization-defined configuration change conditions]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3(1) { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL | AUTOMATED DOCUMENT / NOTIFICATION / PROHIBITION OF CHANGES \"\n" + 
			"  specification: \"The organization employs automated mechanisms to: (a) Document proposed changes to the information system; (b) Notify [Assignment: organized-defined approval authorities] of proposed changes to the information system and request change approval; (c) Highlight proposed changes to the information system that have not been approved or disapproved by [Assignment: organization-defined time period]; (d) Prohibit changes to the information system until designated approvals are received; (e) Document all changes to the information system; and (f) Notify [Assignment: organization-defined personnel] when approved changes to the information system are completed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3(2) { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL | TEST / VALIDATE / DOCUMENT CHANGES \"\n" + 
			"  specification: \"The organization tests, validates, and documents changes to the information system before implementing the changes on the operational system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3(3) { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL | AUTOMATED CHANGE IMPLEMENTATION \"\n" + 
			"  specification: \"The organization employs automated mechanisms to implement changes to the current information system baseline and deploys the updated baseline across the installed base.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3(4) { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL | SECURITY REPRESENTATIVE \"\n" + 
			"  specification: \"The organization requires an information security representative to be a member of the [Assignment: organization-defined configuration change control element].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3(5) { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL | AUTOMATED SECURITY RESPONSE \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined security responses] automatically if baseline configurations are changed in an unauthorized manner.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3(6) { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL | CRYPTOGRAPHY MANAGEMENT \"\n" + 
			"  specification: \"The organization ensures that cryptographic mechanisms used to provide [Assignment: organization-defined security safeguards] are under configuration management.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-4 { \n" + 
			"  name: \"SECURITY IMPACT ANALYSIS \"\n" + 
			"  specification: \"The organization analyzes changes to the information system to determine potential security impacts prior to change implementation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-4(1) { \n" + 
			"  name: \"SECURITY IMPACT ANALYSIS | SEPARATE TEST ENVIRONMENTS \"\n" + 
			"  specification: \"The organization analyzes changes to the information system in a separate test environment before implementation in an operational environment, looking for security impacts due to flaws, weaknesses, incompatibility, or intentional malice.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-4(2) { \n" + 
			"  name: \"SECURITY IMPACT ANALYSIS | VERIFICATION OF SECURITY FUNCTIONS \"\n" + 
			"  specification: \"The organization, after the information system is changed, checks the security functions to verify that the functions are implemented correctly, operating as intended, and producing the desired outcome with regard to meeting the security requirements for the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5 { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE \"\n" + 
			"  specification: \"The organization defines, documents, approves, and enforces physical and logical access restrictions associated with changes to the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(1) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | AUTOMATED ACCESS ENFORCEMENT / AUDITING \"\n" + 
			"  specification: \"The information system enforces access restrictions and supports auditing of the enforcement actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(2) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | REVIEW SYSTEM CHANGES \"\n" + 
			"  specification: \"The organization reviews information system changes [Assignment: organization-defined frequency] and [Assignment: organization-defined circumstances] to determine whether unauthorized changes have occurred.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(3) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | SIGNED COMPONENTS \"\n" + 
			"  specification: \"The information system prevents the installation of [Assignment: organization-defined software and firmware components] without verification that the component has been digitally signed using a certificate that is recognized and approved by the organization.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(4) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | DUAL AUTHORIZATION \"\n" + 
			"  specification: \"The organization enforces dual authorization for implementing changes to [Assignment: organization-defined information system components and system-level information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(5) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | LIMIT PRODUCTION / OPERATIONAL PRIVILEGES \"\n" + 
			"  specification: \"The organization: (a) Limits privileges to change information system components and system-related information within a production or operational environment; and (b) Reviews and reevaluates privileges [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(6) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | LIMIT LIBRARY PRIVILEGES \"\n" + 
			"  specification: \"The organization limits privileges to change software resident within software libraries.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(7) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | AUTOMATIC IMPLEMENTATION OF SECURITY SAFEGUARDS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-6 { \n" + 
			"  name: \"CONFIGURATION SETTINGS \"\n" + 
			"  specification: \"The organization: a. Establishes and documents configuration settings for information technology products employed within the information system using [Assignment: organization-defined security configuration checklists] that reflect the most restrictive mode consistent with operational requirements; b. Implements the configuration settings; c. Identifies, documents, and approves any deviations from established configuration settings for [Assignment: organization-defined information system components] based on [Assignment: organization-defined operational requirements]; and d. Monitors and controls changes to the configuration settings in accordance with organizational policies and procedures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-6(1) { \n" + 
			"  name: \"CONFIGURATION SETTINGS | AUTOMATED CENTRAL MANAGEMENT / APPLICATION / VERIFICATION \"\n" + 
			"  specification: \"The organization employs automated mechanisms to centrally manage, apply, and verify configuration settings for [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-6(2) { \n" + 
			"  name: \"CONFIGURATION SETTINGS | RESPOND TO UNAUTHORIZED CHANGES \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to respond to unauthorized changes to [Assignment: organization-defined configuration settings].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-6(3) { \n" + 
			"  name: \"CONFIGURATION SETTINGS | UNAUTHORIZED CHANGE DETECTION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-6(4) { \n" + 
			"  name: \"CONFIGURATION SETTINGS | CONFORMANCE DEMONSTRATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-7 { \n" + 
			"  name: \"LEAST FUNCTIONALITY \"\n" + 
			"  specification: \"The organization: a. Configures the information system to provide only essential capabilities; and b. Prohibits or restricts the use of the following functions, ports, protocols, and/or services: [Assignment: organization-defined prohibited or restricted functions, ports, protocols, and/or services].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-7(1) { \n" + 
			"  name: \"LEAST FUNCTIONALITY | PERIODIC REVIEW \"\n" + 
			"  specification: \"The organization: (a) Reviews the information system [Assignment: organization-defined frequency] to identify unnecessary and/or nonsecure functions, ports, protocols, and services; and (b) Disables [Assignment: organization-defined functions, ports, protocols, and services within the information system deemed to be unnecessary and/or nonsecure].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-7(2) { \n" + 
			"  name: \"LEAST FUNCTIONALITY | PREVENT PROGRAM EXECUTION \"\n" + 
			"  specification: \"The information system prevents program execution in accordance with [Selection (one or more): [Assignment: organization-defined policies regarding software program usage and restrictions]; rules authorizing the terms and conditions of software program usage].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-7(3) { \n" + 
			"  name: \"LEAST FUNCTIONALITY | REGISTRATION COMPLIANCE \"\n" + 
			"  specification: \"The organization ensures compliance with [Assignment: organization-defined registration requirements for functions, ports, protocols, and services].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-7(4) { \n" + 
			"  name: \"LEAST FUNCTIONALITY | UNAUTHORIZED SOFTWARE / BLACKLISTING \"\n" + 
			"  specification: \"The organization: (a) Identifies [Assignment: organization-defined software programs not authorized to execute on the information system]; (b) Employs an allow-all, deny-by-exception policy to prohibit the execution of unauthorized software programs on the information system; and (c) Reviews and updates the list of unauthorized software programs [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-7(5) { \n" + 
			"  name: \"LEAST FUNCTIONALITY | AUTHORIZED SOFTWARE / WHITELISTING \"\n" + 
			"  specification: \"The organization: (a) Identifies [Assignment: organization-defined software programs authorized to execute on the information system]; (b) Employs a deny-all, permit-by-exception policy to allow the execution of authorized software programs on the information system; and (c) Reviews and updates the list of authorized software programs [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8 { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops and documents an inventory of information system components that:\n" + 
			"				1. Accurately reflects the current information system;\n" + 
			"				2. Includes all components within the authorization boundary of the information system;\n" + 
			"				3. Is at the level of granularity deemed necessary for tracking and reporting; and\n" + 
			"				4. Includes [Assignment: organization-defined information deemed necessary to achieve effective information system component accountability]; and\n" + 
			"			b. Reviews and updates the information system component inventory [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(1) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | UPDATES DURING INSTALLATIONS / REMOVALS \"\n" + 
			"  specification: \"The organization updates the inventory of information system components as an integral part of component installations, removals, and information system updates.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(2) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | AUTOMATED MAINTENANCE \"\n" + 
			"  specification: \"The organization employs automated mechanisms to help maintain an up-to-date, complete, accurate, and readily available inventory of information system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(3) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | AUTOMATED UNAUTHORIZED COMPONENT DETECTION \"\n" + 
			"  specification: \"The organization:(a) Employs automated mechanisms [Assignment: organization-defined frequency] to detect the presence of unauthorized hardware, software, and firmware components within the information system; and (b) Takes the following actions when unauthorized components are detected: [Selection (one or more): disables network access by such components; isolates the components; notifies [Assignment: organization-defined personnel or roles]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(4) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | ACCOUNTABILITY INFORMATION \"\n" + 
			"  specification: \"The organization includes in the information system component inventory information, a means for identifying by [Selection (one or more): name; position; role], individuals responsible/accountable for administering those components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(5) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | NO DUPLICATE ACCOUNTING OF COMPONENTS \"\n" + 
			"  specification: \"The organization verifies that all components within the authorization boundary of the information system are not duplicated in other information system component inventories.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(6) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | ASSESSED CONFIGURATIONS / APPROVED DEVIATIONS \"\n" + 
			"  specification: \"The organization includes assessed component configurations and any approved deviations to current deployed configurations in the information system component inventory.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(7) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | CENTRALIZED REPOSITORY \"\n" + 
			"  specification: \"The organization provides a centralized repository for the inventory of information system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(8) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | AUTOMATED LOCATION TRACKING \"\n" + 
			"  specification: \"The organization employs automated mechanisms to support tracking of information system components by geographic location.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(9) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | ASSIGNMENT OF COMPONENTS TO SYSTEMS \"\n" + 
			"  specification: \"The organization: (a) Assigns [Assignment: organization-defined acquired information system components] to an information system; and (b) Receives an acknowledgement from the information system owner of this assignment.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-9 { \n" + 
			"  name: \"CONFIGURATION MANAGEMENT PLAN \"\n" + 
			"  specification: \"The organization develops, documents, and implements a configuration management plan for the information system that:\n" + 
			"			a. Addresses roles, responsibilities, and configuration management processes and procedures;\n" + 
			"			b. Establishes a process for identifying configuration items throughout the system development life cycle and for managing the configuration of the configuration items;\n" + 
			"			c. Defines the configuration items for the information system and places the configuration items under configuration management; and\n" + 
			"			d. Protects the configuration management plan from unauthorized disclosure and modification.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-9(1) { \n" + 
			"  name: \"CONFIGURATION MANAGEMENT PLAN | ASSIGNMENT OF RESPONSIBILITY \"\n" + 
			"  specification: \"The organization assigns responsibility for developing the configuration management process to organizational personnel that are not directly involved in information system development.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-10 { \n" + 
			"  name: \"SOFTWARE USAGE RESTRICTIONS \"\n" + 
			"  specification: \"The organization: a. Uses software and associated documentation in accordance with contract agreements and copyright laws; b. Tracks the use of software and associated documentation protected by quantity licenses to control copying and distribution; and c. Controls and documents the use of peer-to-peer file sharing technology to ensure that this capability is not used for the unauthorized distribution, display, performance, or reproduction of copyrighted work.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-10(1) { \n" + 
			"  name: \"SOFTWARE USAGE RESTRICTIONS | OPEN SOURCE SOFTWARE \"\n" + 
			"  specification: \"The organization establishes the following restrictions on the use of open source software: [Assignment: organization-defined restrictions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-11 { \n" + 
			"  name: \"USER-INSTALLED SOFTWARE \"\n" + 
			"  specification: \"The organization:a. Establishes [Assignment: organization-defined policies] governing the installation of software by users; b. Enforces software installation policies through [Assignment: organization-defined methods]; andc. Monitors policy compliance at [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-11(1) { \n" + 
			"  name: \"USER-INSTALLED SOFTWARE | ALERTS FOR UNAUTHORIZED INSTALLATIONS \"\n" + 
			"  specification: \"The information system alerts [Assignment: organization-defined personnel or roles] when the unauthorized installation of software is detected.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-11(2) { \n" + 
			"  name: \"USER-INSTALLED SOFTWARE | PROHIBIT INSTALLATION WITHOUT PRIVILEGED STATUS \"\n" + 
			"  specification: \"The information system prohibits user installation of software without explicit privileged status.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CP-1 { \n" + 
			"  name: \"CONTINGENCY PLANNING POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A contingency planning policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the contingency planning policy and associated contingency planning controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Contingency planning policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Contingency planning procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2 { \n" + 
			"  name: \"CONTINGENCY PLAN \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops a contingency plan for the information system that:\n" + 
			"				1. Identifies essential missions and business functions and associated contingency requirements;\n" + 
			"				2. Provides recovery objectives, restoration priorities, and metrics;\n" + 
			"				3. Addresses contingency roles, responsibilities, assigned individuals with contact information;\n" + 
			"				4. Addresses maintaining essential missions and business functions despite an information system disruption, compromise, or failure;\n" + 
			"				5. Addresses eventual, full information system restoration without deterioration of the security safeguards originally planned and implemented; and\n" + 
			"				6. Is reviewed and approved by [Assignment: organization-defined personnel or roles];\n" + 
			"			b. Distributes copies of the contingency plan to [Assignment: organization-defined key contingency personnel (identified by name and/or by role) and organizational elements];\n" + 
			"			c. Coordinates contingency planning activities with incident handling activities;\n" + 
			"			d. Reviews the contingency plan for the information system [Assignment: organization-defined frequency];\n" + 
			"			e. Updates the contingency plan to address changes to the organization, information system, or environment of operation and problems encountered during contingency plan implementation, execution, or testing;\n" + 
			"			f. Communicates contingency plan changes to [Assignment: organization-defined key contingency personnel (identified by name and/or by role) and organizational elements]; and\n" + 
			"			g. Protects the contingency plan from unauthorized disclosure and modification.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(1) { \n" + 
			"  name: \"CONTINGENCY PLAN | COORDINATE WITH RELATED PLANS \"\n" + 
			"  specification: \"The organization coordinates contingency plan development with organizational elements responsible for related plans.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(2) { \n" + 
			"  name: \"CONTINGENCY PLAN | CAPACITY PLANNING \"\n" + 
			"  specification: \"The organization conducts capacity planning so that necessary capacity for information processing, telecommunications, and environmental support exists during contingency operations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(3) { \n" + 
			"  name: \"CONTINGENCY PLAN | RESUME ESSENTIAL MISSIONS / BUSINESS FUNCTIONS \"\n" + 
			"  specification: \"The organization plans for the resumption of essential missions and business functions within [Assignment: organization-defined time period] of contingency plan activation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(4) { \n" + 
			"  name: \"CONTINGENCY PLAN | RESUME ALL MISSIONS / BUSINESS FUNCTIONS \"\n" + 
			"  specification: \"The organization plans for the resumption of all missions and business functions within [Assignment: organization-defined time period] of contingency plan activation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(5) { \n" + 
			"  name: \"CONTINGENCY PLAN | CONTINUE ESSENTIAL MISSIONS / BUSINESS FUNCTIONS \"\n" + 
			"  specification: \"The organization plans for the continuance of essential missions and business functions with little or no loss of operational continuity and sustains that continuity until full information system restoration at primary processing and/or storage sites.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(6) { \n" + 
			"  name: \"CONTINGENCY PLAN | ALTERNATE PROCESSING / STORAGE SITE \"\n" + 
			"  specification: \"The organization plans for the transfer of essential missions and business functions to alternate processing and/or storage sites with little or no loss of operational continuity and sustains that continuity through information system restoration to primary processing and/or storage sites.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(7) { \n" + 
			"  name: \"CONTINGENCY PLAN | COORDINATE WITH EXTERNAL SERVICE PROVIDERS \"\n" + 
			"  specification: \"The organization coordinates its contingency plan with the contingency plans of external service providers to ensure that contingency requirements can be satisfied.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(8) { \n" + 
			"  name: \"CONTINGENCY PLAN | IDENTIFY CRITICAL ASSETS \"\n" + 
			"  specification: \"The organization identifies critical information system assets supporting essential missions and business functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-3 { \n" + 
			"  name: \"CONTINGENCY TRAINING \"\n" + 
			"  specification: \"The organization provides contingency training to information system users consistent with assigned roles and responsibilities: a. Within [Assignment: organization-defined time period] of assuming a contingency role or responsibility; b. When required by information system changes; and c. [Assignment: organization-defined frequency] thereafter.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-3(1) { \n" + 
			"  name: \"CONTINGENCY TRAINING | SIMULATED EVENTS \"\n" + 
			"  specification: \"The organization incorporates simulated events into contingency training to facilitate effective response by personnel in crisis situations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-3(2) { \n" + 
			"  name: \"CONTINGENCY TRAINING | AUTOMATED TRAINING ENVIRONMENTS \"\n" + 
			"  specification: \"The organization employs automated mechanisms to provide a more thorough and realistic contingency training environment.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-4 { \n" + 
			"  name: \"CONTINGENCY PLAN TESTING \"\n" + 
			"  specification: \"The organization: a. Tests the contingency plan for the information system [Assignment: organization-defined frequency] using [Assignment: organization-defined tests] to determine the effectiveness of the plan and the organizational readiness to execute the plan; b. Reviews the contingency plan test results; and c. Initiates corrective actions, if needed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-4(1) { \n" + 
			"  name: \"CONTINGENCY PLAN TESTING | COORDINATE WITH RELATED PLANS \"\n" + 
			"  specification: \"The organization coordinates contingency plan testing with organizational elements responsible for related plans.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-4(2) { \n" + 
			"  name: \"CONTINGENCY PLAN TESTING | ALTERNATE PROCESSING SITE \"\n" + 
			"  specification: \"The organization tests the contingency plan at the alternate processing site: (a) To familiarize contingency personnel with the facility and available resources; and (b) To evaluate the capabilities of the alternate processing site to support contingency operations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-4(3) { \n" + 
			"  name: \"CONTINGENCY PLAN TESTING | AUTOMATED TESTING \"\n" + 
			"  specification: \"The organization employs automated mechanisms to more thoroughly and effectively test the contingency plan.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-4(4) { \n" + 
			"  name: \"CONTINGENCY PLAN TESTING | FULL RECOVERY / RECONSTITUTION \"\n" + 
			"  specification: \"The organization includes a full recovery and reconstitution of the information system to a known state as part of contingency plan testing.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-5 { \n" + 
			"  name: \" \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CP-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-6 { \n" + 
			"  name: \"ALTERNATE STORAGE SITE \"\n" + 
			"  specification: \"The organization: a. Establishes an alternate storage site including necessary agreements to permit the storage and retrieval of information system backup information; and b. Ensures that the alternate storage site provides information security safeguards equivalent to that of the primary site.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-6(1) { \n" + 
			"  name: \"ALTERNATE STORAGE SITE | SEPARATION FROM PRIMARY SITE \"\n" + 
			"  specification: \"The organization identifies an alternate storage site that is separated from the primary storage site to reduce susceptibility to the same threats.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-6(2) { \n" + 
			"  name: \"ALTERNATE STORAGE SITE | RECOVERY TIME / POINT OBJECTIVES \"\n" + 
			"  specification: \"The organization configures the alternate storage site to facilitate recovery operations in accordance with recovery time and recovery point objectives.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-6(3) { \n" + 
			"  name: \"ALTERNATE STORAGE SITE | ACCESSIBILITY \"\n" + 
			"  specification: \"The organization identifies potential accessibility problems to the alternate storage site in the event of an area-wide disruption or disaster and outlines explicit mitigation actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7 { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE \"\n" + 
			"  specification: \"The organization: a. Establishes an alternate processing site including necessary agreements to permit the transfer and resumption of [Assignment: organization-defined information system operations] for essential missions/business functions within [Assignment: organization-defined time periodconsistent with recovery time and recovery point objectives] when the primary processing capabilities are unavailable; b. Ensures that equipment and supplies required to transfer and resume operations are available at the alternate processing site or contracts are in place to support delivery to the site within the organization-defined time period for transfer/resumption; and c. Ensures that the alternate processing site provides information security safeguards equivalent to those of the primary site.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7(1) { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE | SEPARATION FROM PRIMARY SITE \"\n" + 
			"  specification: \"The organization identifies an alternate processing site that is separated from the primary processing site to reduce susceptibility to the same threats.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7(2) { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE | ACCESSIBILITY \"\n" + 
			"  specification: \"The organization identifies potential accessibility problems to the alternate processing site in the event of an area-wide disruption or disaster and outlines explicit mitigation actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7(3) { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE | PRIORITY OF SERVICE \"\n" + 
			"  specification: \"The organization develops alternate processing site agreements that contain priority-of-service provisions in accordance with organizational availability requirements (including recovery time objectives).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7(4) { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE | PREPARATION FOR USE \"\n" + 
			"  specification: \"The organization prepares the alternate processing site so that the site is ready to be used as the operational site supporting essential missions and business functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7(5) { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE | EQUIVALENT INFORMATION SECURITY SAFEGUARDS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CP-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7(6) { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE | INABILITY TO RETURN TO PRIMARY SITE \"\n" + 
			"  specification: \"The organization plans and prepares for circumstances that preclude returning to the primary processing site.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-8 { \n" + 
			"  name: \"TELECOMMUNICATIONS SERVICES \"\n" + 
			"  specification: \"The organization establishes alternate telecommunications services including necessary agreements to permit the resumption of [Assignment: organization-defined information system operations] for essential missions and business functions within [Assignment: organization-defined time period] when the primary telecommunications capabilities are unavailable at either the primary or alternate processing or storage sites.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-8(1) { \n" + 
			"  name: \"TELECOMMUNICATIONS SERVICES | PRIORITY OF SERVICE PROVISIONS \"\n" + 
			"  specification: \"The organization:(a) Develops primary and alternate telecommunications service agreements that contain priority-of-service provisions in accordance with organizational availability requirements (including recovery time objectives); and (b) Requests Telecommunications Service Priority for all telecommunications services used for national security emergency preparedness in the event that the primary and/or alternate telecommunications services are provided by a common carrier.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-8(2) { \n" + 
			"  name: \"TELECOMMUNICATIONS SERVICES | SINGLE POINTS OF FAILURE \"\n" + 
			"  specification: \"The organization obtains alternate telecommunications services to reduce the likelihood of sharing a single point of failure with primary telecommunications services.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-8(3) { \n" + 
			"  name: \"TELECOMMUNICATIONS SERVICES | SEPARATION OF PRIMARY / ALTERNATE PROVIDERS \"\n" + 
			"  specification: \"The organization obtains alternate telecommunications services from providers that are separated from primary service providers to reduce susceptibility to the same threats.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-8(4) { \n" + 
			"  name: \"TELECOMMUNICATIONS SERVICES | PROVIDER CONTINGENCY PLAN \"\n" + 
			"  specification: \"The organization: (a) Requires primary and alternate telecommunications service providers to have contingency plans; (b) Reviews provider contingency plans to ensure that the plans meet organizational contingency requirements; and (c) Obtains evidence of contingency testing/training by providers [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-8(5) { \n" + 
			"  name: \"TELECOMMUNICATIONS SERVICES | ALTERNATE TELECOMMUNICATION SERVICE TESTING \"\n" + 
			"  specification: \"The organization tests alternate telecommunication services [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9 { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP \"\n" + 
			"  specification: \"The organization: a. Conducts backups of user-level information contained in the information system [Assignment: organization-defined frequency consistent with recovery time and recovery point objectives]; b. Conducts backups of system-level information contained in the information system [Assignment: organization-defined frequency consistent with recovery time and recovery point objectives]; c. Conducts backups of information system documentation including security-related documentation [Assignment: organization-defined frequency consistent with recovery time and recovery point objectives]; and d. Protects the confidentiality, integrity, and availability of backup information at storage locations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(1) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | TESTING FOR RELIABILITY / INTEGRITY \"\n" + 
			"  specification: \"The organization tests backup information [Assignment: organization-defined frequency] to verify media reliability and information integrity.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(2) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | TEST RESTORATION USING SAMPLING \"\n" + 
			"  specification: \"The organization uses a sample of backup information in the restoration of selected information system functions as part of contingency plan testing.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(3) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | SEPARATE STORAGE FOR CRITICAL INFORMATION \"\n" + 
			"  specification: \"The organization stores backup copies of [Assignment: organization-defined critical information system software and other security-related information] in a separate facility or in a fire-rated container that is not collocated with the operational system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(4) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | PROTECTION FROM UNAUTHORIZED MODIFICATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CP-9.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(5) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | TRANSFER TO ALTERNATE STORAGE SITE \"\n" + 
			"  specification: \"The organization transfers information system backup information to the alternate storage site [Assignment: organization-defined time period and transfer rate consistent with the recovery time and recovery point objectives].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(6) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | REDUNDANT SECONDARY SYSTEM \"\n" + 
			"  specification: \"The organization accomplishes information system backup by maintaining a redundant secondary system that is not collocated with the primary system and that can be activated without loss of information or disruption to operations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(7) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | DUAL AUTHORIZATION \"\n" + 
			"  specification: \"The organization enforces dual authorization for the deletion or destruction of [Assignment: organization-defined backup information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10 { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION \"\n" + 
			"  specification: \"The organization provides for the recovery and reconstitution of the information system to a known state after a disruption, compromise, or failure.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10(1) { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION | CONTINGENCY PLAN TESTING \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CP-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10(2) { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION | TRANSACTION RECOVERY \"\n" + 
			"  specification: \"The information system implements transaction recovery for systems that are transaction-based.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10(3) { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION | COMPENSATING SECURITY CONTROLS \"\n" + 
			"  specification: \"Withdrawn: Addressed through tailoring procedures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10(4) { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION | RESTORE WITHIN TIME PERIOD \"\n" + 
			"  specification: \"The organization provides the capability to restore information system components within [Assignment: organization-defined restoration time-periods] from configuration-controlled and integrity-protected information representing a known, operational state for the components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10(5) { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION | FAILOVER CAPABILITY \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-13.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10(6) { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION | COMPONENT PROTECTION \"\n" + 
			"  specification: \"The organization protects backup and restoration hardware, firmware, and software.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-11 { \n" + 
			"  name: \"ALTERNATE COMMUNICATIONS PROTOCOLS \"\n" + 
			"  specification: \"The information system provides the capability to employ [Assignment: organization-defined alternative communications protocols] in support of maintaining continuity of operations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-12 { \n" + 
			"  name: \"SAFE MODE \"\n" + 
			"  specification: \"The information system, when [Assignment: organization-defined conditions] are detected, enters a safe mode of operation with [Assignment: organization-defined restrictions of safe mode of operation].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-13 { \n" + 
			"  name: \"ALTERNATIVE SECURITY MECHANISMS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined alternative or supplemental security mechanisms] for satisfying [Assignment: organization-defined security functions] when the primary means of implementing the security function is unavailable or compromised.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control IA-1 { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. An identification and authentication policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the identification and authentication policy and associated identification and authentication controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Identification and authentication policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Identification and authentication procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2 { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION (ORGANIZATIONAL USERS) \"\n" + 
			"  specification: \"The information system uniquely identifies and authenticates organizational users (or processes acting on behalf of organizational users).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(1) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | NETWORK ACCESS TO PRIVILEGED ACCOUNTS \"\n" + 
			"  specification: \"The information system implements multifactor authentication for network access to privileged accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(2) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | NETWORK ACCESS TO NON-PRIVILEGED ACCOUNTS \"\n" + 
			"  specification: \"The information system implements multifactor authentication for network access to non-privileged accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(3) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | LOCAL ACCESS TO PRIVILEGED ACCOUNTS \"\n" + 
			"  specification: \"The information system implements multifactor authentication for local access to privileged accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(4) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | LOCAL ACCESS TO NON-PRIVILEGED ACCOUNTS \"\n" + 
			"  specification: \"The information system implements multifactor authentication for local access to non-privileged accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(5) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | GROUP AUTHENTICATION \"\n" + 
			"  specification: \"The organization requires individuals to be authenticated with an individual authenticator when a group authenticator is employed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(6) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | NETWORK ACCESS TO PRIVILEGED ACCOUNTS - SEPARATE DEVICE \"\n" + 
			"  specification: \"The information system implements multifactor authentication for network access to privileged accounts such that one of the factors is provided by a device separate from the system gaining access and the device meets [Assignment: organization-defined strength of mechanism requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(7) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | NETWORK ACCESS TO NON-PRIVILEGED ACCOUNTS - SEPARATE DEVICE \"\n" + 
			"  specification: \"The information system implements multifactor authentication for network access to non-privileged accounts such that one of the factors is provided by a device separate from the system gaining access and the device meets [Assignment: organization-defined strength of mechanism requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(8) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | NETWORK ACCESS TO PRIVILEGED ACCOUNTS - REPLAY RESISTANT \"\n" + 
			"  specification: \"The information system implements replay-resistant authentication mechanisms for network access to privileged accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(9) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | NETWORK ACCESS TO NON-PRIVILEGED ACCOUNTS - REPLAY RESISTANT \"\n" + 
			"  specification: \"The information system implements replay-resistant authentication mechanisms for network access to non-privileged accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(10) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | SINGLE SIGN-ON \"\n" + 
			"  specification: \"The information system provides a single sign-on capability for [Assignment: organization-defined information system accounts and services].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(11) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | REMOTE ACCESS - SEPARATE DEVICE \"\n" + 
			"  specification: \"The information system implements multifactor authentication for remote access to privileged and non-privileged accounts such that one of the factors is provided by a device separate from the system gaining access and the device meets [Assignment: organization-defined strength of mechanism requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(12) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | ACCEPTANCE OF PIV CREDENTIALS \"\n" + 
			"  specification: \"The information system accepts and electronically verifies Personal Identity Verification (PIV) credentials.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(13) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | OUT-OF-BAND AUTHENTICATION \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined out-of-band authentication] under [Assignment: organization-defined conditions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-3 { \n" + 
			"  name: \"DEVICE IDENTIFICATION AND AUTHENTICATION \"\n" + 
			"  specification: \"The information system uniquely identifies and authenticates [Assignment: organization-defined specific and/or types of devices] before establishing a [Selection (one or more): local; remote; network] connection.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-3(1) { \n" + 
			"  name: \"DEVICE IDENTIFICATION AND AUTHENTICATION | CRYPTOGRAPHIC BIDIRECTIONAL AUTHENTICATION \"\n" + 
			"  specification: \"The information system authenticates [Assignment: organization-defined specific devices and/or types of devices] before establishing [Selection (one or more): local; remote; network] connection using bidirectional authentication that is cryptographically based.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-3(2) { \n" + 
			"  name: \"DEVICE IDENTIFICATION AND AUTHENTICATION | CRYPTOGRAPHIC BIDIRECTIONAL NETWORK AUTHENTICATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into IA-3 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-3(3) { \n" + 
			"  name: \"DEVICE IDENTIFICATION AND AUTHENTICATION | DYNAMIC ADDRESS ALLOCATION \"\n" + 
			"  specification: \"The organization:(a) Standardizes dynamic address allocation lease information and the lease duration assigned to devices in accordance with [Assignment: organization-defined lease information and lease duration]; and (b) Audits lease information when assigned to a device.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-3(4) { \n" + 
			"  name: \"DEVICE IDENTIFICATION AND AUTHENTICATION | DEVICE ATTESTATION \"\n" + 
			"  specification: \"The organization ensures that device identification and authentication based on attestation is handled by [Assignment: organization-defined configuration management process].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4 { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT \"\n" + 
			"  specification: \"The organization manages information system identifiers by:\n" + 
			"			a. Receiving authorization from [Assignment: organization-defined personnel or roles] to assign an individual, group, role, or device identifier;\n" + 
			"			b. Selecting an identifier that identifies an individual, group, role, or device;\n" + 
			"			c. Assigning the identifier to the intended individual, group, role, or device;\n" + 
			"			d. Preventing reuse of identifiers for [Assignment: organization-defined time period]; and\n" + 
			"			e. Disabling the identifier after [Assignment: organization-defined time period of inactivity].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(1) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | PROHIBIT ACCOUNT IDENTIFIERS AS PUBLIC IDENTIFIERS \"\n" + 
			"  specification: \"The organization prohibits the use of information system account identifiers that are the same as public identifiers for individual electronic mail accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(2) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | SUPERVISOR AUTHORIZATION \"\n" + 
			"  specification: \"The organization requires that the registration process to receive an individual identifier includes supervisor authorization.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(3) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | MULTIPLE FORMS OF CERTIFICATION \"\n" + 
			"  specification: \"The organization requires multiple forms of certification of individual identification be presented to the registration authority.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(4) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | IDENTIFY USER STATUS \"\n" + 
			"  specification: \"The organization manages individual identifiers by uniquely identifying each individual as [Assignment: organization-defined characteristic identifying individual status].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(5) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | DYNAMIC MANAGEMENT \"\n" + 
			"  specification: \"The information system dynamically manages identifiers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(6) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | CROSS-ORGANIZATION MANAGEMENT \"\n" + 
			"  specification: \"The organization coordinates with [Assignment: organization-defined external organizations] for cross-organization management of identifiers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(7) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | IN-PERSON REGISTRATION \"\n" + 
			"  specification: \"The organization requires that the registration process to receive an individual identifier be conducted in person before a designated registration authority.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5 { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT \"\n" + 
			"  specification: \"The organization manages information system authenticators by:\n" + 
			"			a. Verifying, as part of the initial authenticator distribution, the identity of the individual, group, role, or device receiving the authenticator;\n" + 
			"			b. Establishing initial authenticator content for authenticators defined by the organization;\n" + 
			"			c. Ensuring that authenticators have sufficient strength of mechanism for their intended use;\n" + 
			"			d. Establishing and implementing administrative procedures for initial authenticator distribution, for lost/compromised or damaged authenticators, and for revoking authenticators;\n" + 
			"			e. Changing default content of authenticators prior to information system installation;\n" + 
			"			f. Establishing minimum and maximum lifetime restrictions and reuse conditions for authenticators;\n" + 
			"			g. Changing/refreshing authenticators [Assignment: organization-defined time period by authenticator type];\n" + 
			"			h. Protecting authenticator content from unauthorized disclosure and modification;\n" + 
			"			i. Requiring individuals to take, and having devices implement, specific security safeguards to protect authenticators; and\n" + 
			"			j. Changing authenticators for group/role accounts when membership to those accounts changes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(1) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | PASSWORD-BASED AUTHENTICATION \"\n" + 
			"  specification: \"The information system, for password-based authentication:\n" + 
			"			(a) Enforces minimum password complexity of [Assignment: organization-defined requirements for case sensitivity, number of characters, mix of upper-case letters, lower-case letters, numbers, and special characters, including minimum requirements for each type];\n" + 
			"			(b) Enforces at least the following number of changed characters when new passwords are created: [Assignment: organization-defined number];\n" + 
			"			(c) Stores and transmits only cryptographically-protected passwords;\n" + 
			"			(d) Enforces password minimum and maximum lifetime restrictions of [Assignment: organization-defined numbers for lifetime minimum, lifetime maximum];\n" + 
			"			(e) Prohibits password reuse for [Assignment: organization-defined number] generations; and\n" + 
			"			(f) Allows the use of a temporary password for system logons with an immediate change to a permanent password.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(2) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | PKI-BASED AUTHENTICATION \"\n" + 
			"  specification: \"The information system, for PKI-based authentication: (a) Validates certifications by constructing and verifying a certification path to an accepted trust anchor including checking certificate status information; (b) Enforces authorized access to the corresponding private key; (c) Maps the authenticated identity to the account of the individual or group; and (d) Implements a local cache of revocation data to support path discovery and validation in case of inability to access revocation information via the network.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(3) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | IN-PERSON OR TRUSTED THIRD-PARTY REGISTRATION \"\n" + 
			"  specification: \"The organization requires that the registration process to receive [Assignment: organization-defined types of and/or specific authenticators] be conducted [Selection: in person; by a trusted third party] before [Assignment: organization-defined registration authority] with authorization by [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(4) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | AUTOMATED SUPPORT FOR PASSWORD STRENGTH DETERMINATION \"\n" + 
			"  specification: \"The organization employs automated tools to determine if password authenticators are sufficiently strong to satisfy [Assignment: organization-defined requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(5) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | CHANGE AUTHENTICATORS PRIOR TO DELIVERY \"\n" + 
			"  specification: \"The organization requires developers/installers of information system components to provide unique authenticators or change default authenticators prior to delivery/installation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(6) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | PROTECTION OF AUTHENTICATORS \"\n" + 
			"  specification: \"The organization protects authenticators commensurate with the security category of the information to which use of the authenticator permits access.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(7) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | NO EMBEDDED UNENCRYPTED STATIC AUTHENTICATORS \"\n" + 
			"  specification: \"The organization ensures that unencrypted static authenticators are not embedded in applications or access scripts or stored on function keys.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(8) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | MULTIPLE INFORMATION SYSTEM ACCOUNTS \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined security safeguards] to manage the risk of compromise due to individuals having accounts on multiple information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(9) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | CROSS-ORGANIZATION CREDENTIAL MANAGEMENT \"\n" + 
			"  specification: \"The organization coordinates with [Assignment: organization-defined external organizations] for cross-organization management of credentials.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(10) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | DYNAMIC CREDENTIAL ASSOCIATION \"\n" + 
			"  specification: \"The information system dynamically provisions identities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(11) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | HARDWARE TOKEN-BASED AUTHENTICATION \"\n" + 
			"  specification: \"The information system, for hardware token-based authentication, employs mechanisms that satisfy [Assignment: organization-defined token quality requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(12) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | BIOMETRIC-BASED AUTHENTICATION \"\n" + 
			"  specification: \"The information system, for biometric-based authentication, employs mechanisms that satisfy [Assignment: organization-defined biometric quality requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(13) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | EXPIRATION OF CACHED AUTHENTICATORS \"\n" + 
			"  specification: \"The information system prohibits the use of cached authenticators after [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(14) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | MANAGING CONTENT OF PKI TRUST STORES \"\n" + 
			"  specification: \"The organization, for PKI-based authentication, employs a deliberate organization-wide methodology for managing the content of PKI trust stores installed across all platforms including networks, operating systems, browsers, and applications.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(15) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | FICAM-APPROVED PRODUCTS AND SERVICES \"\n" + 
			"  specification: \"The organization uses only FICAM-approved path discovery and validation products and services.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-6 { \n" + 
			"  name: \"AUTHENTICATOR FEEDBACK \"\n" + 
			"  specification: \"The information system obscures feedback of authentication information during the authentication process to protect the information from possible exploitation/use by unauthorized individuals.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-7 { \n" + 
			"  name: \"CRYPTOGRAPHIC MODULE AUTHENTICATION \"\n" + 
			"  specification: \"The information system implements mechanisms for authentication to a cryptographic module that meet the requirements of applicable federal laws, Executive Orders, directives, policies, regulations, standards, and guidance for such authentication.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-8 { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION (NON-ORGANIZATIONAL USERS) \"\n" + 
			"  specification: \"The information system uniquely identifies and authenticates non-organizational users (or processes acting on behalf of non-organizational users).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-8(1) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | ACCEPTANCE OF PIV CREDENTIALS FROM OTHER AGENCIES \"\n" + 
			"  specification: \"The information system accepts and electronically verifies Personal Identity Verification (PIV) credentials from other federal agencies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-8(2) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | ACCEPTANCE OF THIRD-PARTY CREDENTIALS \"\n" + 
			"  specification: \"The information system accepts only FICAM-approved third-party credentials.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-8(3) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | USE OF FICAM-APPROVED PRODUCTS \"\n" + 
			"  specification: \"The organization employs only FICAM-approved information system components in [Assignment: organization-defined information systems] to accept third-party credentials.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-8(4) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | USE OF FICAM-ISSUED PROFILES \"\n" + 
			"  specification: \"The information system conforms to FICAM-issued profiles.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-8(5) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | ACCEPTANCE OF PIV-I CREDENTIALS \"\n" + 
			"  specification: \"The information system accepts and electronically verifies Personal Identity Verification-I (PIV-I) credentials.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-9 { \n" + 
			"  name: \"SERVICE IDENTIFICATION AND AUTHENTICATION \"\n" + 
			"  specification: \"The organization identifies and authenticates [Assignment: organization-defined information system services] using [Assignment: organization-defined security safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-9(1) { \n" + 
			"  name: \"SERVICE IDENTIFICATION AND AUTHENTICATION | INFORMATION EXCHANGE \"\n" + 
			"  specification: \"The organization ensures that service providers receive, validate, and transmit identification and authentication information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-9(2) { \n" + 
			"  name: \"SERVICE IDENTIFICATION AND AUTHENTICATION | TRANSMISSION OF DECISIONS \"\n" + 
			"  specification: \"The organization ensures that identification and authentication decisions are transmitted between [Assignment: organization-defined services] consistent with organizational policies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-10 { \n" + 
			"  name: \"ADAPTIVE IDENTIFICATION AND AUTHENTICATION \"\n" + 
			"  specification: \"The organization requires that individuals accessing the information system employ [Assignment: organization-defined supplemental authentication techniques or mechanisms] under specific [Assignment: organization-defined circumstances or situations].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-11 { \n" + 
			"  name: \"RE-AUTHENTICATION \"\n" + 
			"  specification: \"The organization requires users and devices to re-authenticate when [Assignment: organization-defined circumstances or situations requiring re-authentication].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IR-1 { \n" + 
			"  name: \"INCIDENT RESPONSE POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. An incident response policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the incident response policy and associated incident response controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Incident response policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Incident response procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-2 { \n" + 
			"  name: \"INCIDENT RESPONSE TRAINING \"\n" + 
			"  specification: \"The organization provides incident response training to information system users consistent with assigned roles and responsibilities: a. Within [Assignment: organization-defined time period] of assuming an incident response role or responsibility; b. When required by information system changes; and c. [Assignment: organization-defined frequency] thereafter.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-2(1) { \n" + 
			"  name: \"INCIDENT RESPONSE TRAINING | SIMULATED EVENTS \"\n" + 
			"  specification: \"The organization incorporates simulated events into incident response training to facilitate effective response by personnel in crisis situations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-2(2) { \n" + 
			"  name: \"INCIDENT RESPONSE TRAINING | AUTOMATED TRAINING ENVIRONMENTS \"\n" + 
			"  specification: \"The organization employs automated mechanisms to provide a more thorough and realistic incident response training environment.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-3 { \n" + 
			"  name: \"INCIDENT RESPONSE TESTING \"\n" + 
			"  specification: \"The organization tests the incident response capability for the information system [Assignment: organization-defined frequency] using [Assignment: organization-defined tests] to determine the incident response effectiveness and documents the results.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-3(1) { \n" + 
			"  name: \"INCIDENT RESPONSE TESTING | AUTOMATED TESTING \"\n" + 
			"  specification: \"The organization employs automated mechanisms to more thoroughly and effectively test the incident response capability.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-3(2) { \n" + 
			"  name: \"INCIDENT RESPONSE TESTING | COORDINATION WITH RELATED PLANS \"\n" + 
			"  specification: \"The organization coordinates incident response testing with organizational elements responsible for related plans.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4 { \n" + 
			"  name: \"INCIDENT HANDLING \"\n" + 
			"  specification: \"The organization: a. Implements an incident handling capability for security incidents that includes preparation, detection and analysis, containment, eradication, and recovery; b. Coordinates incident handling activities with contingency planning activities; and c. Incorporates lessons learned from ongoing incident handling activities into incident response procedures, training, and testing, and implements the resulting changes accordingly.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(1) { \n" + 
			"  name: \"INCIDENT HANDLING | AUTOMATED INCIDENT HANDLING PROCESSES \"\n" + 
			"  specification: \"The organization employs automated mechanisms to support the incident handling process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(2) { \n" + 
			"  name: \"INCIDENT HANDLING | DYNAMIC RECONFIGURATION \"\n" + 
			"  specification: \"The organization includes dynamic reconfiguration of [Assignment: organization-defined information system components] as part of the incident response capability.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(3) { \n" + 
			"  name: \"INCIDENT HANDLING | CONTINUITY OF OPERATIONS \"\n" + 
			"  specification: \"The organization identifies [Assignment: organization-defined classes of incidents] and [Assignment: organization-defined actions to take in response to classes of incidents] to ensure continuation of organizational missions and business functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(4) { \n" + 
			"  name: \"INCIDENT HANDLING | INFORMATION CORRELATION \"\n" + 
			"  specification: \"The organization correlates incident information and individual incident responses to achieve an organization-wide perspective on incident awareness and response.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(5) { \n" + 
			"  name: \"INCIDENT HANDLING | AUTOMATIC DISABLING OF INFORMATION SYSTEM \"\n" + 
			"  specification: \"The organization implements a configurable capability to automatically disable the information system if [Assignment: organization-defined security violations] are detected.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(6) { \n" + 
			"  name: \"INCIDENT HANDLING | INSIDER THREATS - SPECIFIC CAPABILITIES \"\n" + 
			"  specification: \"The organization implements incident handling capability for insider threats.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(7) { \n" + 
			"  name: \"INCIDENT HANDLING | INSIDER THREATS - INTRA-ORGANIZATION COORDINATION \"\n" + 
			"  specification: \"The organization coordinates incident handling capability for insider threats across [Assignment: organization-defined components or elements of the organization].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(8) { \n" + 
			"  name: \"INCIDENT HANDLING | CORRELATION WITH EXTERNAL ORGANIZATIONS \"\n" + 
			"  specification: \"The organization coordinates with [Assignment: organization-defined external organizations] to correlate and share [Assignment: organization-defined incident information] to achieve a cross-organization perspective on incident awareness and more effective incident responses.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(9) { \n" + 
			"  name: \"INCIDENT HANDLING | DYNAMIC RESPONSE CAPABILITY \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined dynamic response capabilities] to effectively respond to security incidents.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(10) { \n" + 
			"  name: \"INCIDENT HANDLING | SUPPLY CHAIN COORDINATION \"\n" + 
			"  specification: \"The organization coordinates incident handling activities involving supply chain events with other organizations involved in the supply chain.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-5 { \n" + 
			"  name: \"INCIDENTMONITORING \"\n" + 
			"  specification: \"\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-5(1) { \n" + 
			"  name: \"INCIDENT MONITORING | AUTOMATED TRACKING / DATA COLLECTION / ANALYSIS \"\n" + 
			"  specification: \"The organization employs automated mechanisms to assist in the tracking of security incidents and in the collection and analysis of incident information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-6 { \n" + 
			"  name: \"INCIDENT REPORTING \"\n" + 
			"  specification: \"The organization: a. Requires personnel to report suspected security incidents to the organizational incident response capability within [Assignment: organization-defined time period]; and b. Reports security incident information to [Assignment: organization-defined authorities].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-6(1) { \n" + 
			"  name: \"INCIDENT REPORTING | AUTOMATED REPORTING \"\n" + 
			"  specification: \"The organization employs automated mechanisms to assist in the reporting of security incidents.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-6(2) { \n" + 
			"  name: \"INCIDENT REPORTING | VULNERABILITIES RELATED TO INCIDENTS \"\n" + 
			"  specification: \"The organization reports information system vulnerabilities associated with reported security incidents to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-6(3) { \n" + 
			"  name: \"INCIDENT REPORTING | COORDINATION WITH SUPPLY CHAIN \"\n" + 
			"  specification: \"The organization provides security incident information to other organizations involved in the supply chain for information systems or information system components related to the incident.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-7 { \n" + 
			"  name: \"INCIDENT RESPONSE ASSISTANCE \"\n" + 
			"  specification: \"The organization provides an incident response support resource, integral to the organizational incident response capability that offers advice and assistance to users of the information system for the handling and reporting of security incidents.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-7(1) { \n" + 
			"  name: \"INCIDENT RESPONSE ASSISTANCE | AUTOMATION SUPPORT FOR AVAILABILITY OF INFORMATION / SUPPORT \"\n" + 
			"  specification: \"The organization employs automated mechanisms to increase the availability of incident response-related information and support.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-7(2) { \n" + 
			"  name: \"INCIDENT RESPONSE ASSISTANCE | COORDINATION WITH EXTERNAL PROVIDERS \"\n" + 
			"  specification: \"The organization:(a) Establishes a direct, cooperative relationship between its incident response capability and external providers of information system protection capability; and (b) Identifies organizational incident response team members to the external providers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-8 { \n" + 
			"  name: \"INCIDENT RESPONSE PLAN \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops an incident response plan that:\n" + 
			"				1. Provides the organization with a roadmap for implementing its incident response capability;\n" + 
			"				2. Describes the structure and organization of the incident response capability;\n" + 
			"				3. Provides a high-level approach for how the incident response capability fits into the overall organization;\n" + 
			"				4. Meets the unique requirements of the organization, which relate to mission, size, structure, and functions;\n" + 
			"				5. Defines reportable incidents;\n" + 
			"				6. Provides metrics for measuring the incident response capability within the organization;\n" + 
			"				7. Defines the resources and management support needed to effectively maintain and mature an incident response capability; and\n" + 
			"				8. Is reviewed and approved by [Assignment: organization-defined personnel or roles];\n" + 
			"			b. Distributes copies of the incident response plan to [Assignment: organization-defined incident response personnel (identified by name and/or by role) and organizational elements];\n" + 
			"			c. Reviews the incident response plan [Assignment: organization-defined frequency];\n" + 
			"			d. Updates the incident response plan to address system/organizational changes or problems encountered during plan implementation, execution, or testing;\n" + 
			"			e. Communicates incident response plan changes to [Assignment: organization-defined incident response personnel (identified by name and/or by role) and organizational elements]; and\n" + 
			"			f. Protects the incident response plan from unauthorized disclosure and modification.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-9 { \n" + 
			"  name: \"INFORMATION SPILLAGE RESPONSE \"\n" + 
			"  specification: \"The organization responds to information spills by:\n" + 
			"			a. Identifying the specific information involved in the information system contamination;\n" + 
			"			b. Alerting [Assignment: organization-defined personnel or roles] of the information spill using a method of communication not associated with the spill;\n" + 
			"			c. Isolating the contaminated information system or system component;\n" + 
			"			d. Eradicating the information from the contaminated information system or component;\n" + 
			"			e. Identifying other information systems or system components that may have been subsequently contaminated; and\n" + 
			"			f. Performing other [Assignment: organization-defined actions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-9(1) { \n" + 
			"  name: \"INFORMATION SPILLAGE RESPONSE | RESPONSIBLE PERSONNEL \"\n" + 
			"  specification: \"The organization assigns [Assignment: organization-defined personnel or roles] with responsibility for responding to information spills.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-9(2) { \n" + 
			"  name: \"INFORMATION SPILLAGE RESPONSE | TRAINING \"\n" + 
			"  specification: \"The organization provides information spillage response training [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-9(3) { \n" + 
			"  name: \"INFORMATION SPILLAGE RESPONSE | POST-SPILL OPERATIONS \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined procedures] to ensure that organizational personnel impacted by information spills can continue to carry out assigned tasks while contaminated systems are undergoing corrective actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-9(4) { \n" + 
			"  name: \"INFORMATION SPILLAGE RESPONSE | EXPOSURE TO UNAUTHORIZED PERSONNEL \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] for personnel exposed to information not within assigned access authorizations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-10 { \n" + 
			"  name: \"INTEGRATED INFORMATION SECURITY ANALYSIS TEAM \"\n" + 
			"  specification: \"The organization establishes an integrated team of forensic/malicious code analysts, tool developers, and real-time operations personnel.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control MA-1 { \n" + 
			"  name: \"SYSTEM MAINTENANCE POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A system maintenance policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the system maintenance policy and associated system maintenance controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. System maintenance policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. System maintenance procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-2 { \n" + 
			"  name: \"CONTROLLED MAINTENANCE \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Schedules, performs, documents, and reviews records of maintenance and repairs on information system components in accordance with manufacturer or vendor specifications and/or organizational requirements;\n" + 
			"			b. Approves and monitors all maintenance activities, whether performed on site or remotely and whether the equipment is serviced on site or removed to another location;\n" + 
			"			c. Requires that [Assignment: organization-defined personnel or roles] explicitly approve the removal of the information system or system components from organizational facilities for off-site maintenance or repairs;\n" + 
			"			d. Sanitizes equipment to remove all information from associated media prior to removal from organizational facilities for off-site maintenance or repairs;\n" + 
			"			e. Checks all potentially impacted security controls to verify that the controls are still functioning properly following maintenance or repair actions; and\n" + 
			"			f. Includes [Assignment: organization-defined maintenance-related information] in organizational maintenance records.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-2(1) { \n" + 
			"  name: \"CONTROLLED MAINTENANCE | RECORD CONTENT \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MA-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-2(2) { \n" + 
			"  name: \"CONTROLLED MAINTENANCE | AUTOMATED MAINTENANCE ACTIVITIES \"\n" + 
			"  specification: \"The organization: (a) Employs automated mechanisms to schedule, conduct, and document maintenance and repairs; and (b) Produces up-to date, accurate, and complete records of all maintenance and repair actions requested, scheduled, in process, and completed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-3 { \n" + 
			"  name: \"MAINTENANCE TOOLS \"\n" + 
			"  specification: \"The organization approves, controls, and monitors information system maintenance tools.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-3(1) { \n" + 
			"  name: \"MAINTENANCE TOOLS | INSPECT TOOLS \"\n" + 
			"  specification: \"The organization inspects the maintenance tools carried into a facility by maintenance personnel for improper or unauthorized modifications.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-3(2) { \n" + 
			"  name: \"MAINTENANCE TOOLS | INSPECT MEDIA \"\n" + 
			"  specification: \"The organization checks media containing diagnostic and test programs for malicious code before the media are used in the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-3(3) { \n" + 
			"  name: \"MAINTENANCE TOOLS | PREVENT UNAUTHORIZED REMOVAL \"\n" + 
			"  specification: \"The organization prevents the unauthorized removal of maintenance equipment containing organizational information by: (a) Verifying that there is no organizational information contained on the equipment; (b) Sanitizing or destroying the equipment; (c) Retaining the equipment within the facility; or (d) Obtaining an exemption from [Assignment: organization-defined personnel or roles] explicitly authorizing removal of the equipment from the facility.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-3(4) { \n" + 
			"  name: \"MAINTENANCE TOOLS | RESTRICTED TOOL USE \"\n" + 
			"  specification: \"The information system restricts the use of maintenance tools to authorized personnel only.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4 { \n" + 
			"  name: \"NONLOCAL MAINTENANCE \"\n" + 
			"  specification: \"The organization: a. Approves and monitors nonlocal maintenance and diagnostic activities; b. Allows the use of nonlocal maintenance and diagnostic tools only as consistent with organizational policy and documented in the security plan for the information system; c. Employs strong authenticators in the establishment of nonlocal maintenance and diagnostic sessions; d. Maintains records for nonlocal maintenance and diagnostic activities; and e. Terminates session and network connections when nonlocal maintenance is completed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(1) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | AUDITING AND REVIEW \"\n" + 
			"  specification: \"The organization: (a) Audits nonlocal maintenance and diagnostic sessions [Assignment: organization-defined audit events]; and (b) Reviews the records of the maintenance and diagnostic sessions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(2) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | DOCUMENT NONLOCAL MAINTENANCE \"\n" + 
			"  specification: \"The organization documents in the security plan for the information system, the policies and procedures for the establishment and use of nonlocal maintenance and diagnostic connections.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(3) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | COMPARABLE SECURITY / SANITIZATION \"\n" + 
			"  specification: \"The organization:\n" + 
			"			(a) Requires that nonlocal maintenance and diagnostic services be performed from an information system that implements a security capability comparable to the capability implemented on the system being serviced; or\n" + 
			"			(b) Removes the component to be serviced from the information system prior to nonlocal maintenance or diagnostic services, sanitizes the component (with regard to organizational information) before removal from organizational facilities, and after the service is performed, inspects and sanitizes the component (with regard to potentially malicious software) before reconnecting the component to the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(4) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | AUTHENTICATION / SEPARATION OF MAINTENANCE SESSIONS \"\n" + 
			"  specification: \"The organization protects nonlocal maintenance sessions by:\n" + 
			"			(a) Employing [Assignment: organization-defined authenticators that are replay resistant]; and\n" + 
			"			(b) Separating the maintenance sessions from other network sessions with the information system by either:\n" + 
			"				(1) Physically separated communications paths; or\n" + 
			"				(2) Logically separated communications paths based upon encryption.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(5) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | APPROVALS AND NOTIFICATIONS \"\n" + 
			"  specification: \"The organization: (a) Requires the approval of each nonlocal maintenance session by [Assignment: organization-defined personnel or roles]; and (b) Notifies [Assignment: organization-defined personnel or roles] of the date and time of planned nonlocal maintenance.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(6) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to protect the integrity and confidentiality of nonlocal maintenance and diagnostic communications.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(7) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | REMOTE DISCONNECT VERIFICATION \"\n" + 
			"  specification: \"The information system implements remote disconnect verification at the termination of nonlocal maintenance and diagnostic sessions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-5 { \n" + 
			"  name: \"MAINTENANCE PERSONNEL \"\n" + 
			"  specification: \"The organization: a. Establishes a process for maintenance personnel authorization and maintains a list of authorized maintenance organizations or personnel; b. Ensures that non-escorted personnel performing maintenance on the information system have required access authorizations; and c. Designates organizational personnel with required access authorizations and technical competence to supervise the maintenance activities of personnel who do not possess the required access authorizations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-5(1) { \n" + 
			"  name: \"MAINTENANCE PERSONNEL | INDIVIDUALS WITHOUT APPROPRIATE ACCESS \"\n" + 
			"  specification: \"The organization:\n" + 
			"			(a) Implements procedures for the use of maintenance personnel that lack appropriate security clearances or are not U.S. citizens, that include the following requirements:\n" + 
			"				(1) Maintenance personnel who do not have needed access authorizations, clearances, or formal access approvals are escorted and supervised during the performance of maintenance and diagnostic activities on the information system by approved organizational personnel who are fully cleared, have appropriate access authorizations, and are technically qualified;\n" + 
			"				(2) Prior to initiating maintenance or diagnostic activities by personnel who do not have needed access authorizations, clearances or formal access approvals, all volatile information storage components within the information system are sanitized and all nonvolatile storage media are removed or physically disconnected from the system and secured; and\n" + 
			"			(b) Develops and implements alternate security safeguards in the event an information system component cannot be sanitized, removed, or disconnected from the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-5(2) { \n" + 
			"  name: \"MAINTENANCE PERSONNEL | SECURITY CLEARANCES FOR CLASSIFIED SYSTEMS \"\n" + 
			"  specification: \"The organization ensures that personnel performing maintenance and diagnostic activities on an information system processing, storing, or transmitting classified information possess security clearances and formal access approvals for at least the highest classification level and for all compartments of information on the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-5(3) { \n" + 
			"  name: \"MAINTENANCE PERSONNEL | CITIZENSHIP REQUIREMENTS FOR CLASSIFIED SYSTEMS \"\n" + 
			"  specification: \"The organization ensures that personnel performing maintenance and diagnostic activities on an information system processing, storing, or transmitting classified information are U.S. citizens.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-5(4) { \n" + 
			"  name: \"MAINTENANCE PERSONNEL | FOREIGN NATIONALS \"\n" + 
			"  specification: \"The organization ensures that:\n" + 
			"			(a) Cleared foreign nationals (i.e., foreign nationals with appropriate security clearances), are used to conduct maintenance and diagnostic activities on classified information systems only when the systems are jointly owned and operated by the United States and foreign allied governments, or owned and operated solely by foreign allied governments; and\n" + 
			"			(b) Approvals, consents, and detailed operational conditions regarding the use of foreign nationals to conduct maintenance and diagnostic activities on classified information systems are fully documented within Memoranda of Agreements.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-5(5) { \n" + 
			"  name: \"MAINTENANCE PERSONNEL | NONSYSTEM-RELATED MAINTENANCE \"\n" + 
			"  specification: \"The organization ensures that non-escorted personnel performing maintenance activities not directly associated with the information system but in the physical proximity of the system, have required access authorizations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-6 { \n" + 
			"  name: \"TIMELY MAINTENANCE \"\n" + 
			"  specification: \"The organization obtains maintenance support and/or spare parts for [Assignment: organization-defined information system components] within [Assignment: organization-defined time period] of failure.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-6(1) { \n" + 
			"  name: \"TIMELY MAINTENANCE | PREVENTIVE MAINTENANCE \"\n" + 
			"  specification: \"The organization performs preventive maintenance on [Assignment: organization-defined information system components] at [Assignment: organization-defined time intervals].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-6(2) { \n" + 
			"  name: \"TIMELY MAINTENANCE | PREDICTIVE MAINTENANCE \"\n" + 
			"  specification: \"The organization performs predictive maintenance on [Assignment: organization-defined information system components] at [Assignment: organization-defined time intervals].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-6(3) { \n" + 
			"  name: \"TIMELY MAINTENANCE | AUTOMATED SUPPORT FOR PREDICTIVE MAINTENANCE \"\n" + 
			"  specification: \"The organization employs automated mechanisms to transfer predictive maintenance data to a computerized maintenance management system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MP-1 { \n" + 
			"  name: \"MEDIA PROTECTION POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A media protection policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the media protection policy and associated media protection controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Media protection policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Media protection procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-2 { \n" + 
			"  name: \"MEDIA ACCESS \"\n" + 
			"  specification: \"The organization restricts access to [Assignment: organization-defined types of digital and/or non-digital media] to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-2(1) { \n" + 
			"  name: \"MEDIA ACCESS | AUTOMATED RESTRICTED ACCESS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-4 (2).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-2(2) { \n" + 
			"  name: \"MEDIA ACCESS | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-28 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-3 { \n" + 
			"  name: \"MEDIA MARKING \"\n" + 
			"  specification: \"The organization: a. Marks information system media indicating the distribution limitations, handling caveats, and applicable security markings (if any) of the information; and b. Exempts [Assignment: organization-defined types of information system media] from marking as long as the media remain within [Assignment: organization-defined controlled areas].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-4 { \n" + 
			"  name: \"MEDIA STORAGE \"\n" + 
			"  specification: \"The organization: a. Physically controls and securely stores [Assignment: organization-defined types of digital and/or non-digital media] within [Assignment: organization-defined controlled areas]; and b. Protects information system media until the media are destroyed or sanitized using approved equipment, techniques, and procedures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-4(1) { \n" + 
			"  name: \"MEDIA STORAGE | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-28 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-4(2) { \n" + 
			"  name: \"MEDIA STORAGE | AUTOMATED RESTRICTED ACCESS \"\n" + 
			"  specification: \"The organization employs automated mechanisms to restrict access to media storage areas and to audit access attempts and access granted.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-5 { \n" + 
			"  name: \"MEDIA TRANSPORT \"\n" + 
			"  specification: \"The organization: a. Protects and controls [Assignment: organization-defined types of information system media] during transport outside of controlled areas using [Assignment: organization-defined security safeguards]; b. Maintains accountability for information system media during transport outside of controlled areas; c. Documents activities associated with the transport of information system media; and d. Restricts the activities associated with the transport of information system media to authorized personnel.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-5(1) { \n" + 
			"  name: \"MEDIA TRANSPORT | PROTECTION OUTSIDE OF CONTROLLED AREAS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-5.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-5(2) { \n" + 
			"  name: \"MEDIA TRANSPORT | DOCUMENTATION OF ACTIVITIES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-5.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-5(3) { \n" + 
			"  name: \"MEDIA TRANSPORT | CUSTODIANS \"\n" + 
			"  specification: \"The organization employs an identified custodian during transport of information system media outside of controlled areas.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-5(4) { \n" + 
			"  name: \"MEDIA TRANSPORT | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to protect the confidentiality and integrity of information stored on digital media during transport outside of controlled areas.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6 { \n" + 
			"  name: \"MEDIA SANITIZATION \"\n" + 
			"  specification: \"The organization: a. Sanitizes [Assignment: organization-defined information system media] prior to disposal, release out of organizational control, or release for reuse using [Assignment: organization-defined sanitization techniques and procedures] in accordance with applicable federal and organizational standards and policies; and b. Employs sanitization mechanisms with the strength and integrity commensurate with the security category or classification of the information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(1) { \n" + 
			"  name: \"MEDIA SANITIZATION | REVIEW / APPROVE / TRACK / DOCUMENT / VERIFY \"\n" + 
			"  specification: \"The organization reviews, approves, tracks, documents, and verifies media sanitization and disposal actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(2) { \n" + 
			"  name: \"MEDIA SANITIZATION | EQUIPMENT TESTING \"\n" + 
			"  specification: \"The organization tests sanitization equipment and procedures [Assignment: organization-defined frequency] to verify that the intended sanitization is being achieved.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(3) { \n" + 
			"  name: \"MEDIA SANITIZATION | NONDESTRUCTIVE TECHNIQUES \"\n" + 
			"  specification: \"The organization applies nondestructive sanitization techniques to portable storage devices prior to connecting such devices to the information system under the following circumstances: [Assignment: organization-defined circumstances requiring sanitization of portable storage devices].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(4) { \n" + 
			"  name: \"MEDIA SANITIZATION | CONTROLLED UNCLASSIFIED INFORMATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(5) { \n" + 
			"  name: \"MEDIA SANITIZATION | CLASSIFIED INFORMATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(6) { \n" + 
			"  name: \"MEDIA SANITIZATION | MEDIA DESTRUCTION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(7) { \n" + 
			"  name: \"MEDIA SANITIZATION | DUAL AUTHORIZATION \"\n" + 
			"  specification: \"The organization enforces dual authorization for the sanitization of [Assignment: organization-defined information system media].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(8) { \n" + 
			"  name: \"MEDIA SANITIZATION | REMOTE PURGING / WIPING OF INFORMATION \"\n" + 
			"  specification: \"The organization provides the capability to purge/wipe information from [Assignment: organization-defined information systems, system components, or devices] either remotely or under the following conditions: [Assignment: organization-defined conditions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-7 { \n" + 
			"  name: \"MEDIA USE \"\n" + 
			"  specification: \"The organization [Selection: restricts; prohibits] the use of [Assignment: organization-defined types of information system media] on [Assignment: organization-defined information systems or system components] using [Assignment: organization-defined security safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-7(1) { \n" + 
			"  name: \"MEDIA USE | PROHIBIT USE WITHOUT OWNER \"\n" + 
			"  specification: \"The organization prohibits the use of portable storage devices in organizational information systems when such devices have no identifiable owner.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-7(2) { \n" + 
			"  name: \"MEDIA USE | PROHIBIT USE OF SANITIZATION-RESISTANT MEDIA \"\n" + 
			"  specification: \"The organization prohibits the use of sanitization-resistant media in organizational information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-8 { \n" + 
			"  name: \"MEDIA DOWNGRADING \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Establishes [Assignment: organization-defined information system media downgrading process] that includes employing downgrading mechanisms with [Assignment: organization-defined strength and integrity];\n" + 
			"			b. Ensures that the information system media downgrading process is commensurate with the security category and/or classification level of the information to be removed and the access authorizations of the potential recipients of the downgraded information;\n" + 
			"			c. Identifies [Assignment: organization-defined information system media requiring downgrading]; and\n" + 
			"			d. Downgrades the identified information system media using the established process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-8(1) { \n" + 
			"  name: \"MEDIA DOWNGRADING | DOCUMENTATION OF PROCESS \"\n" + 
			"  specification: \"The organization documents information system media downgrading actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-8(2) { \n" + 
			"  name: \"MEDIA DOWNGRADING | EQUIPMENT TESTING \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined tests] of downgrading equipment and procedures to verify correct performance [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-8(3) { \n" + 
			"  name: \"MEDIA DOWNGRADING | CONTROLLED UNCLASSIFIED INFORMATION \"\n" + 
			"  specification: \"The organization downgrades information system media containing [Assignment: organization-defined Controlled Unclassified Information (CUI)] prior to public release in accordance with applicable federal and organizational standards and policies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-8(4) { \n" + 
			"  name: \"MEDIA DOWNGRADING | CLASSIFIED INFORMATION \"\n" + 
			"  specification: \"The organization downgrades information system media containing classified information prior to release to individuals without required access authorizations in accordance with NSA standards and policies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control PE-1 { \n" + 
			"  name: \"PHYSICAL AND ENVIRONMENTAL PROTECTION POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A physical and environmental protection policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the physical and environmental protection policy and associated physical and environmental protection controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Physical and environmental protection policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Physical and environmental protection procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-2 { \n" + 
			"  name: \"PHYSICAL ACCESS AUTHORIZATIONS \"\n" + 
			"  specification: \"The organization: a. Develops, approves, and maintains a list of individuals with authorized access to the facility where the information system resides; b. Issues authorization credentials for facility access; c. Reviews the access list detailing authorized facility access by individuals [Assignment: organization-defined frequency]; and d. Removes individuals from the facility access list when access is no longer required.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-2(1) { \n" + 
			"  name: \"PHYSICAL ACCESS AUTHORIZATIONS | ACCESS BY POSITION / ROLE \"\n" + 
			"  specification: \"The organization authorizes physical access to the facility where the information system resides based on position or role.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-2(2) { \n" + 
			"  name: \"PHYSICAL ACCESS AUTHORIZATIONS | TWO FORMS OF IDENTIFICATION \"\n" + 
			"  specification: \"The organization requires two forms of identification from [Assignment: organization-defined list of acceptable forms of identification] for visitor access to the facility where the information system resides.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-2(3) { \n" + 
			"  name: \"PHYSICAL ACCESS AUTHORIZATIONS | RESTRICT UNESCORTED ACCESS \"\n" + 
			"  specification: \"The organization restricts unescorted access to the facility where the information system resides to personnel with [Selection (one or more): security clearances for all information contained within the system; formal access authorizations for all information contained within the system; need for access to all information contained within the system; [Assignment: organization-defined credentials]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3 { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Enforces physical access authorizations at [Assignment: organization-defined entry/exit points to the facility where the information system resides] by;\n" + 
			"				1. Verifying individual access authorizations before granting access to the facility; and\n" + 
			"				2. Controlling ingress/egress to the facility using [Selection (one or more): [Assignment: organization-defined physical access control systems/devices]; guards];\n" + 
			"			b. Maintains physical access audit logs for [Assignment: organization-defined entry/exit points];\n" + 
			"			c. Provides [Assignment: organization-defined security safeguards] to control access to areas within the facility officially designated as publicly accessible;\n" + 
			"			d. Escorts visitors and monitors visitor activity [Assignment: organization-defined circumstances requiring visitor escorts and monitoring];\n" + 
			"			e. Secures keys, combinations, and other physical access devices;\n" + 
			"			f. Inventories [Assignment: organization-defined physical access devices] every [Assignment: organization-defined frequency]; and\n" + 
			"			g. Changes combinations and keys [Assignment: organization-defined frequency] and/or when keys are lost, combinations are compromised, or individuals are transferred or terminated.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3(1) { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL | INFORMATION SYSTEM ACCESS \"\n" + 
			"  specification: \"The organization enforces physical access authorizations to the information system in addition to the physical access controls for the facility at [Assignment: organization-defined physical spaces containing one or more components of the information system].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3(2) { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL | FACILITY / INFORMATION SYSTEM BOUNDARIES \"\n" + 
			"  specification: \"The organization performs security checks [Assignment: organization-defined frequency] at the physical boundary of the facility or information system for unauthorized exfiltration of information or removal of information system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3(3) { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL | CONTINUOUS GUARDS / ALARMS / MONITORING \"\n" + 
			"  specification: \"The organization employs guards and/or alarms to monitor every physical access point to the facility where the information system resides 24 hours per day, 7 days per week.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3(4) { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL | LOCKABLE CASINGS \"\n" + 
			"  specification: \"The organization uses lockable physical casings to protect [Assignment: organization-defined information system components] from unauthorized physical access.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3(5) { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL | TAMPER PROTECTION \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to [Selection (one or more): detect; prevent] physical tampering or alteration of [Assignment: organization-defined hardware components] within the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3(6) { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL | FACILITY PENETRATION TESTING \"\n" + 
			"  specification: \"The organization employs a penetration testing process that includes [Assignment: organization-defined frequency], unannounced attempts to bypass or circumvent security controls associated with physical access points to the facility.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-4 { \n" + 
			"  name: \"ACCESS CONTROL FOR TRANSMISSION MEDIUM \"\n" + 
			"  specification: \"The organization controls physical access to [Assignment: organization-defined information system distribution and transmission lines] within organizational facilities using [Assignment: organization-defined security safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-5 { \n" + 
			"  name: \"ACCESS CONTROL FOR OUTPUT DEVICES \"\n" + 
			"  specification: \"The organization controls physical access to information system output devices to prevent unauthorized individuals from obtaining the output.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-5(1) { \n" + 
			"  name: \"ACCESS CONTROL FOR OUTPUT DEVICES | ACCESS TO OUTPUT BY AUTHORIZED INDIVIDUALS \"\n" + 
			"  specification: \"The organization:(a) Controls physical access to output from [Assignment: organization-defined output devices]; and (b) Ensures that only authorized individuals receive output from the device.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-5(2) { \n" + 
			"  name: \"ACCESS CONTROL FOR OUTPUT DEVICES | ACCESS TO OUTPUT BY INDIVIDUAL IDENTITY \"\n" + 
			"  specification: \"The information system:(a) Controls physical access to output from [Assignment: organization-defined output devices]; and(b) Links individual identity to receipt of the output from the device.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-5(3) { \n" + 
			"  name: \"ACCESS CONTROL FOR OUTPUT DEVICES | MARKING OUTPUT DEVICES \"\n" + 
			"  specification: \"The organization marks [Assignment: organization-defined information system output devices] indicating the appropriate security marking of the information permitted to be output from the device.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-6 { \n" + 
			"  name: \"MONITORING PHYSICAL ACCESS \"\n" + 
			"  specification: \"The organization: a. Monitors physical access to the facility where the information system resides to detect and respond to physical security incidents; b. Reviews physical access logs [Assignment: organization-defined frequency] and upon occurrence of [Assignment: organization-defined events or potential indications of events]; and c. Coordinates results of reviews and investigations with the organizational incident response capability.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-6(1) { \n" + 
			"  name: \"MONITORING PHYSICAL ACCESS | INTRUSION ALARMS / SURVEILLANCE EQUIPMENT \"\n" + 
			"  specification: \"The organization monitors physical intrusion alarms and surveillance equipment.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-6(2) { \n" + 
			"  name: \"MONITORING PHYSICAL ACCESS | AUTOMATED INTRUSION RECOGNITION / RESPONSES \"\n" + 
			"  specification: \"The organization employs automated mechanisms to recognize [Assignment: organization-defined classes/types of intrusions] and initiate [Assignment: organization-defined response actions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-6(3) { \n" + 
			"  name: \"MONITORING PHYSICAL ACCESS | VIDEO SURVEILLANCE \"\n" + 
			"  specification: \"The organization employs video surveillance of [Assignment: organization-defined operational areas] and retains video recordings for [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-6(4) { \n" + 
			"  name: \"MONITORING PHYSICAL ACCESS | MONITORING PHYSICAL ACCESS TO INFORMATION SYSTEMS \"\n" + 
			"  specification: \"The organization monitors physical access to the information system in addition to the physical access monitoring of the facility as [Assignment: organization-defined physical spaces containing one or more components of the information system].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-7 { \n" + 
			"  name: \"VISITOR CONTROL \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PE-2 and PE-3.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-8 { \n" + 
			"  name: \"VISITOR ACCESS RECORDS \"\n" + 
			"  specification: \"The organization: a. Maintains visitor access records to the facility where the information system resides for [Assignment: organization-defined time period]; and b. Reviews visitor access records [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-8(1) { \n" + 
			"  name: \"VISITOR ACCESS RECORDS | AUTOMATED RECORDS MAINTENANCE / REVIEW \"\n" + 
			"  specification: \"The organization employs automated mechanisms to facilitate the maintenance and review of visitor access records.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-8(2) { \n" + 
			"  name: \"VISITOR ACCESS RECORDS | PHYSICAL ACCESS RECORDS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PE-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-9 { \n" + 
			"  name: \"POWER EQUIPMENT AND CABLING \"\n" + 
			"  specification: \"The organization protects power equipment and power cabling for the information system from damage and destruction.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-9(1) { \n" + 
			"  name: \"POWER EQUIPMENT AND CABLING | REDUNDANT CABLING \"\n" + 
			"  specification: \"The organization employs redundant power cabling paths that are physically separated by [Assignment: organization-defined distance].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-9(2) { \n" + 
			"  name: \"POWER EQUIPMENT AND CABLING | AUTOMATIC VOLTAGE CONTROLS \"\n" + 
			"  specification: \"The organization employs automatic voltage controls for [Assignment: organization-defined critical information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-10 { \n" + 
			"  name: \"EMERGENCY SHUTOFF \"\n" + 
			"  specification: \"The organization: a. Provides the capability of shutting off power to the information system or individual system components in emergency situations; b. Places emergency shutoff switches or devices in [Assignment: organization-defined location by information system or system component] to facilitate safe and easy access for personnel; and c. Protects emergency power shutoff capability from unauthorized activation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-10(1) { \n" + 
			"  name: \"EMERGENCY SHUTOFF | ACCIDENTAL / UNAUTHORIZED ACTIVATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PE-10.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-11 { \n" + 
			"  name: \"EMERGENCY POWER \"\n" + 
			"  specification: \"The organization provides a short-term uninterruptible power supply to facilitate [Selection (one or more): an orderly shutdown of the information system; transition of the information system to long-term alternate power] in the event of a primary power source loss.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-11(1) { \n" + 
			"  name: \"EMERGENCY POWER | LONG-TERM ALTERNATE POWER SUPPLY - MINIMAL OPERATIONAL CAPABILITY \"\n" + 
			"  specification: \"The organization provides a long-term alternate power supply for the information system that is capable of maintaining minimally required operational capability in the event of an extended loss of the primary power source.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-11(2) { \n" + 
			"  name: \"EMERGENCY POWER | LONG-TERM ALTERNATE POWER SUPPLY - SELF-CONTAINED \"\n" + 
			"  specification: \"The organization provides a long-term alternate power supply for the information system that is:(a) Self-contained;(b) Not reliant on external power generation; and (c) Capable of maintaining [Selection: minimally required operational capability; full operational capability] in the event of an extended loss of the primary power source.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-12 { \n" + 
			"  name: \"EMERGENCY LIGHTING \"\n" + 
			"  specification: \"The organization employs and maintains automatic emergency lighting for the information system that activates in the event of a power outage or disruption and that covers emergency exits and evacuation routes within the facility.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-12(1) { \n" + 
			"  name: \"EMERGENCY LIGHTING | ESSENTIAL MISSIONS / BUSINESS FUNCTIONS \"\n" + 
			"  specification: \"The organization provides emergency lighting for all areas within the facility supporting essential missions and business functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-13 { \n" + 
			"  name: \"FIRE PROTECTION \"\n" + 
			"  specification: \"The organization employs and maintains fire suppression and detection devices/systems for the information system that are supported by an independent energy source.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-13(1) { \n" + 
			"  name: \"FIRE PROTECTION | DETECTION DEVICES / SYSTEMS \"\n" + 
			"  specification: \"The organization employs fire detection devices/systems for the information system that activate automatically and notify [Assignment: organization-defined personnel or roles] and [Assignment: organization-defined emergency responders] in the event of a fire.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-13(2) { \n" + 
			"  name: \"FIRE PROTECTION | SUPPRESSION DEVICES / SYSTEMS \"\n" + 
			"  specification: \"The organization employs fire suppression devices/systems for the information system that provide automatic notification of any activation to Assignment: organization-defined personnel or roles] and [Assignment: organization-defined emergency responders].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-13(3) { \n" + 
			"  name: \"FIRE PROTECTION | AUTOMATIC FIRE SUPPRESSION \"\n" + 
			"  specification: \"The organization employs an automatic fire suppression capability for the information system when the facility is not staffed on a continuous basis.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-13(4) { \n" + 
			"  name: \"FIRE PROTECTION | INSPECTIONS \"\n" + 
			"  specification: \"The organization ensures that the facility undergoes [Assignment: organization-defined frequency] inspections by authorized and qualified inspectors and resolves identified deficiencies within [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-14 { \n" + 
			"  name: \"TEMPERATURE AND HUMIDITY CONTROLS \"\n" + 
			"  specification: \"The organization:a. Maintains temperature and humidity levels within the facility where the information system resides at [Assignment: organization-defined acceptable levels]; and b. Monitors temperature and humidity levels [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-14(1) { \n" + 
			"  name: \"TEMPERATURE AND HUMIDITY CONTROLS | AUTOMATIC CONTROLS \"\n" + 
			"  specification: \"The organization employs automatic temperature and humidity controls in the facility to prevent fluctuations potentially harmful to the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-14(2) { \n" + 
			"  name: \"TEMPERATURE AND HUMIDITY CONTROLS | MONITORING WITH ALARMS / NOTIFICATIONS \"\n" + 
			"  specification: \"The organization employs temperature and humidity monitoring that provides an alarm or notification of changes potentially harmful to personnel or equipment.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-15 { \n" + 
			"  name: \"WATER DAMAGE PROTECTION \"\n" + 
			"  specification: \"The organization protects the information system from damage resulting from water leakage by providing master shutoff or isolation valves that are accessible, working properly, and known to key personnel.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-15(1) { \n" + 
			"  name: \"WATER DAMAGE PROTECTION | AUTOMATION SUPPORT \"\n" + 
			"  specification: \"The organization employs automated mechanisms to detect the presence of water in the vicinity of the information system and alerts [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-16 { \n" + 
			"  name: \"DELIVERY AND REMOVAL \"\n" + 
			"  specification: \"The organization authorizes, monitors, and controls [Assignment: organization-defined types of information system components] entering and exiting the facility and maintains records of those items.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-17 { \n" + 
			"  name: \"ALTERNATE WORK SITE \"\n" + 
			"  specification: \"The organization: a. Employs [Assignment: organization-defined security controls] at alternate work sites; b. Assesses as feasible, the effectiveness of security controls at alternate work sites; and c. Provides a means for employees to communicate with information security personnel in case of security incidents or problems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-18 { \n" + 
			"  name: \"LOCATION OF INFORMATION SYSTEM COMPONENTS \"\n" + 
			"  specification: \"The organization positions information system components within the facility to minimize potential damage from [Assignment: organization-defined physical and environmental hazards] and to minimize the opportunity for unauthorized access.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-18(1) { \n" + 
			"  name: \"LOCATION OF INFORMATION SYSTEM COMPONENTS | FACILITY SITE \"\n" + 
			"  specification: \"The organization plans the location or site of the facility where the information system resides with regard to physical and environmental hazards and for existing facilities, considers the physical and environmental hazards in its risk mitigation strategy.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-19 { \n" + 
			"  name: \"INFORMATION LEAKAGE \"\n" + 
			"  specification: \"The organization protects the information system from information leakage due to electromagnetic signals emanations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-19(1) { \n" + 
			"  name: \"INFORMATION LEAKAGE | NATIONAL EMISSIONS / TEMPEST POLICIES AND PROCEDURES \"\n" + 
			"  specification: \"The organization ensures that information system components, associated data communications, and networks are protected in accordance with national emissions and TEMPEST policies and procedures based on the security category or classification of the information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-20 { \n" + 
			"  name: \"ASSET MONITORING AND TRACKING \"\n" + 
			"  specification: \"The organization: a. Employs [Assignment: organization-defined asset location technologies] to track and monitor the location and movement of [Assignment: organization-defined assets] within [Assignment: organization-defined controlled areas]; and b. Ensures that asset location technologies are employed in accordance with applicable federal laws, Executive Orders, directives, regulations, policies, standards, and guidance.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PL-1 { \n" + 
			"  name: \"SECURITY PLANNING POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A security planning policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the security planning policy and associated security planning controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Security planning policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Security planning procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-2 { \n" + 
			"  name: \"SYSTEM SECURITY PLAN \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops a security plan for the information system that:\n" + 
			"				1. Is consistent with the organization’s enterprise architecture;\n" + 
			"				2. Explicitly defines the authorization boundary for the system;\n" + 
			"				3. Describes the operational context of the information system in terms of missions and business processes;\n" + 
			"				4. Provides the security categorization of the information system including supporting rationale;\n" + 
			"				5. Describes the operational environment for the information system and relationships with or connections to other information systems;\n" + 
			"				6. Provides an overview of the security requirements for the system;\n" + 
			"				7. Identifies any relevant overlays, if applicable;\n" + 
			"				8. Describes the security controls in place or planned for meeting those requirements including a rationale for the tailoring decisions; and\n" + 
			"				9. Is reviewed and approved by the authorizing official or designated representative prior to plan implementation;\n" + 
			"			b. Distributes copies of the security plan and communicates subsequent changes to the plan to [Assignment: organization-defined personnel or roles];\n" + 
			"			c. Reviews the security plan for the information system [Assignment: organization-defined frequency];\n" + 
			"			d. Updates the plan to address changes to the information system/environment of operation or problems identified during plan implementation or security control assessments; and\n" + 
			"			e. Protects the security plan from unauthorized disclosure and modification.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-2(1) { \n" + 
			"  name: \"SYSTEM SECURITY PLAN | CONCEPT OF OPERATIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PL-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-2(2) { \n" + 
			"  name: \"SYSTEM SECURITY PLAN | FUNCTIONAL ARCHITECTURE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PL-8.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-2(3) { \n" + 
			"  name: \"SYSTEM SECURITY PLAN | PLAN / COORDINATE WITH OTHER ORGANIZATIONAL ENTITIES \"\n" + 
			"  specification: \"The organization plans and coordinates security-related activities affecting the information system with [Assignment: organization-defined individuals or groups] before conducting such activities in order to reduce the impact on other organizational entities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-3 { \n" + 
			"  name: \"SYSTEM SECURITY PLAN UPDATE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PL-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-4 { \n" + 
			"  name: \"RULES OF BEHAVIOR \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Establishes and makes readily available to individuals requiring access to the information system, the rules that describe their responsibilities and expected behavior with regard to information and information system usage;\n" + 
			"			b. Receives a signed acknowledgment from such individuals, indicating that they have read, understand, and agree to abide by the rules of behavior, before authorizing access to information and the information system;\n" + 
			"			c. Reviews and updates the rules of behavior [Assignment: organization-defined frequency]; and\n" + 
			"			d. Requires individuals who have signed a previous version of the rules of behavior to read and re-sign when the rules of behavior are revised/updated.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-4(1) { \n" + 
			"  name: \"RULES OF BEHAVIOR | SOCIAL MEDIA AND NETWORKING RESTRICTIONS \"\n" + 
			"  specification: \"The organization includes in the rules of behavior, explicit restrictions on the use of social media/networking sites and posting organizational information on public websites.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-5 { \n" + 
			"  name: \"PRIVACY IMPACT ASSESSMENT \"\n" + 
			"  specification: \"Withdrawn: Incorporated into Appendix J, AR-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-6 { \n" + 
			"  name: \"SECURITY-RELATED ACTIVITY PLANNING \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PL-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-7 { \n" + 
			"  name: \"SECURITY CONCEPT OF OPERATIONS \"\n" + 
			"  specification: \"The organization:a. Develops a security Concept of Operations (CONOPS) for the information system containing at a minimum, how the organization intends to operate the system from the perspective of information security; and b. Reviews and updates the CONOPS [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-8 { \n" + 
			"  name: \"INFORMATION SECURITY ARCHITECTURE \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops an information security architecture for the information system that:\n" + 
			"				1. Describes the overall philosophy, requirements, and approach to be taken with regard to protecting the confidentiality, integrity, and availability of organizational information;\n" + 
			"				2. Describes how the information security architecture is integrated into and supports the enterprise architecture; and\n" + 
			"				3. Describes any information security assumptions about, and dependencies on, external services;\n" + 
			"			b. Reviews and updates the information security architecture [Assignment: organization-defined frequency] to reflect updates in the enterprise architecture; and\n" + 
			"			c. Ensures that planned information security architecture changes are reflected in the security plan, the security Concept of Operations (CONOPS), and organizational procurements/acquisitions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-8(1) { \n" + 
			"  name: \"INFORMATION SECURITY ARCHITECTURE | DEFENSE-IN-DEPTH \"\n" + 
			"  specification: \"The organization designs its security architecture using a defense-in-depth approach that: (a) Allocates [Assignment: organization-defined security safeguards] to [Assignment: organization-defined locations and architectural layers]; and (b) Ensures that the allocated security safeguards operate in a coordinated and mutually reinforcing manner.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-8(2) { \n" + 
			"  name: \"INFORMATION SECURITY ARCHITECTURE | SUPPLIER DIVERSITY \"\n" + 
			"  specification: \"The organization requires that [Assignment: organization-defined security safeguards] allocated to [Assignment: organization-defined locations and architectural layers] are obtained from different suppliers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-9 { \n" + 
			"  name: \"CENTRAL MANAGEMENT \"\n" + 
			"  specification: \"The organization centrally manages [Assignment: organization-defined security controls and related processes].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PS-1 { \n" + 
			"  name: \"PERSONNEL SECURITY POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A personnel security policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the personnel security policy and associated personnel security controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Personnel security policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Personnel security procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-2 { \n" + 
			"  name: \"POSITION RISK DESIGNATION \"\n" + 
			"  specification: \"The organization: a. Assigns a risk designation to all organizational positions; b. Establishes screening criteria for individuals filling those positions; and c. Reviews and updates position risk designations [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-3 { \n" + 
			"  name: \"PERSONNEL SCREENING \"\n" + 
			"  specification: \"The organization: a. Screens individuals prior to authorizing access to the information system; and b. Rescreens individuals according to [Assignment: organization-defined conditions requiring rescreening and, where rescreening is so indicated, the frequency of such rescreening].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-3(1) { \n" + 
			"  name: \"PERSONNEL SCREENING | CLASSIFIED INFORMATION \"\n" + 
			"  specification: \"The organization ensures that individuals accessing an information system processing, storing, or transmitting classified information are cleared and indoctrinated to the highest classification level of the information to which they have access on the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-3(2) { \n" + 
			"  name: \"PERSONNEL SCREENING | FORMAL INDOCTRINATION \"\n" + 
			"  specification: \"The organization ensures that individuals accessing an information system processing, storing, or transmitting types of classified information which require formal indoctrination, are formally indoctrinated for all of the relevant types of information to which they have access on the system\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-3(3) { \n" + 
			"  name: \"PERSONNEL SCREENING | INFORMATION WITH SPECIAL PROTECTION MEASURES \"\n" + 
			"  specification: \"The organization ensures that individuals accessing an information system processing, storing, or transmitting information requiring special protection: (a) Have valid access authorizations that are demonstrated by assigned official government duties; and (b) Satisfy [Assignment: organization-defined additional personnel screening criteria].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-4 { \n" + 
			"  name: \"PERSONNEL TERMINATION \"\n" + 
			"  specification: \"The organization, upon termination of individual employment:\n" + 
			"			a. Disables information system access within [Assignment: organization-defined time period];\n" + 
			"			b. Terminates/revokes any authenticators/credentials associated with the individual;\n" + 
			"			c. Conducts exit interviews that include a discussion of [Assignment: organization-defined information security topics];\n" + 
			"			d. Retrieves all security-related organizational information system-related property;\n" + 
			"			e. Retains access to organizational information and information systems formerly controlled by terminated individual; and\n" + 
			"			f. Notifies [Assignment: organization-defined personnel or roles] within [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-4(1) { \n" + 
			"  name: \"PERSONNEL TERMINATION | POST-EMPLOYMENT REQUIREMENTS \"\n" + 
			"  specification: \"The organization: (a) Notifies terminated individuals of applicable, legally binding post-employment requirements for the protection of organizational information; and (b) Requires terminated individuals to sign an acknowledgment of post-employment requirements as part of the organizational termination process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-4(2) { \n" + 
			"  name: \"PERSONNEL TERMINATION | AUTOMATED NOTIFICATION \"\n" + 
			"  specification: \"The organization employs automated mechanisms to notify [Assignment: organization-defined personnel or roles] upon termination of an individual.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-5 { \n" + 
			"  name: \"PERSONNEL TRANSFER \"\n" + 
			"  specification: \"The organization: a. Reviews and confirms ongoing operational need for current logical and physical access authorizations to information systems/facilities when individuals are reassigned or transferred to other positions within the organization; b. Initiates [Assignment: organization-defined transfer or reassignment actions] within [Assignment: organization-defined time period following the formal transfer action]; c. Modifies access authorization as needed to correspond with any changes in operational need due to reassignment or transfer; and d. Notifies [Assignment: organization-defined personnel or roles] within [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-6 { \n" + 
			"  name: \"ACCESS AGREEMENTS \"\n" + 
			"  specification: \"The organization: \n" + 
			"			a. Develops and documents access agreements for organizational information systems;\n" + 
			"			b. Reviews and updates the access agreements [Assignment: organization-defined frequency]; and\n" + 
			"			c. Ensures that individuals requiring access to organizational information and information systems:\n" + 
			"				1. Sign appropriate access agreements prior to being granted access; and\n" + 
			"				2. Re-sign access agreements to maintain access to organizational information systems when access agreements have been updated or [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-6(1) { \n" + 
			"  name: \"ACCESS AGREEMENTS | INFORMATION REQUIRING SPECIAL PROTECTION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PS-3.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-6(2) { \n" + 
			"  name: \"ACCESS AGREEMENTS | CLASSIFIED INFORMATION REQUIRING SPECIAL PROTECTION \"\n" + 
			"  specification: \"The organization ensures that access to classified information requiring special protection is granted only to individuals who: (a) Have a valid access authorization that is demonstrated by assigned official government duties; (b) Satisfy associated personnel security criteria; and (c) Have read, understood, and signed a nondisclosure agreement.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-6(3) { \n" + 
			"  name: \"ACCESS AGREEMENTS | POST-EMPLOYMENT REQUIREMENTS \"\n" + 
			"  specification: \"The organization:(a) Notifies individuals of applicable, legally binding post-employment requirements for protection of organizational information; and (b) Requires individuals to sign an acknowledgment of these requirements, if applicable, as part of granting initial access to covered information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-7 { \n" + 
			"  name: \"THIRD-PARTY PERSONNEL SECURITY \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Establishes personnel security requirements including security roles and responsibilities for third-party providers;\n" + 
			"			b. Requires third-party providers to comply with personnel security policies and procedures established by the organization;\n" + 
			"			c. Documents personnel security requirements;\n" + 
			"			d. Requires third-party providers to notify [Assignment: organization-defined personnel or roles] of any personnel transfers or terminations of third-party personnel who possess organizational credentials and/or badges, or who have information system privileges within [Assignment: organization-defined time period]; and\n" + 
			"			e. Monitors provider compliance.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-8 { \n" + 
			"  name: \"PERSONNEL SANCTIONS \"\n" + 
			"  specification: \"The organization: a. Employs a formal sanctions process for individuals failing to comply with established information security policies and procedures; and b. Notifies [Assignment: organization-defined personnel or roles] within [Assignment: organization-defined time period] when a formal employee sanctions process is initiated, identifying the individual sanctioned and the reason for the sanction.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control RA-1 { \n" + 
			"  name: \"RISK ASSESSMENT POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A risk assessment policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the risk assessment policy and associated risk assessment controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Risk assessment policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Risk assessment procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-2 { \n" + 
			"  name: \"SECURITY CATEGORIZATION \"\n" + 
			"  specification: \"The organization: a. Categorizes information and the information system in accordance with applicable federal laws, Executive Orders, directives, policies, regulations, standards, and guidance; b. Documents the security categorization results (including supporting rationale) in the security plan for the information system; and c. Ensures that the authorizing official or authorizing official designated representative reviews and approves the security categorization decision.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-3 { \n" + 
			"  name: \"RISK ASSESSMENT \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Conducts an assessment of risk, including the likelihood and magnitude of harm, from the unauthorized access, use, disclosure, disruption, modification, or destruction of the information system and the information it processes, stores, or transmits;\n" + 
			"			b. Documents risk assessment results in [Selection: security plan; risk assessment report; [Assignment: organization-defined document]];\n" + 
			"			c. Reviews risk assessment results [Assignment: organization-defined frequency];\n" + 
			"			d. Disseminates risk assessment results to [Assignment: organization-defined personnel or roles]; and\n" + 
			"			e. Updates the risk assessment [Assignment: organization-defined frequency] or whenever there are significant changes to the information system or environment of operation (including the identification of new threats and vulnerabilities), or other conditions that may impact the security state of the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-4 { \n" + 
			"  name: \"RISK ASSESSMENT UPDATE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into RA-3.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5 { \n" + 
			"  name: \"VULNERABILITY SCANNING \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Scans for vulnerabilities in the information system and hosted applications [Assignment: organization-defined frequency and/or randomly in accordance with organization-defined process] and when new vulnerabilities potentially affecting the system/applications are identified and reported;\n" + 
			"			b. Employs vulnerability scanning tools and techniques that facilitate interoperability among tools and automate parts of the vulnerability management process by using standards for:\n" + 
			"				1. Enumerating platforms, software flaws, and improper configurations;\n" + 
			"				2. Formatting checklists and test procedures; and\n" + 
			"				3. Measuring vulnerability impact;\n" + 
			"			c. Analyzes vulnerability scan reports and results from security control assessments;\n" + 
			"			d. Remediates legitimate vulnerabilities [Assignment: organization-defined response times] in accordance with an organizational assessment of risk; and\n" + 
			"			e. Shares information obtained from the vulnerability scanning process and security control assessments with [Assignment: organization-defined personnel or roles] to help eliminate similar vulnerabilities in other information systems (i.e., systemic weaknesses or deficiencies).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(1) { \n" + 
			"  name: \"VULNERABILITY SCANNING | UPDATE TOOL CAPABILITY \"\n" + 
			"  specification: \"The organization employs vulnerability scanning tools that include the capability to readily update the information system vulnerabilities to be scanned.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(2) { \n" + 
			"  name: \"VULNERABILITY SCANNING | UPDATE BY FREQUENCY / PRIOR TO NEW SCAN / WHEN IDENTIFIED \"\n" + 
			"  specification: \"The organization updates the information system vulnerabilities scanned [Selection (one or more): [Assignment: organization-defined frequency]; prior to a new scan; when new vulnerabilities are identified and reported].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(3) { \n" + 
			"  name: \"VULNERABILITY SCANNING | BREADTH / DEPTH OF COVERAGE \"\n" + 
			"  specification: \"The organization employs vulnerability scanning procedures that can identify the breadth and depth of coverage (i.e., information system components scanned and vulnerabilities checked).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(4) { \n" + 
			"  name: \"VULNERABILITY SCANNING | DISCOVERABLE INFORMATION \"\n" + 
			"  specification: \"The organization determines what information about the information system is discoverable by adversaries and subsequently takes [Assignment: organization-defined corrective actions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(5) { \n" + 
			"  name: \"VULNERABILITY SCANNING | PRIVILEGED ACCESS \"\n" + 
			"  specification: \"The information system implements privileged access authorization to [Assignment: organization-identified information system components] for selected [Assignment: organization-defined vulnerability scanning activities].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(6) { \n" + 
			"  name: \"VULNERABILITY SCANNING | AUTOMATED TREND ANALYSES \"\n" + 
			"  specification: \"The organization employs automated mechanisms to compare the results of vulnerability scans over time to determine trends in information system vulnerabilities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(7) { \n" + 
			"  name: \"VULNERABILITY SCANNING | AUTOMATED DETECTION AND NOTIFICATION OF UNAUTHORIZED COMPONENTS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-8.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(8) { \n" + 
			"  name: \"VULNERABILITY SCANNING | REVIEW HISTORIC AUDIT LOGS \"\n" + 
			"  specification: \"The organization reviews historic audit logs to determine if a vulnerability identified in the information system has been previously exploited.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(9) { \n" + 
			"  name: \"VULNERABILITY SCANNING | PENETRATION TESTING AND ANALYSES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CA-8.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(10) { \n" + 
			"  name: \"VULNERABILITY SCANNING | CORRELATE SCANNING INFORMATION \"\n" + 
			"  specification: \"The organization correlates the output from vulnerability scanning tools to determine the presence of multi-vulnerability/multi-hop attack vectors.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-6 { \n" + 
			"  name: \"TECHNICAL SURVEILLANCE COUNTERMEASURES SURVEY \"\n" + 
			"  specification: \"The organization employs a technical surveillance countermeasures survey at [Assignment: organization-defined locations] [Selection (one or more): [Assignment: organization-defined frequency]; [Assignment: organization-defined events or indicators occur]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control SA-1 { \n" + 
			"  name: \"SYSTEM AND SERVICES ACQUISITION POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A system and services acquisition policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the system and services acquisition policy and associated system and services acquisition controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. System and services acquisition policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. System and services acquisition procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-2 { \n" + 
			"  name: \"ALLOCATION OF RESOURCES \"\n" + 
			"  specification: \"The organization: a. Determines information security requirements for the information system or information system service in mission/business process planning; b. Determines, documents, and allocates the resources required to protect the information system or information system service as part of its capital planning and investment control process; and c. Establishes a discrete line item for information security in organizational programming and budgeting documentation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-3 { \n" + 
			"  name: \"SYSTEM DEVELOPMENT LIFE CYCLE \"\n" + 
			"  specification: \"The organization: a. Manages the information system using [Assignment: organization-defined system development life cycle] that incorporates information security considerations; b. Defines and documents information security roles and responsibilities throughout the system development life cycle; c. Identifies individuals having information security roles and responsibilities; and d. Integrates the organizational information security risk management process into system development life cycle activities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4 { \n" + 
			"  name: \"ACQUISITION PROCESS \"\n" + 
			"  specification: \"The organization includes the following requirements, descriptions, and criteria, explicitly or by reference, in the acquisition contract for the information system, system component, or information system service in accordance with applicable federal laws, Executive Orders, directives, policies, regulations, standards, guidelines, and organizational mission/business needs:a. Security functional requirements; b. Security strength requirements; c. Security assurance requirements; d. Security-related documentation requirements; e. Requirements for protecting security-related documentation; f. Description of the information system development environment and environment in which the system is intended to operate; and g. Acceptance criteria.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(1) { \n" + 
			"  name: \"ACQUISITION PROCESS | FUNCTIONAL PROPERTIES OF SECURITY CONTROLS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to provide a description of the functional properties of the security controls to be employed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(2) { \n" + 
			"  name: \"ACQUISITION PROCESS | DESIGN / IMPLEMENTATION INFORMATION FOR SECURITY CONTROLS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to provide design and implementation information for the security controls to be employed that includes: [Selection (one or more): security-relevant external system interfaces; high-level design; low-level design; source code or hardware schematics; [Assignment: organization-defined design/implementation information]] at [Assignment: organization-defined level of detail].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(3) { \n" + 
			"  name: \"ACQUISITION PROCESS | DEVELOPMENT METHODS / TECHNIQUES / PRACTICES \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to demonstrate the use of a system development life cycle that includes [Assignment: organization-defined state-of-the-practice system/security engineering methods, software development methods, testing/evaluation/validation techniques, and quality control processes].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(4) { \n" + 
			"  name: \"ACQUISITION PROCESS | ASSIGNMENT OF COMPONENTS TO SYSTEMS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-8 (9).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(5) { \n" + 
			"  name: \"ACQUISITION PROCESS | SYSTEM / COMPONENT / SERVICE CONFIGURATIONS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to: (a) Deliver the system, component, or service with [Assignment: organization-defined security configurations] implemented; and (b) Use the configurations as the default for any subsequent system, component, or service reinstallation or upgrade.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(6) { \n" + 
			"  name: \"ACQUISITION PROCESS | USE OF INFORMATION ASSURANCE PRODUCTS \"\n" + 
			"  specification: \"The organization: (a) Employs only government off-the-shelf (GOTS) or commercial off-the-shelf (COTS) information assurance (IA) and IA-enabled information technology products that compose an NSA-approved solution to protect classified information when the networks used to transmit the information are at a lower classification level than the information being transmitted; and (b) Ensures that these products have been evaluated and/or validated by NSA or in accordance with NSA-approved procedures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(7) { \n" + 
			"  name: \"ACQUISITION PROCESS | NIAP-APPROVED PROTECTION PROFILES \"\n" + 
			"  specification: \"The organization: (a) Limits the use of commercially provided information assurance (IA) and IA-enabled information technology products to those products that have been successfully evaluated against a National Information Assurance partnership (NIAP)-approved Protection Profile for a specific technology type, if such a profile exists; and (b) Requires, if no NIAP-approved Protection Profile exists for a specific technology type but a commercially provided information technology product relies on cryptographic functionality to enforce its security policy, that the cryptographic module is FIPS-validated.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(8) { \n" + 
			"  name: \"ACQUISITION PROCESS | CONTINUOUS MONITORING PLAN \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to produce a plan for the continuous monitoring of security control effectiveness that contains [Assignment: organization-defined level of detail].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(9) { \n" + 
			"  name: \"ACQUISITION PROCESS | FUNCTIONS / PORTS / PROTOCOLS / SERVICES IN USE \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to identify early in the system development life cycle, the functions, ports, protocols, and services intended for organizational use.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(10) { \n" + 
			"  name: \"ACQUISITION PROCESS | USE OF APPROVED PIV PRODUCTS \"\n" + 
			"  specification: \"The organization employs only information technology products on the FIPS 201-approved products list for Personal Identity Verification (PIV) capability implemented within organizational information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-5 { \n" + 
			"  name: \"INFORMATION SYSTEM DOCUMENTATION \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Obtains administrator documentation for the information system, system component, or information system service that describes:\n" + 
			"				1. Secure configuration, installation, and operation of the system, component, or service;\n" + 
			"				2. Effective use and maintenance of security functions/mechanisms; and\n" + 
			"				3. Known vulnerabilities regarding configuration and use of administrative (i.e., privileged) functions;\n" + 
			"			b. Obtains user documentation for the information system, system component, or information system service that describes:\n" + 
			"				1. User-accessible security functions/mechanisms and how to effectively use those security functions/mechanisms;\n" + 
			"				2. Methods for user interaction, which enables individuals to use the system, component, or service in a more secure manner; and\n" + 
			"				3. User responsibilities in maintaining the security of the system, component, or service;\n" + 
			"			c. Documents attempts to obtain information system, system component, or information system service documentation when such documentation is either unavailable or nonexistent and takes [Assignment: organization-defined actions] in response;\n" + 
			"			d. Protects documentation as required, in accordance with the risk management strategy; and\n" + 
			"			e. Distributes documentation to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-5(1) { \n" + 
			"  name: \"INFORMATION SYSTEM DOCUMENTATION | FUNCTIONAL PROPERTIES OF SECURITY CONTROLS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-4 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-5(2) { \n" + 
			"  name: \"INFORMATION SYSTEM DOCUMENTATION | SECURITY-RELEVANT EXTERNAL SYSTEM INTERFACES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-4 (2).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-5(3) { \n" + 
			"  name: \"INFORMATION SYSTEM DOCUMENTATION | HIGH-LEVEL DESIGN \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-4 (2).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-5(4) { \n" + 
			"  name: \"INFORMATION SYSTEM DOCUMENTATION | LOW-LEVEL DESIGN \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-4 (2).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-5(5) { \n" + 
			"  name: \"INFORMATION SYSTEM DOCUMENTATION | SOURCE CODE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-4 (2).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-6 { \n" + 
			"  name: \"SOFTWARE USAGE RESTRICTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-10 and SI-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-7 { \n" + 
			"  name: \"USER-INSTALLED SOFTWARE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-11 and SI-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-8 { \n" + 
			"  name: \"SECURITY ENGINEERING PRINCIPLES \"\n" + 
			"  specification: \"The organization applies information system security engineering principles in the specification, design, development, implementation, and modification of the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-9 { \n" + 
			"  name: \"EXTERNAL INFORMATION SYSTEM SERVICES \"\n" + 
			"  specification: \"The organization: a. Requires that providers of external information system services comply with organizational information security requirements and employ [Assignment: organization-defined security controls] in accordance with applicable federal laws, Executive Orders, directives, policies, regulations, standards, and guidance; b. Defines and documents government oversight and user roles and responsibilities with regard to external information system services; and c. Employs [Assignment: organization-defined processes, methods, and techniques] to monitor security control compliance by external service providers on an ongoing basis.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-9(1) { \n" + 
			"  name: \"EXTERNAL INFORMATION SYSTEMS | RISK ASSESSMENTS / ORGANIZATIONAL APPROVALS \"\n" + 
			"  specification: \"The organization: (a) Conducts an organizational assessment of risk prior to the acquisition or outsourcing of dedicated information security services; and (b) Ensures that the acquisition or outsourcing of dedicated information security services is approved by [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-9(2) { \n" + 
			"  name: \"EXTERNAL INFORMATION SYSTEMS | IDENTIFICATION OF FUNCTIONS / PORTS / PROTOCOLS / SERVICES \"\n" + 
			"  specification: \"The organization requires providers of [Assignment: organization-defined external information system services] to identify the functions, ports, protocols, and other services required for the use of such services.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-9(3) { \n" + 
			"  name: \"EXTERNAL INFORMATION SYSTEMS | ESTABLISH / MAINTAIN TRUST RELATIONSHIP WITH PROVIDERS \"\n" + 
			"  specification: \"The organization establishes, documents, and maintains trust relationships with external service providers based on [Assignment: organization-defined security requirements, properties, factors, or conditions defining acceptable trust relationships].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-9(4) { \n" + 
			"  name: \"EXTERNAL INFORMATION SYSTEMS | CONSISTENT INTERESTS OF CONSUMERS AND PROVIDERS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to ensure that the interests of [Assignment: organization-defined external service providers] are consistent with and reflect organizational interests.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-9(5) { \n" + 
			"  name: \"EXTERNAL INFORMATION SYSTEMS | PROCESSING, STORAGE, AND SERVICE LOCATION \"\n" + 
			"  specification: \"The organization restricts the location of [Selection (one or more): information processing; information/data; information system services] to [Assignment: organization-defined locations] based on [Assignment: organization-defined requirements or conditions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10 { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to:\n" + 
			"			a. Perform configuration management during system, component, or service [Selection (one or more): design; development; implementation; operation];\n" + 
			"			b. Document, manage, and control the integrity of changes to [Assignment: organization-defined configuration items under configuration management];\n" + 
			"			c. Implement only organization-approved changes to the system, component, or service;\n" + 
			"			d. Document approved changes to the system, component, or service and the potential security impacts of such changes; and\n" + 
			"			e. Track security flaws and flaw resolution within the system, component, or service and report findings to [Assignment: organization-defined personnel].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10(1) { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT | SOFTWARE / FIRMWARE INTEGRITY VERIFICATION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to enable integrity verification of software and firmware components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10(2) { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT | ALTERNATIVE CONFIGURATION MANAGEMENT PROCESSES \"\n" + 
			"  specification: \"The organization provides an alternate configuration management process using organizational personnel in the absence of a dedicated developer configuration management team.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10(3) { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT | HARDWARE INTEGRITY VERIFICATION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to enable integrity verification of hardware components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10(4) { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT | TRUSTED GENERATION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to employ tools for comparing newly generated versions of security-relevant hardware descriptions and software/firmware source and object code with previous versions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10(5) { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT | MAPPING INTEGRITY FOR VERSION CONTROL \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to maintain the integrity of the mapping between the master build data (hardware drawings and software/firmware code) describing the current version of security-relevant hardware, software, and firmware and the on-site master copy of the data for the current version.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10(6) { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT | TRUSTED DISTRIBUTION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to execute procedures for ensuring that security-relevant hardware, software, and firmware updates distributed to the organization are exactly as specified by the master copies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11 { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to:\n" + 
			"			a. Create and implement a security assessment plan;\n" + 
			"			b. Perform [Selection (one or more): unit; integration; system; regression] testing/evaluation at [Assignment: organization-defined depth and coverage];\n" + 
			"			c. Produce evidence of the execution of the security assessment plan and the results of the security testing/evaluation;\n" + 
			"			d. Implement a verifiable flaw remediation process; and\n" + 
			"			e. Correct flaws identified during security testing/evaluation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(1) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | STATIC CODE ANALYSIS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to employ static code analysis tools to identify common flaws and document the results of the analysis.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(2) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | THREAT AND VULNERABILITY ANALYSES \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to perform threat and vulnerability analyses and subsequent testing/evaluation of the as-built system, component, or service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(3) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | INDEPENDENT VERIFICATION OF ASSESSMENT PLANS / EVIDENCE \"\n" + 
			"  specification: \"The organization: (a) Requires an independent agent satisfying [Assignment: organization-defined independence criteria] to verify the correct implementation of the developer security assessment plan and the evidence produced during security testing/evaluation; and (b) Ensures that the independent agent is either provided with sufficient information to complete the verification process or granted the authority to obtain such information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(4) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | MANUAL CODE REVIEWS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to perform a manual code review of [Assignment: organization-defined specific code] using [Assignment: organization-defined processes, procedures, and/or techniques].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(5) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | PENETRATION TESTING \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to perform penetration testing at [Assignment: organization-defined breadth/depth] and with [Assignment: organization-defined constraints].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(6) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | ATTACK SURFACE REVIEWS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to perform attack surface reviews.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(7) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | VERIFY SCOPE OF TESTING / EVALUATION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to verify that the scope of security testing/evaluation provides complete coverage of required security controls at [Assignment: organization-defined depth of testing/evaluation].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(8) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | DYNAMIC CODE ANALYSIS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to employ dynamic code analysis tools to identify common flaws and document the results of the analysis.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12 { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION \"\n" + 
			"  specification: \"The organization protects against supply chain threats to the information system, system component, or information system service by employing [Assignment: organization-defined security safeguards] as part of a comprehensive, defense-in-breadth information security strategy.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(1) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | ACQUISITION STRATEGIES / TOOLS / METHODS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined tailored acquisition strategies, contract tools, and procurement methods] for the purchase of the information system, system component, or information system service from suppliers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(2) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | SUPPLIER REVIEWS \"\n" + 
			"  specification: \"The organization conducts a supplier review prior to entering into a contractual agreement to acquire the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(3) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | TRUSTED SHIPPING AND WAREHOUSING \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-12 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(4) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | DIVERSITY OF SUPPLIERS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-12 (13).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(5) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | LIMITATION OF HARM \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to limit harm from potential adversaries identifying and targeting the organizational supply chain.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(6) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | MINIMIZING PROCUREMENT TIME \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-12 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(7) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | ASSESSMENTS PRIOR TO SELECTION / ACCEPTANCE / UPDATE \"\n" + 
			"  specification: \"The organization conducts an assessment of the information system, system component, or information system service prior to selection, acceptance, or update.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(8) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | USE OF ALL-SOURCE INTELLIGENCE \"\n" + 
			"  specification: \"The organization uses all-source intelligence analysis of suppliers and potential suppliers of the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(9) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | OPERATIONS SECURITY \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined Operations Security (OPSEC) safeguards] in accordance with classification guides to protect supply chain-related information for the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(10) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | VALIDATE AS GENUINE AND NOT ALTERED \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to validate that the information system or system component received is genuine and has not been altered.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(11) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | PENETRATION TESTING / ANALYSIS OF ELEMENTS, PROCESSES, AND ACTORS \"\n" + 
			"  specification: \"The organization employs [Selection (one or more): organizational analysis, independent third-party analysis, organizational penetration testing, independent third-party penetration testing] of [Assignment: organization-defined supply chain elements, processes, and actors] associated with the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(12) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | INTER-ORGANIZATIONAL AGREEMENTS \"\n" + 
			"  specification: \"The organization establishes inter-organizational agreements and procedures with entities involved in the supply chain for the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(13) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | CRITICAL INFORMATION SYSTEM COMPONENTS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to ensure an adequate supply of [Assignment: organization-defined critical information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(14) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | IDENTITY AND TRACEABILITY \"\n" + 
			"  specification: \"The organization establishes and retains unique identification of [Assignment: organization-defined supply chain elements, processes, and actors] for the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(15) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | PROCESSES TO ADDRESS WEAKNESSES OR DEFICIENCIES \"\n" + 
			"  specification: \"The organization establishes a process to address weaknesses or deficiencies in supply chain elements identified during independent or organizational assessments of such elements.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-13 { \n" + 
			"  name: \"TRUSTWORTHINESS \"\n" + 
			"  specification: \"The organization: a. Describes the trustworthiness required in the [Assignment: organization-defined information system, information system component, or information system service] supporting its critical missions/business functions; and b. Implements [Assignment: organization-defined assurance overlay] to achieve such trustworthiness.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-14 { \n" + 
			"  name: \"CRITICALITY ANALYSIS \"\n" + 
			"  specification: \"The organization identifies critical information system components and functions by performing a criticality analysis for [Assignment: organization-defined information systems, information system components, or information system services] at [Assignment: organization-defined decision points in the system development life cycle].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15 { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Requires the developer of the information system, system component, or information system service to follow a documented development process that:\n" + 
			"				1. Explicitly addresses security requirements;\n" + 
			"				2. Identifies the standards and tools used in the development process;\n" + 
			"				3. Documents the specific tool options and tool configurations used in the development process; and\n" + 
			"				4. Documents, manages, and ensures the integrity of changes to the process and/or tools used in development; and\n" + 
			"			b. Reviews the development process, standards, tools, and tool options/configurations [Assignment: organization-defined frequency] to determine if the process, standards, tools, and tool options/configurations selected and employed can satisfy [Assignment: organization-defined security requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(1) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | QUALITY METRICS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to: (a) Define quality metrics at the beginning of the development process; and (b) Provide evidence of meeting the quality metrics [Selection (one or more): [Assignment: organization-defined frequency]; [Assignment: organization-defined program review milestones]; upon delivery].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(2) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | SECURITY TRACKING TOOLS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to select and employ a security tracking tool for use during the development process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(3) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | CRITICALITY ANALYSIS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to perform a criticality analysis at [Assignment: organization-defined breadth/depth] and at [Assignment: organization-defined decision points in the system development life cycle].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(4) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | THREAT MODELING / VULNERABILITY ANALYSIS \"\n" + 
			"  specification: \"The organization requires that developers perform threat modeling and a vulnerability analysis for the information system at [Assignment: organization-defined breadth/depth] that: (a) Uses [Assignment: organization-defined information concerning impact, environment of operations, known or assumed threats, and acceptable risk levels]; (b) Employs [Assignment: organization-defined tools and methods]; and (c) Produces evidence that meets [Assignment: organization-defined acceptance criteria].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(5) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | ATTACK SURFACE REDUCTION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to reduce attack surfaces to [Assignment: organization-defined thresholds].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(6) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | CONTINUOUS IMPROVEMENT \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to implement an explicit process to continuously improve the development process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(7) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | AUTOMATED VULNERABILITY ANALYSIS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to: (a) Perform an automated vulnerability analysis using [Assignment: organization-defined tools]; (b) Determine the exploitation potential for discovered vulnerabilities; (c) Determine potential risk mitigations for delivered vulnerabilities; and (d) Deliver the outputs of the tools and results of the analysis to [Assignment: organization-defined personnel or roles].		\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(8) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | REUSE OF THREAT / VULNERABILITY INFORMATION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to use threat modeling and vulnerability analyses from similar systems, components, or services to inform the current development process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(9) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | USE OF LIVE DATA \"\n" + 
			"  specification: \"The organization approves, documents, and controls the use of live data in development and test environments for the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(10) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | INCIDENT RESPONSE PLAN \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to provide an incident response plan.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(11) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | ARCHIVE INFORMATION SYSTEM / COMPONENT \"\n" + 
			"  specification: \"The organization requires the developer of the information system or system component to archive the system or component to be released or delivered together with the corresponding evidence supporting the final security review.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-16 { \n" + 
			"  name: \"DEVELOPER-PROVIDED TRAINING \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to provide [Assignment: organization-defined training] on the correct use and operation of the implemented security functions, controls, and/or mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17 { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to produce a design specification and security architecture that: a. Is consistent with and supportive of the organization’s security architecture which is established within and is an integrated part of the organization’s enterprise architecture; b. Accurately and completely describes the required security functionality, and the allocation of security controls among physical and logical components; and c. Expresses how individual security functions, mechanisms, and services work together to provide required security capabilities and a unified approach to protection.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(1) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | FORMAL POLICY MODEL \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to: (a) Produce, as an integral part of the development process, a formal policy model describing the [Assignment: organization-defined elements of organizational security policy] to be enforced; and (b) Prove that the formal policy model is internally consistent and sufficient to enforce the defined elements of the organizational security policy when implemented. \n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(2) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | SECURITY-RELEVANT COMPONENTS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to: (a) Define security-relevant hardware, software, and firmware; and (b) Provide a rationale that the definition for security-relevant hardware, software, and firmware is complete.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(3) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | FORMAL CORRESPONDENCE \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to:\n" + 
			"			(a) Produce, as an integral part of the development process, a formal top-level specification that specifies the interfaces to security-relevant hardware, software, and firmware in terms of exceptions, error messages, and effects;\n" + 
			"			(b) Show via proof to the extent feasible with additional informal demonstration as necessary, that the formal top-level specification is consistent with the formal policy model;\n" + 
			"			(c) Show via informal demonstration, that the formal top-level specification completely covers the interfaces to security-relevant hardware, software, and firmware;\n" + 
			"			(d) Show that the formal top-level specification is an accurate description of the implemented security-relevant hardware, software, and firmware; and\n" + 
			"			(e) Describe the security-relevant hardware, software, and firmware mechanisms not addressed in the formal top-level specification but strictly internal to the security-relevant hardware, software, and firmware.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(4) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | INFORMAL CORRESPONDENCE \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to:(a) Produce, as an integral part of the development process, an informal descriptive top-level specification that specifies the interfaces to security-relevant hardware, software, and firmware in terms of exceptions, error messages, and effects;			(b) Show via [Selection: informal demonstration, convincing argument with formal methods as feasible] that the descriptive top-level specification is consistent with the formal policy model;			(c) Show via informal demonstration, that the descriptive top-level specification completely covers the interfaces to security-relevant hardware, software, and firmware;			(d) Show that the descriptive top-level specification is an accurate description of the interfaces to security-relevant hardware, software, and firmware; and			(e) Describe the security-relevant hardware, software, and firmware mechanisms not addressed in the descriptive top-level specification but strictly internal to the security-relevant hardware, software, and firmware.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(5) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | CONCEPTUALLY SIMPLE DESIGN \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to: (a) Design and structure the security-relevant hardware, software, and firmware to use a complete, conceptually simple protection mechanism with precisely defined semantics; and (b) Internally structure the security-relevant hardware, software, and firmware with specific regard for this mechanism.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(6) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | CONCEPTUALLY SIMPLE DESIGN \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to structure security-relevant hardware, software, and firmware to facilitate testing.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(7) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | CONCEPTUALLY SIMPLE DESIGN \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to structure security-relevant hardware, software, and firmware to facilitate controlling access with least privilege.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-18 { \n" + 
			"  name: \"TAMPER RESISTANCE AND DETECTION \"\n" + 
			"  specification: \"The organization implements a tamper protection program for the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-18(1) { \n" + 
			"  name: \"TAMPER RESISTANCE AND DETECTION | MULTIPLE PHASES OF SDLC \"\n" + 
			"  specification: \"The organization employs anti-tamper technologies and techniques during multiple phases in the system development life cycle including design, development, integration, operations, and maintenance.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-18(2) { \n" + 
			"  name: \"TAMPER RESISTANCE AND DETECTION | INSPECTION OF INFORMATION SYSTEMS, COMPONENTS, OR DEVICES \"\n" + 
			"  specification: \"The organization inspects [Assignment: organization-defined information systems, system components, or devices] [Selection (one or more): at random; at [Assignment: organization-defined frequency], upon [Assignment: organization-defined indications of need for inspection]] to detect tampering.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-19 { \n" + 
			"  name: \"COMPONENT AUTHENTICITY \"\n" + 
			"  specification: \"The organization: a. Develops and implements anti-counterfeit policy and procedures that include the means to detect and prevent counterfeit components from entering the information system; and b. Reports counterfeit information system components to [Selection (one or more): source of counterfeit component; [Assignment: organization-defined external reporting organizations]; [Assignment: organization-defined personnel or roles]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-19(1) { \n" + 
			"  name: \"COMPONENT AUTHENTICITY | ANTI-COUNTERFEIT TRAINING \"\n" + 
			"  specification: \"The organization trains [Assignment: organization-defined personnel or roles] to detect counterfeit information system components (including hardware, software, and firmware).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-19(2) { \n" + 
			"  name: \"COMPONENT AUTHENTICITY | CONFIGURATION CONTROL FOR COMPONENT SERVICE / REPAIR \"\n" + 
			"  specification: \"The organization maintains configuration control over [Assignment: organization-defined information system components] awaiting service/repair and serviced/repaired components awaiting return to service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-19(3) { \n" + 
			"  name: \"COMPONENT AUTHENTICITY | COMPONENT DISPOSAL \"\n" + 
			"  specification: \"The organization disposes of information system components using [Assignment: organization-defined techniques and methods].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-19(4) { \n" + 
			"  name: \"COMPONENT AUTHENTICITY | ANTI-COUNTERFEIT SCANNING \"\n" + 
			"  specification: \"The organization scans for counterfeit information system components [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-20 { \n" + 
			"  name: \"CUSTOMIZED DEVELOPMENT OF CRITICAL COMPONENTS \"\n" + 
			"  specification: \"The organization re-implements or custom develops [Assignment: organization-defined critical information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-21 { \n" + 
			"  name: \"DEVELOPER SCREENING \"\n" + 
			"  specification: \"The organization requires that the developer of [Assignment: organization-defined information system, system component, or information system service]: a. Have appropriate access authorizations as determined by assigned [Assignment: organization-defined official government duties]; and b. Satisfy [Assignment: organization-defined additional personnel screening criteria].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-21(1) { \n" + 
			"  name: \"DEVELOPER SCREENING | VALIDATION OF SCREENING \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service take [Assignment: organization-defined actions] to ensure that the required access authorizations and screening criteria are satisfied.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-22 { \n" + 
			"  name: \"UNSUPPORTED SYSTEM COMPONENTS \"\n" + 
			"  specification: \"The organization: a. Replaces information system components when support for the components is no longer available from the developer, vendor, or manufacturer; and b. Provides justification and documents approval for the continued use of unsupported system components required to satisfy mission/business needs.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-22(1) { \n" + 
			"  name: \"UNSUPPORTED SYSTEM COMPONENTS | ALTERNATIVE SOURCES FOR CONTINUED SUPPORT \"\n" + 
			"  specification: \"The organization provides [Selection (one or more): in-house support; [Assignment: organization-defined support from external providers]] for unsupported information system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SC-1 { \n" + 
			"  name: \"SYSTEM AND COMMUNICATIONS PROTECTION POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A system and communications protection policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the system and communications protection policy and associated system and communications protection controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. System and communications protection policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. System and communications protection procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-2 { \n" + 
			"  name: \"APPLICATION PARTITIONING \"\n" + 
			"  specification: \"The information system separates user functionality (including user interface services) from information system management functionality.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-2(1) { \n" + 
			"  name: \"APPLICATION PARTITIONING | INTERFACES FOR NON-PRIVILEGED USERS \"\n" + 
			"  specification: \"The information system prevents the presentation of information system management-related functionality at an interface for non-privileged users.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-3 { \n" + 
			"  name: \"SECURITY FUNCTION ISOLATION \"\n" + 
			"  specification: \"The information system isolates security functions from nonsecurity functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-3(1) { \n" + 
			"  name: \"SECURITY FUNCTION ISOLATION | HARDWARE SEPARATION \"\n" + 
			"  specification: \"The information system utilizes underlying hardware separation mechanisms to implement security function isolation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-3(2) { \n" + 
			"  name: \"SECURITY FUNCTION ISOLATION | ACCESS / FLOW CONTROL FUNCTIONS \"\n" + 
			"  specification: \"The information system isolates security functions enforcing access and information flow control from nonsecurity functions and from other security functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-3(3) { \n" + 
			"  name: \"SECURITY FUNCTION ISOLATION | MINIMIZE NONSECURITY FUNCTIONALITY \"\n" + 
			"  specification: \"The organization minimizes the number of nonsecurity functions included within the isolation boundary containing security functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-3(4) { \n" + 
			"  name: \"SECURITY FUNCTION ISOLATION | MODULE COUPLING AND COHESIVENESS \"\n" + 
			"  specification: \"The organization implements security functions as largely independent modules that maximize internal cohesiveness within modules and minimize coupling between modules.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-3(5) { \n" + 
			"  name: \"SECURITY FUNCTION ISOLATION | LAYERED STRUCTURES \"\n" + 
			"  specification: \"The organization implements security functions as a layered structure minimizing interactions between layers of the design and avoiding any dependence by lower layers on the functionality or correctness of higher layers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-4 { \n" + 
			"  name: \"INFORMATION IN SHARED RESOURCES \"\n" + 
			"  specification: \"The information system prevents unauthorized and unintended information transfer via shared system resources.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-4(1) { \n" + 
			"  name: \"INFORMATION IN SHARED RESOURCES | SECURITY LEVELS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-4(2) { \n" + 
			"  name: \"INFORMATION IN SHARED RESOURCES | PERIODS PROCESSING \"\n" + 
			"  specification: \"The information system prevents unauthorized information transfer via shared resources in accordance with [Assignment: organization-defined procedures] when system processing explicitly switches between different information classification levels or security categories.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-5 { \n" + 
			"  name: \"DENIAL OF SERVICE PROTECTION \"\n" + 
			"  specification: \"The information system protects against or limits the effects of the following types of denial of service attacks: [Assignment: organization-defined types of denial of service attacks or references to sources for such information] by employing [Assignment: organization-defined security safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-5(1) { \n" + 
			"  name: \"DENIAL OF SERVICE PROTECTION | RESTRICT INTERNAL USERS \"\n" + 
			"  specification: \"The information system restricts the ability of individuals to launch [Assignment: organization-defined denial of service attacks] against other information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-5(2) { \n" + 
			"  name: \"DENIAL OF SERVICE PROTECTION | EXCESS CAPACITY / BANDWIDTH / REDUNDANCY \"\n" + 
			"  specification: \"The information system manages excess capacity, bandwidth, or other redundancy to limit the effects of information flooding denial of service attacks.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-5(3) { \n" + 
			"  name: \"DENIAL OF SERVICE PROTECTION | DETECTION / MONITORING \"\n" + 
			"  specification: \"The organization: (a) Employs [Assignment: organization-defined monitoring tools] to detect indicators of denial of service attacks against the information system; and (b) Monitors [Assignment: organization-defined information system resources] to determine if sufficient resources exist to prevent effective denial of service attacks.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-6 { \n" + 
			"  name: \"RESOURCE AVAILABILITY \"\n" + 
			"  specification: \"The information system protects the availability of resources by allocating [Assignment: organization-defined resources] by [Selection (one or more); priority; quota; [Assignment: organization-defined security safeguards]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7 { \n" + 
			"  name: \"BOUNDARY PROTECTION \"\n" + 
			"  specification: \"The information system: a. Monitors and controls communications at the external boundary of the system and at key internal boundaries within the system; b. Implements subnetworks for publicly accessible system components that are [Selection: physically; logically] separated from internal organizational networks; and c. Connects to external networks or information systems only through managed interfaces consisting of boundary protection devices arranged in accordance with an organizational security architecture. \n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(1) { \n" + 
			"  name: \"BOUNDARY PROTECTION | PHYSICALLY SEPARATED SUBNETWORKS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(2) { \n" + 
			"  name: \"BOUNDARY PROTECTION | PUBLIC ACCESS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(3) { \n" + 
			"  name: \"BOUNDARY PROTECTION | ACCESS POINTS \"\n" + 
			"  specification: \"The organization limits the number of external network connections to the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(4) { \n" + 
			"  name: \"BOUNDARY PROTECTION | EXTERNAL TELECOMMUNICATIONS SERVICES \"\n" + 
			"  specification: \"The organization: (a) Implements a managed interface for each external telecommunication service; (b) Establishes a traffic flow policy for each managed interface; (c) Protects the confidentiality and integrity of the information being transmitted across each interface; (d) Documents each exception to the traffic flow policy with a supporting mission/business need and duration of that need; and (e) Reviews exceptions to the traffic flow policy [Assignment: organization-defined frequency] and removes exceptions that are no longer supported by an explicit mission/business need.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(5) { \n" + 
			"  name: \"BOUNDARY PROTECTION | DENY BY DEFAULT / ALLOW BY EXCEPTION \"\n" + 
			"  specification: \"The information system at managed interfaces denies network communications traffic by default and allows network communications traffic by exception (i.e., deny all, permit by exception).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(6) { \n" + 
			"  name: \"BOUNDARY PROTECTION | RESPONSE TO RECOGNIZED FAILURES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-7 (18).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(7) { \n" + 
			"  name: \"BOUNDARY PROTECTION | PREVENT SPLIT TUNNELING FOR REMOTE DEVICES \"\n" + 
			"  specification: \"The information system, in conjunction with a remote device, prevents the device from simultaneously establishing non-remote connections with the system and communicating via some other connection to resources in external networks.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(8) { \n" + 
			"  name: \"BOUNDARY PROTECTION | ROUTE TRAFFIC TO AUTHENTICATED PROXY SERVERS \"\n" + 
			"  specification: \"The information system routes [Assignment: organization-defined internal communications traffic] to [Assignment: organization-defined external networks] through authenticated proxy servers at managed interfaces.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(9) { \n" + 
			"  name: \"BOUNDARY PROTECTION | RESTRICT THREATENING OUTGOING COMMUNICATIONS TRAFFIC \"\n" + 
			"  specification: \"The information system: (a) Detects and denies outgoing communications traffic posing a threat to external information systems; and (b) Audits the identity of internal users associated with denied communications.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(10) { \n" + 
			"  name: \"BOUNDARY PROTECTION | PREVENT UNAUTHORIZED EXFILTRATION \"\n" + 
			"  specification: \"The organization prevents the unauthorized exfiltration of information across managed interfaces.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(11) { \n" + 
			"  name: \"BOUNDARY PROTECTION | RESTRICT INCOMING COMMUNICATIONS TRAFFIC \"\n" + 
			"  specification: \"The information system only allows incoming communications from [Assignment: organization-defined authorized sources] to be routed to [Assignment: organization-defined authorized destinations].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(12) { \n" + 
			"  name: \"BOUNDARY PROTECTION | HOST-BASED PROTECTION \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined host-based boundary protection mechanisms] at [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(13) { \n" + 
			"  name: \"BOUNDARY PROTECTION | ISOLATION OF SECURITY TOOLS / MECHANISMS / SUPPORT COMPONENTS \"\n" + 
			"  specification: \"The organization isolates [Assignment: organization-defined information security tools, mechanisms, and support components] from other internal information system components by implementing physically separate subnetworks with managed interfaces to other components of the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(14) { \n" + 
			"  name: \"BOUNDARY PROTECTION | PROTECTS AGAINST UNAUTHORIZED PHYSICAL CONNECTIONS \"\n" + 
			"  specification: \"The organization protects against unauthorized physical connections at [Assignment: organization-defined managed interfaces].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(15) { \n" + 
			"  name: \"BOUNDARY PROTECTION | ROUTE PRIVILEGED NETWORK ACCESSES \"\n" + 
			"  specification: \"The information system routes all networked, privileged accesses through a dedicated, managed interface for purposes of access control and auditing.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(16) { \n" + 
			"  name: \"BOUNDARY PROTECTION | PREVENT DISCOVERY OF COMPONENTS / DEVICES \"\n" + 
			"  specification: \"The information system prevents discovery of specific system components composing a managed interface.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(17) { \n" + 
			"  name: \"BOUNDARY PROTECTION | AUTOMATED ENFORCEMENT OF PROTOCOL FORMATS \"\n" + 
			"  specification: \"The information system enforces adherence to protocol formats.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(18) { \n" + 
			"  name: \"BOUNDARY PROTECTION | FAIL SECURE \"\n" + 
			"  specification: \"The information system fails securely in the event of an operational failure of a boundary protection device.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(19) { \n" + 
			"  name: \"BOUNDARY PROTECTION | BLOCKS COMMUNICATION FROM NON-ORGANIZATIONALLY CONFIGURED HOSTS \"\n" + 
			"  specification: \"The information system blocks both inbound and outbound communications traffic between [Assignment: organization-defined communication clients] that are independently configured by end users and external service providers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(20) { \n" + 
			"  name: \"BOUNDARY PROTECTION | DYNAMIC ISOLATION / SEGREGATION \"\n" + 
			"  specification: \"The information system provides the capability to dynamically isolate/segregate [Assignment: organization-defined information system components] from other components of the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(21) { \n" + 
			"  name: \"BOUNDARY PROTECTION | ISOLATION OF INFORMATION SYSTEM COMPONENTS \"\n" + 
			"  specification: \"The organization employs boundary protection mechanisms to separate [Assignment: organization-defined information system components] supporting [Assignment: organization-defined missions and/or business functions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(22) { \n" + 
			"  name: \"BOUNDARY PROTECTION | SEPARATE SUBNETS FOR CONNECTING TO DIFFERENT SECURITY DOMAINS \"\n" + 
			"  specification: \"The information system implements separate network addresses (i.e., different subnets) to connect to systems in different security domains.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(23) { \n" + 
			"  name: \"BOUNDARY PROTECTION | DISABLE SENDER FEEDBACK ON PROTOCOL VALIDATION FAILURE \"\n" + 
			"  specification: \"The information system disables feedback to senders on protocol format validation failure.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-8 { \n" + 
			"  name: \"TRANSMISSION CONFIDENTIALITY AND INTEGRITY \"\n" + 
			"  specification: \"The information system protects the [Selection (one or more): confidentiality; integrity] of transmitted information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-8(1) { \n" + 
			"  name: \"TRANSMISSION CONFIDENTIALITY AND INTEGRITY | CRYPTOGRAPHIC OR ALTERNATE PHYSICAL PROTECTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to [Selection (one or more): prevent unauthorized disclosure of information; detect changes to information] during transmission unless otherwise protected by [Assignment: organization-defined alternative physical safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-8(2) { \n" + 
			"  name: \"TRANSMISSION CONFIDENTIALITY AND INTEGRITY | PRE / POST TRANSMISSION HANDLING \"\n" + 
			"  specification: \"The information system maintains the [Selection (one or more): confidentiality; integrity] of information during preparation for transmission and during reception.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-8(3) { \n" + 
			"  name: \"TRANSMISSION CONFIDENTIALITY AND INTEGRITY | CRYPTOGRAPHIC PROTECTION FOR MESSAGE EXTERNALS \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to protect message externals unless otherwise protected by [Assignment: organization-defined alternative physical safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-8(4) { \n" + 
			"  name: \"TRANSMISSION CONFIDENTIALITY AND INTEGRITY | CONCEAL / RANDOMIZE COMMUNICATIONS \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to conceal or randomize communication patterns unless otherwise protected by [Assignment: organization-defined alternative physical safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-9 { \n" + 
			"  name: \"TRANSMISSION CONFIDENTIALITY \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-8.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-10 { \n" + 
			"  name: \"NETWORK DISCONNECT \"\n" + 
			"  specification: \"The information system terminates the network connection associated with a communications session at the end of the session or after [Assignment: organization-defined time period] of inactivity.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-11 { \n" + 
			"  name: \"TRUSTED PATH \"\n" + 
			"  specification: \"The information system establishes a trusted communications path between the user and the following security functions of the system: [Assignment: organization-defined security functions to include at a minimum, information system authentication and re-authentication].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-11(1) { \n" + 
			"  name: \"TRUSTED PATH | LOGICAL ISOLATION \"\n" + 
			"  specification: \"The information system provides a trusted communications path that is logically isolated and distinguishable from other paths.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-12 { \n" + 
			"  name: \"CRYPTOGRAPHIC KEY ESTABLISHMENT AND MANAGEMENT \"\n" + 
			"  specification: \"The organization establishes and manages cryptographic keys for required cryptography employed within the information system in accordance with [Assignment: organization-defined requirements for key generation, distribution, storage, access, and destruction].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-12(1) { \n" + 
			"  name: \"CRYPTOGRAPHIC KEY ESTABLISHMENT AND MANAGEMENT | AVAILABILITY \"\n" + 
			"  specification: \"The organization maintains availability of information in the event of the loss of cryptographic keys by users.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-12(2) { \n" + 
			"  name: \"CRYPTOGRAPHIC KEY ESTABLISHMENT AND MANAGEMENT | SYMMETRIC KEYS \"\n" + 
			"  specification: \"The organization produces, controls, and distributes symmetric cryptographic keys using [Selection: NIST FIPS-compliant; NSA-approved] key management technology and processes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-12(3) { \n" + 
			"  name: \"CRYPTOGRAPHIC KEY ESTABLISHMENT AND MANAGEMENT | ASYMMETRIC KEYS \"\n" + 
			"  specification: \"The organization produces, controls, and distributes asymmetric cryptographic keys using [Selection: NSA-approved key management technology and processes; approved PKI Class 3 certificates or prepositioned keying material; approved PKI Class 3 or Class 4 certificates and hardware security tokens that protect the user’s private key].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-12(4) { \n" + 
			"  name: \"CRYPTOGRAPHIC KEY ESTABLISHMENT AND MANAGEMENT | PKI CERTIFICATES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-12.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-12(5) { \n" + 
			"  name: \"CRYPTOGRAPHIC KEY ESTABLISHMENT AND MANAGEMENT | PKI CERTIFICATES / HARDWARE TOKENS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-12.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-13 { \n" + 
			"  name: \"CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined cryptographic uses and type of cryptography required for each use] in accordance with applicable federal laws, Executive Orders, directives, policies, regulations, and standards.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-13(1) { \n" + 
			"  name: \"CRYPTOGRAPHIC PROTECTION | FIPS-VALIDATED CRYPTOGRAPHY \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-13.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-13(2) { \n" + 
			"  name: \"CRYPTOGRAPHIC PROTECTION | NSA-APPROVED CRYPTOGRAPHY \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-13.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-13(3) { \n" + 
			"  name: \"CRYPTOGRAPHIC PROTECTION | INDIVIDUALS WITHOUT FORMAL ACCESS APPROVALS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-13.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-13(4) { \n" + 
			"  name: \"CRYPTOGRAPHIC PROTECTION | DIGITAL SIGNATURES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-13.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-14 { \n" + 
			"  name: \"PUBLIC ACCESS PROTECTIONS \"\n" + 
			"  specification: \"Withdrawn: Capability provided by AC-2, AC-3, AC-5, AC-6, SI-3, SI-4, SI-5, SI-7, SI-10.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-15 { \n" + 
			"  name: \"COLLABORATIVE COMPUTING DEVICES \"\n" + 
			"  specification: \"The information system: a. Prohibits remote activation of collaborative computing devices with the following exceptions: [Assignment: organization-defined exceptions where remote activation is to be allowed]; and b. Provides an explicit indication of use to users physically present at the devices.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-15(1) { \n" + 
			"  name: \"COLLABORATIVE COMPUTING DEVICES | PHYSICAL DISCONNECT \"\n" + 
			"  specification: \"The information system provides physical disconnect of collaborative computing devices in a manner that supports ease of use.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-15(2) { \n" + 
			"  name: \"COLLABORATIVE COMPUTING DEVICES | BLOCKING INBOUND / OUTBOUND COMMUNICATIONS TRAFFIC \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-15(3) { \n" + 
			"  name: \"COLLABORATIVE COMPUTING DEVICES | DISABLING / REMOVAL IN SECURE WORK AREAS \"\n" + 
			"  specification: \"The organization disables or removes collaborative computing devices from [Assignment: organization-defined information systems or information system components] in [Assignment: organization-defined secure work areas].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-15(4) { \n" + 
			"  name: \"COLLABORATIVE COMPUTING DEVICES | EXPLICITLY INDICATE CURRENT PARTICIPANTS \"\n" + 
			"  specification: \"The information system provides an explicit indication of current participants in [Assignment: organization-defined online meetings and teleconferences].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-16 { \n" + 
			"  name: \"TRANSMISSION OF SECURITY ATTRIBUTES \"\n" + 
			"  specification: \"The information system associates [Assignment: organization-defined security attributes] with information exchanged between information systems and between system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-16(1) { \n" + 
			"  name: \"TRANSMISSION OF SECURITY ATTRIBUTES | INTEGRITY VALIDATION \"\n" + 
			"  specification: \"The information system validates the integrity of transmitted security attributes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-17 { \n" + 
			"  name: \"PUBLIC KEY INFRASTRUCTURE CERTIFICATES \"\n" + 
			"  specification: \"The organization issues public key certificates under an [Assignment: organization-defined certificate policy] or obtains public key certificates from an approved service provider.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-18 { \n" + 
			"  name: \"MOBILE CODE \"\n" + 
			"  specification: \"The organization: a. Defines acceptable and unacceptable mobile code and mobile code technologies; b. Establishes usage restrictions and implementation guidance for acceptable mobile code and mobile code technologies; and c. Authorizes, monitors, and controls the use of mobile code within the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-18(1) { \n" + 
			"  name: \"MOBILE CODE | IDENTIFY UNACCEPTABLE CODE / TAKE CORRECTIVE ACTIONS \"\n" + 
			"  specification: \"The information system identifies [Assignment: organization-defined unacceptable mobile code] and takes [Assignment: organization-defined corrective actions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-18(2) { \n" + 
			"  name: \"MOBILE CODE | ACQUISITION / DEVELOPMENT / USE \"\n" + 
			"  specification: \"The organization ensures that the acquisition, development, and use of mobile code to be deployed in the information system meets [Assignment: organization-defined mobile code requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-18(3) { \n" + 
			"  name: \"MOBILE CODE | PREVENT DOWNLOADING / EXECUTION \"\n" + 
			"  specification: \"The information system prevents the download and execution of [Assignment: organization-defined unacceptable mobile code].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-18(4) { \n" + 
			"  name: \"MOBILE CODE | PREVENT AUTOMATIC EXECUTION \"\n" + 
			"  specification: \"The information system prevents the automatic execution of mobile code in [Assignment: organization-defined software applications] and enforces [Assignment: organization-defined actions] prior to executing the code.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-18(5) { \n" + 
			"  name: \"MOBILE CODE | ALLOW EXECUTION ONLY IN CONFINED ENVIRONMENTS \"\n" + 
			"  specification: \"The organization allows execution of permitted mobile code only in confined virtual machine environments.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-19 { \n" + 
			"  name: \"VOICE OVER INTERNET PROTOCOL \"\n" + 
			"  specification: \"The organization: a. Establishes usage restrictions and implementation guidance for Voice over Internet Protocol (VoIP) technologies based on the potential to cause damage to the information system if used maliciously; and b. Authorizes, monitors, and controls the use of VoIP within the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-20 { \n" + 
			"  name: \"SECURE NAME / ADDRESS RESOLUTION SERVICE (AUTHORITATIVE SOURCE) \"\n" + 
			"  specification: \"The information system: a. Provides additional data origin authentication and integrity verification artifacts along with the authoritative name resolution data the system returns in response to external name/address resolution queries; and b. Provides the means to indicate the security status of child zones and (if the child supports secure resolution services) to enable verification of a chain of trust among parent and child domains, when operating as part of a distributed, hierarchical namespace.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-20(1) { \n" + 
			"  name: \"SECURE NAME / ADDRESS RESOLUTION SERVICE (AUTHORITATIVE SOURCE) | CHILD SUBSPACES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-20.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-20(2) { \n" + 
			"  name: \"SECURE NAME / ADDRESS RESOLUTION SERVICE (AUTHORITATIVE SOURCE) | DATA ORIGIN / INTEGRITY \"\n" + 
			"  specification: \"The information system provides data origin and integrity protection artifacts for internal name/address resolution queries.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-21 { \n" + 
			"  name: \"SECURE NAME / ADDRESS RESOLUTION SERVICE (RECURSIVE OR CACHING RESOLVER) \"\n" + 
			"  specification: \"The information system requests and performs data origin authentication and data integrity verification on the name/address resolution responses the system receives from authoritative sources.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-21(1) { \n" + 
			"  name: \"SECURE NAME / ADDRESS RESOLUTION SERVICE (RECURSIVE OR CACHING RESOLVER) | DATA ORIGIN / INTEGRITY \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-21.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-22 { \n" + 
			"  name: \"ARCHITECTURE AND PROVISIONING FOR NAME / ADDRESS RESOLUTION SERVICE \"\n" + 
			"  specification: \"The information systems that collectively provide name/address resolution service for an organization are fault-tolerant and implement internal/external role separation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-23 { \n" + 
			"  name: \"SESSION AUTHENTICITY \"\n" + 
			"  specification: \"The information system protects the authenticity of communications sessions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-23(1) { \n" + 
			"  name: \"SESSION AUTHENTICITY | INVALIDATE SESSION IDENTIFIERS AT LOGOUT \"\n" + 
			"  specification: \"The information system invalidates session identifiers upon user logout or other session termination.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-23(2) { \n" + 
			"  name: \"SESSION AUTHENTICITY | USER-INITIATED LOGOUTS / MESSAGE DISPLAYS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-12 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-23(3) { \n" + 
			"  name: \" \"\n" + 
			"  specification: \"The information system generates a unique session identifier for each session with [Assignment: organization-defined randomness requirements] and recognizes only session identifiers that are system-generated.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-23(4) { \n" + 
			"  name: \"SESSION AUTHENTICITY | UNIQUE SESSION IDENTIFIERS WITH RANDOMIZATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-23 (3).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-23(5) { \n" + 
			"  name: \"SESSION AUTHENTICITY | ALLOWED CERTIFICATE AUTHORITIES \"\n" + 
			"  specification: \"The information system only allows the use of [Assignment: organization-defined certificate authorities] for verification of the establishment of protected sessions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-24 { \n" + 
			"  name: \"FAIL IN KNOWN STATE \"\n" + 
			"  specification: \"The information system fails to a [Assignment: organization-defined known-state] for [Assignment: organization-defined types of failures] preserving [Assignment: organization-defined system state information] in failure.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-25 { \n" + 
			"  name: \"THIN NODES \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined information system components] with minimal functionality and information storage.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-26 { \n" + 
			"  name: \"HONEYPOTS \"\n" + 
			"  specification: \"The information system includes components specifically designed to be the target of malicious attacks for the purpose of detecting, deflecting, and analyzing such attacks.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-26(1) { \n" + 
			"  name: \"HONEYPOTS | DETECTION OF MALICIOUS CODE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-35.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-27 { \n" + 
			"  name: \"PLATFORM-INDEPENDENT APPLICATIONS \"\n" + 
			"  specification: \"The information system includes: [Assignment: organization-defined platform-independent applications].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-28 { \n" + 
			"  name: \"PROTECTION OF INFORMATION AT REST \"\n" + 
			"  specification: \"The information system protects the [Selection (one or more): confidentiality; integrity] of [Assignment: organization-defined information at rest].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-28(1) { \n" + 
			"  name: \"PROTECTION OF INFORMATION AT REST | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to prevent unauthorized disclosure and modification of [Assignment: organization-defined information] on [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-28(2) { \n" + 
			"  name: \"PROTECTION OF INFORMATION AT REST | OFF-LINE STORAGE \"\n" + 
			"  specification: \"The organization removes from online storage and stores off-line in a secure location [Assignment: organization-defined information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-29 { \n" + 
			"  name: \"HETEROGENEITY \"\n" + 
			"  specification: \"The organization employs a diverse set of information technologies for [Assignment: organization-defined information system components] in the implementation of the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-29(1) { \n" + 
			"  name: \"HETEROGENEITY | VIRTUALIZATION TECHNIQUES \"\n" + 
			"  specification: \"The organization employs virtualization techniques to support the deployment of a diversity of operating systems and applications that are changed [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-30 { \n" + 
			"  name: \"CONCEALMENT AND MISDIRECTION \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined concealment and misdirection techniques] for [Assignment: organization-defined information systems] at [Assignment: organization-defined time periods] to confuse and mislead adversaries.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-30(1) { \n" + 
			"  name: \"CONCEALMENT AND MISDIRECTION | VIRTUALIZATION TECHNIQUES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-29 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-30(2) { \n" + 
			"  name: \"CONCEALMENT AND MISDIRECTION | RANDOMNESS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined techniques] to introduce randomness into organizational operations and assets.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-30(3) { \n" + 
			"  name: \"CONCEALMENT AND MISDIRECTION | CHANGE PROCESSING / STORAGE LOCATIONS \"\n" + 
			"  specification: \"The organization changes the location of [Assignment: organization-defined processing and/or storage] [Selection: [Assignment: organization-defined time frequency]; at random time intervals]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-30(4) { \n" + 
			"  name: \"CONCEALMENT AND MISDIRECTION | MISLEADING INFORMATION \"\n" + 
			"  specification: \"The organization employs realistic, but misleading information in [Assignment: organization-defined information system components] with regard to its security state or posture.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-30(5) { \n" + 
			"  name: \"CONCEALMENT AND MISDIRECTION | CONCEALMENT OF SYSTEM COMPONENTS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined techniques] to hide or conceal [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-31 { \n" + 
			"  name: \"COVERT CHANNEL ANALYSIS \"\n" + 
			"  specification: \"The organization: a. Performs a covert channel analysis to identify those aspects of communications within the information system that are potential avenues for covert [Selection (one or more): storage; timing] channels; and b. Estimates the maximum bandwidth of those channels.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-31(1) { \n" + 
			"  name: \"COVERT CHANNEL ANALYSIS | TEST COVERT CHANNELS FOR EXPLOITABILITY \"\n" + 
			"  specification: \"The organization tests a subset of the identified covert channels to determine which channels are exploitable.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-31(2) { \n" + 
			"  name: \"COVERT CHANNEL ANALYSIS | MAXIMUM BANDWIDTH \"\n" + 
			"  specification: \"The organization reduces the maximum bandwidth for identified covert [Selection (one or more); storage; timing] channels to [Assignment: organization-defined values].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-31(3) { \n" + 
			"  name: \"COVERT CHANNEL ANALYSIS | MEASURE BANDWIDTH IN OPERATIONAL ENVIRONMENTS \"\n" + 
			"  specification: \"The organization measures the bandwidth of [Assignment: organization-defined subset of identified covert channels] in the operational environment of the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-32 { \n" + 
			"  name: \"INFORMATION SYSTEM PARTITIONING \"\n" + 
			"  specification: \"The organization partitions the information system into [Assignment: organization-defined information system components] residing in separate physical domains or environments based on [Assignment: organization-defined circumstances for physical separation of components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-33 { \n" + 
			"  name: \"TRANSMISSION PREPARATION INTEGRITY \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-8.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-34 { \n" + 
			"  name: \"NON-MODIFIABLE EXECUTABLE PROGRAMS \"\n" + 
			"  specification: \"The information system at [Assignment: organization-defined information system components]: a. Loads and executes the operating environment from hardware-enforced, read-only media; and b. Loads and executes [Assignment: organization-defined applications] from hardware-enforced, read-only media.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-34(1) { \n" + 
			"  name: \"NON-MODIFIABLE EXECUTABLE PROGRAMS | NO WRITABLE STORAGE \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined information system components] with no writeable storage that is persistent across component restart or power on/off.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-34(2) { \n" + 
			"  name: \"NON-MODIFIABLE EXECUTABLE PROGRAMS | INTEGRITY PROTECTION / READ-ONLY MEDIA \"\n" + 
			"  specification: \"The organization protects the integrity of information prior to storage on read-only media and controls the media after such information has been recorded onto the media.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-34(3) { \n" + 
			"  name: \"NON-MODIFIABLE EXECUTABLE PROGRAMS | HARDWARE-BASED PROTECTION \"\n" + 
			"  specification: \"The organization: (a) Employs hardware-based, write-protect for [Assignment: organization-defined information system firmware components]; and (b) Implements specific procedures for [Assignment: organization-defined authorized individuals] to manually disable hardware write-protect for firmware modifications and re-enable the write-protect prior to returning to operational mode.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-35 { \n" + 
			"  name: \"HONEYCLIENTS \"\n" + 
			"  specification: \"The information system includes components that proactively seek to identify malicious websites and/or web-based malicious code.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-36 { \n" + 
			"  name: \"DISTRIBUTED PROCESSING AND STORAGE \"\n" + 
			"  specification: \"The organization distributes [Assignment: organization-defined processing and storage] across multiple physical locations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-36(1) { \n" + 
			"  name: \"DISTRIBUTED PROCESSING AND STORAGE | POLLING TECHNIQUES \"\n" + 
			"  specification: \"The organization employs polling techniques to identify potential faults, errors, or compromises to [Assignment: organization-defined distributed processing and storage components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-37 { \n" + 
			"  name: \"OUT-OF-BAND CHANNELS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined out-of-band channels] for the physical delivery or electronic transmission of [Assignment: organization-defined information, information system components, or devices] to [Assignment: organization-defined individuals or information systems].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-37(1) { \n" + 
			"  name: \"OUT-OF-BAND CHANNELS | ENSURE DELIVERY / TRANSMISSION \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to ensure that only [Assignment: organization-defined individuals or information systems] receive the [Assignment: organization-defined information, information system components, or devices].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-38 { \n" + 
			"  name: \"OPERATIONS SECURITY \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to ensure that only [Assignment: organization-defined individuals or information systems] receive the [Assignment: organization-defined information, information system components, or devices].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-39 { \n" + 
			"  name: \"PROCESS ISOLATION \"\n" + 
			"  specification: \"The information system maintains a separate execution domain for each executing process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-39(1) { \n" + 
			"  name: \"PROCESS ISOLATION | HARDWARE SEPARATION \"\n" + 
			"  specification: \"The information system implements underlying hardware separation mechanisms to facilitate process separation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-39(2) { \n" + 
			"  name: \"PROCESS ISOLATION | THREAD ISOLATION \"\n" + 
			"  specification: \"The information system maintains a separate execution domain for each thread in [Assignment: organization-defined multi-threaded processing].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-40 { \n" + 
			"  name: \"WIRELESS LINK PROTECTION \"\n" + 
			"  specification: \"The information system protects external and internal [Assignment: organization-defined wireless links] from [Assignment: organization-defined types of signal parameter attacks or references to sources for such attacks].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-40(1) { \n" + 
			"  name: \"WIRELESS LINK PROTECTION | ELECTROMAGNETIC INTERFERENCE \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms that achieve [Assignment: organization-defined level of protection] against the effects of intentional electromagnetic interference.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-40(2) { \n" + 
			"  name: \"WIRELESS LINK PROTECTION | REDUCE DETECTION POTENTIAL \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to reduce the detection potential of wireless links to [Assignment: organization-defined level of reduction].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-40(3) { \n" + 
			"  name: \"WIRELESS LINK PROTECTION | IMITATIVE OR MANIPULATIVE COMMUNICATIONS DECEPTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to identify and reject wireless transmissions that are deliberate attempts to achieve imitative or manipulative communications deception based on signal parameters.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-40(4) { \n" + 
			"  name: \"WIRELESS LINK PROTECTION | SIGNAL PARAMETER IDENTIFICATION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to prevent the identification of [Assignment: organization-defined wireless transmitters] by using the transmitter signal parameters.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-41 { \n" + 
			"  name: \"PORT AND I/O DEVICE ACCESS \"\n" + 
			"  specification: \"The organization physically disables or removes [Assignment: organization-defined connection ports or input/output devices] on [Assignment: organization-defined information systems or information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-42 { \n" + 
			"  name: \"SENSOR CAPABILITY AND DATA \"\n" + 
			"  specification: \"The information system: a. Prohibits the remote activation of environmental sensing capabilities with the following exceptions: [Assignment: organization-defined exceptions where remote activation of sensors is allowed]; andb. Provides an explicit indication of sensor use to [Assignment: organization-defined class of users].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-42(1) { \n" + 
			"  name: \"SENSOR CAPABILITY AND DATA | REPORTING TO AUTHORIZED INDIVIDUALS OR ROLES \"\n" + 
			"  specification: \"The organization ensures that the information system is configured so that data or information collected by the [Assignment: organization-defined sensors] is only reported to authorized individuals or roles.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-42(2) { \n" + 
			"  name: \"SENSOR CAPABILITY AND DATA | AUTHORIZED USE \"\n" + 
			"  specification: \"The organization employs the following measures: [Assignment: organization-defined measures], so that data or information collected by [Assignment: organization-defined sensors] is only used for authorized purposes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-42(3) { \n" + 
			"  name: \"SENSOR CAPABILITY AND DATA | PROHIBIT USE OF DEVICES \"\n" + 
			"  specification: \"The organization prohibits the use of devices possessing [Assignment: organization-defined environmental sensing capabilities] in [Assignment: organization-defined facilities, areas, or systems].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-43 { \n" + 
			"  name: \"USAGE RESTRICTIONS \"\n" + 
			"  specification: \"The organization: a.Establishes usage restrictions and implementation guidance for [Assignment: organization-defined information system components] based on the potential to cause damage to the information system if used maliciously; and b. Authorizes, monitors, and controls the use of such components within the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-44 { \n" + 
			"  name: \"DETONATION CHAMBERS \"\n" + 
			"  specification: \"The organization employs a detonation chamber capability within [Assignment: organization-defined information system, system component, or location].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SI-1 { \n" + 
			"  name: \"SYSTEM AND INFORMATION INTEGRITY POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A system and information integrity policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the system and information integrity policy and associated system and information integrity controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. System and information integrity policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. System and information integrity procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2 { \n" + 
			"  name: \"FLAW REMEDIATION \"\n" + 
			"  specification: \"The organization: a. Identifies, reports, and corrects information system flaws; b. Tests software and firmware updates related to flaw remediation for effectiveness and potential side effects before installation; c. Installs security-relevant software and firmware updates within [Assignment: organization-defined time period] of the release of the updates; and d. Incorporates flaw remediation into the organizational configuration management process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2(1) { \n" + 
			"  name: \"FLAW REMEDIATION | CENTRAL MANAGEMENT \"\n" + 
			"  specification: \"The organization centrally manages the flaw remediation process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2(2) { \n" + 
			"  name: \"FLAW REMEDIATION | AUTOMATED FLAW REMEDIATION STATUS \"\n" + 
			"  specification: \"The organization employs automated mechanisms [Assignment: organization-defined frequency] to determine the state of information system components with regard to flaw remediation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2(3) { \n" + 
			"  name: \"FLAW REMEDIATION | TIME TO REMEDIATE FLAWS / BENCHMARKS FOR CORRECTIVE ACTIONS \"\n" + 
			"  specification: \"The organization: (a) Measures the time between flaw identification and flaw remediation; and (b) Establishes [Assignment: organization-defined benchmarks] for taking corrective actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2(4) { \n" + 
			"  name: \"FLAW REMEDIATION | AUTOMATED PATCH MANAGEMENT TOOLS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2(5) { \n" + 
			"  name: \"FLAW REMEDIATION | AUTOMATIC SOFTWARE / FIRMWARE UPDATES \"\n" + 
			"  specification: \"The organization installs [Assignment: organization-defined security-relevant software and firmware updates] automatically to [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2(6) { \n" + 
			"  name: \"FLAW REMEDIATION | REMOVAL OF PREVIOUS VERSIONS OF SOFTWARE / FIRMWARE \"\n" + 
			"  specification: \"The organization removes [Assignment: organization-defined software and firmware components] after updated versions have been installed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3 { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Employs malicious code protection mechanisms at information system entry and exit points to detect and eradicate malicious code;\n" + 
			"			b. Updates malicious code protection mechanisms whenever new releases are available in accordance with organizational configuration management policy and procedures;\n" + 
			"			c. Configures malicious code protection mechanisms to:\n" + 
			"				1. Perform periodic scans of the information system [Assignment: organization-defined frequency] and real-time scans of files from external sources at [Selection (one or more); endpoint; network entry/exit points] as the files are downloaded, opened, or executed in accordance with organizational security policy; and\n" + 
			"				2. [Selection (one or more): block malicious code; quarantine malicious code; send alert to administrator; [Assignment: organization-defined action]] in response to malicious code detection; and\n" + 
			"			d. Addresses the receipt of false positives during malicious code detection and eradication and the resulting potential impact on the availability of the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(1) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | CENTRAL MANAGEMENT \"\n" + 
			"  specification: \"The organization centrally manages malicious code protection mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(2) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | AUTOMATIC UPDATES \"\n" + 
			"  specification: \"The information system automatically updates malicious code protection mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(3) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | NON-PRIVILEGED USERS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-6 (10).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(4) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | UPDATES ONLY BY PRIVILEGED USERS \"\n" + 
			"  specification: \"The information system updates malicious code protection mechanisms only when directed by a privileged user.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(5) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | PORTABLE STORAGE DEVICES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(6) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | TESTING / VERIFICATION \"\n" + 
			"  specification: \"The organization: (a) Tests malicious code protection mechanisms [Assignment: organization-defined frequency] by introducing a known benign, non-spreading test case into the information system; and (b) Verifies that both detection of the test case and associated incident reporting occur.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(7) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | NONSIGNATURE-BASED DETECTION \"\n" + 
			"  specification: \"The information system implements nonsignature-based malicious code detection mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(8) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | DETECT UNAUTHORIZED COMMANDS \"\n" + 
			"  specification: \"The information system detects [Assignment: organization-defined unauthorized operating system commands] through the kernel application programming interface at [Assignment: organization-defined information system hardware components] and [Selection (one or more): issues a warning; audits the command execution; prevents the execution of the command].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(9) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | AUTHENTICATE REMOTE COMMANDS \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined security safeguards] to authenticate [Assignment: organization-defined remote commands].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(10) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | MALICIOUS CODE ANALYSIS \"\n" + 
			"  specification: \"The organization: (a) Employs [Assignment: organization-defined tools and techniques] to analyze the characteristics and behavior of malicious code; and (b) Incorporates the results from malicious code analysis into organizational incident response and flaw remediation processes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4 { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Monitors the information system to detect:\n" + 
			"				1. Attacks and indicators of potential attacks in accordance with [Assignment: organization-defined monitoring objectives]; and\n" + 
			"				2. Unauthorized local, network, and remote connections;\n" + 
			"			b. Identifies unauthorized use of the information system through [Assignment: organization-defined techniques and methods];\n" + 
			"			c. Deploys monitoring devices:\n" + 
			"				1. Strategically within the information system to collect organization-determined essential information; and\n" + 
			"				2. At ad hoc locations within the system to track specific types of transactions of interest to the organization;\n" + 
			"			d. Protects information obtained from intrusion-monitoring tools from unauthorized access, modification, and deletion;\n" + 
			"			e. Heightens the level of information system monitoring activity whenever there is an indication of increased risk to organizational operations and assets, individuals, other organizations, or the Nation based on law enforcement information, intelligence information, or other credible sources of information;\n" + 
			"			f. Obtains legal opinion with regard to information system monitoring activities in accordance with applicable federal laws, Executive Orders, directives, policies, or regulations; and\n" + 
			"			g. Provides [Assignment: organization-defined information system monitoring information] to [Assignment: organization-defined personnel or roles] [Selection (one or more): as needed; [Assignment: organization-defined frequency]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(1) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | SYSTEM-WIDE INTRUSION DETECTION SYSTEM \"\n" + 
			"  specification: \"The organization connects and configures individual intrusion detection tools into an information system-wide intrusion detection system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(2) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | AUTOMATED TOOLS FOR REAL-TIME ANALYSIS \"\n" + 
			"  specification: \"The organization employs automated tools to support near real-time analysis of events.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(3) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | AUTOMATED TOOL INTEGRATION \"\n" + 
			"  specification: \"The organization employs automated tools to integrate intrusion detection tools into access control and flow control mechanisms for rapid response to attacks by enabling reconfiguration of these mechanisms in support of attack isolation and elimination.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(4) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | INBOUND AND OUTBOUND COMMUNICATIONS TRAFFIC \"\n" + 
			"  specification: \"The information system monitors inbound and outbound communications traffic [Assignment: organization-defined frequency] for unusual or unauthorized activities or conditions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(5) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | SYSTEM-GENERATED ALERTS \"\n" + 
			"  specification: \"The information system alerts [Assignment: organization-defined personnel or roles] when the following indications of compromise or potential compromise occur: [Assignment: organization-defined compromise indicators].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(6) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | RESTRICT NON-PRIVILEGED USERS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-6 (10).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(7) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | AUTOMATED RESPONSE TO SUSPICIOUS EVENTS \"\n" + 
			"  specification: \"The information system notifies [Assignment: organization-defined incident response personnel (identified by name and/or by role)] of detected suspicious events and takes [Assignment: organization-defined least-disruptive actions to terminate suspicious events].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(8) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | PROTECTION OF MONITORING INFORMATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(9) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | TESTING OF MONITORING TOOLS \"\n" + 
			"  specification: \"The organization tests intrusion-monitoring tools [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(10) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | VISIBILITY OF ENCRYPTED COMMUNICATIONS \"\n" + 
			"  specification: \"The organization makes provisions so that [Assignment: organization-defined encrypted communications traffic] is visible to [Assignment: organization-defined information system monitoring tools].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(11) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | ANALYZE COMMUNICATIONS TRAFFIC ANOMALIES \"\n" + 
			"  specification: \"The organization analyzes outbound communications traffic at the external boundary of the information system and selected [Assignment: organization-defined interior points within the system (e.g., subnetworks, subsystems)] to discover anomalies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(12) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | AUTOMATED ALERTS \"\n" + 
			"  specification: \"The organization employs automated mechanisms to alert security personnel of the following inappropriate or unusual activities with security implications: [Assignment: organization-defined activities that trigger alerts].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(13) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | ANALYZE TRAFFIC / EVENT PATTERNS \"\n" + 
			"  specification: \"The organization: (a) Analyzes communications traffic/event patterns for the information system; (b) Develops profiles representing common traffic patterns and/or events; and (c) Uses the traffic/event profiles in tuning system-monitoring devices to reduce the number of false positives and the number of false negatives.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(14) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | WIRELESS INTRUSION DETECTION \"\n" + 
			"  specification: \"The organization employs a wireless intrusion detection system to identify rogue wireless devices and to detect attack attempts and potential compromises/breaches to the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(15) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | WIRELESS TO WIRELINE COMMUNICATIONS \"\n" + 
			"  specification: \"The organization employs an intrusion detection system to monitor wireless communications traffic as the traffic passes from wireless to wireline networks.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(16) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | CORRELATE MONITORING INFORMATION \"\n" + 
			"  specification: \"The organization correlates information from monitoring tools employed throughout the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(17) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | INTEGRATED SITUATIONAL AWARENESS \"\n" + 
			"  specification: \"The organization correlates information from monitoring physical, cyber, and supply chain activities to achieve integrated, organization-wide situational awareness.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(18) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | ANALYZE TRAFFIC / COVERT EXFILTRATION \"\n" + 
			"  specification: \"The organization analyzes outbound communications traffic at the external boundary of the information system (i.e., system perimeter) and at [Assignment: organization-defined interior points within the system (e.g., subsystems, subnetworks)] to detect covert exfiltration of information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(19) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | INDIVIDUALS POSING GREATER RISK \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined additional monitoring] of individuals who have been identified by [Assignment: organization-defined sources] as posing an increased level of risk.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(20) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | PRIVILEGED USERS \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined additional monitoring] of privileged users.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(21) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | PROBATIONARY PERIODS \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined additional monitoring] of individuals during [Assignment: organization-defined probationary period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(22) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | UNAUTHORIZED NETWORK SERVICES \"\n" + 
			"  specification: \"The information system detects network services that have not been authorized or approved by [Assignment: organization-defined authorization or approval processes] and [Selection (one or more): audits; alerts [Assignment: organization-defined personnel or roles]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(23) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | HOST-BASED DEVICES \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined host-based monitoring mechanisms] at [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(24) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | INDICATORS OF COMPROMISE \"\n" + 
			"  specification: \"The information system discovers, collects, distributes, and uses indicators of compromise.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-5 { \n" + 
			"  name: \"SECURITY ALERTS, ADVISORIES, AND DIRECTIVES \"\n" + 
			"  specification: \"The organization: a. Receives information system security alerts, advisories, and directives from [Assignment: organization-defined external organizations] on an ongoing basis; b. Generates internal security alerts, advisories, and directives as deemed necessary; c. Disseminates security alerts, advisories, and directives to: [Selection (one or more): [Assignment: organization-defined personnel or roles]; [Assignment: organization-defined elements within the organization]; [Assignment: organization-defined external organizations]]; and d. Implements security directives in accordance with established time frames, or notifies the issuing organization of the degree of noncompliance.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-5(1) { \n" + 
			"  name: \"SECURITY ALERTS, ADVISORIES, AND DIRECTIVES | AUTOMATED ALERTS AND ADVISORIES \"\n" + 
			"  specification: \"The organization employs automated mechanisms to make security alert and advisory information available throughout the organization.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-6 { \n" + 
			"  name: \"SECURITY FUNCTION VERIFICATION \"\n" + 
			"  specification: \"The information system: a. Verifies the correct operation of [Assignment: organization-defined security functions]; b. Performs this verification [Selection (one or more): [Assignment: organization-defined system transitional states]; upon command by user with appropriate privilege; [Assignment: organization-defined frequency]]; c. Notifies [Assignment: organization-defined personnel or roles] of failed security verification tests; and d. [Selection (one or more): shuts the information system down; restarts the information system; [Assignment: organization-defined alternative action(s)]] when anomalies are discovered.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-6(1) { \n" + 
			"  name: \"SECURITY FUNCTION VERIFICATION | NOTIFICATION OF FAILED SECURITY TESTS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-6(2) { \n" + 
			"  name: \"SECURITY FUNCTION VERIFICATION | AUTOMATION SUPPORT FOR DISTRIBUTED TESTING \"\n" + 
			"  specification: \"The information system implements automated mechanisms to support the management of distributed security testing.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-6(3) { \n" + 
			"  name: \"SECURITY FUNCTION VERIFICATION | REPORT VERIFICATION RESULTS \"\n" + 
			"  specification: \"The organization reports the results of security function verification to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7 { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY \"\n" + 
			"  specification: \"The organization employs integrity verification tools to detect unauthorized changes to [Assignment: organization-defined software, firmware, and information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(1) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | INTEGRITY CHECKS \"\n" + 
			"  specification: \"The information system performs an integrity check of [Assignment: organization-defined software, firmware, and information] [Selection (one or more): at startup; at [Assignment: organization-defined transitional states or security-relevant events]; [Assignment: organization-defined frequency]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(2) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | AUTOMATED NOTIFICATIONS OF INTEGRITY VIOLATIONS \"\n" + 
			"  specification: \"The organization employs automated tools that provide notification to [Assignment: organization-defined personnel or roles] upon discovering discrepancies during integrity verification.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(3) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | CENTRALLY-MANAGED INTEGRITY TOOLS \"\n" + 
			"  specification: \"The organization employs centrally managed integrity verification tools.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(4) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | TAMPER-EVIDENT PACKAGING \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-12.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(5) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | AUTOMATED RESPONSE TO INTEGRITY VIOLATIONS \"\n" + 
			"  specification: \"The information system automatically [Selection (one or more): shuts the information system down; restarts the information system; implements [Assignment: organization-defined security safeguards]] when integrity violations are discovered.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(6) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to detect unauthorized changes to software, firmware, and information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(7) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | INTEGRATION OF DETECTION AND RESPONSE \"\n" + 
			"  specification: \"The organization incorporates the detection of unauthorized [Assignment: organization-defined security-relevant changes to the information system] into the organizational incident response capability.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(8) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | AUDITING CAPABILITY FOR SIGNIFICANT EVENTS \"\n" + 
			"  specification: \"The information system, upon detection of a potential integrity violation, provides the capability to audit the event and initiates the following actions: [Selection (one or more): generates an audit record; alerts current user; alerts [Assignment: organization-defined personnel or roles]; [Assignment: organization-defined other actions]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(9) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | VERIFY BOOT PROCESS \"\n" + 
			"  specification: \"The information system verifies the integrity of the boot process of [Assignment: organization-defined devices].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(10) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | PROTECTION OF BOOT FIRMWARE \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined security safeguards] to protect the integrity of boot firmware in [Assignment: organization-defined devices].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(11) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | CONFINED ENVIRONMENTS WITH LIMITED PRIVILEGES \"\n" + 
			"  specification: \"The organization requires that [Assignment: organization-defined user-installed software] execute in a confined physical or virtual machine environment with limited privileges.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(12) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | CONFINED ENVIRONMENTS WITH LIMITED PRIVILEGES \"\n" + 
			"  specification: \"The organization requires that the integrity of [Assignment: organization-defined user-installed software] be verified prior to execution.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(13) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | CODE EXECUTION IN PROTECTED ENVIRONMENTS \"\n" + 
			"  specification: \"The organization allows execution of binary or machine-executable code obtained from sources with limited or no warranty and without the provision of source code only in confined physical or virtual machine environments and with the explicit approval of [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(14) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | BINARY OR MACHINE EXECUTABLE CODE \"\n" + 
			"  specification: \"The organization: (a) Prohibits the use of binary or machine-executable code from sources with limited or no warranty and without the provision of source code; and (b) Provides exceptions to the source code requirement only for compelling mission/operational requirements and with the approval of the authorizing official.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(15) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | CODE AUTHENTICATION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to authenticate [Assignment: organization-defined software or firmware components] prior to installation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(16) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | TIME LIMIT ON PROCESS EXECUTION W/O SUPERVISION \"\n" + 
			"  specification: \"The organization does not allow processes to execute without supervision for more than [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-8 { \n" + 
			"  name: \"SPAM PROTECTION \"\n" + 
			"  specification: \"The organization: a. Employs spam protection mechanisms at information system entry and exit points to detect and take action on unsolicited messages; and b. Updates spam protection mechanisms when new releases are available in accordance with organizational configuration management policy and procedures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-8(1) { \n" + 
			"  name: \"SPAM PROTECTION | CENTRAL MANAGEMENT \"\n" + 
			"  specification: \"The organization centrally manages spam protection mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-8(2) { \n" + 
			"  name: \"SPAM PROTECTION | AUTOMATIC UPDATES \"\n" + 
			"  specification: \"The information system automatically updates spam protection mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-8(3) { \n" + 
			"  name: \"SPAM PROTECTION | CONTINUOUS LEARNING CAPABILITY \"\n" + 
			"  specification: \"The information system implements spam protection mechanisms with a learning capability to more effectively identify legitimate communications traffic.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-9 { \n" + 
			"  name: \"INFORMATION INPUT RESTRICTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-2, AC-3, AC-5, AC-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-10 { \n" + 
			"  name: \"INFORMATION INPUT VALIDATION \"\n" + 
			"  specification: \"The information system checks the validity of [Assignment: organization-defined information inputs].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-10(1) { \n" + 
			"  name: \"INFORMATION INPUT VALIDATION | MANUAL OVERRIDE CAPABILITY \"\n" + 
			"  specification: \"The information system: (a) Provides a manual override capability for input validation of [Assignment: organization-defined inputs]; (b) Restricts the use of the manual override capability to only [Assignment: organization-defined authorized individuals]; and (c) Audits the use of the manual override capability.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-10(2) { \n" + 
			"  name: \"INFORMATION INPUT VALIDATION | REVIEW / RESOLUTION OF ERRORS \"\n" + 
			"  specification: \"The organization ensures that input validation errors are reviewed and resolved within [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-10(3) { \n" + 
			"  name: \"INFORMATION INPUT VALIDATION | PREDICTABLE BEHAVIOR \"\n" + 
			"  specification: \"The information system behaves in a predictable and documented manner that reflects organizational and system objectives when invalid inputs are received.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-10(4) { \n" + 
			"  name: \"INFORMATION INPUT VALIDATION | REVIEW / TIMING INTERACTIONS \"\n" + 
			"  specification: \"The organization accounts for timing interactions among information system components in determining appropriate responses for invalid inputs.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-10(5) { \n" + 
			"  name: \"INFORMATION INPUT VALIDATION | RESTRICT INPUTS TO TRUSTED SOURCES AND APPROVED FORMATS \"\n" + 
			"  specification: \"The organization restricts the use of information inputs to [Assignment: organization-defined trusted sources] and/or [Assignment: organization-defined formats].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-11 { \n" + 
			"  name: \"ERROR HANDLING \"\n" + 
			"  specification: \"The information system: a. Generates error messages that provide information necessary for corrective actions without revealing information that could be exploited by adversaries; and b. Reveals error messages only to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-12 { \n" + 
			"  name: \"INFORMATION HANDLING AND RETENTION \"\n" + 
			"  specification: \"The organization handles and retains information within the information system and information output from the system in accordance with applicable federal laws, Executive Orders, directives, policies, regulations, standards, and operational requirements.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-13 { \n" + 
			"  name: \"PREDICTABLE FAILURE PREVENTION \"\n" + 
			"  specification: \"The organization: a. Determines mean time to failure (MTTF) for [Assignment: organization-defined information system components] in specific environments of operation; and b. Provides substitute information system components and a means to exchange active and standby components at [Assignment: organization-defined MTTF substitution criteria].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-13(1) { \n" + 
			"  name: \"PREDICTABLE FAILURE PREVENTION | TRANSFERRING COMPONENT RESPONSIBILITIES \"\n" + 
			"  specification: \"The organization takes information system components out of service by transferring component responsibilities to substitute components no later than [Assignment: organization-defined fraction or percentage] of mean time to failure.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-13(2) { \n" + 
			"  name: \"PREDICTABLE FAILURE PREVENTION | TIME LIMIT ON PROCESS EXECUTION WITHOUT SUPERVISION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-7 (16).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-13(3) { \n" + 
			"  name: \"PREDICTABLE FAILURE PREVENTION | MANUAL TRANSFER BETWEEN COMPONENTS \"\n" + 
			"  specification: \"The organization manually initiates transfers between active and standby information system components [Assignment: organization-defined frequency] if the mean time to failure exceeds [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-13(4) { \n" + 
			"  name: \"PREDICTABLE FAILURE PREVENTION | STANDBY COMPONENT INSTALLATION / NOTIFICATION \"\n" + 
			"  specification: \"The organization, if information system component failures are detected: (a) Ensures that the standby components are successfully and transparently installed within [Assignment: organization-defined time period]; and (b) [Selection (one or more): activates [Assignment: organization-defined alarm]; automatically shuts down the information system].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-13(5) { \n" + 
			"  name: \"PREDICTABLE FAILURE PREVENTION | FAILOVER CAPABILITY \"\n" + 
			"  specification: \"The organization provides [Selection: real-time; near real-time] [Assignment: organization-defined failover capability] for the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-14 { \n" + 
			"  name: \"NON-PERSISTENCE \"\n" + 
			"  specification: \"The organization implements non-persistent [Assignment: organization-defined information system components and services] that are initiated in a known state and terminated [Selection (one or more): upon end of session of use; periodically at [Assignment: organization-defined frequency]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-14(1) { \n" + 
			"  name: \"NON-PERSISTENCE | REFRESH FROM TRUSTED SOURCES \"\n" + 
			"  specification: \"The organization ensures that software and data employed during information system component and service refreshes are obtained from [Assignment: organization-defined trusted sources].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-15 { \n" + 
			"  name: \"INFORMATION OUTPUT FILTERING \"\n" + 
			"  specification: \"The information system validates information output from [Assignment: organization-defined software programs and/or applications] to ensure that the information is consistent with the expected content.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-16 { \n" + 
			"  name: \"MEMORY PROTECTION \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined security safeguards] to protect its memory from unauthorized code execution.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-17 { \n" + 
			"  name: \"FAIL-SAFE PROCEDURES \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined fail-safe procedures] when [Assignment: organization-defined failure conditions occur].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"\n" + 
			"\n" + 
			"}}\n" + 
			"";
	
	public static String data_old = "camel model TSMModel {\n" + 
			"	 security model SEC {\n" + 
			"	 	security capability CAP1 {\n" + 
			"\n" + 
			"	 	}\n" + 
			"	 	\n" + 
			"	 	security capability CAP2 {\n" + 
			"	 		controls [MUSASEC.AC-11(1), MUSASEC.AU-13(2), MUSASEC.AC-17(6)]\n" + 
			"	 	}\n" + 
			"	 	\n" + 
			"	 }\n" + 
			"\n" + 
			"\n" + 
			"location model TSMLocation {\n" + 
			"	region EU {\n" + 
			"		name: Europe \n" + 
			"	}\n" + 
			"\n" + 
			"	country DE {\n" + 
			"		name: Germany\n" + 
			"		// parentRegions is not a compulsory property \n" + 
			"		// parent regions [TSMLocation.EU]\n" + 
			"	}\n" + 
			"\n" + 
			"	country UK {\n" + 
			"		name: UnitedKingdom\n" + 
			"		parent regions [TSMLocation.EU]\n" + 
			"	}\n" + 
			"\n" + 
			"	country FI {\n" + 
			"		name: Finland\n" + 
			"		parent regions [TSMLocation.EU]\n" + 
			"	}\n" + 
			"\n" + 
			"} \n" + 
			"\n" + 
			"requirement model TSMRequirement {\n" + 
			"	// example of QuantitativeHardwareRequirement only applicable to a specific Component \n" + 
			"	quantitative hardware JourneyPlanner {\n" + 
			"		ram: 1024..   // always in MEGABYTES\n" + 
			"		storage: 10.. // always in GIGABYTES // TODO: Is it required to link with range values and units???\n" + 
			"	}\n" + 
			"\n" + 
			"	/**TUT: maybe 32 cores is a little bit too much if we are talking about microservices **/\n" + 
			"	quantitative hardware CoreIntensive {\n" + 
			"		core: 4..16\n" + 
			"		ram: 1024..8192\n" + 
			"	}\n" + 
			"\n" + 
			"	quantitative hardware CPUIntensive {\n" + 
			"		core: 1..       // min and max number of CPU cores\n" + 
			"		ram: 1024..8192 // size of RAM \n" + 
			"		cpu: 1.0..      // min and max CPU frequency\n" + 
			"	}\n" + 
			"\n" + 
			"	quantitative hardware StorageIntensive {\n" + 
			"		storage: 1000..\n" + 
			"	}\n" + 
			"\n" + 
			"	os Ubuntu {os: Ubuntu 64os}\n" + 
			"\n" + 
			"	location requirement GermanyReq {\n" + 
			"		locations [TSMLocation.DE]\n" + 
			"	}\n" + 
			"\n" + 
			"	location requirement UKReq {\n" + 
			"		locations [TSMLocation.UK]\n" + 
			"	}\n" + 
			"	\n" + 
			"	location requirement FinlandReq {\n" + 
			"		locations [TSMLocation.FI]\n" + 
			"	}\n" + 
			"	\n" + 
			"} // requirement model TSMRequirement\n" + 
			"\n" + 
			"type model TUTType {\n" + 
			"	enumeration VMTypeEnum {\n" + 
			"		values [ 'M1.MICRO' : 0,\n" + 
			"		'M1.TINY' : 1,\n" + 
			"		'M1.SMALL' : 2,\n" + 
			"		'M1.MEDIUM' : 3,\n" + 
			"		'M1.LARGE' : 4,\n" + 
			"		'M1.XLARGE' : 5,\n" + 
			"		'M1.XXLARGE' : 6,\n" + 
			"		'M2.SMALL' : 7,\n" + 
			"		'M2.MEDIUM' : 8,\n" + 
			"		'M2.LARGE' : 9,\n" + 
			"		'M2.XLARGE' : 10,\n" + 
			"		'C1.SMALL' : 11,\n" + 
			"		'C1.MEDIUM' : 12,\n" + 
			"		'C1.LARGE' : 13,\n" + 
			"		'C1.XLARGE' : 14,\n" + 
			"		'C1.XXLARGE' : 15 ]\n" + 
			"	}\n" + 
			"	range MemoryRange {\n" + 
			"		primitive type: IntType\n" + 
			"		lower limit {\n" + 
			"			int value 256 included\n" + 
			"		}\n" + 
			"		upper limit {\n" + 
			"			int value 32768 included\n" + 
			"		}\n" + 
			"	}\n" + 
			"	range StorageRange {\n" + 
			"		primitive type: IntType\n" + 
			"		lower limit {\n" + 
			"			int value 0 included\n" + 
			"		}\n" + 
			"		upper limit {\n" + 
			"			int value 160 included\n" + 
			"		}\n" + 
			"	}\n" + 
			"	range CoresRange {\n" + 
			"		primitive type: IntType\n" + 
			"		lower limit {\n" + 
			"			int value 1 included\n" + 
			"		}\n" + 
			"		upper limit {\n" + 
			"			int value 16 included\n" + 
			"		}\n" + 
			"	}\n" + 
			"	string value type StringValueType {\n" + 
			"		primitive type: StringType\n" + 
			"	}\n" + 
			"	\n" + 
			"	list StorageList {\n" + 
			"		values [ int value 0,\n" + 
			"		int value 20,\n" + 
			"		int value 40,\n" + 
			"		int value 80,\n" + 
			"		int value 160 ]\n" + 
			"	}\n" + 
			"	\n" + 
			"	list MemoryList {\n" + 
			"		values [ int value 256,\n" + 
			"		int value 512,\n" + 
			"		int value 2048,\n" + 
			"		int value 4096,\n" + 
			"		int value 8192,\n" + 
			"		int value 16384,\n" + 
			"		int value 32768 ]\n" + 
			"	}\n" + 
			"	\n" + 
			"	list CoresList {\n" + 
			"		values [ int value 1,\n" + 
			"		int value 2,\n" + 
			"		int value 4,\n" + 
			"		int value 8,\n" + 
			"		int value 16 ]\n" + 
			"	}\n" + 
			"	\n" + 
			"	range Range_0_100 {\n" + 
			"		primitive type: IntType\n" + 
			"		lower limit {int value 0 included}\n" + 
			"		upper limit {int value 100}\n" + 
			"	}\n" + 
			"	\n" + 
			"	range Range_0_10000 {\n" + 
			"		primitive type: IntType\n" + 
			"		lower limit {int value 0}\n" + 
			"		upper limit {int value 10000 included}\n" + 
			"	}\n" + 
			"	\n" + 
			"	range DoubleRange_0_100 {\n" + 
			"		primitive type: DoubleType\n" + 
			"		lower limit {double value 0.0 included}\n" + 
			"		upper limit {double value 100.0 included}\n" + 
			"	}	\n" + 
			"} // type model TUTType\n" + 
			"\n" + 
			"unit model TSMUnit {\n" + 
			"	storage unit { StorageUnit: GIGABYTES }\n" + 
			"	\n" + 
			"	time interval unit {minutes: MINUTES}\n" + 
			"	\n" + 
			"	time interval unit {seconds: SECONDS}\n" + 
			"	\n" + 
			"	/* \n" + 
			"	 * TODO: Is it required in MUSA??? \n" + 
			"	memory unit { MemoryUnit: MEGABYTES }\n" + 
			"	*/\n" + 
			"	\n" + 
			"	/* some examples... */\n" + 
			"    monetary unit {Euro: EUROS}\n" + 
			"	throughput unit {SimulationsPerSecondUnit: TRANSACTIONS_PER_SECOND}\n" + 
			"	time interval unit {ResponseTimeUnit: MILLISECONDS}\n" + 
			"	time interval unit {ExperimentMakespanInSecondsUnit: SECONDS}\n" + 
			"	transaction unit {NumberOfSimulationsLeftInExperimentUnit: TRANSACTIONS}\n" + 
			"	dimensionless {AvailabilityUnit: PERCENTAGE}\n" + 
			"	dimensionless {CPUUnit: PERCENTAGE}\n" + 
			"} // unit model TUTUnit\n" + 
			"\n" + 
			"\n" + 
			"application TSMApplication {\n" + 
			"	version: 'v1.0'\n" + 
			"	owner: TUTOrganisation.test_user1\n" + 
			"	deployment models [TSMModel.TSMDeployment]\n" + 
			"} // application TSMApplication\n" + 
			"\n" + 
			"\n" + 
			"\n" + 
			"organisation model TUTOrganisation {\n" + 
			"	organisation TUT {\n" + 
			"		www: 'http://www.tut.fi/en/home'\n" + 
			"		postal address: 'Korkeakoulunkatu 10, 33720 Tampere, Finland'\n" + 
			"		email: 'test.test@tut.fi'\n" + 
			"	}\n" + 
			"\n" + 
			"	user test_user1 {\n" + 
			"		first name: test_name\n" + 
			"		last name: test_surname\n" + 
			"		email: 'test_name.test_surname@tut.fi'\n" + 
			"		musa credentials {\n" + 
			"		    end time: 2017-11-01\n" + 
			"			username: 'mcp'\n" + 
			"			password: 'test_name_surname'\n" + 
			"			}\n" + 
			"	}\n" + 
			"	\n" + 
			"	user test_user2 {\n" + 
			"	first name: user2_name\n" + 
			"	last name: user2_surname\n" + 
			"	email: 'its email'\n" + 
			"	musa credentials {\n" + 
			"	    username: 'user2'\n" + 
			"	    password: 'user2_passw'\n" + 
			"	    }\n" + 
			"	}\n" + 
			"	\n" + 
			"	user group test_group {\n" + 
			"	users [TUTOrganisation.test_user1, TUTOrganisation.test_user2]\n" + 
			"	}\n" + 
			"	\n" + 
			"	\n" + 
			"	role devop\n" + 
			"	\n" + 
			"    role assignment test_nameDevop {\n" + 
			"        start: 2016-02-26\n" + 
			"        end: 2017-02-26\n" + 
			"        assigned on: 2016-02-25\n" + 
			"        users: [TUTOrganisation.test_user1, TUTOrganisation.test_user2]\n" + 
			"        role: devop\n" + 
			"    }\n" + 
			"    \n" + 
			"    role assignment test_groupDevop {\n" + 
			"        start: 2016-02-01\n" + 
			"        end: 2017-02-26\n" + 
			"        assigned on:  2016-02-25\n" + 
			"        role: devop\n" + 
			"        user groups: [TUTOrganisation.test_group]\n" + 
			"    }\n" + 
			"\n" + 
			"	security level: HIGH\n" + 
			"} // organisation model TUTOrganisation\n" + 
			"\n" + 
			"\n" + 
			"deployment model TSMDeployment {\n" + 
			"// example of VMRequirementSet only applicable to a specific Component \n" + 
			"	requirement set JourneyPlannerHostRS {\n" + 
			"		os: TSMRequirement.Ubuntu\n" + 
			"		quantitative hardware: TSMRequirement.JourneyPlanner\n" + 
			"		location: TSMRequirement.UKReq\n" + 
			"	}\n" + 
			"\n" + 
			"	requirement set CoreIntensiveUbuntuFinlandRS {\n" + 
			"		os: TSMRequirement.Ubuntu\n" + 
			"		quantitative hardware: TSMRequirement.CoreIntensive\n" + 
			"		location: TSMRequirement.UKReq\n" + 
			"	}\n" + 
			"\n" + 
			"	requirement set CPUIntensiveUbuntuFinlandRS {\n" + 
			"		os: TSMRequirement.Ubuntu\n" + 
			"		quantitative hardware: TSMRequirement.CPUIntensive\n" + 
			"		location: TSMRequirement.UKReq\n" + 
			"	}\n" + 
			"\n" + 
			"	requirement set CPUIntensiveUbuntuUKRS {\n" + 
			"		os: TSMRequirement.Ubuntu\n" + 
			"		quantitative hardware: TSMRequirement.CPUIntensive\n" + 
			"		location: TSMRequirement.UKReq\n" + 
			"	}\n" + 
			"	\n" + 
			"	requirement set StorageIntensiveUbuntuFinlandRS {\n" + 
			"		os: TSMRequirement.Ubuntu\n" + 
			"		quantitative hardware: TSMRequirement.StorageIntensive\n" + 
			"		location: TSMRequirement.UKReq\n" + 
			"	}\n" + 
			"\n" + 
			"\n" + 
			"	vm JourneyPlannerVM {\n" + 
			"		requirement set JourneyPlannerHostRS\n" + 
			"		provided host JourneyPlannerHost\n" + 
			"	}\n" + 
			"\n" + 
			"\n" + 
			"	vm CoreIntensiveUbuntuFinlandVM {\n" + 
			"		requirement set CoreIntensiveUbuntuFinlandRS\n" + 
			"		provided host CoreIntensiveUbuntuFinlandHost\n" + 
			"	}\n" + 
			"\n" + 
			"	vm CPUIntensiveUbuntuFinlandVM {\n" + 
			"		requirement set CPUIntensiveUbuntuFinlandRS\n" + 
			"		provided host CPUIntensiveUbuntuFinlandHost\n" + 
			"	}\n" + 
			"\n" + 
			"	vm StorageIntensiveUbuntuFinlandVM {\n" + 
			"		requirement set StorageIntensiveUbuntuFinlandRS\n" + 
			"		provided host StorageIntensiveUbuntuFinlandHost\n" + 
			"	}\n" + 
			"\n" + 
			"	vm CPUIntensiveUbuntuUKVM {\n" + 
			"		requirement set CPUIntensiveUbuntuUKRS\n" + 
			"		provided host CPUIntensiveUbuntuUKHost\n" + 
			"	}\n" + 
			"\n" + 
			"    pool poolTSM{\n" + 
			"        type: VM\n" + 
			"        policy: round-robin\n" + 
			"        required host CPUIntensiveUbuntuUKVM.CPUIntensiveUbuntuUKHost\n" + 
			"        required host CPUIntensiveUbuntuFinlandVM.CPUIntensiveUbuntuFinlandHost\n" + 
			"        \n" + 
			"        provided host hostPoolTSM\n" + 
			"    }\n" + 
			"    \n" + 
			"    container containerTSM{\n" + 
			"        type: docker_swarm\n" + 
			"        allocationStrategy: custom\n" + 
			"        required host poolTSM {\n" + 
			"            manager: CPUIntensiveUbuntuUKVM.CPUIntensiveUbuntuUKHost\n" + 
			"        }\n" + 
			"    }\n" + 
			"\n" + 
			"	internal component TSMEngine {\n" + 
			"		type: COTS.IDM\n" + 
			"		order:  3\n" + 
			"		IP public:  false\n" + 
			"		provided security capability TSMEngineCap\n" + 
			"		required security capability TSMEngineCapReq\n" + 
			"\n" + 
			"		provided communication TSMEnginePort { port: 8185 }\n" + 
			"		provided communication TSMEngineRESTPort { port: 443 }	\n" + 
			"		required communication IDManagerPortReq {port: 3000 mandatory}\n" + 
			"		required communication ConsumptionEstimatorPortReq {port: 9090 mandatory}\n" + 
			"		required communication JourneyPlannerPortReq {port: 8085  mandatory}\n" + 
			"		required communication DatabasePortReq {port: 3306 mandatory}\n" + 
			"		\n" + 
			"		required container containerTSM {host: CPUIntensiveUbuntuFinlandVM.CPUIntensiveUbuntuFinlandHost}\n" + 
			"		\n" + 
			"		configuration TSMEngineConfigurationCHEF {\n" + 
			"			CHEF configuration manager C1 { //Configuration Management tool\n" + 
			"				cookbook: 'tut'\n" + 
			"				recipe: 'musa_tsme'\n" + 
			"			}\n" + 
			"		}			\n" + 
			"	}\n" + 
			"\n" + 
			"	internal component JourneyPlanner {\n" + 
			"	type: SERVICE.Firewall\n" + 
			"			order: 4\n" + 
			"		provided security capability JourneyPlannerCap {\n" + 
			"security capability SEC.CAP1\n" + 
			"		}\n" + 
			"\n" + 
			"		provided communication JourneyPlannerPort {port: 8085}\n" + 
			"		required communication ConsumptionEstimatorPortReq {port: 9090 mandatory}\n" + 
			"		required host JourneyPlannerHostReq\n" + 
			"		configuration JourneyPlannerManualConfiguration{\n" + 
			"			CHEF configuration manager C1 {\n" + 
			"				cookbook: 'tut'\n" + 
			"				recipe: 'musa_mjp'\n" + 
			"			}\n" + 
			"		}\n" + 
			"	}\n" + 
			"\n" + 
			"	internal component ConsumptionEstimator {\n" + 
			"	IP public: false\n" + 
			"	provided security capability ccc {\n" + 
			"	security capability SEC.CAP2 }\n" + 
			"	\n" + 
			"	\n" + 
			"	\n" + 
			"		provided communication ConsumptionEstimatorPort {port: 9090}\n" + 
			"		required host CPUIntensiveUbuntuUKHostReq\n" + 
			"		configuration ConsumptionManualEstimatorConfiguration{\n" + 
			"			CHEF configuration manager C1 {\n" + 
			"				cookbook: 'tut'\n" + 
			"				recipe: 'musa_cec'\n" + 
			"			}\n" + 
			"		}\n" + 
			"	}\n" + 
			"\n" + 
			"	internal component IDMAM {\n" + 
			"		provided communication IDManagerPort {port: 3000}\n" + 
			"		provided communication MongoDBPort {port: 27017}\n" + 
			"		required host StorageIntensiveUbuntuFinlandHostReq\n" + 
			"		configuration IDManagerManualConfiguration{\n" + 
			"			CHEF configuration manager C1 { //Configuration Management tool\n" + 
			"				cookbook: 'tut'\n" + 
			"				recipe: 'musa_idm'  // IDM database installed within the same recipe\n" + 
			"			}\n" + 
			"		}\n" + 
			"	}\n" + 
			"	\n" + 
			"	internal component JourneyDatabase {\n" + 
			"		provided communication DatabasePort {port: 3306}\n" + 
			"		required host StorageIntensiveUbuntuFinlandHostReq\n" + 
			"		configuration DatabaseManualConfiguration{\n" + 
			"			CHEF configuration manager C1 { //Configuration Management tool\n" + 
			"				cookbook: 'tut'\n" + 
			"				recipe: 'musa_db'\n" + 
			"			}\n" + 
			"		}\n" + 
			"	}\n" + 
			"\n" + 
			"\n" + 
			"	hosting JourneyPlannerToSpecificJourneyPlannerHost {\n" + 
			"		from JourneyPlanner.JourneyPlannerHostReq to pool poolTSM.hostPoolTSM\n" + 
			"	}\n" + 
			"\n" + 
			"	hosting ConsumptionEstimatorToCPUIntensiveUbuntuUK {\n" + 
			"		from ConsumptionEstimator.CPUIntensiveUbuntuUKHostReq to CPUIntensiveUbuntuUKVM.CPUIntensiveUbuntuUKHost\n" + 
			"	}\n" + 
			"\n" + 
			"\n" + 
			"	hosting DatabaseToStorageIntensiveUbuntuFinland {\n" + 
			"		from IDMAM.StorageIntensiveUbuntuFinlandHostReq to StorageIntensiveUbuntuFinlandVM.StorageIntensiveUbuntuFinlandHost\n" + 
			"	}\n" + 
			"\n" + 
			"	communication TSMEngineToDatabase {\n" + 
			"		type: REMOTE\n" + 
			"		from TSMEngine.DatabasePortReq to IDMAM.MongoDBPort\n" + 
			"		protocol MYSQL\n" + 
			"	}\n" + 
			"\n" + 
			"	communication TSMEngineToIDManager {\n" + 
			"		type: REMOTE		\n" + 
			"		from TSMEngine.IDManagerPortReq to IDMAM.IDManagerPort\n" + 
			"	}\n" + 
			"\n" + 
			"	communication TSMEngineToJourneyPlanner {\n" + 
			"		type: REMOTE\n" + 
			"		from TSMEngine.JourneyPlannerPortReq to JourneyPlanner.JourneyPlannerPort\n" + 
			"	}\n" + 
			"\n" + 
			"	communication JourneyPlannerToConsumptionEstimator {\n" + 
			"		type: REMOTE		\n" + 
			"		from JourneyPlanner.ConsumptionEstimatorPortReq to ConsumptionEstimator.ConsumptionEstimatorPort\n" + 
			"	}\n" + 
			"	\n" + 
			"	capability match TSMEngineToJourneyPlanner {\n" + 
			"        		from TSMEngine.TSMEngineCapReq to  JourneyPlanner.JourneyPlannerCap\n" + 
			"	}\n" + 
			"\n" + 
			"	\n" + 
			"} // deployment model TSMDeployment\n" + 
			"\n" + 
			"\n" + 
			"\n" + 
			"\n" + 
			"\n" + 
			"//Metric model for TSM App\n" + 
			"metric model TSMMetric {\n" + 
			"	window Win5Min {\n" + 
			"		window type: SLIDING\n" + 
			"		size type: TIME_ONLY\n" + 
			"		time size: 5\n" + 
			"		unit: TSMModel.TSMUnit.minutes\n" + 
			"	}\n" + 
			"\n" + 
			"	window Win1Min {\n" + 
			"		window type: SLIDING\n" + 
			"		size type: TIME_ONLY\n" + 
			"		time size: 1\n" + 
			"		unit: TSMModel.TSMUnit.minutes\n" + 
			"	}\n" + 
			"\n" + 
			"	schedule Schedule1Min {\n" + 
			"		type: FIXED_RATE\n" + 
			"		interval: 1\n" + 
			"		unit: TSMModel.TSMUnit.minutes\n" + 
			"	}\n" + 
			"\n" + 
			"	schedule Schedule1Sec {\n" + 
			"		type: FIXED_RATE\n" + 
			"		interval: 1\n" + 
			"		unit: TSMModel.TSMUnit.seconds\n" + 
			"	}\n" + 
			"\n" + 
			"	property AvailabilityProperty {\n" + 
			"		type: MEASURABLE\n" + 
			"		sensors [TSMMetric.AvailabilitySensor]\n" + 
			"	}\n" + 
			"\n" + 
			"	property CPUProperty {\n" + 
			"		type: MEASURABLE\n" + 
			"		sensors [TSMMetric.CPUSensor]\n" + 
			"	}\n" + 
			"\n" + 
			"	property ResponseTimeProperty {\n" + 
			"		type: MEASURABLE\n" + 
			"		sensors [TSMMetric.ResponseTimeSensor]\n" + 
			"	}\n" + 
			"\n" + 
			"	property FrequencyOfVulnerabilityScanningProperty {\n" + 
			"		type: MEASURABLE\n" + 
			"		sensors [TSMMetric.FreqOfVulnScanSensor]\n" + 
			"	}\n" + 
			"\n" + 
			"	sensor AvailabilitySensor {\n" + 
			"		configuration: 'MMTAgent.Availability'\n" + 
			"		push\n" + 
			"	}\n" + 
			"\n" + 
			"	sensor CPUSensor {\n" + 
			"		configuration: 'MMTAgent.CPU'\n" + 
			"		push\n" + 
			"	}\n" + 
			"\n" + 
			"	sensor ResponseTimeSensor {\n" + 
			"		push\n" + 
			"	}\n" + 
			"\n" + 
			"	sensor FreqOfVulnScanSensor {\n" + 
			"		configuration: 'MMTAgent.FreqOfVulnScan'\n" + 
			"		push\n" + 
			"	}\n" + 
			"	raw metric AvailabilityMetric {\n" + 
			"		value direction: 1\n" + 
			"		layer: SaaS\n" + 
			"		property: TSMModel.TSMMetric.AvailabilityProperty\n" + 
			"		unit: TSMModel.TSMUnit.AvailabilityUnit\n" + 
			"		value type: TSMModel.TUTType.DoubleRange_0_100\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric CPUMetric {\n" + 
			"		value direction: 0\n" + 
			"		layer: IaaS\n" + 
			"		property: TSMModel.TSMMetric.CPUProperty\n" + 
			"		unit: TSMModel.TSMUnit.CPUUnit\n" + 
			"		value type: TSMModel.TUTType.Range_0_100\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric ResponseTimeMetric {\n" + 
			"		value direction: 0\n" + 
			"		layer: SaaS\n" + 
			"		property: TSMModel.TSMMetric.ResponseTimeProperty\n" + 
			"		unit: TSMModel.TSMUnit.ResponseTimeUnit\n" + 
			"		value type: TSMModel.TUTType.Range_0_10000\n" + 
			"	}\n" + 
			"\n" + 
			"	composite metric MeanValueOfResponseTimeOfAllTSMEngineMetric {\n" + 
			"		value direction: 0\n" + 
			"		layer: SaaS\n" + 
			"		property: TSMModel.TSMMetric.ResponseTimeProperty\n" + 
			"		unit: TSMModel.TSMUnit.ResponseTimeUnit\n" + 
			"\n" + 
			"		metric formula MeanValueOfResponseTimeOfAllTSMEngineFormula {\n" + 
			"			function arity: UNARY\n" + 
			"			function pattern: MAP\n" + 
			"			MEAN(TSMModel.TSMMetric.ResponseTimeMetric)\n" + 
			"		}\n" + 
			"	}\n" + 
			"\n" + 
			"	composite metric CPUAverage {\n" + 
			"		description: \"Average usage of the CPU\"\n" + 
			"		value direction: 1\n" + 
			"		layer: PaaS\n" + 
			"		property: TSMModel.TSMMetric.CPUProperty\n" + 
			"		unit: TSMModel.TSMUnit.CPUUnit\n" + 
			"\n" + 
			"		metric formula Formula_Average {\n" + 
			"			function arity: UNARY\n" + 
			"			function pattern: REDUCE\n" + 
			"			MEAN( TSMModel.TSMMetric.CPUMetric )\n" + 
			"		}\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric context TSMEngineAvailabilityContext {\n" + 
			"		metric: TSMModel.TSMMetric.AvailabilityMetric\n" + 
			"		sensor: TSMMetric.AvailabilitySensor\n" + 
			"		component: TSMModel.TSMDeployment.TSMEngine\n" + 
			"		quantifier: ANY\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric context CPUMetricConditionContext {\n" + 
			"		metric: TSMModel.TSMMetric.CPUMetric\n" + 
			"		sensor: TSMMetric.CPUSensor\n" + 
			"		component: TSMModel.TSMDeployment.TSMEngine\n" + 
			"		quantifier: ANY\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric context TSMEngineResponseTimeContext {\n" + 
			"		metric: TSMModel.TSMMetric.ResponseTimeMetric\n" + 
			"		sensor: TSMMetric.ResponseTimeSensor\n" + 
			"		component: TSMModel.TSMDeployment.TSMEngine\n" + 
			"		quantifier: ANY\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric context JourneyPlannerResponseTimeContext {\n" + 
			"		metric: TSMModel.TSMMetric.ResponseTimeMetric\n" + 
			"		sensor: TSMMetric.ResponseTimeSensor\n" + 
			"		component: TSMModel.TSMDeployment.JourneyPlanner\n" + 
			"		quantifier: ANY\n" + 
			"	}\n" + 
			"\n" + 
			"	raw metric context CPURawMetricContext {\n" + 
			"		metric: TSMModel.TSMMetric.CPUMetric\n" + 
			"		sensor: TSMMetric.CPUSensor\n" + 
			"		component: TSMModel.TSMDeployment.TSMEngine\n" + 
			"		schedule: TSMModel.TSMMetric.Schedule1Sec\n" + 
			"		quantifier: ALL\n" + 
			"	}\n" + 
			"\n" + 
			"	composite metric context CPUAvgMetricContextAll {\n" + 
			"		metric: TSMModel.TSMMetric.CPUAverage\n" + 
			"		component: TSMModel.TSMDeployment.TSMEngine\n" + 
			"		window: TSMModel.TSMMetric.Win5Min\n" + 
			"		schedule: TSMModel.TSMMetric.Schedule1Min\n" + 
			"		composing metric contexts [TSMModel.TSMMetric.CPURawMetricContext]\n" + 
			"		quantifier: ALL\n" + 
			"	}\n" + 
			"\n" + 
			"	composite metric context CPUAvgMetricContextAny {\n" + 
			"		metric: TSMModel.TSMMetric.CPUAverage\n" + 
			"		component: TSMModel.TSMDeployment.TSMEngine\n" + 
			"		window: TSMModel.TSMMetric.Win1Min\n" + 
			"		schedule: TSMModel.TSMMetric.Schedule1Min\n" + 
			"		composing metric contexts [TSMModel.TSMMetric.CPURawMetricContext]\n" + 
			"		quantifier: ANY\n" + 
			"	}\n" + 
			"\n" + 
			"	metric condition TSMEngineAvailabilityCondition {\n" + 
			"		context: TSMModel.TSMMetric.TSMEngineAvailabilityContext\n" + 
			"		threshold: 99.0\n" + 
			"		comparison operator: >\n" + 
			"	}\n" + 
			"\n" + 
			"	metric condition CPUMetricCondition {\n" + 
			"		context: TSMModel.TSMMetric.CPUMetricConditionContext\n" + 
			"		threshold: 80.0\n" + 
			"		comparison operator: >\n" + 
			"	}\n" + 
			"\n" + 
			"	metric condition TSMEngineResponseTimeCondition {\n" + 
			"		context: TSMModel.TSMMetric.TSMEngineResponseTimeContext\n" + 
			"		threshold: 0.3\n" + 
			"		comparison operator: <\n" + 
			"	}\n" + 
			"\n" + 
			"	metric condition JourneyPlannerResponseTimeCondition {\n" + 
			"		context: TSMModel.TSMMetric.JourneyPlannerResponseTimeContext\n" + 
			"		threshold: 700.0\n" + 
			"		comparison operator: >\n" + 
			"	}\n" + 
			"\n" + 
			"	metric condition CPUAvgMetricConditionAll {\n" + 
			"		context: TSMModel.TSMMetric.CPUAvgMetricContextAll\n" + 
			"		threshold: 50.0\n" + 
			"		comparison operator: >\n" + 
			"	}\n" + 
			"\n" + 
			"	metric condition CPUAvgMetricConditionAny {\n" + 
			"		context: TSMModel.TSMMetric.CPUAvgMetricContextAny\n" + 
			"		threshold: 80.0\n" + 
			"		comparison operator: >\n" + 
			"	}\n" + 
			"} // metric model TSMMetric {\n" + 
			"\n" + 
			"\n" + 
			"\n" + 
			"\n" + 
			"	internal component type COTS.Web\n" + 
			"	internal component type COTS.Storage\n" + 
			"	internal component type COTS.IDM \n" + 
			"	internal component type COTS.Firewall \n" + 
			"\n" + 
			"\n" + 
			"	internal component type SERVICE.Web\n" + 
			"	internal component type SERVICE.Storage\n" + 
			"	internal component type SERVICE.IDM \n" + 
			"	internal component type SERVICE.Firewall \n" + 
			"\n" + 
			"\n" + 
			"	internal component type AGENT.IDM_SPECS\n" + 
			"	internal component type AGENT.IDC_AC\n" + 
			"	internal component type AGENT.IDS_MONTIMAGE\n" + 
			"	internal component type AGENT.Test1\n" + 
			"	internal component type AGENT.Test2\n" + 
			"	\n" + 
			"		security model MUSASEC {\n" + 
			"\n" + 
			"  domain AC { name: \"ACCESS CONTROL\" } \n" + 
			"  domain AT { name: \"AWARENESS AND TRAINING\" } \n" + 
			"  domain AU { name: \"AUDIT AND ACCOUNTABILITY\" } \n" + 
			"  domain CA { name: \"SECURITY ASSESSMENT AND AUTHORIZATION\" } \n" + 
			"  domain CM { name: \"CONFIGURATION MANAGEMENT\" } \n" + 
			"  domain CP { name: \"CONTIGENCY PLANNING\" } \n" + 
			"  domain IA { name: \"IDENTIFICATION AND AUTHENTICATION\" } \n" + 
			"  domain IR { name: \"INCIDENT RESPONSE\" } \n" + 
			"  domain MA { name: \"MAINTENANCE\" } \n" + 
			"  domain MP { name: \"MEDIA PROTECTION\" } \n" + 
			"  domain PE { name: \"PHYSICAL AND ENVIRONMENTAL PROTECTION\" } \n" + 
			"  domain PL { name: \"PLANNING\" } \n" + 
			"  domain PS { name: \"PERSONNEL SECURITY\" } \n" + 
			"  domain RA { name: \"RISK ASSESSMENT\" } \n" + 
			"  domain SA { name: \"SYSTEM AND SERVICES ACQUISITION\" } \n" + 
			"  domain SC { name: \"SYSTEM AND COMMUNICATIONS PROTECTION\" } \n" + 
			"  domain SI { name: \"SYSTEM AND INFORMATION INTEGRITY\" } \n" + 
			"  security control AC-1 { \n" + 
			"  name: \"ACCESS CONTROL POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. An access control policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the access control policy and associated access controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Access control policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Access control procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2 { \n" + 
			"  name: \"ACCOUNT MANAGEMENT \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Identifies and selects the following types of information system accounts to support organizational missions/business functions: [Assignment: organization-defined information system account types];\n" + 
			"			b. Assigns account managers for information system accounts;\n" + 
			"			c. Establishes conditions for group and role membership;\n" + 
			"			d. Specifies authorized users of the information system, group and role membership, and access authorizations (i.e., privileges) and other attributes (as required) for each account;\n" + 
			"			e. Requires approvals by [Assignment: organization-defined personnel or roles] for requests to create information system accounts;\n" + 
			"			f. Creates, enables, modifies, disables, and removes information system accounts in accordance with [Assignment: organization-defined procedures or conditions];\n" + 
			"			g. Monitors the use of information system accounts;\n" + 
			"			h. Notifies account managers:\n" + 
			"				1. When accounts are no longer required;\n" + 
			"				2. When users are terminated or transferred; and\n" + 
			"				3. When individual information system usage or need-to-know changes;\n" + 
			"			i. Authorizes access to the information system based on:\n" + 
			"				1. A valid access authorization;\n" + 
			"				2. Intended system usage; and\n" + 
			"				3. Other attributes as required by the organization or associated missions/business functions;\n" + 
			"			j. Reviews accounts for compliance with account management requirements [Assignment: organization-defined frequency]; and\n" + 
			"			k. Establishes a process for reissuing shared/group account credentials (if deployed) when individuals are removed from the group.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(1) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | AUTOMATED SYSTEM ACCOUNT MANAGEMENT \"\n" + 
			"  specification: \"The organization employs automated mechanisms to support the management of information system accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(2) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | REMOVAL OF TEMPORARY / EMERGENCY ACCOUNTS \"\n" + 
			"  specification: \"The information system automatically [Selection: removes; disables] temporary and emergency accounts after [Assignment: organization-defined time period for each type of account].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(3) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | DISABLE INACTIVE ACCOUNTS \"\n" + 
			"  specification: \"The information system automatically disables inactive accounts after [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(4) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | AUTOMATED AUDIT ACTIONS \"\n" + 
			"  specification: \"The information system automatically audits account creation, modification, enabling, disabling, and removal actions, and notifies [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(5) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | INACTIVITY LOGOUT \"\n" + 
			"  specification: \"The organization requires that users log out when [Assignment: organization-defined time-period of expected inactivity or description of when to log out].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(6) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | DYNAMIC PRIVILEGE MANAGEMENT \"\n" + 
			"  specification: \"The information system implements the following dynamic privilege management capabilities: [Assignment: organization-defined list of dynamic privilege management capabilities].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(7) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | ROLE-BASED SCHEMES \"\n" + 
			"  specification: \"The organization: (a) Establishes and administers privileged user accounts in accordance with a role-based access scheme that organizes allowed information system access and privileges into roles; (b) Monitors privileged role assignments; and (c) Takes [Assignment: organization-defined actions] when privileged role assignments are no longer appropriate.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(8) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | DYNAMIC ACCOUNT CREATION \"\n" + 
			"  specification: \"The information system creates [Assignment: organization-defined information system accounts] dynamically.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(9) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | RESTRICTIONS ON USE OF SHARED / GROUP ACCOUNTS \"\n" + 
			"  specification: \"The organization only permits the use of shared/group accounts that meet [Assignment: organization-defined conditions for establishing shared/group accounts].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(10) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | SHARED / GROUP ACCOUNT CREDENTIAL TERMINATION \"\n" + 
			"  specification: \"The information system terminates shared/group account credentials when members leave the group.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(11) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | USAGE CONDITIONS \"\n" + 
			"  specification: \"The information system enforces [Assignment: organization-defined circumstances and/or usage conditions] for [Assignment: organization-defined information system accounts].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(12) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | ACCOUNT MONITORING / ATYPICAL USAGE \"\n" + 
			"  specification: \"The organization:(a) Monitors information system accounts for [Assignment: organization-defined atypical usage]; and (b) Reports atypical usage of information system accounts to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-2(13) { \n" + 
			"  name: \"ACCOUNT MANAGEMENT | DISABLE ACCOUNTS FOR HIGH-RISK INDIVIDUALS \"\n" + 
			"  specification: \"The organization disables accounts of users posing a significant risk within [Assignment: organization-defined time period] of discovery of the risk.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3 { \n" + 
			"  name: \"ACCESS ENFORCEMENT \"\n" + 
			"  specification: \"The information system enforces approved authorizations for logical access to information and system resources in accordance with applicable access control policies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(1) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | RESTRICTED ACCESS TO PRIVILEGED FUNCTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(2) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | DUAL AUTHORIZATION \"\n" + 
			"  specification: \"The information system enforces dual authorization for [Assignment: organization-defined privileged commands and/or other organization-defined actions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(3) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | MANDATORY ACCESS CONTROL \"\n" + 
			"  specification: \"The information system enforces [Assignment: organization-defined mandatory access control policy] over all subjects and objects where the policy:\n" + 
			"			(a) Is uniformly enforced across all subjects and objects within the boundary of the information system;\n" + 
			"			(b) Specifies that a subject that has been granted access to information is constrained from doing any of the following;\n" + 
			"				(1) Passing the information to unauthorized subjects or objects;\n" + 
			"				(2) Granting its privileges to other subjects;\n" + 
			"				(3) Changing one or more security attributes on subjects, objects, the information system, or information system components;\n" + 
			"				(4) Choosing the security attributes and attribute values to be associated with newly created or modified objects; or\n" + 
			"				(5) Changing the rules governing access control; and\n" + 
			"			(c) Specifies that [Assignment: organization-defined subjects] may explicitly be granted [Assignment: organization-defined privileges (i.e., they are trusted subjects)] such that they are not limited by some or all of the above constraints.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(4) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | DISCRETIONARY ACCESS CONTROL \"\n" + 
			"  specification: \"The information system enforces [Assignment: organization-defined discretionary access control policy] over defined subjects and objects where the policy specifies that a subject that has been granted access to information can do one or more of the following:\n" + 
			"			(a) Pass the information to any other subjects or objects;\n" + 
			"			(b) Grant its privileges to other subjects;\n" + 
			"			(c) Change security attributes on subjects, objects, the information system, or the information system’s components;\n" + 
			"			(d) Choose the security attributes to be associated with newly created or revised objects; or\n" + 
			"			(e) Change the rules governing access control.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(5) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | SECURITY-RELEVANT INFORMATION \"\n" + 
			"  specification: \"The information system prevents access to [Assignment: organization-defined security-relevant information] except during secure, non-operable system states.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(6) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | PROTECTION OF USER AND SYSTEM INFORMATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-4 and SC-28.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(7) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | ROLE-BASED ACCESS CONTROL \"\n" + 
			"  specification: \"The information system enforces a role-based access control policy over defined subjects and objects and controls access based upon [Assignment: organization-defined roles and users authorized to assume such roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(8) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | REVOCATION OF ACCESS AUTHORIZATIONS \"\n" + 
			"  specification: \"The information system enforces the revocation of access authorizations resulting from changes to the security attributes of subjects and objects based on [Assignment: organization-defined rules governing the timing of revocations of access authorizations].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(9) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | CONTROLLED RELEASE \"\n" + 
			"  specification: \"The information system does not release information outside of the established system boundary unless: (a) The receiving [Assignment: organization-defined information system or system component] provides [Assignment: organization-defined security safeguards]; and (b) [Assignment: organization-defined security safeguards] are used to validate the appropriateness of the information designated for release.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-3(10) { \n" + 
			"  name: \"ACCESS ENFORCEMENT | AUDITED OVERRIDE OF ACCESS CONTROL MECHANISMS \"\n" + 
			"  specification: \"The organization employs an audited override of automated access control mechanisms under [Assignment: organization-defined conditions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4 { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT \"\n" + 
			"  specification: \"The information system enforces approved authorizations for controlling the flow of information within the system and between interconnected systems based on [Assignment: organization-defined information flow control policies].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(1) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | OBJECT SECURITY ATTRIBUTES \"\n" + 
			"  specification: \"The information system uses [Assignment: organization-defined security attributes] associated with [Assignment: organization-defined information, source, and destination objects] to enforce [Assignment: organization-defined information flow control policies] as a basis for flow control decisions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(2) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | PROCESSING DOMAINS \"\n" + 
			"  specification: \"The information system uses protected processing domains to enforce [Assignment: organization-defined information flow control policies] as a basis for flow control decisions\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(3) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | DYNAMIC INFORMATION FLOW CONTROL \"\n" + 
			"  specification: \"The information system enforces dynamic information flow control based on [Assignment: organization-defined policies].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(4) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | CONTENT CHECK ENCRYPTED INFORMATION \"\n" + 
			"  specification: \"The information system prevents encrypted information from bypassing content-checking mechanisms by [Selection (one or more): decrypting the information; blocking the flow of the encrypted information; terminating communications sessions attempting to pass encrypted information; [Assignment: organization-defined procedure or method]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(5) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | EMBEDDED DATA TYPES \"\n" + 
			"  specification: \"The information system enforces [Assignment: organization-defined limitations] on embedding data types within other data types.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(6) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | METADATA \"\n" + 
			"  specification: \"The information system enforces information flow control based on [Assignment: organization-defined metadata].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(7) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | ONE-WAY FLOW MECHANISMS \"\n" + 
			"  specification: \"The information system enforces [Assignment: organization-defined one-way information flows] using hardware mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(8) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | SECURITY POLICY FILTERS \"\n" + 
			"  specification: \"The information system enforces information flow control using [Assignment: organization-defined security policy filters] as a basis for flow control decisions for [Assignment: organization-defined information flows].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(9) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | HUMAN REVIEWS \"\n" + 
			"  specification: \"The information system enforces the use of human reviews for [Assignment: organization-defined information flows] under the following conditions: [Assignment: organization-defined conditions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(10) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | ENABLE / DISABLE SECURITY POLICY FILTERS \"\n" + 
			"  specification: \"The information system provides the capability for privileged administrators to enable/disable [Assignment: organization-defined security policy filters] under the following conditions: [Assignment: organization-defined conditions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(11) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | CONFIGURATION OF SECURITY POLICY FILTERS \"\n" + 
			"  specification: \"The information system provides the capability for privileged administrators to configure [Assignment: organization-defined security policy filters] to support different security policies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(12) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | DATA TYPE IDENTIFIERS \"\n" + 
			"  specification: \"The information system, when transferring information between different security domains, uses [Assignment: organization-defined data type identifiers] to validate data essential for information flow decisions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(13) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | DECOMPOSITION INTO POLICY-RELEVANT SUBCOMPONENTS \"\n" + 
			"  specification: \"The information system, when transferring information between different security domains, decomposes information into [Assignment: organization-defined policy-relevant subcomponents] for submission to policy enforcement mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(14) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | SECURITY POLICY FILTER CONSTRAINTS \"\n" + 
			"  specification: \"The information system, when transferring information between different security domains, implements [Assignment: organization-defined security policy filters] requiring fully enumerated formats that restrict data structure and content.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(15) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | DETECTION OF UNSANCTIONED INFORMATION \"\n" + 
			"  specification: \"The information system, when transferring information between different security domains, examines the information for the presence of [Assignment: organized-defined unsanctioned information] and prohibits the transfer of such information in accordance with the [Assignment: organization-defined security policy].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(16) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | INFORMATION TRANSFERS ON INTERCONNECTED SYSTEMS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(17) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | | DOMAIN AUTHENTICATION \"\n" + 
			"  specification: \"The information system uniquely identifies and authenticates source and destination points by [Selection (one or more): organization, system, application, individual] for information transfer.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(18) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | SECURITY ATTRIBUTE BINDING \"\n" + 
			"  specification: \"The information system binds security attributes to information using [Assignment: organization-defined binding techniques] to facilitate information flow policy enforcement.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(19) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | VALIDATION OF METADATA \"\n" + 
			"  specification: \"The information system, when transferring information between different security domains, applies the same security policy filtering to metadata as it applies to data payloads.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(20) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | APPROVED SOLUTIONS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined solutions in approved configurations] to control the flow of [Assignment: organization-defined information] across security domains.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(21) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | PHYSICAL / LOGICAL SEPARATION OF INFORMATION FLOWS \"\n" + 
			"  specification: \"The information system separates information flows logically or physically using [Assignment: organization-defined mechanisms and/or techniques] to accomplish [Assignment: organization-defined required separations by types of information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-4(22) { \n" + 
			"  name: \"INFORMATION FLOW ENFORCEMENT | ACCESS ONLY \"\n" + 
			"  specification: \"The information system provides access from a single device to computing platforms, applications, or data residing on multiple different security domains, while preventing any information flow between the different security domains.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-5 { \n" + 
			"  name: \"SEPARATION OF DUTIES \"\n" + 
			"  specification: \"The organization: a. Separates [Assignment: organization-defined duties of individuals]; b. Documents separation of duties of individuals; and c. Defines information system access authorizations to support separation of duties.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6 { \n" + 
			"  name: \"SEPARATION OF DUTIES \"\n" + 
			"  specification: \"The organization: a. Separates [Assignment: organization-defined duties of individuals]; b. Documents separation of duties of individuals; and c. Defines information system access authorizations to support separation of duties.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(1) { \n" + 
			"  name: \"LEAST PRIVILEGE | AUTHORIZE ACCESS TO SECURITY FUNCTIONS \"\n" + 
			"  specification: \"The organization explicitly authorizes access to [Assignment: organization-defined security functions (deployed in hardware, software, and firmware) and security-relevant information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(2) { \n" + 
			"  name: \"LEAST PRIVILEGE | NON-PRIVILEGED ACCESS FOR NONSECURITY FUNCTIONS \"\n" + 
			"  specification: \"The organization requires that users of information system accounts, or roles, with access to [Assignment: organization-defined security functions or security-relevant information], use non-privileged accounts or roles, when accessing nonsecurity functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(3) { \n" + 
			"  name: \"LEAST PRIVILEGE | NETWORK ACCESS TO PRIVILEGED COMMANDS \"\n" + 
			"  specification: \"The organization authorizes network access to [Assignment: organization-defined privileged commands] only for [Assignment: organization-defined compelling operational needs] and documents the rationale for such access in the security plan for the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(4) { \n" + 
			"  name: \"LEAST PRIVILEGE | SEPARATE PROCESSING DOMAINS \"\n" + 
			"  specification: \"The information system provides separate processing domains to enable finer-grained allocation of user privileges.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(5) { \n" + 
			"  name: \"LEAST PRIVILEGE | PRIVILEGED ACCOUNTS \"\n" + 
			"  specification: \"The organization restricts privileged accounts on the information system to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(6) { \n" + 
			"  name: \"LEAST PRIVILEGE | PRIVILEGED ACCESS BY NON-ORGANIZATIONAL USERS \"\n" + 
			"  specification: \"The organization prohibits privileged access to the information system by non-organizational users.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(7) { \n" + 
			"  name: \"LEAST PRIVILEGE | REVIEW OF USER PRIVILEGES \"\n" + 
			"  specification: \"The organization: (a) Reviews [Assignment: organization-defined frequency] the privileges assigned to [Assignment: organization-defined roles or classes of users] to validate the need for such privileges; and (b) Reassigns or removes privileges, if necessary, to correctly reflect organizational mission/business needs.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(8) { \n" + 
			"  name: \"LEAST PRIVILEGE | PRIVILEGE LEVELS FOR CODE EXECUTION \"\n" + 
			"  specification: \"The information system prevents [Assignment: organization-defined software] from executing at higher privilege levels than users executing the software\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(9) { \n" + 
			"  name: \"LEAST PRIVILEGE | AUDITING USE OF PRIVILEGED FUNCTIONS \"\n" + 
			"  specification: \"The information system audits the execution of privileged functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-6(10) { \n" + 
			"  name: \"LEAST PRIVILEGE | PROHIBIT NON-PRIVILEGED USERS FROM EXECUTING PRIVILEGED FUNCTIONS \"\n" + 
			"  specification: \"The information system prevents non-privileged users from executing privileged functions to include disabling, circumventing, or altering implemented security safeguards/countermeasures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-7 { \n" + 
			"  name: \"UNSUCCESSFUL LOGON ATTEMPTS \"\n" + 
			"  specification: \"The information system: a. Enforces a limit of [Assignment: organization-defined number] consecutive invalid logon attempts by a user during a [Assignment: organization-defined time period]; and b. Automatically [Selection: locks the account/node for an [Assignment: organization-defined time period]; locks the account/node until released by an administrator; delays next logon prompt according to [Assignment: organization-defined delay algorithm]] when the maximum number of unsuccessful attempts is exceeded.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-7(1) { \n" + 
			"  name: \"UNSUCCESSFUL LOGON ATTEMPTS | AUTOMATIC ACCOUNT LOCK \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-7(2) { \n" + 
			"  name: \"UNSUCCESSFUL LOGON ATTEMPTS | PURGE / WIPE MOBILE DEVICE \"\n" + 
			"  specification: \"The information system purges/wipes information from [Assignment: organization-defined mobile devices] based on [Assignment: organization-defined purging/wiping requirements/techniques] after [Assignment: organization-defined number] consecutive, unsuccessful device logon attempts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-8 { \n" + 
			"  name: \"SYSTEM USE NOTIFICATION \"\n" + 
			"  specification: \"The information system:\n" + 
			"			a. Displays to users [Assignment: organization-defined system use notification message or banner] before granting access to the system that provides privacy and security notices consistent with applicable federal laws, Executive Orders, directives, policies, regulations, standards, and guidance and states that:\n" + 
			"				1. Users are accessing a U.S. Government information system;\n" + 
			"				2. Information system usage may be monitored, recorded, and subject to audit;\n" + 
			"				3. Unauthorized use of the information system is prohibited and subject to criminal and civil penalties; and\n" + 
			"				4. Use of the information system indicates consent to monitoring and recording;\n" + 
			"			b. Retains the notification message or banner on the screen until users acknowledge the usage conditions and take explicit actions to log on to or further access the information system; and\n" + 
			"			c. For publicly accessible systems:\n" + 
			"				1. Displays system use information [Assignment: organization-defined conditions], before granting further access;\n" + 
			"				2. Displays references, if any, to monitoring, recording, or auditing that are consistent with privacy accommodations for such systems that generally prohibit those activities; and\n" + 
			"				3. Includes a description of the authorized uses of the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-9 { \n" + 
			"  name: \"PREVIOUS LOGON (ACCESS) NOTIFICATION \"\n" + 
			"  specification: \"The information system notifies the user, upon successful logon (access) to the system, of the date and time of the last logon (access).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-9(1) { \n" + 
			"  name: \"PREVIOUS LOGON NOTIFICATION | UNSUCCESSFUL LOGONS \"\n" + 
			"  specification: \"The information system notifies the user, upon successful logon/access, of the number of unsuccessful logon/access attempts since the last successful logon/access.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-9(2) { \n" + 
			"  name: \"PREVIOUS LOGON NOTIFICATION | SUCCESSFUL / UNSUCCESSFUL LOGONS \"\n" + 
			"  specification: \"The information system notifies the user of the number of [Selection: successful logons/accesses; unsuccessful logon/access attempts; both] during [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-9(3) { \n" + 
			"  name: \"PREVIOUS LOGON NOTIFICATION | NOTIFICATION OF ACCOUNT CHANGES \"\n" + 
			"  specification: \"The information system notifies the user of changes to [Assignment: organization-defined security-related characteristics/parameters of the user’s account] during [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-9(4) { \n" + 
			"  name: \"PREVIOUS LOGON NOTIFICATION | ADDITIONAL LOGON INFORMATION \"\n" + 
			"  specification: \"The information system notifies the user, upon successful logon (access), of the following additional information: [Assignment: organization-defined information to be included in addition to the date and time of the last logon (access)].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-10 { \n" + 
			"  name: \"CONCURRENT SESSION CONTROL \"\n" + 
			"  specification: \"The information system limits the number of concurrent sessions for each [Assignment: organization-defined account and/or account type] to [Assignment: organization-defined number].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-11 { \n" + 
			"  name: \"SESSION LOCK \"\n" + 
			"  specification: \"The information system: a. Prevents further access to the system by initiating a session lock after [Assignment: organization-defined time period] of inactivity or upon receiving a request from a user; and b. Retains the session lock until the user reestablishes access using established identification and authentication procedures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-11(1) { \n" + 
			"  name: \"SESSION LOCK | PATTERN-HIDING DISPLAYS \"\n" + 
			"  specification: \"The information system conceals, via the session lock, information previously visible on the display with a publicly viewable image.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-12 { \n" + 
			"  name: \"SESSION TERMINATION \"\n" + 
			"  specification: \"The information system automatically terminates a user session after [Assignment: organization-defined conditions or trigger events requiring session disconnect].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-12(1) { \n" + 
			"  name: \"SESSION TERMINATION | USER-INITIATED LOGOUTS / MESSAGE DISPLAYS \"\n" + 
			"  specification: \"The information system: (a) Provides a logout capability for user-initiated communications sessions whenever authentication is used to gain access to [Assignment: organization-defined information resources]; and (b) Displays an explicit logout message to users indicating the reliable termination of authenticated communications sessions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-13 { \n" + 
			"  name: \"SUPERVISION AND REVIEW — ACCESS CONTROL \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-2 and AU-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-14 { \n" + 
			"  name: \"PERMITTED ACTIONS WITHOUT IDENTIFICATION OR AUTHENTICATION \"\n" + 
			"  specification: \"The organization: a. Identifies [Assignment: organization-defined user actions] that can be performed on the information system without identification or authentication consistent with organizational missions/business functions; and b. Documents and provides supporting rationale in the security plan for the information system, user actions not requiring identification or authentication.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-14(1) { \n" + 
			"  name: \"PERMITTED ACTIONS WITHOUT IDENTIFICATION OR AUTHENTICATION | NECESSARY USES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-14.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-15 { \n" + 
			"  name: \"AUTOMATED MARKING \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-3.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16 { \n" + 
			"  name: \"SECURITY ATTRIBUTES \"\n" + 
			"  specification: \"The organization: a. Provides the means to associate [Assignment: organization-defined types of security attributes] having [Assignment: organization-defined security attribute values] with information in storage, in process, and/or in transmission; b. Ensures that the security attribute associations are made and retained with the information; c. Establishes the permitted [Assignment: organization-defined security attributes] for [Assignment: organization-defined information systems]; and d. Determines the permitted [Assignment: organization-defined values or ranges] for each of the established security attributes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(1) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | DYNAMIC ATTRIBUTE ASSOCIATION \"\n" + 
			"  specification: \"The information system dynamically associates security attributes with [Assignment: organization-defined subjects and objects] in accordance with [Assignment: organization-defined security policies] as information is created and combined.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(2) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | ATTRIBUTE VALUE CHANGES BY AUTHORIZED INDIVIDUALS \"\n" + 
			"  specification: \"The information system provides authorized individuals (or processes acting on behalf of individuals) the capability to define or change the value of associated security attributes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(3) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | MAINTENANCE OF ATTRIBUTE ASSOCIATIONS BY INFORMATION SYSTEM \"\n" + 
			"  specification: \"The information system maintains the association and integrity of [Assignment: organization-defined security attributes] to [Assignment: organization-defined subjects and objects].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(4) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | ASSOCIATION OF ATTRIBUTES BY AUTHORIZED INDIVIDUALS \"\n" + 
			"  specification: \"The information system supports the association of [Assignment: organization-defined security attributes] with [Assignment: organization-defined subjects and objects] by authorized individuals (or processes acting on behalf of individuals).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(5) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | ATTRIBUTE DISPLAYS FOR OUTPUT DEVICES \"\n" + 
			"  specification: \"The information system displays security attributes in human-readable form on each object that the system transmits to output devices to identify [Assignment: organization-identified special dissemination, handling, or distribution instructions] using [Assignment: organization-identified human-readable, standard naming conventions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(6) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | MAINTENANCE OF ATTRIBUTE ASSOCIATION BY ORGANIZATION \"\n" + 
			"  specification: \"The organization allows personnel to associate, and maintain the association of [Assignment: organization-defined security attributes] with [Assignment: organization-defined subjects and objects] in accordance with [Assignment: organization-defined security policies].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(7) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | CONSISTENT ATTRIBUTE INTERPRETATION \"\n" + 
			"  specification: \"The organization provides a consistent interpretation of security attributes transmitted between distributed information system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(8) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | ASSOCIATION TECHNIQUES / TECHNOLOGIES \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined techniques or technologies] with [Assignment: organization-defined level of assurance] in associating security attributes to information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(9) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | ATTRIBUTE REASSIGNMENT \"\n" + 
			"  specification: \"The organization ensures that security attributes associated with information are reassigned only via re-grading\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-16(10) { \n" + 
			"  name: \"SECURITY ATTRIBUTES | ATTRIBUTE CONFIGURATION BY AUTHORIZED INDIVIDUALS \"\n" + 
			"  specification: \"The information system provides authorized individuals the capability to define or change the type and value of security attributes available for association with subjects and objects.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17 { \n" + 
			"  name: \"REMOTE ACCESS \"\n" + 
			"  specification: \"The organization: a. Establishes and documents usage restrictions, configuration/connection requirements, and implementation guidance for each type of remote access allowed; and b. Authorizes remote access to the information system prior to allowing such connections.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(1) { \n" + 
			"  name: \"REMOTE ACCESS | AUTOMATED MONITORING / CONTROL \"\n" + 
			"  specification: \"The information system monitors and controls remote access methods.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(2) { \n" + 
			"  name: \"REMOTE ACCESS | PROTECTION OF CONFIDENTIALITY / INTEGRITY USING ENCRYPTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to protect the confidentiality and integrity of remote access sessions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(3) { \n" + 
			"  name: \"REMOTE ACCESS | MANAGED ACCESS CONTROL POINTS \"\n" + 
			"  specification: \"The information system routes all remote accesses through [Assignment: organization-defined number] managed network access control points.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(4) { \n" + 
			"  name: \"REMOTE ACCESS | PRIVILEGED COMMANDS / ACCESS \"\n" + 
			"  specification: \"The organization: (a) Authorizes the execution of privileged commands and access to security-relevant information via remote access only for [Assignment: organization-defined needs]; and (b) Documents the rationale for such access in the security plan for the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(5) { \n" + 
			"  name: \"REMOTE ACCESS | MONITORING FOR UNAUTHORIZED CONNECTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(6) { \n" + 
			"  name: \"REMOTE ACCESS | PROTECTION OF INFORMATION \"\n" + 
			"  specification: \"The organization ensures that users protect information about remote access mechanisms from unauthorized use and disclosure.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(7) { \n" + 
			"  name: \"REMOTE ACCESS | ADDITIONAL PROTECTION FOR SECURITY FUNCTION ACCESS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-3 (10).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(8) { \n" + 
			"  name: \"REMOTE ACCESS | DISABLE NONSECURE NETWORK PROTOCOLS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-17(9) { \n" + 
			"  name: \"REMOTE ACCESS | DISCONNECT / DISABLE ACCESS \"\n" + 
			"  specification: \"The organization provides the capability to expeditiously disconnect or disable remote access to the information system within [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-18 { \n" + 
			"  name: \"WIRELESS ACCESS \"\n" + 
			"  specification: \"The organization: a. Establishes usage restrictions, configuration/connection requirements, and implementation guidance for wireless access; and b. Authorizes wireless access to the information system prior to allowing such connections.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-18(1) { \n" + 
			"  name: \"WIRELESS ACCESS | AUTHENTICATION AND ENCRYPTION \"\n" + 
			"  specification: \"The information system protects wireless access to the system using authentication of [Selection (one or more): users; devices] and encryption.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-18(2) { \n" + 
			"  name: \"WIRELESS ACCESS | MONITORING UNAUTHORIZED CONNECTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-18(3) { \n" + 
			"  name: \"WIRELESS ACCESS | DISABLE WIRELESS NETWORKING \"\n" + 
			"  specification: \"The organization disables, when not intended for use, wireless networking capabilities internally embedded within information system components prior to issuance and deployment.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-18(4) { \n" + 
			"  name: \"WIRELESS ACCESS | RESTRICT CONFIGURATIONS BY USERS \"\n" + 
			"  specification: \"The organization identifies and explicitly authorizes users allowed to independently configure wireless networking capabilities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-18(5) { \n" + 
			"  name: \"WIRELESS ACCESS | ANTENNAS / TRANSMISSION POWER LEVELS \"\n" + 
			"  specification: \"The organization selects radio antennas and calibrates transmission power levels to reduce the probability that usable signals can be received outside of organization-controlled boundaries.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-19 { \n" + 
			"  name: \"ACCESS CONTROL FOR MOBILE DEVICES \"\n" + 
			"  specification: \"The organization: a. Establishes usage restrictions, configuration requirements, connection requirements, and implementation guidance for organization-controlled mobile devices; and b. Authorizes the connection of mobile devices to organizational information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-19(1) { \n" + 
			"  name: \"ACCESS CONTROL FOR MOBILE DEVICES | USE OF WRITABLE / PORTABLE STORAGE DEVICES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-19(2) { \n" + 
			"  name: \"ACCESS CONTROL FOR MOBILE DEVICES | USE OF PERSONALLY OWNED PORTABLE STORAGE DEVICES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-19(3) { \n" + 
			"  name: \"ACCESS CONTROL FOR MOBILE DEVICES | USE OF PORTABLE STORAGE DEVICES WITH NO IDENTIFIABLE OWNER \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-19(4) { \n" + 
			"  name: \"ACCESS CONTROL FOR MOBILE DEVICES | RESTRICTIONS FOR CLASSIFIED INFORMATION \"\n" + 
			"  specification: \"The organization:\n" + 
			"			(a) Prohibits the use of unclassified mobile devices in facilities containing information systems processing, storing, or transmitting classified information unless specifically permitted by the authorizing official; and\n" + 
			"			(b) Enforces the following restrictions on individuals permitted by the authorizing official to use unclassified mobile devices in facilities containing information systems processing, storing, or transmitting classified information:\n" + 
			"				(1) Connection of unclassified mobile devices to classified information systems is prohibited;\n" + 
			"				(2) Connection of unclassified mobile devices to unclassified information systems requires approval from the authorizing official;\n" + 
			"				(3) Use of internal or external modems or wireless interfaces within the unclassified mobile devices is prohibited; and\n" + 
			"				(4) Unclassified mobile devices and the information stored on those devices are subject to random reviews and inspections by [Assignment: organization-defined security officials], and if classified information is found, the incident handling policy is followed.\n" + 
			"			(c) Restricts the connection of classified mobile devices to classified information systems in accordance with [Assignment: organization-defined security policies].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-19(5) { \n" + 
			"  name: \"ACCESS CONTROL FOR MOBILE DEVICES | FULL DEVICE / CONTAINER-BASED ENCRYPTION \"\n" + 
			"  specification: \"The organization employs [Selection: full-device encryption; container encryption] to protect the confidentiality and integrity of information on [Assignment: organization-defined mobile devices].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-20 { \n" + 
			"  name: \"USE OF EXTERNAL INFORMATION SYSTEMS \"\n" + 
			"  specification: \"The organization establishes terms and conditions, consistent with any trust relationships established with other organizations owning, operating, and/or maintaining external information systems, allowing authorized individuals to: a. Access the information system from external information systems; and b. Process, store, or transmit organization-controlled information using external information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-20(1) { \n" + 
			"  name: \"USE OF EXTERNAL INFORMATION SYSTEMS | LIMITS ON AUTHORIZED USE \"\n" + 
			"  specification: \"The organization permits authorized individuals to use an external information system to access the information system or to process, store, or transmit organization-controlled information only when the organization: (a) Verifies the implementation of required security controls on the external system as specified in the organization’s information security policy and security plan; or (b) Retains approved information system connection or processing agreements with the organizational entity hosting the external information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-20(2) { \n" + 
			"  name: \"USE OF EXTERNAL INFORMATION SYSTEMS | PORTABLE STORAGE DEVICES \"\n" + 
			"  specification: \"The organization [Selection: restricts; prohibits] the use of organization-controlled portable storage devices by authorized individuals on external information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-20(3) { \n" + 
			"  name: \"USE OF EXTERNAL INFORMATION SYSTEMS | NON-ORGANIZATIONALLY OWNED SYSTEMS / COMPONENTS / DEVICES \"\n" + 
			"  specification: \"The organization [Selection: restricts; prohibits] the use of non-organizationally owned information systems, system components, or devices to process, store, or transmit organizational information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-20(4) { \n" + 
			"  name: \"USE OF EXTERNAL INFORMATION SYSTEMS | NETWORK ACCESSIBLE STORAGE DEVICES \"\n" + 
			"  specification: \"The organization prohibits the use of [Assignment: organization-defined network accessible storage devices] in external information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-21 { \n" + 
			"  name: \"INFORMATION SHARING \"\n" + 
			"  specification: \"The organization: a. Facilitates information sharing by enabling authorized users to determine whether access authorizations assigned to the sharing partner match the access restrictions on the information for [Assignment: organization-defined information sharing circumstances where user discretion is required]; and b. Employs [Assignment: organization-defined automated mechanisms or manual processes] to assist users in making information sharing/collaboration decisions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-21(1) { \n" + 
			"  name: \"INFORMATION SHARING | AUTOMATED DECISION SUPPORT \"\n" + 
			"  specification: \"The information system enforces information-sharing decisions by authorized users based on access authorizations of sharing partners and access restrictions on information to be shared.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-21(2) { \n" + 
			"  name: \"INFORMATION SHARING | INFORMATION SEARCH AND RETRIEVAL \"\n" + 
			"  specification: \"The information system implements information search and retrieval services that enforce [Assignment: organization-defined information sharing restrictions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-22 { \n" + 
			"  name: \"PUBLICLY ACCESSIBLE CONTENT \"\n" + 
			"  specification: \"The organization: a. Designates individuals authorized to post information onto a publicly accessible information system; b. Trains authorized individuals to ensure that publicly accessible information does not contain nonpublic information; c. Reviews the proposed content of information prior to posting onto the publicly accessible information system to ensure that nonpublic information is not included; and d. Reviews the content on the publicly accessible information system for nonpublic information [Assignment: organization-defined frequency] and removes such information, if discovered.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-23 { \n" + 
			"  name: \"DATA MINING PROTECTION \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined data mining prevention and detection techniques] for [Assignment: organization-defined data storage objects] to adequately detect and protect against data mining.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-24 { \n" + 
			"  name: \"ACCESS CONTROL DECISIONS \"\n" + 
			"  specification: \"The organization establishes procedures to ensure [Assignment: organization-defined access control decisions] are applied to each access request prior to access enforcement.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-24(1) { \n" + 
			"  name: \"ACCESS CONTROL DECISIONS | TRANSMIT ACCESS AUTHORIZATION INFORMATION \"\n" + 
			"  specification: \"The information system transmits [Assignment: organization-defined access authorization information] using [Assignment: organization-defined security safeguards] to [Assignment: organization-defined information systems] that enforce access control decisions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-24(2) { \n" + 
			"  name: \"ACCESS CONTROL DECISIONS | NO USER OR PROCESS IDENTITY \"\n" + 
			"  specification: \"The information system enforces access control decisions based on [Assignment: organization-defined security attributes] that do not include the identity of the user or process acting on behalf of the user.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AC-25 { \n" + 
			"  name: \"REFERENCE MONITOR \"\n" + 
			"  specification: \"The information system implements a reference monitor for [Assignment: organization-defined access control policies] that is tamperproof, always invoked, and small enough to be subject to analysis and testing, the completeness of which can be assured.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AC \n" + 
			"  } \n" + 
			"  security control AT-1 { \n" + 
			"  name: \"SECURITY AWARENESS AND TRAINING POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The information system implements a reference monitor for [Assignment: organization-defined access control policies] that is tamperproof, always invoked, and small enough to be subject to analysis and testing, the completeness of which can be assured.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-2 { \n" + 
			"  name: \"SECURITY AWARENESS AND TRAINING POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization provides basic security awareness training to information system users (including managers, senior executives, and contractors): a. As part of initial training for new users; b. When required by information system changes; and c. [Assignment: organization-defined frequency] thereafter.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-2(1) { \n" + 
			"  name: \"SECURITY AWARENESS | PRACTICAL EXERCISES \"\n" + 
			"  specification: \"The organization includes practical exercises in security awareness training that simulate actual cyber attacks.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-2(2) { \n" + 
			"  name: \"SECURITY AWARENESS | INSIDER THREAT \"\n" + 
			"  specification: \"The organization includes security awareness training on recognizing and reporting potential indicators of insider threat.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-3 { \n" + 
			"  name: \"ROLE-BASED SECURITY TRAINING \"\n" + 
			"  specification: \"The organization provides role-based security training to personnel with assigned security roles and responsibilities: a. Before authorizing access to the information system or performing assigned duties; b. When required by information system changes; and c. [Assignment: organization-defined frequency] thereafter.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-3(1) { \n" + 
			"  name: \"ROLE-BASED SECURITY TRAINING | ENVIRONMENTAL CONTROLS \"\n" + 
			"  specification: \"The organization provides [Assignment: organization-defined personnel or roles] with initial and [Assignment: organization-defined frequency] training in the employment and operation of environmental controls.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-3(2) { \n" + 
			"  name: \"ROLE-BASED SECURITY TRAINING | PHYSICAL SECURITY CONTROLS \"\n" + 
			"  specification: \"The organization provides [Assignment: organization-defined personnel or roles] with initial and [Assignment: organization-defined frequency] training in the employment and operation of physical security controls.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-3(3) { \n" + 
			"  name: \"ROLE-BASED SECURITY TRAINING | PRACTICAL EXERCISES \"\n" + 
			"  specification: \"The organization includes practical exercises in security training that reinforce training objectives.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-3(4) { \n" + 
			"  name: \"ROLE-BASED SECURITY TRAINING | SUSPICIOUS COMMUNICATIONS AND ANOMALOUS SYSTEM BEHAVIOR \"\n" + 
			"  specification: \"The organization provides training to its personnel on [Assignment: organization-defined indicators of malicious code] to recognize suspicious communications and anomalous behavior in organizational information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-4 { \n" + 
			"  name: \"SECURITY TRAINING RECORDS \"\n" + 
			"  specification: \"The organization: a. Documents and monitors individual information system security training activities including basic security awareness training and specific information system security training; and b. Retains individual training records for [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AT-5 { \n" + 
			"  name: \"CONTACTS WITH SECURITY GROUPS AND ASSOCIATIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PM-15.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AT \n" + 
			"  } \n" + 
			"  security control AU-1 { \n" + 
			"  name: \"AUDIT AND ACCOUNTABILITY POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. An audit and accountability policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the audit and accountability policy and associated audit and accountability controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Audit and accountability policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Audit and accountability procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-2 { \n" + 
			"  name: \"AUDIT EVENTS \"\n" + 
			"  specification: \" The organization:\n" + 
			"			a. Determines that the information system is capable of auditing the following events: [Assignment: organization-defined auditable events];\n" + 
			"			b. Coordinates the security audit function with other organizational entities requiring audit-related information to enhance mutual support and to help guide the selection of auditable events;\n" + 
			"			c. Provides a rationale for why the auditable events are deemed to be adequate to support after-the-fact investigations of security incidents; and\n" + 
			"			d. Determines that the following events are to be audited within the information system: [Assignment: organization-defined audited events (the subset of the auditable events defined in AU-2 a.) along with the frequency of (or situation requiring) auditing for each identified event].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-2(1) { \n" + 
			"  name: \"AUDIT EVENTS | COMPILATION OF AUDIT RECORDS FROM MULTIPLE SOURCES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AU-12.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-2(2) { \n" + 
			"  name: \"AUDIT EVENTS | SELECTION OF AUDIT EVENTS BY COMPONENT \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AU-12.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-2(3) { \n" + 
			"  name: \"AUDIT EVENTS | REVIEWS AND UPDATES \"\n" + 
			"  specification: \"The organization reviews and updates the audited events [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-2(4) { \n" + 
			"  name: \"AUDIT EVENTS | PRIVILEGED FUNCTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-6 (9).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-3 { \n" + 
			"  name: \"CONTENT OF AUDIT RECORDS \"\n" + 
			"  specification: \"The information system generates audit records containing information that establishes what type of event occurred, when the event occurred, where the event occurred, the source of the event, the outcome of the event, and the identity of any individuals or subjects associated with the event.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-3(1) { \n" + 
			"  name: \"CONTENT OF AUDIT RECORDS | ADDITIONAL AUDIT INFORMATION \"\n" + 
			"  specification: \"The information system generates audit records containing the following additional information: [Assignment: organization-defined additional, more detailed information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-3(2) { \n" + 
			"  name: \"CONTENT OF AUDIT RECORDS | CENTRALIZED MANAGEMENT OF PLANNED AUDIT RECORD CONTENT \"\n" + 
			"  specification: \"The information system provides centralized management and configuration of the content to be captured in audit records generated by [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-4 { \n" + 
			"  name: \"AUDIT STORAGE CAPACITY \"\n" + 
			"  specification: \"The organization allocates audit record storage capacity in accordance with [Assignment: organization-defined audit record storage requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-4(1) { \n" + 
			"  name: \"AUDIT STORAGE CAPACITY | TRANSFER TO ALTERNATE STORAGE \"\n" + 
			"  specification: \"The information system off-loads audit records [Assignment: organization-defined frequency] onto a different system or media than the system being audited.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-5 { \n" + 
			"  name: \"RESPONSE TO AUDIT PROCESSING FAILURES \"\n" + 
			"  specification: \"The information system: a. Alerts [Assignment: organization-defined personnel or roles] in the event of an audit processing failure; and b. Takes the following additional actions: [Assignment: organization-defined actions to be taken (e.g., shut down information system, overwrite oldest audit records, stop generating audit records)].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-5(1) { \n" + 
			"  name: \"RESPONSE TO AUDIT PROCESSING FAILURES | AUDIT STORAGE CAPACITY \"\n" + 
			"  specification: \"The information system provides a warning to [Assignment: organization-defined personnel, roles, and/or locations] within [Assignment: organization-defined time period] when allocated audit record storage volume reaches [Assignment: organization-defined percentage] of repository maximum audit record storage capacity.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-5(2) { \n" + 
			"  name: \"RESPONSE TO AUDIT PROCESSING FAILURES | REAL-TIME ALERTS \"\n" + 
			"  specification: \"The information system provides an alert in [Assignment: organization-defined real-time period] to [Assignment: organization-defined personnel, roles, and/or locations] when the following audit failure events occur: [Assignment: organization-defined audit failure events requiring real-time alerts].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-5(3) { \n" + 
			"  name: \"RESPONSE TO AUDIT PROCESSING FAILURES | CONFIGURABLE TRAFFIC VOLUME THRESHOLDS \"\n" + 
			"  specification: \"The information system enforces configurable network communications traffic volume thresholds reflecting limits on auditing capacity and [Selection: rejects; delays] network traffic above those thresholds.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-5(4) { \n" + 
			"  name: \"RESPONSE TO AUDIT PROCESSING FAILURES | SHUTDOWN ON FAILURE \"\n" + 
			"  specification: \"The information system invokes a [Selection: full system shutdown; partial system shutdown; degraded operational mode with limited mission/business functionality available] in the event of [Assignment: organization-defined audit failures], unless an alternate audit capability exists.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6 { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING \"\n" + 
			"  specification: \"The organization: a. Reviews and analyzes information system audit records [Assignment: organization-defined frequency] for indications of [Assignment: organization-defined inappropriate or unusual activity]; and b. Reports findings to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(1) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | PROCESS INTEGRATION \"\n" + 
			"  specification: \"The organization employs automated mechanisms to integrate audit review, analysis, and reporting processes to support organizational processes for investigation and response to suspicious activities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(2) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | AUTOMATED SECURITY ALERTS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(3) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | CORRELATE AUDIT REPOSITORIES \"\n" + 
			"  specification: \"The organization analyzes and correlates audit records across different repositories to gain organization-wide situational awareness.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(4) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | CENTRAL REVIEW AND ANALYSIS \"\n" + 
			"  specification: \"The information system provides the capability to centrally review and analyze audit records from multiple components within the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(5) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | INTEGRATION / SCANNING AND MONITORING CAPABILITIES \"\n" + 
			"  specification: \"The organization integrates analysis of audit records with analysis of [Selection (one or more): vulnerability scanning information; performance data; information system monitoring information; [Assignment: organization-defined data/information collected from other sources]] to further enhance the ability to identify inappropriate or unusual activity.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(6) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | CORRELATION WITH PHYSICAL MONITORING \"\n" + 
			"  specification: \"The organization correlates information from audit records with information obtained from monitoring physical access to further enhance the ability to identify suspicious, inappropriate, unusual, or malevolent activity.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(7) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | PERMITTED ACTIONS \"\n" + 
			"  specification: \"The organization specifies the permitted actions for each [Selection (one or more): information system process; role; user] associated with the review, analysis, and reporting of audit information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(8) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | FULL TEXT ANALYSIS OF PRIVILEGED COMMANDS \"\n" + 
			"  specification: \"The organization performs a full text analysis of audited privileged commands in a physically distinct component or subsystem of the information system, or other information system that is dedicated to that analysis.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(9) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | CORRELATION WITH INFORMATION FROM NONTECHNICAL SOURCES \"\n" + 
			"  specification: \"The organization correlates information from nontechnical sources with audit information to enhance organization-wide situational awareness.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-6(10) { \n" + 
			"  name: \"AUDIT REVIEW, ANALYSIS, AND REPORTING | AUDIT LEVEL ADJUSTMENT \"\n" + 
			"  specification: \"The organization adjusts the level of audit review, analysis, and reporting within the information system when there is a change in risk based on law enforcement information, intelligence information, or other credible sources of information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-7 { \n" + 
			"  name: \"AUDIT REDUCTION AND REPORT GENERATION \"\n" + 
			"  specification: \"The information system provides an audit reduction and report generation capability that: a. Supports on-demand audit review, analysis, and reporting requirements and after-the-fact investigations of security incidents; and b. Does not alter the original content or time ordering of audit records.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-7(1) { \n" + 
			"  name: \"AUDIT REDUCTION AND REPORT GENERATION | AUTOMATIC PROCESSING \"\n" + 
			"  specification: \"The information system provides the capability to process audit records for events of interest based on [Assignment: organization-defined audit fields within audit records].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-7(2) { \n" + 
			"  name: \"AUDIT REDUCTION AND REPORT GENERATION | AUTOMATIC SORT AND SEARCH \"\n" + 
			"  specification: \"The information system provides the capability to sort and search audit records for events of interest based on the content of [Assignment: organization-defined audit fields within audit records].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-8 { \n" + 
			"  name: \"TIME STAMPS \"\n" + 
			"  specification: \"The information system: a. Uses internal system clocks to generate time stamps for audit records; and b. Records time stamps for audit records that can be mapped to Coordinated Universal Time (UTC) or Greenwich Mean Time (GMT) and meets [Assignment: organization-defined granularity of time measurement].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-8(1) { \n" + 
			"  name: \"TIME STAMPS | SYNCHRONIZATION WITH AUTHORITATIVE TIME SOURCE \"\n" + 
			"  specification: \"The information system: (a) Compares the internal information system clocks [Assignment: organization-defined frequency] with [Assignment: organization-defined authoritative time source]; and (b) Synchronizes the internal system clocks to the authoritative time source when the time difference is greater than [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-8(2) { \n" + 
			"  name: \"TIME STAMPS | SECONDARY AUTHORITATIVE TIME SOURCE \"\n" + 
			"  specification: \"The information system identifies a secondary authoritative time source that is located in a different geographic region than the primary authoritative time source.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9 { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION \"\n" + 
			"  specification: \"The information system protects audit information and audit tools from unauthorized access, modification, and deletion.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9(1) { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION | HARDWARE WRITE-ONCE MEDIA \"\n" + 
			"  specification: \"The information system writes audit trails to hardware-enforced, write-once media.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9(2) { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION | AUDIT BACKUP ON SEPARATE PHYSICAL SYSTEMS / COMPONENTS \"\n" + 
			"  specification: \"The information system backs up audit records [Assignment: organization-defined frequency] onto a physically different system or system component than the system or component being audited.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9(3) { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to protect the integrity of audit information and audit tools.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9(4) { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION | ACCESS BY SUBSET OF PRIVILEGED USERS \"\n" + 
			"  specification: \"The organization authorizes access to management of audit functionality to only [Assignment: organization-defined subset of privileged users].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9(5) { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION | DUAL AUTHORIZATION \"\n" + 
			"  specification: \"The organization enforces dual authorization for [Selection (one or more): movement; deletion] of [Assignment: organization-defined audit information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-9(6) { \n" + 
			"  name: \"PROTECTION OF AUDIT INFORMATION | READ ONLY ACCESS \"\n" + 
			"  specification: \"The organization authorizes read-only access to audit information to [Assignment: organization-defined subset of privileged users].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-10 { \n" + 
			"  name: \"NON-REPUDIATION \"\n" + 
			"  specification: \"The information system protects against an individual (or process acting on behalf of an individual) falsely denying having performed [Assignment: organization-defined actions to be covered by non-repudiation].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-10(1) { \n" + 
			"  name: \"NON-REPUDIATION | ASSOCIATION OF IDENTITIES \"\n" + 
			"  specification: \"The information system: (a) Binds the identity of the information producer with the information to [Assignment: organization-defined strength of binding]; and (b) Provides the means for authorized individuals to determine the identity of the producer of the information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-10(2) { \n" + 
			"  name: \"NON-REPUDIATION | VALIDATE BINDING OF INFORMATION PRODUCER IDENTITY \"\n" + 
			"  specification: \"The information system: (a) Validates the binding of the information producer identity to the information at [Assignment: organization-defined frequency]; and (b) Performs [Assignment: organization-defined actions] in the event of a validation error.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-10(3) { \n" + 
			"  name: \"NON-REPUDIATION | CHAIN OF CUSTODY \"\n" + 
			"  specification: \"The information system maintains reviewer/releaser identity and credentials within the established chain of custody for all information reviewed or released.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-10(4) { \n" + 
			"  name: \"NON-REPUDIATION | VALIDATE BINDING OF INFORMATION REVIEWER IDENTITY \"\n" + 
			"  specification: \"The information system: (a) Validates the binding of the information reviewer identity to the information at the transfer or release points prior to release/transfer between [Assignment: organization-defined security domains]; and (b) Performs [Assignment: organization-defined actions] in the event of a validation error.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-10(5) { \n" + 
			"  name: \"NON-REPUDIATION | DIGITAL SIGNATURES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-11 { \n" + 
			"  name: \"AUDIT RECORD RETENTION \"\n" + 
			"  specification: \"The organization retains audit records for [Assignment: organization-defined time period consistent with records retention policy] to provide support for after-the-fact investigations of security incidents and to meet regulatory and organizational information retention requirements.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-11(1) { \n" + 
			"  name: \"AUDIT RECORD RETENTION | LONG-TERM RETRIEVAL CAPABILITY \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined measures] to ensure that long-term audit records generated by the information system can be retrieved.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-12 { \n" + 
			"  name: \"AUDIT GENERATION \"\n" + 
			"  specification: \"The information system: a. Provides audit record generation capability for the auditable events defined in AU-2 a. at [Assignment: organization-defined information system components];b. Allows [Assignment: organization-defined personnel or roles] to select which auditable events are to be audited by specific components of the information system; and c. Generates audit records for the events defined in AU-2 d. with the content defined in AU-3.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-12(1) { \n" + 
			"  name: \"AUDIT GENERATION | SYSTEM-WIDE / TIME-CORRELATED AUDIT TRAIL \"\n" + 
			"  specification: \"The information system compiles audit records from [Assignment: organization-defined information system components] into a system-wide (logical or physical) audit trail that is time-correlated to within [Assignment: organization-defined level of tolerance for the relationship between time stamps of individual records in the audit trail].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-12(2) { \n" + 
			"  name: \"AUDIT GENERATION | STANDARDIZED FORMATS \"\n" + 
			"  specification: \"The information system produces a system-wide (logical or physical) audit trail composed of audit records in a standardized format.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-12(3) { \n" + 
			"  name: \"AUDIT GENERATION | CHANGES BY AUTHORIZED INDIVIDUALS \"\n" + 
			"  specification: \"The information system provides the capability for [Assignment: organization-defined individuals or roles] to change the auditing to be performed on [Assignment: organization-defined information system components] based on [Assignment: organization-defined selectable event criteria] within [Assignment: organization-defined time thresholds].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-13 { \n" + 
			"  name: \"MONITORING FOR INFORMATION DISCLOSURE \"\n" + 
			"  specification: \"The organization monitors [Assignment: organization-defined open source information and/or information sites] [Assignment: organization-defined frequency] for evidence of unauthorized disclosure of organizational information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-13(1) { \n" + 
			"  name: \"MONITORING FOR INFORMATION DISCLOSURE | USE OF AUTOMATED TOOLS \"\n" + 
			"  specification: \"The organization employs automated mechanisms to determine if organizational information has been disclosed in an unauthorized manner.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-13(2) { \n" + 
			"  name: \"MONITORING FOR INFORMATION DISCLOSURE | REVIEW OF MONITORED SITES \"\n" + 
			"  specification: \"The organization reviews the open source information sites being monitored [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-14 { \n" + 
			"  name: \"SESSION AUDIT \"\n" + 
			"  specification: \"The information system provides the capability for authorized users to select a user session to capture/record or view/hear.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-14(1) { \n" + 
			"  name: \"SESSION AUDIT | SYSTEM START-UP \"\n" + 
			"  specification: \"The information system initiates session audits at system start-up.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-14(2) { \n" + 
			"  name: \"SESSION AUDIT | CAPTURE/RECORD AND LOG CONTENT \"\n" + 
			"  specification: \"The information system provides the capability for authorized users to capture/record and log content related to a user session.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-14(3) { \n" + 
			"  name: \"SESSION AUDIT | REMOTE VIEWING / LISTENING \"\n" + 
			"  specification: \"The information system provides the capability for authorized users to remotely view/hear all content related to an established user session in real time.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-15 { \n" + 
			"  name: \"ALTERNATE AUDIT CAPABILITY \"\n" + 
			"  specification: \"The organization provides an alternate audit capability in the event of a failure in primary audit capability that provides [Assignment: organization-defined alternate audit functionality].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-16 { \n" + 
			"  name: \"CROSS-ORGANIZATIONAL AUDITING \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined methods] for coordinating [Assignment: organization-defined audit information] among external organizations when audit information is transmitted across organizational boundaries.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-16(1) { \n" + 
			"  name: \"CROSS-ORGANIZATIONAL AUDITING | IDENTITY PRESERVATION \"\n" + 
			"  specification: \"The organization requires that the identity of individuals be preserved in cross-organizational audit trails.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control AU-16(2) { \n" + 
			"  name: \"CROSS-ORGANIZATIONAL AUDITING | SHARING OF AUDIT INFORMATION \"\n" + 
			"  specification: \"The organization provides cross-organizational audit information to [Assignment: organization-defined organizations] based on [Assignment: organization-defined cross-organizational sharing agreements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.AU \n" + 
			"  } \n" + 
			"  security control CA-1 { \n" + 
			"  name: \"SECURITY ASSESSMENT AND AUTHORIZATION POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A security assessment and authorization policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the security assessment and authorization policy and associated security assessment and authorization controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Security assessment and authorization policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Security assessment and authorization procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-2 { \n" + 
			"  name: \"SECURITY ASSESSMENTS \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops a security assessment plan that describes the scope of the assessment including:\n" + 
			"				1. Security controls and control enhancements under assessment;\n" + 
			"				2. Assessment procedures to be used to determine security control effectiveness; and\n" + 
			"				3. Assessment environment, assessment team, and assessment roles and responsibilities;\n" + 
			"			b. Assesses the security controls in the information system and its environment of operation [Assignment: organization-defined frequency] to determine the extent to which the controls are implemented correctly, operating as intended, and producing the desired outcome with respect to meeting established security requirements;\n" + 
			"			c. Produces a security assessment report that documents the results of the assessment; and\n" + 
			"			d. Provides the results of the security control assessment to [Assignment: organization-defined individuals or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-2(1) { \n" + 
			"  name: \"SECURITY ASSESSMENTS | INDEPENDENT ASSESSORS \"\n" + 
			"  specification: \"The organization employs assessors or assessment teams with [Assignment: organization-defined level of independence] to conduct security control assessments.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-2(2) { \n" + 
			"  name: \"SECURITY ASSESSMENTS | SPECIALIZED ASSESSMENTS \"\n" + 
			"  specification: \"The organization includes as part of security control assessments, [Assignment: organization-defined frequency], [Selection: announced; unannounced], [Selection (one or more): in-depth monitoring; vulnerability scanning; malicious user testing; insider threat assessment; performance/load testing; [Assignment: organization-defined other forms of security assessment]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-2(3) { \n" + 
			"  name: \"SECURITY ASSESSMENTS | EXTERNAL ORGANIZATIONS \"\n" + 
			"  specification: \"The organization accepts the results of an assessment of [Assignment: organization-defined information system] performed by [Assignment: organization-defined external organization] when the assessment meets [Assignment: organization-defined requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-3 { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS \"\n" + 
			"  specification: \"The organization: a. Authorizes connections from the information system to other information systems through the use of Interconnection Security Agreements; b. Documents, for each interconnection, the interface characteristics, security requirements, and the nature of the information communicated; and c. Reviews and updates Interconnection Security Agreements [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-3(1) { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS | UNCLASSIFIED NATIONAL SECURITY SYSTEM CONNECTIONS \"\n" + 
			"  specification: \"The organization prohibits the direct connection of an [Assignment: organization-defined unclassified, national security system] to an external network without the use of [Assignment: organization-defined boundary protection device].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-3(2) { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS | CLASSIFIED NATIONAL SECURITY SYSTEM CONNECTIONS \"\n" + 
			"  specification: \"The organization prohibits the direct connection of a classified, national security system to an external network without the use of [Assignment: organization-defined boundary protection device].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-3(3) { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS | UNCLASSIFIED NON-NATIONAL SECURITY SYSTEM CONNECTIONS \"\n" + 
			"  specification: \"The organization prohibits the direct connection of an [Assignment: organization-defined unclassified, non-national security system] to an external network without the use of [Assignment; organization-defined boundary protection device].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-3(4) { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS | CONNECTIONS TO PUBLIC NETWORKS \"\n" + 
			"  specification: \"The organization prohibits the direct connection of an [Assignment: organization-defined information system] to a public network.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-3(5) { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS | RESTRICTIONS ON EXTERNAL SYSTEM CONNECTIONS \"\n" + 
			"  specification: \"The organization employs [Selection: allow-all, deny-by-exception; deny-all, permit-by-exception] policy for allowing [Assignment: organization-defined information systems] to connect to external information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-4 { \n" + 
			"  name: \"SYSTEM INTERCONNECTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CA-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-5 { \n" + 
			"  name: \"PLAN OF ACTION AND MILESTONES \"\n" + 
			"  specification: \"The organization: a. Develops a plan of action and milestones for the information system to document the organization’s planned remedial actions to correct weaknesses or deficiencies noted during the assessment of the security controls and to reduce or eliminate known vulnerabilities in the system; and b. Updates existing plan of action and milestones [Assignment: organization-defined frequency] based on the findings from security controls assessments, security impact analyses, and continuous monitoring activities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-5(1) { \n" + 
			"  name: \"PLAN OF ACTION AND MILESTONES | AUTOMATION SUPPORT FOR ACCURACY / CURRENCY \"\n" + 
			"  specification: \"The organization employs automated mechanisms to help ensure that the plan of action and milestones for the information system is accurate, up to date, and readily available.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-6 { \n" + 
			"  name: \"SECURITY AUTHORIZATION \"\n" + 
			"  specification: \" The organization: a. Assigns a senior-level executive or manager as the authorizing official for the information system; b. Ensures that the authorizing official authorizes the information system for processing before commencing operations; and c. Updates the security authorization [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-7 { \n" + 
			"  name: \"CONTINUOUS MONITORING \"\n" + 
			"  specification: \"The organization develops a continuous monitoring strategy and implements a continuous monitoring program that includes:\n" + 
			"			a. Establishment of [Assignment: organization-defined metrics] to be monitored;\n" + 
			"			b. Establishment of [Assignment: organization-defined frequencies] for monitoring and [Assignment: organization-defined frequencies] for assessments supporting such monitoring;\n" + 
			"			c. Ongoing security control assessments in accordance with the organizational continuous monitoring strategy;\n" + 
			"			d. Ongoing security status monitoring of organization-defined metrics in accordance with the organizational continuous monitoring strategy;\n" + 
			"			e. Correlation and analysis of security-related information generated by assessments and monitoring;\n" + 
			"			f. Response actions to address results of the analysis of security-related information; and\n" + 
			"			g. Reporting the security status of organization and the information system to [Assignment: organization-defined personnel or roles] [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-7(1) { \n" + 
			"  name: \"CONTINUOUS MONITORING | INDEPENDENT ASSESSMENT \"\n" + 
			"  specification: \"The organization employs assessors or assessment teams with [Assignment: organization-defined level of independence] to monitor the security controls in the information system on an ongoing basis.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-7(2) { \n" + 
			"  name: \"CONTINUOUS MONITORING | TYPES OF ASSESSMENTS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CA-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-7(3) { \n" + 
			"  name: \"CONTINUOUS MONITORING | TREND ANALYSES \"\n" + 
			"  specification: \"The organization employs trend analyses to determine if security control implementations, the frequency of continuous monitoring activities, and/or the types of activities used in the continuous monitoring process need to be modified based on empirical data.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-8 { \n" + 
			"  name: \"PENETRATION TESTING \"\n" + 
			"  specification: \" \n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-8(1) { \n" + 
			"  name: \"PENETRATION TESTING | INDEPENDENT PENETRATION AGENT OR TEAM \"\n" + 
			"  specification: \"The organization employs an independent penetration agent or penetration team to perform penetration testing on the information system or system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-8(2) { \n" + 
			"  name: \"PENETRATION TESTING | RED TEAM EXERCISES \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined red team exercises] to simulate attempts by adversaries to compromise organizational information systems in accordance with [Assignment: organization-defined rules of engagement].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-9 { \n" + 
			"  name: \"INTERNAL SYSTEM CONNECTIONS \"\n" + 
			"  specification: \"The organization: a. Authorizes internal connections of [Assignment: organization-defined information system components or classes of components] to the information system; and b. Documents, for each internal connection, the interface characteristics, security requirements, and the nature of the information communicated.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CA-9(1) { \n" + 
			"  name: \"INTERNAL SYSTEM CONNECTIONS | SECURITY COMPLIANCE CHECKS \"\n" + 
			"  specification: \"The information system performs security compliance checks on constituent system components prior to the establishment of the internal connection.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CA \n" + 
			"  } \n" + 
			"  security control CM-1 { \n" + 
			"  name: \"CONFIGURATION MANAGEMENT POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A configuration management policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the configuration management policy and associated configuration management controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Configuration management policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Configuration management procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2 { \n" + 
			"  name: \"BASELINE CONFIGURATION \"\n" + 
			"  specification: \"The organization develops, documents, and maintains under configuration control, a current baseline configuration of the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(1) { \n" + 
			"  name: \"BASELINE CONFIGURATION | REVIEWS AND UPDATES \"\n" + 
			"  specification: \"The organization reviews and updates the baseline configuration of the information system: (a) [Assignment: organization-defined frequency]; (b) When required due to [Assignment organization-defined circumstances]; and (c) As an integral part of information system component installations and upgrades.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(2) { \n" + 
			"  name: \"BASELINE CONFIGURATION | AUTOMATION SUPPORT FOR ACCURACY / CURRENCY \"\n" + 
			"  specification: \"The organization employs automated mechanisms to maintain an up-to-date, complete, accurate, and readily available baseline configuration of the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(3) { \n" + 
			"  name: \"BASELINE CONFIGURATION | RETENTION OF PREVIOUS CONFIGURATIONS \"\n" + 
			"  specification: \"The organization retains [Assignment: organization-defined previous versions of baseline configurations of the information system] to support rollback.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(4) { \n" + 
			"  name: \"BASELINE CONFIGURATION | UNAUTHORIZED SOFTWARE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(5) { \n" + 
			"  name: \"BASELINE CONFIGURATION | AUTHORIZED SOFTWARE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(6) { \n" + 
			"  name: \"BASELINE CONFIGURATION | DEVELOPMENT AND TEST ENVIRONMENTS \"\n" + 
			"  specification: \"The organization maintains a baseline configuration for information system development and test environments that is managed separately from the operational baseline configuration.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-2(7) { \n" + 
			"  name: \"BASELINE CONFIGURATION | CONFIGURE SYSTEMS, COMPONENTS, OR DEVICES FOR HIGH-RISK AREAS \"\n" + 
			"  specification: \"The organization: (a) Issues [Assignment: organization-defined information systems, system components, or devices] with [Assignment: organization-defined configurations] to individuals traveling to locations that the organization deems to be of significant risk; and (b) Applies [Assignment: organization-defined security safeguards] to the devices when the individuals return.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3 { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Determines the types of changes to the information system that are configuration-controlled;\n" + 
			"			b. Reviews proposed configuration-controlled changes to the information system and approves or disapproves such changes with explicit consideration for security impact analyses;\n" + 
			"			c. Documents configuration change decisions associated with the information system;\n" + 
			"			d. Implements approved configuration-controlled changes to the information system;\n" + 
			"			e. Retains records of configuration-controlled changes to the information system for [Assignment: organization-defined time period];\n" + 
			"			f. Audits and reviews activities associated with configuration-controlled changes to the information system; and\n" + 
			"			g. Coordinates and provides oversight for configuration change control activities through [Assignment: organization-defined configuration change control element (e.g., committee, board)] that convenes [Selection (one or more): [Assignment: organization-defined frequency]; [Assignment: organization-defined configuration change conditions]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3(1) { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL | AUTOMATED DOCUMENT / NOTIFICATION / PROHIBITION OF CHANGES \"\n" + 
			"  specification: \"The organization employs automated mechanisms to: (a) Document proposed changes to the information system; (b) Notify [Assignment: organized-defined approval authorities] of proposed changes to the information system and request change approval; (c) Highlight proposed changes to the information system that have not been approved or disapproved by [Assignment: organization-defined time period]; (d) Prohibit changes to the information system until designated approvals are received; (e) Document all changes to the information system; and (f) Notify [Assignment: organization-defined personnel] when approved changes to the information system are completed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3(2) { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL | TEST / VALIDATE / DOCUMENT CHANGES \"\n" + 
			"  specification: \"The organization tests, validates, and documents changes to the information system before implementing the changes on the operational system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3(3) { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL | AUTOMATED CHANGE IMPLEMENTATION \"\n" + 
			"  specification: \"The organization employs automated mechanisms to implement changes to the current information system baseline and deploys the updated baseline across the installed base.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3(4) { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL | SECURITY REPRESENTATIVE \"\n" + 
			"  specification: \"The organization requires an information security representative to be a member of the [Assignment: organization-defined configuration change control element].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3(5) { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL | AUTOMATED SECURITY RESPONSE \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined security responses] automatically if baseline configurations are changed in an unauthorized manner.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-3(6) { \n" + 
			"  name: \"CONFIGURATION CHANGE CONTROL | CRYPTOGRAPHY MANAGEMENT \"\n" + 
			"  specification: \"The organization ensures that cryptographic mechanisms used to provide [Assignment: organization-defined security safeguards] are under configuration management.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-4 { \n" + 
			"  name: \"SECURITY IMPACT ANALYSIS \"\n" + 
			"  specification: \"The organization analyzes changes to the information system to determine potential security impacts prior to change implementation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-4(1) { \n" + 
			"  name: \"SECURITY IMPACT ANALYSIS | SEPARATE TEST ENVIRONMENTS \"\n" + 
			"  specification: \"The organization analyzes changes to the information system in a separate test environment before implementation in an operational environment, looking for security impacts due to flaws, weaknesses, incompatibility, or intentional malice.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-4(2) { \n" + 
			"  name: \"SECURITY IMPACT ANALYSIS | VERIFICATION OF SECURITY FUNCTIONS \"\n" + 
			"  specification: \"The organization, after the information system is changed, checks the security functions to verify that the functions are implemented correctly, operating as intended, and producing the desired outcome with regard to meeting the security requirements for the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5 { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE \"\n" + 
			"  specification: \"The organization defines, documents, approves, and enforces physical and logical access restrictions associated with changes to the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(1) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | AUTOMATED ACCESS ENFORCEMENT / AUDITING \"\n" + 
			"  specification: \"The information system enforces access restrictions and supports auditing of the enforcement actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(2) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | REVIEW SYSTEM CHANGES \"\n" + 
			"  specification: \"The organization reviews information system changes [Assignment: organization-defined frequency] and [Assignment: organization-defined circumstances] to determine whether unauthorized changes have occurred.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(3) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | SIGNED COMPONENTS \"\n" + 
			"  specification: \"The information system prevents the installation of [Assignment: organization-defined software and firmware components] without verification that the component has been digitally signed using a certificate that is recognized and approved by the organization.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(4) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | DUAL AUTHORIZATION \"\n" + 
			"  specification: \"The organization enforces dual authorization for implementing changes to [Assignment: organization-defined information system components and system-level information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(5) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | LIMIT PRODUCTION / OPERATIONAL PRIVILEGES \"\n" + 
			"  specification: \"The organization: (a) Limits privileges to change information system components and system-related information within a production or operational environment; and (b) Reviews and reevaluates privileges [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(6) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | LIMIT LIBRARY PRIVILEGES \"\n" + 
			"  specification: \"The organization limits privileges to change software resident within software libraries.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-5(7) { \n" + 
			"  name: \"ACCESS RESTRICTIONS FOR CHANGE | AUTOMATIC IMPLEMENTATION OF SECURITY SAFEGUARDS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-6 { \n" + 
			"  name: \"CONFIGURATION SETTINGS \"\n" + 
			"  specification: \"The organization: a. Establishes and documents configuration settings for information technology products employed within the information system using [Assignment: organization-defined security configuration checklists] that reflect the most restrictive mode consistent with operational requirements; b. Implements the configuration settings; c. Identifies, documents, and approves any deviations from established configuration settings for [Assignment: organization-defined information system components] based on [Assignment: organization-defined operational requirements]; and d. Monitors and controls changes to the configuration settings in accordance with organizational policies and procedures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-6(1) { \n" + 
			"  name: \"CONFIGURATION SETTINGS | AUTOMATED CENTRAL MANAGEMENT / APPLICATION / VERIFICATION \"\n" + 
			"  specification: \"The organization employs automated mechanisms to centrally manage, apply, and verify configuration settings for [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-6(2) { \n" + 
			"  name: \"CONFIGURATION SETTINGS | RESPOND TO UNAUTHORIZED CHANGES \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to respond to unauthorized changes to [Assignment: organization-defined configuration settings].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-6(3) { \n" + 
			"  name: \"CONFIGURATION SETTINGS | UNAUTHORIZED CHANGE DETECTION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-6(4) { \n" + 
			"  name: \"CONFIGURATION SETTINGS | CONFORMANCE DEMONSTRATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-7 { \n" + 
			"  name: \"LEAST FUNCTIONALITY \"\n" + 
			"  specification: \"The organization: a. Configures the information system to provide only essential capabilities; and b. Prohibits or restricts the use of the following functions, ports, protocols, and/or services: [Assignment: organization-defined prohibited or restricted functions, ports, protocols, and/or services].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-7(1) { \n" + 
			"  name: \"LEAST FUNCTIONALITY | PERIODIC REVIEW \"\n" + 
			"  specification: \"The organization: (a) Reviews the information system [Assignment: organization-defined frequency] to identify unnecessary and/or nonsecure functions, ports, protocols, and services; and (b) Disables [Assignment: organization-defined functions, ports, protocols, and services within the information system deemed to be unnecessary and/or nonsecure].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-7(2) { \n" + 
			"  name: \"LEAST FUNCTIONALITY | PREVENT PROGRAM EXECUTION \"\n" + 
			"  specification: \"The information system prevents program execution in accordance with [Selection (one or more): [Assignment: organization-defined policies regarding software program usage and restrictions]; rules authorizing the terms and conditions of software program usage].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-7(3) { \n" + 
			"  name: \"LEAST FUNCTIONALITY | REGISTRATION COMPLIANCE \"\n" + 
			"  specification: \"The organization ensures compliance with [Assignment: organization-defined registration requirements for functions, ports, protocols, and services].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-7(4) { \n" + 
			"  name: \"LEAST FUNCTIONALITY | UNAUTHORIZED SOFTWARE / BLACKLISTING \"\n" + 
			"  specification: \"The organization: (a) Identifies [Assignment: organization-defined software programs not authorized to execute on the information system]; (b) Employs an allow-all, deny-by-exception policy to prohibit the execution of unauthorized software programs on the information system; and (c) Reviews and updates the list of unauthorized software programs [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-7(5) { \n" + 
			"  name: \"LEAST FUNCTIONALITY | AUTHORIZED SOFTWARE / WHITELISTING \"\n" + 
			"  specification: \"The organization: (a) Identifies [Assignment: organization-defined software programs authorized to execute on the information system]; (b) Employs a deny-all, permit-by-exception policy to allow the execution of authorized software programs on the information system; and (c) Reviews and updates the list of authorized software programs [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8 { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops and documents an inventory of information system components that:\n" + 
			"				1. Accurately reflects the current information system;\n" + 
			"				2. Includes all components within the authorization boundary of the information system;\n" + 
			"				3. Is at the level of granularity deemed necessary for tracking and reporting; and\n" + 
			"				4. Includes [Assignment: organization-defined information deemed necessary to achieve effective information system component accountability]; and\n" + 
			"			b. Reviews and updates the information system component inventory [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(1) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | UPDATES DURING INSTALLATIONS / REMOVALS \"\n" + 
			"  specification: \"The organization updates the inventory of information system components as an integral part of component installations, removals, and information system updates.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(2) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | AUTOMATED MAINTENANCE \"\n" + 
			"  specification: \"The organization employs automated mechanisms to help maintain an up-to-date, complete, accurate, and readily available inventory of information system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(3) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | AUTOMATED UNAUTHORIZED COMPONENT DETECTION \"\n" + 
			"  specification: \"The organization:(a) Employs automated mechanisms [Assignment: organization-defined frequency] to detect the presence of unauthorized hardware, software, and firmware components within the information system; and (b) Takes the following actions when unauthorized components are detected: [Selection (one or more): disables network access by such components; isolates the components; notifies [Assignment: organization-defined personnel or roles]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(4) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | ACCOUNTABILITY INFORMATION \"\n" + 
			"  specification: \"The organization includes in the information system component inventory information, a means for identifying by [Selection (one or more): name; position; role], individuals responsible/accountable for administering those components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(5) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | NO DUPLICATE ACCOUNTING OF COMPONENTS \"\n" + 
			"  specification: \"The organization verifies that all components within the authorization boundary of the information system are not duplicated in other information system component inventories.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(6) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | ASSESSED CONFIGURATIONS / APPROVED DEVIATIONS \"\n" + 
			"  specification: \"The organization includes assessed component configurations and any approved deviations to current deployed configurations in the information system component inventory.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(7) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | CENTRALIZED REPOSITORY \"\n" + 
			"  specification: \"The organization provides a centralized repository for the inventory of information system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(8) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | AUTOMATED LOCATION TRACKING \"\n" + 
			"  specification: \"The organization employs automated mechanisms to support tracking of information system components by geographic location.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-8(9) { \n" + 
			"  name: \"INFORMATION SYSTEM COMPONENT INVENTORY | ASSIGNMENT OF COMPONENTS TO SYSTEMS \"\n" + 
			"  specification: \"The organization: (a) Assigns [Assignment: organization-defined acquired information system components] to an information system; and (b) Receives an acknowledgement from the information system owner of this assignment.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-9 { \n" + 
			"  name: \"CONFIGURATION MANAGEMENT PLAN \"\n" + 
			"  specification: \"The organization develops, documents, and implements a configuration management plan for the information system that:\n" + 
			"			a. Addresses roles, responsibilities, and configuration management processes and procedures;\n" + 
			"			b. Establishes a process for identifying configuration items throughout the system development life cycle and for managing the configuration of the configuration items;\n" + 
			"			c. Defines the configuration items for the information system and places the configuration items under configuration management; and\n" + 
			"			d. Protects the configuration management plan from unauthorized disclosure and modification.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-9(1) { \n" + 
			"  name: \"CONFIGURATION MANAGEMENT PLAN | ASSIGNMENT OF RESPONSIBILITY \"\n" + 
			"  specification: \"The organization assigns responsibility for developing the configuration management process to organizational personnel that are not directly involved in information system development.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-10 { \n" + 
			"  name: \"SOFTWARE USAGE RESTRICTIONS \"\n" + 
			"  specification: \"The organization: a. Uses software and associated documentation in accordance with contract agreements and copyright laws; b. Tracks the use of software and associated documentation protected by quantity licenses to control copying and distribution; and c. Controls and documents the use of peer-to-peer file sharing technology to ensure that this capability is not used for the unauthorized distribution, display, performance, or reproduction of copyrighted work.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-10(1) { \n" + 
			"  name: \"SOFTWARE USAGE RESTRICTIONS | OPEN SOURCE SOFTWARE \"\n" + 
			"  specification: \"The organization establishes the following restrictions on the use of open source software: [Assignment: organization-defined restrictions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-11 { \n" + 
			"  name: \"USER-INSTALLED SOFTWARE \"\n" + 
			"  specification: \"The organization:a. Establishes [Assignment: organization-defined policies] governing the installation of software by users; b. Enforces software installation policies through [Assignment: organization-defined methods]; andc. Monitors policy compliance at [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-11(1) { \n" + 
			"  name: \"USER-INSTALLED SOFTWARE | ALERTS FOR UNAUTHORIZED INSTALLATIONS \"\n" + 
			"  specification: \"The information system alerts [Assignment: organization-defined personnel or roles] when the unauthorized installation of software is detected.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CM-11(2) { \n" + 
			"  name: \"USER-INSTALLED SOFTWARE | PROHIBIT INSTALLATION WITHOUT PRIVILEGED STATUS \"\n" + 
			"  specification: \"The information system prohibits user installation of software without explicit privileged status.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CM \n" + 
			"  } \n" + 
			"  security control CP-1 { \n" + 
			"  name: \"CONTINGENCY PLANNING POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A contingency planning policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the contingency planning policy and associated contingency planning controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Contingency planning policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Contingency planning procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2 { \n" + 
			"  name: \"CONTINGENCY PLAN \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops a contingency plan for the information system that:\n" + 
			"				1. Identifies essential missions and business functions and associated contingency requirements;\n" + 
			"				2. Provides recovery objectives, restoration priorities, and metrics;\n" + 
			"				3. Addresses contingency roles, responsibilities, assigned individuals with contact information;\n" + 
			"				4. Addresses maintaining essential missions and business functions despite an information system disruption, compromise, or failure;\n" + 
			"				5. Addresses eventual, full information system restoration without deterioration of the security safeguards originally planned and implemented; and\n" + 
			"				6. Is reviewed and approved by [Assignment: organization-defined personnel or roles];\n" + 
			"			b. Distributes copies of the contingency plan to [Assignment: organization-defined key contingency personnel (identified by name and/or by role) and organizational elements];\n" + 
			"			c. Coordinates contingency planning activities with incident handling activities;\n" + 
			"			d. Reviews the contingency plan for the information system [Assignment: organization-defined frequency];\n" + 
			"			e. Updates the contingency plan to address changes to the organization, information system, or environment of operation and problems encountered during contingency plan implementation, execution, or testing;\n" + 
			"			f. Communicates contingency plan changes to [Assignment: organization-defined key contingency personnel (identified by name and/or by role) and organizational elements]; and\n" + 
			"			g. Protects the contingency plan from unauthorized disclosure and modification.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(1) { \n" + 
			"  name: \"CONTINGENCY PLAN | COORDINATE WITH RELATED PLANS \"\n" + 
			"  specification: \"The organization coordinates contingency plan development with organizational elements responsible for related plans.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(2) { \n" + 
			"  name: \"CONTINGENCY PLAN | CAPACITY PLANNING \"\n" + 
			"  specification: \"The organization conducts capacity planning so that necessary capacity for information processing, telecommunications, and environmental support exists during contingency operations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(3) { \n" + 
			"  name: \"CONTINGENCY PLAN | RESUME ESSENTIAL MISSIONS / BUSINESS FUNCTIONS \"\n" + 
			"  specification: \"The organization plans for the resumption of essential missions and business functions within [Assignment: organization-defined time period] of contingency plan activation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(4) { \n" + 
			"  name: \"CONTINGENCY PLAN | RESUME ALL MISSIONS / BUSINESS FUNCTIONS \"\n" + 
			"  specification: \"The organization plans for the resumption of all missions and business functions within [Assignment: organization-defined time period] of contingency plan activation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(5) { \n" + 
			"  name: \"CONTINGENCY PLAN | CONTINUE ESSENTIAL MISSIONS / BUSINESS FUNCTIONS \"\n" + 
			"  specification: \"The organization plans for the continuance of essential missions and business functions with little or no loss of operational continuity and sustains that continuity until full information system restoration at primary processing and/or storage sites.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(6) { \n" + 
			"  name: \"CONTINGENCY PLAN | ALTERNATE PROCESSING / STORAGE SITE \"\n" + 
			"  specification: \"The organization plans for the transfer of essential missions and business functions to alternate processing and/or storage sites with little or no loss of operational continuity and sustains that continuity through information system restoration to primary processing and/or storage sites.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(7) { \n" + 
			"  name: \"CONTINGENCY PLAN | COORDINATE WITH EXTERNAL SERVICE PROVIDERS \"\n" + 
			"  specification: \"The organization coordinates its contingency plan with the contingency plans of external service providers to ensure that contingency requirements can be satisfied.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-2(8) { \n" + 
			"  name: \"CONTINGENCY PLAN | IDENTIFY CRITICAL ASSETS \"\n" + 
			"  specification: \"The organization identifies critical information system assets supporting essential missions and business functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-3 { \n" + 
			"  name: \"CONTINGENCY TRAINING \"\n" + 
			"  specification: \"The organization provides contingency training to information system users consistent with assigned roles and responsibilities: a. Within [Assignment: organization-defined time period] of assuming a contingency role or responsibility; b. When required by information system changes; and c. [Assignment: organization-defined frequency] thereafter.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-3(1) { \n" + 
			"  name: \"CONTINGENCY TRAINING | SIMULATED EVENTS \"\n" + 
			"  specification: \"The organization incorporates simulated events into contingency training to facilitate effective response by personnel in crisis situations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-3(2) { \n" + 
			"  name: \"CONTINGENCY TRAINING | AUTOMATED TRAINING ENVIRONMENTS \"\n" + 
			"  specification: \"The organization employs automated mechanisms to provide a more thorough and realistic contingency training environment.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-4 { \n" + 
			"  name: \"CONTINGENCY PLAN TESTING \"\n" + 
			"  specification: \"The organization: a. Tests the contingency plan for the information system [Assignment: organization-defined frequency] using [Assignment: organization-defined tests] to determine the effectiveness of the plan and the organizational readiness to execute the plan; b. Reviews the contingency plan test results; and c. Initiates corrective actions, if needed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-4(1) { \n" + 
			"  name: \"CONTINGENCY PLAN TESTING | COORDINATE WITH RELATED PLANS \"\n" + 
			"  specification: \"The organization coordinates contingency plan testing with organizational elements responsible for related plans.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-4(2) { \n" + 
			"  name: \"CONTINGENCY PLAN TESTING | ALTERNATE PROCESSING SITE \"\n" + 
			"  specification: \"The organization tests the contingency plan at the alternate processing site: (a) To familiarize contingency personnel with the facility and available resources; and (b) To evaluate the capabilities of the alternate processing site to support contingency operations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-4(3) { \n" + 
			"  name: \"CONTINGENCY PLAN TESTING | AUTOMATED TESTING \"\n" + 
			"  specification: \"The organization employs automated mechanisms to more thoroughly and effectively test the contingency plan.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-4(4) { \n" + 
			"  name: \"CONTINGENCY PLAN TESTING | FULL RECOVERY / RECONSTITUTION \"\n" + 
			"  specification: \"The organization includes a full recovery and reconstitution of the information system to a known state as part of contingency plan testing.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-5 { \n" + 
			"  name: \" \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CP-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-6 { \n" + 
			"  name: \"ALTERNATE STORAGE SITE \"\n" + 
			"  specification: \"The organization: a. Establishes an alternate storage site including necessary agreements to permit the storage and retrieval of information system backup information; and b. Ensures that the alternate storage site provides information security safeguards equivalent to that of the primary site.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-6(1) { \n" + 
			"  name: \"ALTERNATE STORAGE SITE | SEPARATION FROM PRIMARY SITE \"\n" + 
			"  specification: \"The organization identifies an alternate storage site that is separated from the primary storage site to reduce susceptibility to the same threats.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-6(2) { \n" + 
			"  name: \"ALTERNATE STORAGE SITE | RECOVERY TIME / POINT OBJECTIVES \"\n" + 
			"  specification: \"The organization configures the alternate storage site to facilitate recovery operations in accordance with recovery time and recovery point objectives.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-6(3) { \n" + 
			"  name: \"ALTERNATE STORAGE SITE | ACCESSIBILITY \"\n" + 
			"  specification: \"The organization identifies potential accessibility problems to the alternate storage site in the event of an area-wide disruption or disaster and outlines explicit mitigation actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7 { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE \"\n" + 
			"  specification: \"The organization: a. Establishes an alternate processing site including necessary agreements to permit the transfer and resumption of [Assignment: organization-defined information system operations] for essential missions/business functions within [Assignment: organization-defined time periodconsistent with recovery time and recovery point objectives] when the primary processing capabilities are unavailable; b. Ensures that equipment and supplies required to transfer and resume operations are available at the alternate processing site or contracts are in place to support delivery to the site within the organization-defined time period for transfer/resumption; and c. Ensures that the alternate processing site provides information security safeguards equivalent to those of the primary site.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7(1) { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE | SEPARATION FROM PRIMARY SITE \"\n" + 
			"  specification: \"The organization identifies an alternate processing site that is separated from the primary processing site to reduce susceptibility to the same threats.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7(2) { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE | ACCESSIBILITY \"\n" + 
			"  specification: \"The organization identifies potential accessibility problems to the alternate processing site in the event of an area-wide disruption or disaster and outlines explicit mitigation actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7(3) { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE | PRIORITY OF SERVICE \"\n" + 
			"  specification: \"The organization develops alternate processing site agreements that contain priority-of-service provisions in accordance with organizational availability requirements (including recovery time objectives).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7(4) { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE | PREPARATION FOR USE \"\n" + 
			"  specification: \"The organization prepares the alternate processing site so that the site is ready to be used as the operational site supporting essential missions and business functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7(5) { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE | EQUIVALENT INFORMATION SECURITY SAFEGUARDS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CP-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-7(6) { \n" + 
			"  name: \"ALTERNATE PROCESSING SITE | INABILITY TO RETURN TO PRIMARY SITE \"\n" + 
			"  specification: \"The organization plans and prepares for circumstances that preclude returning to the primary processing site.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-8 { \n" + 
			"  name: \"TELECOMMUNICATIONS SERVICES \"\n" + 
			"  specification: \"The organization establishes alternate telecommunications services including necessary agreements to permit the resumption of [Assignment: organization-defined information system operations] for essential missions and business functions within [Assignment: organization-defined time period] when the primary telecommunications capabilities are unavailable at either the primary or alternate processing or storage sites.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-8(1) { \n" + 
			"  name: \"TELECOMMUNICATIONS SERVICES | PRIORITY OF SERVICE PROVISIONS \"\n" + 
			"  specification: \"The organization:(a) Develops primary and alternate telecommunications service agreements that contain priority-of-service provisions in accordance with organizational availability requirements (including recovery time objectives); and (b) Requests Telecommunications Service Priority for all telecommunications services used for national security emergency preparedness in the event that the primary and/or alternate telecommunications services are provided by a common carrier.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-8(2) { \n" + 
			"  name: \"TELECOMMUNICATIONS SERVICES | SINGLE POINTS OF FAILURE \"\n" + 
			"  specification: \"The organization obtains alternate telecommunications services to reduce the likelihood of sharing a single point of failure with primary telecommunications services.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-8(3) { \n" + 
			"  name: \"TELECOMMUNICATIONS SERVICES | SEPARATION OF PRIMARY / ALTERNATE PROVIDERS \"\n" + 
			"  specification: \"The organization obtains alternate telecommunications services from providers that are separated from primary service providers to reduce susceptibility to the same threats.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-8(4) { \n" + 
			"  name: \"TELECOMMUNICATIONS SERVICES | PROVIDER CONTINGENCY PLAN \"\n" + 
			"  specification: \"The organization: (a) Requires primary and alternate telecommunications service providers to have contingency plans; (b) Reviews provider contingency plans to ensure that the plans meet organizational contingency requirements; and (c) Obtains evidence of contingency testing/training by providers [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-8(5) { \n" + 
			"  name: \"TELECOMMUNICATIONS SERVICES | ALTERNATE TELECOMMUNICATION SERVICE TESTING \"\n" + 
			"  specification: \"The organization tests alternate telecommunication services [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9 { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP \"\n" + 
			"  specification: \"The organization: a. Conducts backups of user-level information contained in the information system [Assignment: organization-defined frequency consistent with recovery time and recovery point objectives]; b. Conducts backups of system-level information contained in the information system [Assignment: organization-defined frequency consistent with recovery time and recovery point objectives]; c. Conducts backups of information system documentation including security-related documentation [Assignment: organization-defined frequency consistent with recovery time and recovery point objectives]; and d. Protects the confidentiality, integrity, and availability of backup information at storage locations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(1) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | TESTING FOR RELIABILITY / INTEGRITY \"\n" + 
			"  specification: \"The organization tests backup information [Assignment: organization-defined frequency] to verify media reliability and information integrity.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(2) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | TEST RESTORATION USING SAMPLING \"\n" + 
			"  specification: \"The organization uses a sample of backup information in the restoration of selected information system functions as part of contingency plan testing.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(3) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | SEPARATE STORAGE FOR CRITICAL INFORMATION \"\n" + 
			"  specification: \"The organization stores backup copies of [Assignment: organization-defined critical information system software and other security-related information] in a separate facility or in a fire-rated container that is not collocated with the operational system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(4) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | PROTECTION FROM UNAUTHORIZED MODIFICATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CP-9.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(5) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | TRANSFER TO ALTERNATE STORAGE SITE \"\n" + 
			"  specification: \"The organization transfers information system backup information to the alternate storage site [Assignment: organization-defined time period and transfer rate consistent with the recovery time and recovery point objectives].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(6) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | REDUNDANT SECONDARY SYSTEM \"\n" + 
			"  specification: \"The organization accomplishes information system backup by maintaining a redundant secondary system that is not collocated with the primary system and that can be activated without loss of information or disruption to operations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-9(7) { \n" + 
			"  name: \"INFORMATION SYSTEM BACKUP | DUAL AUTHORIZATION \"\n" + 
			"  specification: \"The organization enforces dual authorization for the deletion or destruction of [Assignment: organization-defined backup information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10 { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION \"\n" + 
			"  specification: \"The organization provides for the recovery and reconstitution of the information system to a known state after a disruption, compromise, or failure.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10(1) { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION | CONTINGENCY PLAN TESTING \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CP-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10(2) { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION | TRANSACTION RECOVERY \"\n" + 
			"  specification: \"The information system implements transaction recovery for systems that are transaction-based.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10(3) { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION | COMPENSATING SECURITY CONTROLS \"\n" + 
			"  specification: \"Withdrawn: Addressed through tailoring procedures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10(4) { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION | RESTORE WITHIN TIME PERIOD \"\n" + 
			"  specification: \"The organization provides the capability to restore information system components within [Assignment: organization-defined restoration time-periods] from configuration-controlled and integrity-protected information representing a known, operational state for the components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10(5) { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION | FAILOVER CAPABILITY \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-13.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-10(6) { \n" + 
			"  name: \"INFORMATION SYSTEM RECOVERY AND RECONSTITUTION | COMPONENT PROTECTION \"\n" + 
			"  specification: \"The organization protects backup and restoration hardware, firmware, and software.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-11 { \n" + 
			"  name: \"ALTERNATE COMMUNICATIONS PROTOCOLS \"\n" + 
			"  specification: \"The information system provides the capability to employ [Assignment: organization-defined alternative communications protocols] in support of maintaining continuity of operations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-12 { \n" + 
			"  name: \"SAFE MODE \"\n" + 
			"  specification: \"The information system, when [Assignment: organization-defined conditions] are detected, enters a safe mode of operation with [Assignment: organization-defined restrictions of safe mode of operation].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control CP-13 { \n" + 
			"  name: \"ALTERNATIVE SECURITY MECHANISMS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined alternative or supplemental security mechanisms] for satisfying [Assignment: organization-defined security functions] when the primary means of implementing the security function is unavailable or compromised.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.CP \n" + 
			"  } \n" + 
			"  security control IA-1 { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. An identification and authentication policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the identification and authentication policy and associated identification and authentication controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Identification and authentication policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Identification and authentication procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2 { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION (ORGANIZATIONAL USERS) \"\n" + 
			"  specification: \"The information system uniquely identifies and authenticates organizational users (or processes acting on behalf of organizational users).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(1) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | NETWORK ACCESS TO PRIVILEGED ACCOUNTS \"\n" + 
			"  specification: \"The information system implements multifactor authentication for network access to privileged accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(2) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | NETWORK ACCESS TO NON-PRIVILEGED ACCOUNTS \"\n" + 
			"  specification: \"The information system implements multifactor authentication for network access to non-privileged accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(3) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | LOCAL ACCESS TO PRIVILEGED ACCOUNTS \"\n" + 
			"  specification: \"The information system implements multifactor authentication for local access to privileged accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(4) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | LOCAL ACCESS TO NON-PRIVILEGED ACCOUNTS \"\n" + 
			"  specification: \"The information system implements multifactor authentication for local access to non-privileged accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(5) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | GROUP AUTHENTICATION \"\n" + 
			"  specification: \"The organization requires individuals to be authenticated with an individual authenticator when a group authenticator is employed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(6) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | NETWORK ACCESS TO PRIVILEGED ACCOUNTS - SEPARATE DEVICE \"\n" + 
			"  specification: \"The information system implements multifactor authentication for network access to privileged accounts such that one of the factors is provided by a device separate from the system gaining access and the device meets [Assignment: organization-defined strength of mechanism requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(7) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | NETWORK ACCESS TO NON-PRIVILEGED ACCOUNTS - SEPARATE DEVICE \"\n" + 
			"  specification: \"The information system implements multifactor authentication for network access to non-privileged accounts such that one of the factors is provided by a device separate from the system gaining access and the device meets [Assignment: organization-defined strength of mechanism requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(8) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | NETWORK ACCESS TO PRIVILEGED ACCOUNTS - REPLAY RESISTANT \"\n" + 
			"  specification: \"The information system implements replay-resistant authentication mechanisms for network access to privileged accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(9) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | NETWORK ACCESS TO NON-PRIVILEGED ACCOUNTS - REPLAY RESISTANT \"\n" + 
			"  specification: \"The information system implements replay-resistant authentication mechanisms for network access to non-privileged accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(10) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | SINGLE SIGN-ON \"\n" + 
			"  specification: \"The information system provides a single sign-on capability for [Assignment: organization-defined information system accounts and services].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(11) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | REMOTE ACCESS - SEPARATE DEVICE \"\n" + 
			"  specification: \"The information system implements multifactor authentication for remote access to privileged and non-privileged accounts such that one of the factors is provided by a device separate from the system gaining access and the device meets [Assignment: organization-defined strength of mechanism requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(12) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | ACCEPTANCE OF PIV CREDENTIALS \"\n" + 
			"  specification: \"The information system accepts and electronically verifies Personal Identity Verification (PIV) credentials.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-2(13) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | OUT-OF-BAND AUTHENTICATION \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined out-of-band authentication] under [Assignment: organization-defined conditions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-3 { \n" + 
			"  name: \"DEVICE IDENTIFICATION AND AUTHENTICATION \"\n" + 
			"  specification: \"The information system uniquely identifies and authenticates [Assignment: organization-defined specific and/or types of devices] before establishing a [Selection (one or more): local; remote; network] connection.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-3(1) { \n" + 
			"  name: \"DEVICE IDENTIFICATION AND AUTHENTICATION | CRYPTOGRAPHIC BIDIRECTIONAL AUTHENTICATION \"\n" + 
			"  specification: \"The information system authenticates [Assignment: organization-defined specific devices and/or types of devices] before establishing [Selection (one or more): local; remote; network] connection using bidirectional authentication that is cryptographically based.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-3(2) { \n" + 
			"  name: \"DEVICE IDENTIFICATION AND AUTHENTICATION | CRYPTOGRAPHIC BIDIRECTIONAL NETWORK AUTHENTICATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into IA-3 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-3(3) { \n" + 
			"  name: \"DEVICE IDENTIFICATION AND AUTHENTICATION | DYNAMIC ADDRESS ALLOCATION \"\n" + 
			"  specification: \"The organization:(a) Standardizes dynamic address allocation lease information and the lease duration assigned to devices in accordance with [Assignment: organization-defined lease information and lease duration]; and (b) Audits lease information when assigned to a device.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-3(4) { \n" + 
			"  name: \"DEVICE IDENTIFICATION AND AUTHENTICATION | DEVICE ATTESTATION \"\n" + 
			"  specification: \"The organization ensures that device identification and authentication based on attestation is handled by [Assignment: organization-defined configuration management process].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4 { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT \"\n" + 
			"  specification: \"The organization manages information system identifiers by:\n" + 
			"			a. Receiving authorization from [Assignment: organization-defined personnel or roles] to assign an individual, group, role, or device identifier;\n" + 
			"			b. Selecting an identifier that identifies an individual, group, role, or device;\n" + 
			"			c. Assigning the identifier to the intended individual, group, role, or device;\n" + 
			"			d. Preventing reuse of identifiers for [Assignment: organization-defined time period]; and\n" + 
			"			e. Disabling the identifier after [Assignment: organization-defined time period of inactivity].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(1) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | PROHIBIT ACCOUNT IDENTIFIERS AS PUBLIC IDENTIFIERS \"\n" + 
			"  specification: \"The organization prohibits the use of information system account identifiers that are the same as public identifiers for individual electronic mail accounts.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(2) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | SUPERVISOR AUTHORIZATION \"\n" + 
			"  specification: \"The organization requires that the registration process to receive an individual identifier includes supervisor authorization.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(3) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | MULTIPLE FORMS OF CERTIFICATION \"\n" + 
			"  specification: \"The organization requires multiple forms of certification of individual identification be presented to the registration authority.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(4) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | IDENTIFY USER STATUS \"\n" + 
			"  specification: \"The organization manages individual identifiers by uniquely identifying each individual as [Assignment: organization-defined characteristic identifying individual status].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(5) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | DYNAMIC MANAGEMENT \"\n" + 
			"  specification: \"The information system dynamically manages identifiers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(6) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | CROSS-ORGANIZATION MANAGEMENT \"\n" + 
			"  specification: \"The organization coordinates with [Assignment: organization-defined external organizations] for cross-organization management of identifiers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-4(7) { \n" + 
			"  name: \"IDENTIFIER MANAGEMENT | IN-PERSON REGISTRATION \"\n" + 
			"  specification: \"The organization requires that the registration process to receive an individual identifier be conducted in person before a designated registration authority.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5 { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT \"\n" + 
			"  specification: \"The organization manages information system authenticators by:\n" + 
			"			a. Verifying, as part of the initial authenticator distribution, the identity of the individual, group, role, or device receiving the authenticator;\n" + 
			"			b. Establishing initial authenticator content for authenticators defined by the organization;\n" + 
			"			c. Ensuring that authenticators have sufficient strength of mechanism for their intended use;\n" + 
			"			d. Establishing and implementing administrative procedures for initial authenticator distribution, for lost/compromised or damaged authenticators, and for revoking authenticators;\n" + 
			"			e. Changing default content of authenticators prior to information system installation;\n" + 
			"			f. Establishing minimum and maximum lifetime restrictions and reuse conditions for authenticators;\n" + 
			"			g. Changing/refreshing authenticators [Assignment: organization-defined time period by authenticator type];\n" + 
			"			h. Protecting authenticator content from unauthorized disclosure and modification;\n" + 
			"			i. Requiring individuals to take, and having devices implement, specific security safeguards to protect authenticators; and\n" + 
			"			j. Changing authenticators for group/role accounts when membership to those accounts changes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(1) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | PASSWORD-BASED AUTHENTICATION \"\n" + 
			"  specification: \"The information system, for password-based authentication:\n" + 
			"			(a) Enforces minimum password complexity of [Assignment: organization-defined requirements for case sensitivity, number of characters, mix of upper-case letters, lower-case letters, numbers, and special characters, including minimum requirements for each type];\n" + 
			"			(b) Enforces at least the following number of changed characters when new passwords are created: [Assignment: organization-defined number];\n" + 
			"			(c) Stores and transmits only cryptographically-protected passwords;\n" + 
			"			(d) Enforces password minimum and maximum lifetime restrictions of [Assignment: organization-defined numbers for lifetime minimum, lifetime maximum];\n" + 
			"			(e) Prohibits password reuse for [Assignment: organization-defined number] generations; and\n" + 
			"			(f) Allows the use of a temporary password for system logons with an immediate change to a permanent password.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(2) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | PKI-BASED AUTHENTICATION \"\n" + 
			"  specification: \"The information system, for PKI-based authentication: (a) Validates certifications by constructing and verifying a certification path to an accepted trust anchor including checking certificate status information; (b) Enforces authorized access to the corresponding private key; (c) Maps the authenticated identity to the account of the individual or group; and (d) Implements a local cache of revocation data to support path discovery and validation in case of inability to access revocation information via the network.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(3) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | IN-PERSON OR TRUSTED THIRD-PARTY REGISTRATION \"\n" + 
			"  specification: \"The organization requires that the registration process to receive [Assignment: organization-defined types of and/or specific authenticators] be conducted [Selection: in person; by a trusted third party] before [Assignment: organization-defined registration authority] with authorization by [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(4) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | AUTOMATED SUPPORT FOR PASSWORD STRENGTH DETERMINATION \"\n" + 
			"  specification: \"The organization employs automated tools to determine if password authenticators are sufficiently strong to satisfy [Assignment: organization-defined requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(5) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | CHANGE AUTHENTICATORS PRIOR TO DELIVERY \"\n" + 
			"  specification: \"The organization requires developers/installers of information system components to provide unique authenticators or change default authenticators prior to delivery/installation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(6) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | PROTECTION OF AUTHENTICATORS \"\n" + 
			"  specification: \"The organization protects authenticators commensurate with the security category of the information to which use of the authenticator permits access.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(7) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | NO EMBEDDED UNENCRYPTED STATIC AUTHENTICATORS \"\n" + 
			"  specification: \"The organization ensures that unencrypted static authenticators are not embedded in applications or access scripts or stored on function keys.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(8) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | MULTIPLE INFORMATION SYSTEM ACCOUNTS \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined security safeguards] to manage the risk of compromise due to individuals having accounts on multiple information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(9) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | CROSS-ORGANIZATION CREDENTIAL MANAGEMENT \"\n" + 
			"  specification: \"The organization coordinates with [Assignment: organization-defined external organizations] for cross-organization management of credentials.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(10) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | DYNAMIC CREDENTIAL ASSOCIATION \"\n" + 
			"  specification: \"The information system dynamically provisions identities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(11) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | HARDWARE TOKEN-BASED AUTHENTICATION \"\n" + 
			"  specification: \"The information system, for hardware token-based authentication, employs mechanisms that satisfy [Assignment: organization-defined token quality requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(12) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | BIOMETRIC-BASED AUTHENTICATION \"\n" + 
			"  specification: \"The information system, for biometric-based authentication, employs mechanisms that satisfy [Assignment: organization-defined biometric quality requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(13) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | EXPIRATION OF CACHED AUTHENTICATORS \"\n" + 
			"  specification: \"The information system prohibits the use of cached authenticators after [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(14) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | MANAGING CONTENT OF PKI TRUST STORES \"\n" + 
			"  specification: \"The organization, for PKI-based authentication, employs a deliberate organization-wide methodology for managing the content of PKI trust stores installed across all platforms including networks, operating systems, browsers, and applications.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-5(15) { \n" + 
			"  name: \"AUTHENTICATOR MANAGEMENT | FICAM-APPROVED PRODUCTS AND SERVICES \"\n" + 
			"  specification: \"The organization uses only FICAM-approved path discovery and validation products and services.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-6 { \n" + 
			"  name: \"AUTHENTICATOR FEEDBACK \"\n" + 
			"  specification: \"The information system obscures feedback of authentication information during the authentication process to protect the information from possible exploitation/use by unauthorized individuals.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-7 { \n" + 
			"  name: \"CRYPTOGRAPHIC MODULE AUTHENTICATION \"\n" + 
			"  specification: \"The information system implements mechanisms for authentication to a cryptographic module that meet the requirements of applicable federal laws, Executive Orders, directives, policies, regulations, standards, and guidance for such authentication.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-8 { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION (NON-ORGANIZATIONAL USERS) \"\n" + 
			"  specification: \"The information system uniquely identifies and authenticates non-organizational users (or processes acting on behalf of non-organizational users).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-8(1) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | ACCEPTANCE OF PIV CREDENTIALS FROM OTHER AGENCIES \"\n" + 
			"  specification: \"The information system accepts and electronically verifies Personal Identity Verification (PIV) credentials from other federal agencies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-8(2) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | ACCEPTANCE OF THIRD-PARTY CREDENTIALS \"\n" + 
			"  specification: \"The information system accepts only FICAM-approved third-party credentials.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-8(3) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | USE OF FICAM-APPROVED PRODUCTS \"\n" + 
			"  specification: \"The organization employs only FICAM-approved information system components in [Assignment: organization-defined information systems] to accept third-party credentials.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-8(4) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | USE OF FICAM-ISSUED PROFILES \"\n" + 
			"  specification: \"The information system conforms to FICAM-issued profiles.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-8(5) { \n" + 
			"  name: \"IDENTIFICATION AND AUTHENTICATION | ACCEPTANCE OF PIV-I CREDENTIALS \"\n" + 
			"  specification: \"The information system accepts and electronically verifies Personal Identity Verification-I (PIV-I) credentials.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-9 { \n" + 
			"  name: \"SERVICE IDENTIFICATION AND AUTHENTICATION \"\n" + 
			"  specification: \"The organization identifies and authenticates [Assignment: organization-defined information system services] using [Assignment: organization-defined security safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-9(1) { \n" + 
			"  name: \"SERVICE IDENTIFICATION AND AUTHENTICATION | INFORMATION EXCHANGE \"\n" + 
			"  specification: \"The organization ensures that service providers receive, validate, and transmit identification and authentication information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-9(2) { \n" + 
			"  name: \"SERVICE IDENTIFICATION AND AUTHENTICATION | TRANSMISSION OF DECISIONS \"\n" + 
			"  specification: \"The organization ensures that identification and authentication decisions are transmitted between [Assignment: organization-defined services] consistent with organizational policies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-10 { \n" + 
			"  name: \"ADAPTIVE IDENTIFICATION AND AUTHENTICATION \"\n" + 
			"  specification: \"The organization requires that individuals accessing the information system employ [Assignment: organization-defined supplemental authentication techniques or mechanisms] under specific [Assignment: organization-defined circumstances or situations].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IA-11 { \n" + 
			"  name: \"RE-AUTHENTICATION \"\n" + 
			"  specification: \"The organization requires users and devices to re-authenticate when [Assignment: organization-defined circumstances or situations requiring re-authentication].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IA \n" + 
			"  } \n" + 
			"  security control IR-1 { \n" + 
			"  name: \"INCIDENT RESPONSE POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. An incident response policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the incident response policy and associated incident response controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Incident response policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Incident response procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-2 { \n" + 
			"  name: \"INCIDENT RESPONSE TRAINING \"\n" + 
			"  specification: \"The organization provides incident response training to information system users consistent with assigned roles and responsibilities: a. Within [Assignment: organization-defined time period] of assuming an incident response role or responsibility; b. When required by information system changes; and c. [Assignment: organization-defined frequency] thereafter.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-2(1) { \n" + 
			"  name: \"INCIDENT RESPONSE TRAINING | SIMULATED EVENTS \"\n" + 
			"  specification: \"The organization incorporates simulated events into incident response training to facilitate effective response by personnel in crisis situations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-2(2) { \n" + 
			"  name: \"INCIDENT RESPONSE TRAINING | AUTOMATED TRAINING ENVIRONMENTS \"\n" + 
			"  specification: \"The organization employs automated mechanisms to provide a more thorough and realistic incident response training environment.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-3 { \n" + 
			"  name: \"INCIDENT RESPONSE TESTING \"\n" + 
			"  specification: \"The organization tests the incident response capability for the information system [Assignment: organization-defined frequency] using [Assignment: organization-defined tests] to determine the incident response effectiveness and documents the results.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-3(1) { \n" + 
			"  name: \"INCIDENT RESPONSE TESTING | AUTOMATED TESTING \"\n" + 
			"  specification: \"The organization employs automated mechanisms to more thoroughly and effectively test the incident response capability.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-3(2) { \n" + 
			"  name: \"INCIDENT RESPONSE TESTING | COORDINATION WITH RELATED PLANS \"\n" + 
			"  specification: \"The organization coordinates incident response testing with organizational elements responsible for related plans.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4 { \n" + 
			"  name: \"INCIDENT HANDLING \"\n" + 
			"  specification: \"The organization: a. Implements an incident handling capability for security incidents that includes preparation, detection and analysis, containment, eradication, and recovery; b. Coordinates incident handling activities with contingency planning activities; and c. Incorporates lessons learned from ongoing incident handling activities into incident response procedures, training, and testing, and implements the resulting changes accordingly.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(1) { \n" + 
			"  name: \"INCIDENT HANDLING | AUTOMATED INCIDENT HANDLING PROCESSES \"\n" + 
			"  specification: \"The organization employs automated mechanisms to support the incident handling process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(2) { \n" + 
			"  name: \"INCIDENT HANDLING | DYNAMIC RECONFIGURATION \"\n" + 
			"  specification: \"The organization includes dynamic reconfiguration of [Assignment: organization-defined information system components] as part of the incident response capability.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(3) { \n" + 
			"  name: \"INCIDENT HANDLING | CONTINUITY OF OPERATIONS \"\n" + 
			"  specification: \"The organization identifies [Assignment: organization-defined classes of incidents] and [Assignment: organization-defined actions to take in response to classes of incidents] to ensure continuation of organizational missions and business functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(4) { \n" + 
			"  name: \"INCIDENT HANDLING | INFORMATION CORRELATION \"\n" + 
			"  specification: \"The organization correlates incident information and individual incident responses to achieve an organization-wide perspective on incident awareness and response.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(5) { \n" + 
			"  name: \"INCIDENT HANDLING | AUTOMATIC DISABLING OF INFORMATION SYSTEM \"\n" + 
			"  specification: \"The organization implements a configurable capability to automatically disable the information system if [Assignment: organization-defined security violations] are detected.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(6) { \n" + 
			"  name: \"INCIDENT HANDLING | INSIDER THREATS - SPECIFIC CAPABILITIES \"\n" + 
			"  specification: \"The organization implements incident handling capability for insider threats.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(7) { \n" + 
			"  name: \"INCIDENT HANDLING | INSIDER THREATS - INTRA-ORGANIZATION COORDINATION \"\n" + 
			"  specification: \"The organization coordinates incident handling capability for insider threats across [Assignment: organization-defined components or elements of the organization].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(8) { \n" + 
			"  name: \"INCIDENT HANDLING | CORRELATION WITH EXTERNAL ORGANIZATIONS \"\n" + 
			"  specification: \"The organization coordinates with [Assignment: organization-defined external organizations] to correlate and share [Assignment: organization-defined incident information] to achieve a cross-organization perspective on incident awareness and more effective incident responses.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(9) { \n" + 
			"  name: \"INCIDENT HANDLING | DYNAMIC RESPONSE CAPABILITY \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined dynamic response capabilities] to effectively respond to security incidents.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-4(10) { \n" + 
			"  name: \"INCIDENT HANDLING | SUPPLY CHAIN COORDINATION \"\n" + 
			"  specification: \"The organization coordinates incident handling activities involving supply chain events with other organizations involved in the supply chain.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-5 { \n" + 
			"  name: \"INCIDENTMONITORING \"\n" + 
			"  specification: \"\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-5(1) { \n" + 
			"  name: \"INCIDENT MONITORING | AUTOMATED TRACKING / DATA COLLECTION / ANALYSIS \"\n" + 
			"  specification: \"The organization employs automated mechanisms to assist in the tracking of security incidents and in the collection and analysis of incident information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-6 { \n" + 
			"  name: \"INCIDENT REPORTING \"\n" + 
			"  specification: \"The organization: a. Requires personnel to report suspected security incidents to the organizational incident response capability within [Assignment: organization-defined time period]; and b. Reports security incident information to [Assignment: organization-defined authorities].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-6(1) { \n" + 
			"  name: \"INCIDENT REPORTING | AUTOMATED REPORTING \"\n" + 
			"  specification: \"The organization employs automated mechanisms to assist in the reporting of security incidents.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-6(2) { \n" + 
			"  name: \"INCIDENT REPORTING | VULNERABILITIES RELATED TO INCIDENTS \"\n" + 
			"  specification: \"The organization reports information system vulnerabilities associated with reported security incidents to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-6(3) { \n" + 
			"  name: \"INCIDENT REPORTING | COORDINATION WITH SUPPLY CHAIN \"\n" + 
			"  specification: \"The organization provides security incident information to other organizations involved in the supply chain for information systems or information system components related to the incident.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-7 { \n" + 
			"  name: \"INCIDENT RESPONSE ASSISTANCE \"\n" + 
			"  specification: \"The organization provides an incident response support resource, integral to the organizational incident response capability that offers advice and assistance to users of the information system for the handling and reporting of security incidents.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-7(1) { \n" + 
			"  name: \"INCIDENT RESPONSE ASSISTANCE | AUTOMATION SUPPORT FOR AVAILABILITY OF INFORMATION / SUPPORT \"\n" + 
			"  specification: \"The organization employs automated mechanisms to increase the availability of incident response-related information and support.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-7(2) { \n" + 
			"  name: \"INCIDENT RESPONSE ASSISTANCE | COORDINATION WITH EXTERNAL PROVIDERS \"\n" + 
			"  specification: \"The organization:(a) Establishes a direct, cooperative relationship between its incident response capability and external providers of information system protection capability; and (b) Identifies organizational incident response team members to the external providers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-8 { \n" + 
			"  name: \"INCIDENT RESPONSE PLAN \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops an incident response plan that:\n" + 
			"				1. Provides the organization with a roadmap for implementing its incident response capability;\n" + 
			"				2. Describes the structure and organization of the incident response capability;\n" + 
			"				3. Provides a high-level approach for how the incident response capability fits into the overall organization;\n" + 
			"				4. Meets the unique requirements of the organization, which relate to mission, size, structure, and functions;\n" + 
			"				5. Defines reportable incidents;\n" + 
			"				6. Provides metrics for measuring the incident response capability within the organization;\n" + 
			"				7. Defines the resources and management support needed to effectively maintain and mature an incident response capability; and\n" + 
			"				8. Is reviewed and approved by [Assignment: organization-defined personnel or roles];\n" + 
			"			b. Distributes copies of the incident response plan to [Assignment: organization-defined incident response personnel (identified by name and/or by role) and organizational elements];\n" + 
			"			c. Reviews the incident response plan [Assignment: organization-defined frequency];\n" + 
			"			d. Updates the incident response plan to address system/organizational changes or problems encountered during plan implementation, execution, or testing;\n" + 
			"			e. Communicates incident response plan changes to [Assignment: organization-defined incident response personnel (identified by name and/or by role) and organizational elements]; and\n" + 
			"			f. Protects the incident response plan from unauthorized disclosure and modification.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-9 { \n" + 
			"  name: \"INFORMATION SPILLAGE RESPONSE \"\n" + 
			"  specification: \"The organization responds to information spills by:\n" + 
			"			a. Identifying the specific information involved in the information system contamination;\n" + 
			"			b. Alerting [Assignment: organization-defined personnel or roles] of the information spill using a method of communication not associated with the spill;\n" + 
			"			c. Isolating the contaminated information system or system component;\n" + 
			"			d. Eradicating the information from the contaminated information system or component;\n" + 
			"			e. Identifying other information systems or system components that may have been subsequently contaminated; and\n" + 
			"			f. Performing other [Assignment: organization-defined actions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-9(1) { \n" + 
			"  name: \"INFORMATION SPILLAGE RESPONSE | RESPONSIBLE PERSONNEL \"\n" + 
			"  specification: \"The organization assigns [Assignment: organization-defined personnel or roles] with responsibility for responding to information spills.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-9(2) { \n" + 
			"  name: \"INFORMATION SPILLAGE RESPONSE | TRAINING \"\n" + 
			"  specification: \"The organization provides information spillage response training [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-9(3) { \n" + 
			"  name: \"INFORMATION SPILLAGE RESPONSE | POST-SPILL OPERATIONS \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined procedures] to ensure that organizational personnel impacted by information spills can continue to carry out assigned tasks while contaminated systems are undergoing corrective actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-9(4) { \n" + 
			"  name: \"INFORMATION SPILLAGE RESPONSE | EXPOSURE TO UNAUTHORIZED PERSONNEL \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] for personnel exposed to information not within assigned access authorizations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control IR-10 { \n" + 
			"  name: \"INTEGRATED INFORMATION SECURITY ANALYSIS TEAM \"\n" + 
			"  specification: \"The organization establishes an integrated team of forensic/malicious code analysts, tool developers, and real-time operations personnel.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.IR \n" + 
			"  } \n" + 
			"  security control MA-1 { \n" + 
			"  name: \"SYSTEM MAINTENANCE POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A system maintenance policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the system maintenance policy and associated system maintenance controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. System maintenance policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. System maintenance procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-2 { \n" + 
			"  name: \"CONTROLLED MAINTENANCE \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Schedules, performs, documents, and reviews records of maintenance and repairs on information system components in accordance with manufacturer or vendor specifications and/or organizational requirements;\n" + 
			"			b. Approves and monitors all maintenance activities, whether performed on site or remotely and whether the equipment is serviced on site or removed to another location;\n" + 
			"			c. Requires that [Assignment: organization-defined personnel or roles] explicitly approve the removal of the information system or system components from organizational facilities for off-site maintenance or repairs;\n" + 
			"			d. Sanitizes equipment to remove all information from associated media prior to removal from organizational facilities for off-site maintenance or repairs;\n" + 
			"			e. Checks all potentially impacted security controls to verify that the controls are still functioning properly following maintenance or repair actions; and\n" + 
			"			f. Includes [Assignment: organization-defined maintenance-related information] in organizational maintenance records.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-2(1) { \n" + 
			"  name: \"CONTROLLED MAINTENANCE | RECORD CONTENT \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MA-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-2(2) { \n" + 
			"  name: \"CONTROLLED MAINTENANCE | AUTOMATED MAINTENANCE ACTIVITIES \"\n" + 
			"  specification: \"The organization: (a) Employs automated mechanisms to schedule, conduct, and document maintenance and repairs; and (b) Produces up-to date, accurate, and complete records of all maintenance and repair actions requested, scheduled, in process, and completed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-3 { \n" + 
			"  name: \"MAINTENANCE TOOLS \"\n" + 
			"  specification: \"The organization approves, controls, and monitors information system maintenance tools.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-3(1) { \n" + 
			"  name: \"MAINTENANCE TOOLS | INSPECT TOOLS \"\n" + 
			"  specification: \"The organization inspects the maintenance tools carried into a facility by maintenance personnel for improper or unauthorized modifications.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-3(2) { \n" + 
			"  name: \"MAINTENANCE TOOLS | INSPECT MEDIA \"\n" + 
			"  specification: \"The organization checks media containing diagnostic and test programs for malicious code before the media are used in the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-3(3) { \n" + 
			"  name: \"MAINTENANCE TOOLS | PREVENT UNAUTHORIZED REMOVAL \"\n" + 
			"  specification: \"The organization prevents the unauthorized removal of maintenance equipment containing organizational information by: (a) Verifying that there is no organizational information contained on the equipment; (b) Sanitizing or destroying the equipment; (c) Retaining the equipment within the facility; or (d) Obtaining an exemption from [Assignment: organization-defined personnel or roles] explicitly authorizing removal of the equipment from the facility.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-3(4) { \n" + 
			"  name: \"MAINTENANCE TOOLS | RESTRICTED TOOL USE \"\n" + 
			"  specification: \"The information system restricts the use of maintenance tools to authorized personnel only.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4 { \n" + 
			"  name: \"NONLOCAL MAINTENANCE \"\n" + 
			"  specification: \"The organization: a. Approves and monitors nonlocal maintenance and diagnostic activities; b. Allows the use of nonlocal maintenance and diagnostic tools only as consistent with organizational policy and documented in the security plan for the information system; c. Employs strong authenticators in the establishment of nonlocal maintenance and diagnostic sessions; d. Maintains records for nonlocal maintenance and diagnostic activities; and e. Terminates session and network connections when nonlocal maintenance is completed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(1) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | AUDITING AND REVIEW \"\n" + 
			"  specification: \"The organization: (a) Audits nonlocal maintenance and diagnostic sessions [Assignment: organization-defined audit events]; and (b) Reviews the records of the maintenance and diagnostic sessions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(2) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | DOCUMENT NONLOCAL MAINTENANCE \"\n" + 
			"  specification: \"The organization documents in the security plan for the information system, the policies and procedures for the establishment and use of nonlocal maintenance and diagnostic connections.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(3) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | COMPARABLE SECURITY / SANITIZATION \"\n" + 
			"  specification: \"The organization:\n" + 
			"			(a) Requires that nonlocal maintenance and diagnostic services be performed from an information system that implements a security capability comparable to the capability implemented on the system being serviced; or\n" + 
			"			(b) Removes the component to be serviced from the information system prior to nonlocal maintenance or diagnostic services, sanitizes the component (with regard to organizational information) before removal from organizational facilities, and after the service is performed, inspects and sanitizes the component (with regard to potentially malicious software) before reconnecting the component to the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(4) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | AUTHENTICATION / SEPARATION OF MAINTENANCE SESSIONS \"\n" + 
			"  specification: \"The organization protects nonlocal maintenance sessions by:\n" + 
			"			(a) Employing [Assignment: organization-defined authenticators that are replay resistant]; and\n" + 
			"			(b) Separating the maintenance sessions from other network sessions with the information system by either:\n" + 
			"				(1) Physically separated communications paths; or\n" + 
			"				(2) Logically separated communications paths based upon encryption.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(5) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | APPROVALS AND NOTIFICATIONS \"\n" + 
			"  specification: \"The organization: (a) Requires the approval of each nonlocal maintenance session by [Assignment: organization-defined personnel or roles]; and (b) Notifies [Assignment: organization-defined personnel or roles] of the date and time of planned nonlocal maintenance.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(6) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to protect the integrity and confidentiality of nonlocal maintenance and diagnostic communications.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-4(7) { \n" + 
			"  name: \"NONLOCAL MAINTENANCE | REMOTE DISCONNECT VERIFICATION \"\n" + 
			"  specification: \"The information system implements remote disconnect verification at the termination of nonlocal maintenance and diagnostic sessions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-5 { \n" + 
			"  name: \"MAINTENANCE PERSONNEL \"\n" + 
			"  specification: \"The organization: a. Establishes a process for maintenance personnel authorization and maintains a list of authorized maintenance organizations or personnel; b. Ensures that non-escorted personnel performing maintenance on the information system have required access authorizations; and c. Designates organizational personnel with required access authorizations and technical competence to supervise the maintenance activities of personnel who do not possess the required access authorizations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-5(1) { \n" + 
			"  name: \"MAINTENANCE PERSONNEL | INDIVIDUALS WITHOUT APPROPRIATE ACCESS \"\n" + 
			"  specification: \"The organization:\n" + 
			"			(a) Implements procedures for the use of maintenance personnel that lack appropriate security clearances or are not U.S. citizens, that include the following requirements:\n" + 
			"				(1) Maintenance personnel who do not have needed access authorizations, clearances, or formal access approvals are escorted and supervised during the performance of maintenance and diagnostic activities on the information system by approved organizational personnel who are fully cleared, have appropriate access authorizations, and are technically qualified;\n" + 
			"				(2) Prior to initiating maintenance or diagnostic activities by personnel who do not have needed access authorizations, clearances or formal access approvals, all volatile information storage components within the information system are sanitized and all nonvolatile storage media are removed or physically disconnected from the system and secured; and\n" + 
			"			(b) Develops and implements alternate security safeguards in the event an information system component cannot be sanitized, removed, or disconnected from the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-5(2) { \n" + 
			"  name: \"MAINTENANCE PERSONNEL | SECURITY CLEARANCES FOR CLASSIFIED SYSTEMS \"\n" + 
			"  specification: \"The organization ensures that personnel performing maintenance and diagnostic activities on an information system processing, storing, or transmitting classified information possess security clearances and formal access approvals for at least the highest classification level and for all compartments of information on the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-5(3) { \n" + 
			"  name: \"MAINTENANCE PERSONNEL | CITIZENSHIP REQUIREMENTS FOR CLASSIFIED SYSTEMS \"\n" + 
			"  specification: \"The organization ensures that personnel performing maintenance and diagnostic activities on an information system processing, storing, or transmitting classified information are U.S. citizens.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-5(4) { \n" + 
			"  name: \"MAINTENANCE PERSONNEL | FOREIGN NATIONALS \"\n" + 
			"  specification: \"The organization ensures that:\n" + 
			"			(a) Cleared foreign nationals (i.e., foreign nationals with appropriate security clearances), are used to conduct maintenance and diagnostic activities on classified information systems only when the systems are jointly owned and operated by the United States and foreign allied governments, or owned and operated solely by foreign allied governments; and\n" + 
			"			(b) Approvals, consents, and detailed operational conditions regarding the use of foreign nationals to conduct maintenance and diagnostic activities on classified information systems are fully documented within Memoranda of Agreements.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-5(5) { \n" + 
			"  name: \"MAINTENANCE PERSONNEL | NONSYSTEM-RELATED MAINTENANCE \"\n" + 
			"  specification: \"The organization ensures that non-escorted personnel performing maintenance activities not directly associated with the information system but in the physical proximity of the system, have required access authorizations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-6 { \n" + 
			"  name: \"TIMELY MAINTENANCE \"\n" + 
			"  specification: \"The organization obtains maintenance support and/or spare parts for [Assignment: organization-defined information system components] within [Assignment: organization-defined time period] of failure.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-6(1) { \n" + 
			"  name: \"TIMELY MAINTENANCE | PREVENTIVE MAINTENANCE \"\n" + 
			"  specification: \"The organization performs preventive maintenance on [Assignment: organization-defined information system components] at [Assignment: organization-defined time intervals].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-6(2) { \n" + 
			"  name: \"TIMELY MAINTENANCE | PREDICTIVE MAINTENANCE \"\n" + 
			"  specification: \"The organization performs predictive maintenance on [Assignment: organization-defined information system components] at [Assignment: organization-defined time intervals].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MA-6(3) { \n" + 
			"  name: \"TIMELY MAINTENANCE | AUTOMATED SUPPORT FOR PREDICTIVE MAINTENANCE \"\n" + 
			"  specification: \"The organization employs automated mechanisms to transfer predictive maintenance data to a computerized maintenance management system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MA \n" + 
			"  } \n" + 
			"  security control MP-1 { \n" + 
			"  name: \"MEDIA PROTECTION POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A media protection policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the media protection policy and associated media protection controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Media protection policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Media protection procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-2 { \n" + 
			"  name: \"MEDIA ACCESS \"\n" + 
			"  specification: \"The organization restricts access to [Assignment: organization-defined types of digital and/or non-digital media] to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-2(1) { \n" + 
			"  name: \"MEDIA ACCESS | AUTOMATED RESTRICTED ACCESS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-4 (2).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-2(2) { \n" + 
			"  name: \"MEDIA ACCESS | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-28 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-3 { \n" + 
			"  name: \"MEDIA MARKING \"\n" + 
			"  specification: \"The organization: a. Marks information system media indicating the distribution limitations, handling caveats, and applicable security markings (if any) of the information; and b. Exempts [Assignment: organization-defined types of information system media] from marking as long as the media remain within [Assignment: organization-defined controlled areas].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-4 { \n" + 
			"  name: \"MEDIA STORAGE \"\n" + 
			"  specification: \"The organization: a. Physically controls and securely stores [Assignment: organization-defined types of digital and/or non-digital media] within [Assignment: organization-defined controlled areas]; and b. Protects information system media until the media are destroyed or sanitized using approved equipment, techniques, and procedures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-4(1) { \n" + 
			"  name: \"MEDIA STORAGE | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-28 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-4(2) { \n" + 
			"  name: \"MEDIA STORAGE | AUTOMATED RESTRICTED ACCESS \"\n" + 
			"  specification: \"The organization employs automated mechanisms to restrict access to media storage areas and to audit access attempts and access granted.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-5 { \n" + 
			"  name: \"MEDIA TRANSPORT \"\n" + 
			"  specification: \"The organization: a. Protects and controls [Assignment: organization-defined types of information system media] during transport outside of controlled areas using [Assignment: organization-defined security safeguards]; b. Maintains accountability for information system media during transport outside of controlled areas; c. Documents activities associated with the transport of information system media; and d. Restricts the activities associated with the transport of information system media to authorized personnel.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-5(1) { \n" + 
			"  name: \"MEDIA TRANSPORT | PROTECTION OUTSIDE OF CONTROLLED AREAS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-5.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-5(2) { \n" + 
			"  name: \"MEDIA TRANSPORT | DOCUMENTATION OF ACTIVITIES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-5.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-5(3) { \n" + 
			"  name: \"MEDIA TRANSPORT | CUSTODIANS \"\n" + 
			"  specification: \"The organization employs an identified custodian during transport of information system media outside of controlled areas.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-5(4) { \n" + 
			"  name: \"MEDIA TRANSPORT | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to protect the confidentiality and integrity of information stored on digital media during transport outside of controlled areas.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6 { \n" + 
			"  name: \"MEDIA SANITIZATION \"\n" + 
			"  specification: \"The organization: a. Sanitizes [Assignment: organization-defined information system media] prior to disposal, release out of organizational control, or release for reuse using [Assignment: organization-defined sanitization techniques and procedures] in accordance with applicable federal and organizational standards and policies; and b. Employs sanitization mechanisms with the strength and integrity commensurate with the security category or classification of the information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(1) { \n" + 
			"  name: \"MEDIA SANITIZATION | REVIEW / APPROVE / TRACK / DOCUMENT / VERIFY \"\n" + 
			"  specification: \"The organization reviews, approves, tracks, documents, and verifies media sanitization and disposal actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(2) { \n" + 
			"  name: \"MEDIA SANITIZATION | EQUIPMENT TESTING \"\n" + 
			"  specification: \"The organization tests sanitization equipment and procedures [Assignment: organization-defined frequency] to verify that the intended sanitization is being achieved.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(3) { \n" + 
			"  name: \"MEDIA SANITIZATION | NONDESTRUCTIVE TECHNIQUES \"\n" + 
			"  specification: \"The organization applies nondestructive sanitization techniques to portable storage devices prior to connecting such devices to the information system under the following circumstances: [Assignment: organization-defined circumstances requiring sanitization of portable storage devices].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(4) { \n" + 
			"  name: \"MEDIA SANITIZATION | CONTROLLED UNCLASSIFIED INFORMATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(5) { \n" + 
			"  name: \"MEDIA SANITIZATION | CLASSIFIED INFORMATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(6) { \n" + 
			"  name: \"MEDIA SANITIZATION | MEDIA DESTRUCTION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(7) { \n" + 
			"  name: \"MEDIA SANITIZATION | DUAL AUTHORIZATION \"\n" + 
			"  specification: \"The organization enforces dual authorization for the sanitization of [Assignment: organization-defined information system media].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-6(8) { \n" + 
			"  name: \"MEDIA SANITIZATION | REMOTE PURGING / WIPING OF INFORMATION \"\n" + 
			"  specification: \"The organization provides the capability to purge/wipe information from [Assignment: organization-defined information systems, system components, or devices] either remotely or under the following conditions: [Assignment: organization-defined conditions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-7 { \n" + 
			"  name: \"MEDIA USE \"\n" + 
			"  specification: \"The organization [Selection: restricts; prohibits] the use of [Assignment: organization-defined types of information system media] on [Assignment: organization-defined information systems or system components] using [Assignment: organization-defined security safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-7(1) { \n" + 
			"  name: \"MEDIA USE | PROHIBIT USE WITHOUT OWNER \"\n" + 
			"  specification: \"The organization prohibits the use of portable storage devices in organizational information systems when such devices have no identifiable owner.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-7(2) { \n" + 
			"  name: \"MEDIA USE | PROHIBIT USE OF SANITIZATION-RESISTANT MEDIA \"\n" + 
			"  specification: \"The organization prohibits the use of sanitization-resistant media in organizational information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-8 { \n" + 
			"  name: \"MEDIA DOWNGRADING \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Establishes [Assignment: organization-defined information system media downgrading process] that includes employing downgrading mechanisms with [Assignment: organization-defined strength and integrity];\n" + 
			"			b. Ensures that the information system media downgrading process is commensurate with the security category and/or classification level of the information to be removed and the access authorizations of the potential recipients of the downgraded information;\n" + 
			"			c. Identifies [Assignment: organization-defined information system media requiring downgrading]; and\n" + 
			"			d. Downgrades the identified information system media using the established process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-8(1) { \n" + 
			"  name: \"MEDIA DOWNGRADING | DOCUMENTATION OF PROCESS \"\n" + 
			"  specification: \"The organization documents information system media downgrading actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-8(2) { \n" + 
			"  name: \"MEDIA DOWNGRADING | EQUIPMENT TESTING \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined tests] of downgrading equipment and procedures to verify correct performance [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-8(3) { \n" + 
			"  name: \"MEDIA DOWNGRADING | CONTROLLED UNCLASSIFIED INFORMATION \"\n" + 
			"  specification: \"The organization downgrades information system media containing [Assignment: organization-defined Controlled Unclassified Information (CUI)] prior to public release in accordance with applicable federal and organizational standards and policies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control MP-8(4) { \n" + 
			"  name: \"MEDIA DOWNGRADING | CLASSIFIED INFORMATION \"\n" + 
			"  specification: \"The organization downgrades information system media containing classified information prior to release to individuals without required access authorizations in accordance with NSA standards and policies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.MP \n" + 
			"  } \n" + 
			"  security control PE-1 { \n" + 
			"  name: \"PHYSICAL AND ENVIRONMENTAL PROTECTION POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A physical and environmental protection policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the physical and environmental protection policy and associated physical and environmental protection controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Physical and environmental protection policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Physical and environmental protection procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-2 { \n" + 
			"  name: \"PHYSICAL ACCESS AUTHORIZATIONS \"\n" + 
			"  specification: \"The organization: a. Develops, approves, and maintains a list of individuals with authorized access to the facility where the information system resides; b. Issues authorization credentials for facility access; c. Reviews the access list detailing authorized facility access by individuals [Assignment: organization-defined frequency]; and d. Removes individuals from the facility access list when access is no longer required.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-2(1) { \n" + 
			"  name: \"PHYSICAL ACCESS AUTHORIZATIONS | ACCESS BY POSITION / ROLE \"\n" + 
			"  specification: \"The organization authorizes physical access to the facility where the information system resides based on position or role.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-2(2) { \n" + 
			"  name: \"PHYSICAL ACCESS AUTHORIZATIONS | TWO FORMS OF IDENTIFICATION \"\n" + 
			"  specification: \"The organization requires two forms of identification from [Assignment: organization-defined list of acceptable forms of identification] for visitor access to the facility where the information system resides.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-2(3) { \n" + 
			"  name: \"PHYSICAL ACCESS AUTHORIZATIONS | RESTRICT UNESCORTED ACCESS \"\n" + 
			"  specification: \"The organization restricts unescorted access to the facility where the information system resides to personnel with [Selection (one or more): security clearances for all information contained within the system; formal access authorizations for all information contained within the system; need for access to all information contained within the system; [Assignment: organization-defined credentials]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3 { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Enforces physical access authorizations at [Assignment: organization-defined entry/exit points to the facility where the information system resides] by;\n" + 
			"				1. Verifying individual access authorizations before granting access to the facility; and\n" + 
			"				2. Controlling ingress/egress to the facility using [Selection (one or more): [Assignment: organization-defined physical access control systems/devices]; guards];\n" + 
			"			b. Maintains physical access audit logs for [Assignment: organization-defined entry/exit points];\n" + 
			"			c. Provides [Assignment: organization-defined security safeguards] to control access to areas within the facility officially designated as publicly accessible;\n" + 
			"			d. Escorts visitors and monitors visitor activity [Assignment: organization-defined circumstances requiring visitor escorts and monitoring];\n" + 
			"			e. Secures keys, combinations, and other physical access devices;\n" + 
			"			f. Inventories [Assignment: organization-defined physical access devices] every [Assignment: organization-defined frequency]; and\n" + 
			"			g. Changes combinations and keys [Assignment: organization-defined frequency] and/or when keys are lost, combinations are compromised, or individuals are transferred or terminated.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3(1) { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL | INFORMATION SYSTEM ACCESS \"\n" + 
			"  specification: \"The organization enforces physical access authorizations to the information system in addition to the physical access controls for the facility at [Assignment: organization-defined physical spaces containing one or more components of the information system].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3(2) { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL | FACILITY / INFORMATION SYSTEM BOUNDARIES \"\n" + 
			"  specification: \"The organization performs security checks [Assignment: organization-defined frequency] at the physical boundary of the facility or information system for unauthorized exfiltration of information or removal of information system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3(3) { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL | CONTINUOUS GUARDS / ALARMS / MONITORING \"\n" + 
			"  specification: \"The organization employs guards and/or alarms to monitor every physical access point to the facility where the information system resides 24 hours per day, 7 days per week.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3(4) { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL | LOCKABLE CASINGS \"\n" + 
			"  specification: \"The organization uses lockable physical casings to protect [Assignment: organization-defined information system components] from unauthorized physical access.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3(5) { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL | TAMPER PROTECTION \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to [Selection (one or more): detect; prevent] physical tampering or alteration of [Assignment: organization-defined hardware components] within the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-3(6) { \n" + 
			"  name: \"PHYSICAL ACCESS CONTROL | FACILITY PENETRATION TESTING \"\n" + 
			"  specification: \"The organization employs a penetration testing process that includes [Assignment: organization-defined frequency], unannounced attempts to bypass or circumvent security controls associated with physical access points to the facility.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-4 { \n" + 
			"  name: \"ACCESS CONTROL FOR TRANSMISSION MEDIUM \"\n" + 
			"  specification: \"The organization controls physical access to [Assignment: organization-defined information system distribution and transmission lines] within organizational facilities using [Assignment: organization-defined security safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-5 { \n" + 
			"  name: \"ACCESS CONTROL FOR OUTPUT DEVICES \"\n" + 
			"  specification: \"The organization controls physical access to information system output devices to prevent unauthorized individuals from obtaining the output.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-5(1) { \n" + 
			"  name: \"ACCESS CONTROL FOR OUTPUT DEVICES | ACCESS TO OUTPUT BY AUTHORIZED INDIVIDUALS \"\n" + 
			"  specification: \"The organization:(a) Controls physical access to output from [Assignment: organization-defined output devices]; and (b) Ensures that only authorized individuals receive output from the device.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-5(2) { \n" + 
			"  name: \"ACCESS CONTROL FOR OUTPUT DEVICES | ACCESS TO OUTPUT BY INDIVIDUAL IDENTITY \"\n" + 
			"  specification: \"The information system:(a) Controls physical access to output from [Assignment: organization-defined output devices]; and(b) Links individual identity to receipt of the output from the device.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-5(3) { \n" + 
			"  name: \"ACCESS CONTROL FOR OUTPUT DEVICES | MARKING OUTPUT DEVICES \"\n" + 
			"  specification: \"The organization marks [Assignment: organization-defined information system output devices] indicating the appropriate security marking of the information permitted to be output from the device.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-6 { \n" + 
			"  name: \"MONITORING PHYSICAL ACCESS \"\n" + 
			"  specification: \"The organization: a. Monitors physical access to the facility where the information system resides to detect and respond to physical security incidents; b. Reviews physical access logs [Assignment: organization-defined frequency] and upon occurrence of [Assignment: organization-defined events or potential indications of events]; and c. Coordinates results of reviews and investigations with the organizational incident response capability.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-6(1) { \n" + 
			"  name: \"MONITORING PHYSICAL ACCESS | INTRUSION ALARMS / SURVEILLANCE EQUIPMENT \"\n" + 
			"  specification: \"The organization monitors physical intrusion alarms and surveillance equipment.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-6(2) { \n" + 
			"  name: \"MONITORING PHYSICAL ACCESS | AUTOMATED INTRUSION RECOGNITION / RESPONSES \"\n" + 
			"  specification: \"The organization employs automated mechanisms to recognize [Assignment: organization-defined classes/types of intrusions] and initiate [Assignment: organization-defined response actions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-6(3) { \n" + 
			"  name: \"MONITORING PHYSICAL ACCESS | VIDEO SURVEILLANCE \"\n" + 
			"  specification: \"The organization employs video surveillance of [Assignment: organization-defined operational areas] and retains video recordings for [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-6(4) { \n" + 
			"  name: \"MONITORING PHYSICAL ACCESS | MONITORING PHYSICAL ACCESS TO INFORMATION SYSTEMS \"\n" + 
			"  specification: \"The organization monitors physical access to the information system in addition to the physical access monitoring of the facility as [Assignment: organization-defined physical spaces containing one or more components of the information system].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-7 { \n" + 
			"  name: \"VISITOR CONTROL \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PE-2 and PE-3.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-8 { \n" + 
			"  name: \"VISITOR ACCESS RECORDS \"\n" + 
			"  specification: \"The organization: a. Maintains visitor access records to the facility where the information system resides for [Assignment: organization-defined time period]; and b. Reviews visitor access records [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-8(1) { \n" + 
			"  name: \"VISITOR ACCESS RECORDS | AUTOMATED RECORDS MAINTENANCE / REVIEW \"\n" + 
			"  specification: \"The organization employs automated mechanisms to facilitate the maintenance and review of visitor access records.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-8(2) { \n" + 
			"  name: \"VISITOR ACCESS RECORDS | PHYSICAL ACCESS RECORDS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PE-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-9 { \n" + 
			"  name: \"POWER EQUIPMENT AND CABLING \"\n" + 
			"  specification: \"The organization protects power equipment and power cabling for the information system from damage and destruction.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-9(1) { \n" + 
			"  name: \"POWER EQUIPMENT AND CABLING | REDUNDANT CABLING \"\n" + 
			"  specification: \"The organization employs redundant power cabling paths that are physically separated by [Assignment: organization-defined distance].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-9(2) { \n" + 
			"  name: \"POWER EQUIPMENT AND CABLING | AUTOMATIC VOLTAGE CONTROLS \"\n" + 
			"  specification: \"The organization employs automatic voltage controls for [Assignment: organization-defined critical information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-10 { \n" + 
			"  name: \"EMERGENCY SHUTOFF \"\n" + 
			"  specification: \"The organization: a. Provides the capability of shutting off power to the information system or individual system components in emergency situations; b. Places emergency shutoff switches or devices in [Assignment: organization-defined location by information system or system component] to facilitate safe and easy access for personnel; and c. Protects emergency power shutoff capability from unauthorized activation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-10(1) { \n" + 
			"  name: \"EMERGENCY SHUTOFF | ACCIDENTAL / UNAUTHORIZED ACTIVATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PE-10.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-11 { \n" + 
			"  name: \"EMERGENCY POWER \"\n" + 
			"  specification: \"The organization provides a short-term uninterruptible power supply to facilitate [Selection (one or more): an orderly shutdown of the information system; transition of the information system to long-term alternate power] in the event of a primary power source loss.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-11(1) { \n" + 
			"  name: \"EMERGENCY POWER | LONG-TERM ALTERNATE POWER SUPPLY - MINIMAL OPERATIONAL CAPABILITY \"\n" + 
			"  specification: \"The organization provides a long-term alternate power supply for the information system that is capable of maintaining minimally required operational capability in the event of an extended loss of the primary power source.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-11(2) { \n" + 
			"  name: \"EMERGENCY POWER | LONG-TERM ALTERNATE POWER SUPPLY - SELF-CONTAINED \"\n" + 
			"  specification: \"The organization provides a long-term alternate power supply for the information system that is:(a) Self-contained;(b) Not reliant on external power generation; and (c) Capable of maintaining [Selection: minimally required operational capability; full operational capability] in the event of an extended loss of the primary power source.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-12 { \n" + 
			"  name: \"EMERGENCY LIGHTING \"\n" + 
			"  specification: \"The organization employs and maintains automatic emergency lighting for the information system that activates in the event of a power outage or disruption and that covers emergency exits and evacuation routes within the facility.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-12(1) { \n" + 
			"  name: \"EMERGENCY LIGHTING | ESSENTIAL MISSIONS / BUSINESS FUNCTIONS \"\n" + 
			"  specification: \"The organization provides emergency lighting for all areas within the facility supporting essential missions and business functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-13 { \n" + 
			"  name: \"FIRE PROTECTION \"\n" + 
			"  specification: \"The organization employs and maintains fire suppression and detection devices/systems for the information system that are supported by an independent energy source.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-13(1) { \n" + 
			"  name: \"FIRE PROTECTION | DETECTION DEVICES / SYSTEMS \"\n" + 
			"  specification: \"The organization employs fire detection devices/systems for the information system that activate automatically and notify [Assignment: organization-defined personnel or roles] and [Assignment: organization-defined emergency responders] in the event of a fire.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-13(2) { \n" + 
			"  name: \"FIRE PROTECTION | SUPPRESSION DEVICES / SYSTEMS \"\n" + 
			"  specification: \"The organization employs fire suppression devices/systems for the information system that provide automatic notification of any activation to Assignment: organization-defined personnel or roles] and [Assignment: organization-defined emergency responders].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-13(3) { \n" + 
			"  name: \"FIRE PROTECTION | AUTOMATIC FIRE SUPPRESSION \"\n" + 
			"  specification: \"The organization employs an automatic fire suppression capability for the information system when the facility is not staffed on a continuous basis.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-13(4) { \n" + 
			"  name: \"FIRE PROTECTION | INSPECTIONS \"\n" + 
			"  specification: \"The organization ensures that the facility undergoes [Assignment: organization-defined frequency] inspections by authorized and qualified inspectors and resolves identified deficiencies within [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-14 { \n" + 
			"  name: \"TEMPERATURE AND HUMIDITY CONTROLS \"\n" + 
			"  specification: \"The organization:a. Maintains temperature and humidity levels within the facility where the information system resides at [Assignment: organization-defined acceptable levels]; and b. Monitors temperature and humidity levels [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-14(1) { \n" + 
			"  name: \"TEMPERATURE AND HUMIDITY CONTROLS | AUTOMATIC CONTROLS \"\n" + 
			"  specification: \"The organization employs automatic temperature and humidity controls in the facility to prevent fluctuations potentially harmful to the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-14(2) { \n" + 
			"  name: \"TEMPERATURE AND HUMIDITY CONTROLS | MONITORING WITH ALARMS / NOTIFICATIONS \"\n" + 
			"  specification: \"The organization employs temperature and humidity monitoring that provides an alarm or notification of changes potentially harmful to personnel or equipment.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-15 { \n" + 
			"  name: \"WATER DAMAGE PROTECTION \"\n" + 
			"  specification: \"The organization protects the information system from damage resulting from water leakage by providing master shutoff or isolation valves that are accessible, working properly, and known to key personnel.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-15(1) { \n" + 
			"  name: \"WATER DAMAGE PROTECTION | AUTOMATION SUPPORT \"\n" + 
			"  specification: \"The organization employs automated mechanisms to detect the presence of water in the vicinity of the information system and alerts [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-16 { \n" + 
			"  name: \"DELIVERY AND REMOVAL \"\n" + 
			"  specification: \"The organization authorizes, monitors, and controls [Assignment: organization-defined types of information system components] entering and exiting the facility and maintains records of those items.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-17 { \n" + 
			"  name: \"ALTERNATE WORK SITE \"\n" + 
			"  specification: \"The organization: a. Employs [Assignment: organization-defined security controls] at alternate work sites; b. Assesses as feasible, the effectiveness of security controls at alternate work sites; and c. Provides a means for employees to communicate with information security personnel in case of security incidents or problems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-18 { \n" + 
			"  name: \"LOCATION OF INFORMATION SYSTEM COMPONENTS \"\n" + 
			"  specification: \"The organization positions information system components within the facility to minimize potential damage from [Assignment: organization-defined physical and environmental hazards] and to minimize the opportunity for unauthorized access.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-18(1) { \n" + 
			"  name: \"LOCATION OF INFORMATION SYSTEM COMPONENTS | FACILITY SITE \"\n" + 
			"  specification: \"The organization plans the location or site of the facility where the information system resides with regard to physical and environmental hazards and for existing facilities, considers the physical and environmental hazards in its risk mitigation strategy.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-19 { \n" + 
			"  name: \"INFORMATION LEAKAGE \"\n" + 
			"  specification: \"The organization protects the information system from information leakage due to electromagnetic signals emanations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-19(1) { \n" + 
			"  name: \"INFORMATION LEAKAGE | NATIONAL EMISSIONS / TEMPEST POLICIES AND PROCEDURES \"\n" + 
			"  specification: \"The organization ensures that information system components, associated data communications, and networks are protected in accordance with national emissions and TEMPEST policies and procedures based on the security category or classification of the information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PE-20 { \n" + 
			"  name: \"ASSET MONITORING AND TRACKING \"\n" + 
			"  specification: \"The organization: a. Employs [Assignment: organization-defined asset location technologies] to track and monitor the location and movement of [Assignment: organization-defined assets] within [Assignment: organization-defined controlled areas]; and b. Ensures that asset location technologies are employed in accordance with applicable federal laws, Executive Orders, directives, regulations, policies, standards, and guidance.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PE \n" + 
			"  } \n" + 
			"  security control PL-1 { \n" + 
			"  name: \"SECURITY PLANNING POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A security planning policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the security planning policy and associated security planning controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Security planning policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Security planning procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-2 { \n" + 
			"  name: \"SYSTEM SECURITY PLAN \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops a security plan for the information system that:\n" + 
			"				1. Is consistent with the organization’s enterprise architecture;\n" + 
			"				2. Explicitly defines the authorization boundary for the system;\n" + 
			"				3. Describes the operational context of the information system in terms of missions and business processes;\n" + 
			"				4. Provides the security categorization of the information system including supporting rationale;\n" + 
			"				5. Describes the operational environment for the information system and relationships with or connections to other information systems;\n" + 
			"				6. Provides an overview of the security requirements for the system;\n" + 
			"				7. Identifies any relevant overlays, if applicable;\n" + 
			"				8. Describes the security controls in place or planned for meeting those requirements including a rationale for the tailoring decisions; and\n" + 
			"				9. Is reviewed and approved by the authorizing official or designated representative prior to plan implementation;\n" + 
			"			b. Distributes copies of the security plan and communicates subsequent changes to the plan to [Assignment: organization-defined personnel or roles];\n" + 
			"			c. Reviews the security plan for the information system [Assignment: organization-defined frequency];\n" + 
			"			d. Updates the plan to address changes to the information system/environment of operation or problems identified during plan implementation or security control assessments; and\n" + 
			"			e. Protects the security plan from unauthorized disclosure and modification.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-2(1) { \n" + 
			"  name: \"SYSTEM SECURITY PLAN | CONCEPT OF OPERATIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PL-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-2(2) { \n" + 
			"  name: \"SYSTEM SECURITY PLAN | FUNCTIONAL ARCHITECTURE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PL-8.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-2(3) { \n" + 
			"  name: \"SYSTEM SECURITY PLAN | PLAN / COORDINATE WITH OTHER ORGANIZATIONAL ENTITIES \"\n" + 
			"  specification: \"The organization plans and coordinates security-related activities affecting the information system with [Assignment: organization-defined individuals or groups] before conducting such activities in order to reduce the impact on other organizational entities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-3 { \n" + 
			"  name: \"SYSTEM SECURITY PLAN UPDATE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PL-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-4 { \n" + 
			"  name: \"RULES OF BEHAVIOR \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Establishes and makes readily available to individuals requiring access to the information system, the rules that describe their responsibilities and expected behavior with regard to information and information system usage;\n" + 
			"			b. Receives a signed acknowledgment from such individuals, indicating that they have read, understand, and agree to abide by the rules of behavior, before authorizing access to information and the information system;\n" + 
			"			c. Reviews and updates the rules of behavior [Assignment: organization-defined frequency]; and\n" + 
			"			d. Requires individuals who have signed a previous version of the rules of behavior to read and re-sign when the rules of behavior are revised/updated.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-4(1) { \n" + 
			"  name: \"RULES OF BEHAVIOR | SOCIAL MEDIA AND NETWORKING RESTRICTIONS \"\n" + 
			"  specification: \"The organization includes in the rules of behavior, explicit restrictions on the use of social media/networking sites and posting organizational information on public websites.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-5 { \n" + 
			"  name: \"PRIVACY IMPACT ASSESSMENT \"\n" + 
			"  specification: \"Withdrawn: Incorporated into Appendix J, AR-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-6 { \n" + 
			"  name: \"SECURITY-RELATED ACTIVITY PLANNING \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PL-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-7 { \n" + 
			"  name: \"SECURITY CONCEPT OF OPERATIONS \"\n" + 
			"  specification: \"The organization:a. Develops a security Concept of Operations (CONOPS) for the information system containing at a minimum, how the organization intends to operate the system from the perspective of information security; and b. Reviews and updates the CONOPS [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-8 { \n" + 
			"  name: \"INFORMATION SECURITY ARCHITECTURE \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops an information security architecture for the information system that:\n" + 
			"				1. Describes the overall philosophy, requirements, and approach to be taken with regard to protecting the confidentiality, integrity, and availability of organizational information;\n" + 
			"				2. Describes how the information security architecture is integrated into and supports the enterprise architecture; and\n" + 
			"				3. Describes any information security assumptions about, and dependencies on, external services;\n" + 
			"			b. Reviews and updates the information security architecture [Assignment: organization-defined frequency] to reflect updates in the enterprise architecture; and\n" + 
			"			c. Ensures that planned information security architecture changes are reflected in the security plan, the security Concept of Operations (CONOPS), and organizational procurements/acquisitions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-8(1) { \n" + 
			"  name: \"INFORMATION SECURITY ARCHITECTURE | DEFENSE-IN-DEPTH \"\n" + 
			"  specification: \"The organization designs its security architecture using a defense-in-depth approach that: (a) Allocates [Assignment: organization-defined security safeguards] to [Assignment: organization-defined locations and architectural layers]; and (b) Ensures that the allocated security safeguards operate in a coordinated and mutually reinforcing manner.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-8(2) { \n" + 
			"  name: \"INFORMATION SECURITY ARCHITECTURE | SUPPLIER DIVERSITY \"\n" + 
			"  specification: \"The organization requires that [Assignment: organization-defined security safeguards] allocated to [Assignment: organization-defined locations and architectural layers] are obtained from different suppliers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PL-9 { \n" + 
			"  name: \"CENTRAL MANAGEMENT \"\n" + 
			"  specification: \"The organization centrally manages [Assignment: organization-defined security controls and related processes].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PL \n" + 
			"  } \n" + 
			"  security control PS-1 { \n" + 
			"  name: \"PERSONNEL SECURITY POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A personnel security policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the personnel security policy and associated personnel security controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Personnel security policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Personnel security procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-2 { \n" + 
			"  name: \"POSITION RISK DESIGNATION \"\n" + 
			"  specification: \"The organization: a. Assigns a risk designation to all organizational positions; b. Establishes screening criteria for individuals filling those positions; and c. Reviews and updates position risk designations [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-3 { \n" + 
			"  name: \"PERSONNEL SCREENING \"\n" + 
			"  specification: \"The organization: a. Screens individuals prior to authorizing access to the information system; and b. Rescreens individuals according to [Assignment: organization-defined conditions requiring rescreening and, where rescreening is so indicated, the frequency of such rescreening].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-3(1) { \n" + 
			"  name: \"PERSONNEL SCREENING | CLASSIFIED INFORMATION \"\n" + 
			"  specification: \"The organization ensures that individuals accessing an information system processing, storing, or transmitting classified information are cleared and indoctrinated to the highest classification level of the information to which they have access on the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-3(2) { \n" + 
			"  name: \"PERSONNEL SCREENING | FORMAL INDOCTRINATION \"\n" + 
			"  specification: \"The organization ensures that individuals accessing an information system processing, storing, or transmitting types of classified information which require formal indoctrination, are formally indoctrinated for all of the relevant types of information to which they have access on the system\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-3(3) { \n" + 
			"  name: \"PERSONNEL SCREENING | INFORMATION WITH SPECIAL PROTECTION MEASURES \"\n" + 
			"  specification: \"The organization ensures that individuals accessing an information system processing, storing, or transmitting information requiring special protection: (a) Have valid access authorizations that are demonstrated by assigned official government duties; and (b) Satisfy [Assignment: organization-defined additional personnel screening criteria].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-4 { \n" + 
			"  name: \"PERSONNEL TERMINATION \"\n" + 
			"  specification: \"The organization, upon termination of individual employment:\n" + 
			"			a. Disables information system access within [Assignment: organization-defined time period];\n" + 
			"			b. Terminates/revokes any authenticators/credentials associated with the individual;\n" + 
			"			c. Conducts exit interviews that include a discussion of [Assignment: organization-defined information security topics];\n" + 
			"			d. Retrieves all security-related organizational information system-related property;\n" + 
			"			e. Retains access to organizational information and information systems formerly controlled by terminated individual; and\n" + 
			"			f. Notifies [Assignment: organization-defined personnel or roles] within [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-4(1) { \n" + 
			"  name: \"PERSONNEL TERMINATION | POST-EMPLOYMENT REQUIREMENTS \"\n" + 
			"  specification: \"The organization: (a) Notifies terminated individuals of applicable, legally binding post-employment requirements for the protection of organizational information; and (b) Requires terminated individuals to sign an acknowledgment of post-employment requirements as part of the organizational termination process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-4(2) { \n" + 
			"  name: \"PERSONNEL TERMINATION | AUTOMATED NOTIFICATION \"\n" + 
			"  specification: \"The organization employs automated mechanisms to notify [Assignment: organization-defined personnel or roles] upon termination of an individual.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-5 { \n" + 
			"  name: \"PERSONNEL TRANSFER \"\n" + 
			"  specification: \"The organization: a. Reviews and confirms ongoing operational need for current logical and physical access authorizations to information systems/facilities when individuals are reassigned or transferred to other positions within the organization; b. Initiates [Assignment: organization-defined transfer or reassignment actions] within [Assignment: organization-defined time period following the formal transfer action]; c. Modifies access authorization as needed to correspond with any changes in operational need due to reassignment or transfer; and d. Notifies [Assignment: organization-defined personnel or roles] within [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-6 { \n" + 
			"  name: \"ACCESS AGREEMENTS \"\n" + 
			"  specification: \"The organization: \n" + 
			"			a. Develops and documents access agreements for organizational information systems;\n" + 
			"			b. Reviews and updates the access agreements [Assignment: organization-defined frequency]; and\n" + 
			"			c. Ensures that individuals requiring access to organizational information and information systems:\n" + 
			"				1. Sign appropriate access agreements prior to being granted access; and\n" + 
			"				2. Re-sign access agreements to maintain access to organizational information systems when access agreements have been updated or [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-6(1) { \n" + 
			"  name: \"ACCESS AGREEMENTS | INFORMATION REQUIRING SPECIAL PROTECTION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into PS-3.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-6(2) { \n" + 
			"  name: \"ACCESS AGREEMENTS | CLASSIFIED INFORMATION REQUIRING SPECIAL PROTECTION \"\n" + 
			"  specification: \"The organization ensures that access to classified information requiring special protection is granted only to individuals who: (a) Have a valid access authorization that is demonstrated by assigned official government duties; (b) Satisfy associated personnel security criteria; and (c) Have read, understood, and signed a nondisclosure agreement.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-6(3) { \n" + 
			"  name: \"ACCESS AGREEMENTS | POST-EMPLOYMENT REQUIREMENTS \"\n" + 
			"  specification: \"The organization:(a) Notifies individuals of applicable, legally binding post-employment requirements for protection of organizational information; and (b) Requires individuals to sign an acknowledgment of these requirements, if applicable, as part of granting initial access to covered information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-7 { \n" + 
			"  name: \"THIRD-PARTY PERSONNEL SECURITY \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Establishes personnel security requirements including security roles and responsibilities for third-party providers;\n" + 
			"			b. Requires third-party providers to comply with personnel security policies and procedures established by the organization;\n" + 
			"			c. Documents personnel security requirements;\n" + 
			"			d. Requires third-party providers to notify [Assignment: organization-defined personnel or roles] of any personnel transfers or terminations of third-party personnel who possess organizational credentials and/or badges, or who have information system privileges within [Assignment: organization-defined time period]; and\n" + 
			"			e. Monitors provider compliance.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control PS-8 { \n" + 
			"  name: \"PERSONNEL SANCTIONS \"\n" + 
			"  specification: \"The organization: a. Employs a formal sanctions process for individuals failing to comply with established information security policies and procedures; and b. Notifies [Assignment: organization-defined personnel or roles] within [Assignment: organization-defined time period] when a formal employee sanctions process is initiated, identifying the individual sanctioned and the reason for the sanction.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.PS \n" + 
			"  } \n" + 
			"  security control RA-1 { \n" + 
			"  name: \"RISK ASSESSMENT POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A risk assessment policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the risk assessment policy and associated risk assessment controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. Risk assessment policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. Risk assessment procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-2 { \n" + 
			"  name: \"SECURITY CATEGORIZATION \"\n" + 
			"  specification: \"The organization: a. Categorizes information and the information system in accordance with applicable federal laws, Executive Orders, directives, policies, regulations, standards, and guidance; b. Documents the security categorization results (including supporting rationale) in the security plan for the information system; and c. Ensures that the authorizing official or authorizing official designated representative reviews and approves the security categorization decision.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-3 { \n" + 
			"  name: \"RISK ASSESSMENT \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Conducts an assessment of risk, including the likelihood and magnitude of harm, from the unauthorized access, use, disclosure, disruption, modification, or destruction of the information system and the information it processes, stores, or transmits;\n" + 
			"			b. Documents risk assessment results in [Selection: security plan; risk assessment report; [Assignment: organization-defined document]];\n" + 
			"			c. Reviews risk assessment results [Assignment: organization-defined frequency];\n" + 
			"			d. Disseminates risk assessment results to [Assignment: organization-defined personnel or roles]; and\n" + 
			"			e. Updates the risk assessment [Assignment: organization-defined frequency] or whenever there are significant changes to the information system or environment of operation (including the identification of new threats and vulnerabilities), or other conditions that may impact the security state of the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-4 { \n" + 
			"  name: \"RISK ASSESSMENT UPDATE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into RA-3.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5 { \n" + 
			"  name: \"VULNERABILITY SCANNING \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Scans for vulnerabilities in the information system and hosted applications [Assignment: organization-defined frequency and/or randomly in accordance with organization-defined process] and when new vulnerabilities potentially affecting the system/applications are identified and reported;\n" + 
			"			b. Employs vulnerability scanning tools and techniques that facilitate interoperability among tools and automate parts of the vulnerability management process by using standards for:\n" + 
			"				1. Enumerating platforms, software flaws, and improper configurations;\n" + 
			"				2. Formatting checklists and test procedures; and\n" + 
			"				3. Measuring vulnerability impact;\n" + 
			"			c. Analyzes vulnerability scan reports and results from security control assessments;\n" + 
			"			d. Remediates legitimate vulnerabilities [Assignment: organization-defined response times] in accordance with an organizational assessment of risk; and\n" + 
			"			e. Shares information obtained from the vulnerability scanning process and security control assessments with [Assignment: organization-defined personnel or roles] to help eliminate similar vulnerabilities in other information systems (i.e., systemic weaknesses or deficiencies).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(1) { \n" + 
			"  name: \"VULNERABILITY SCANNING | UPDATE TOOL CAPABILITY \"\n" + 
			"  specification: \"The organization employs vulnerability scanning tools that include the capability to readily update the information system vulnerabilities to be scanned.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(2) { \n" + 
			"  name: \"VULNERABILITY SCANNING | UPDATE BY FREQUENCY / PRIOR TO NEW SCAN / WHEN IDENTIFIED \"\n" + 
			"  specification: \"The organization updates the information system vulnerabilities scanned [Selection (one or more): [Assignment: organization-defined frequency]; prior to a new scan; when new vulnerabilities are identified and reported].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(3) { \n" + 
			"  name: \"VULNERABILITY SCANNING | BREADTH / DEPTH OF COVERAGE \"\n" + 
			"  specification: \"The organization employs vulnerability scanning procedures that can identify the breadth and depth of coverage (i.e., information system components scanned and vulnerabilities checked).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(4) { \n" + 
			"  name: \"VULNERABILITY SCANNING | DISCOVERABLE INFORMATION \"\n" + 
			"  specification: \"The organization determines what information about the information system is discoverable by adversaries and subsequently takes [Assignment: organization-defined corrective actions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(5) { \n" + 
			"  name: \"VULNERABILITY SCANNING | PRIVILEGED ACCESS \"\n" + 
			"  specification: \"The information system implements privileged access authorization to [Assignment: organization-identified information system components] for selected [Assignment: organization-defined vulnerability scanning activities].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(6) { \n" + 
			"  name: \"VULNERABILITY SCANNING | AUTOMATED TREND ANALYSES \"\n" + 
			"  specification: \"The organization employs automated mechanisms to compare the results of vulnerability scans over time to determine trends in information system vulnerabilities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(7) { \n" + 
			"  name: \"VULNERABILITY SCANNING | AUTOMATED DETECTION AND NOTIFICATION OF UNAUTHORIZED COMPONENTS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-8.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(8) { \n" + 
			"  name: \"VULNERABILITY SCANNING | REVIEW HISTORIC AUDIT LOGS \"\n" + 
			"  specification: \"The organization reviews historic audit logs to determine if a vulnerability identified in the information system has been previously exploited.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(9) { \n" + 
			"  name: \"VULNERABILITY SCANNING | PENETRATION TESTING AND ANALYSES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CA-8.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-5(10) { \n" + 
			"  name: \"VULNERABILITY SCANNING | CORRELATE SCANNING INFORMATION \"\n" + 
			"  specification: \"The organization correlates the output from vulnerability scanning tools to determine the presence of multi-vulnerability/multi-hop attack vectors.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control RA-6 { \n" + 
			"  name: \"TECHNICAL SURVEILLANCE COUNTERMEASURES SURVEY \"\n" + 
			"  specification: \"The organization employs a technical surveillance countermeasures survey at [Assignment: organization-defined locations] [Selection (one or more): [Assignment: organization-defined frequency]; [Assignment: organization-defined events or indicators occur]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.RA \n" + 
			"  } \n" + 
			"  security control SA-1 { \n" + 
			"  name: \"SYSTEM AND SERVICES ACQUISITION POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A system and services acquisition policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the system and services acquisition policy and associated system and services acquisition controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. System and services acquisition policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. System and services acquisition procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-2 { \n" + 
			"  name: \"ALLOCATION OF RESOURCES \"\n" + 
			"  specification: \"The organization: a. Determines information security requirements for the information system or information system service in mission/business process planning; b. Determines, documents, and allocates the resources required to protect the information system or information system service as part of its capital planning and investment control process; and c. Establishes a discrete line item for information security in organizational programming and budgeting documentation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-3 { \n" + 
			"  name: \"SYSTEM DEVELOPMENT LIFE CYCLE \"\n" + 
			"  specification: \"The organization: a. Manages the information system using [Assignment: organization-defined system development life cycle] that incorporates information security considerations; b. Defines and documents information security roles and responsibilities throughout the system development life cycle; c. Identifies individuals having information security roles and responsibilities; and d. Integrates the organizational information security risk management process into system development life cycle activities.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4 { \n" + 
			"  name: \"ACQUISITION PROCESS \"\n" + 
			"  specification: \"The organization includes the following requirements, descriptions, and criteria, explicitly or by reference, in the acquisition contract for the information system, system component, or information system service in accordance with applicable federal laws, Executive Orders, directives, policies, regulations, standards, guidelines, and organizational mission/business needs:a. Security functional requirements; b. Security strength requirements; c. Security assurance requirements; d. Security-related documentation requirements; e. Requirements for protecting security-related documentation; f. Description of the information system development environment and environment in which the system is intended to operate; and g. Acceptance criteria.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(1) { \n" + 
			"  name: \"ACQUISITION PROCESS | FUNCTIONAL PROPERTIES OF SECURITY CONTROLS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to provide a description of the functional properties of the security controls to be employed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(2) { \n" + 
			"  name: \"ACQUISITION PROCESS | DESIGN / IMPLEMENTATION INFORMATION FOR SECURITY CONTROLS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to provide design and implementation information for the security controls to be employed that includes: [Selection (one or more): security-relevant external system interfaces; high-level design; low-level design; source code or hardware schematics; [Assignment: organization-defined design/implementation information]] at [Assignment: organization-defined level of detail].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(3) { \n" + 
			"  name: \"ACQUISITION PROCESS | DEVELOPMENT METHODS / TECHNIQUES / PRACTICES \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to demonstrate the use of a system development life cycle that includes [Assignment: organization-defined state-of-the-practice system/security engineering methods, software development methods, testing/evaluation/validation techniques, and quality control processes].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(4) { \n" + 
			"  name: \"ACQUISITION PROCESS | ASSIGNMENT OF COMPONENTS TO SYSTEMS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-8 (9).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(5) { \n" + 
			"  name: \"ACQUISITION PROCESS | SYSTEM / COMPONENT / SERVICE CONFIGURATIONS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to: (a) Deliver the system, component, or service with [Assignment: organization-defined security configurations] implemented; and (b) Use the configurations as the default for any subsequent system, component, or service reinstallation or upgrade.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(6) { \n" + 
			"  name: \"ACQUISITION PROCESS | USE OF INFORMATION ASSURANCE PRODUCTS \"\n" + 
			"  specification: \"The organization: (a) Employs only government off-the-shelf (GOTS) or commercial off-the-shelf (COTS) information assurance (IA) and IA-enabled information technology products that compose an NSA-approved solution to protect classified information when the networks used to transmit the information are at a lower classification level than the information being transmitted; and (b) Ensures that these products have been evaluated and/or validated by NSA or in accordance with NSA-approved procedures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(7) { \n" + 
			"  name: \"ACQUISITION PROCESS | NIAP-APPROVED PROTECTION PROFILES \"\n" + 
			"  specification: \"The organization: (a) Limits the use of commercially provided information assurance (IA) and IA-enabled information technology products to those products that have been successfully evaluated against a National Information Assurance partnership (NIAP)-approved Protection Profile for a specific technology type, if such a profile exists; and (b) Requires, if no NIAP-approved Protection Profile exists for a specific technology type but a commercially provided information technology product relies on cryptographic functionality to enforce its security policy, that the cryptographic module is FIPS-validated.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(8) { \n" + 
			"  name: \"ACQUISITION PROCESS | CONTINUOUS MONITORING PLAN \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to produce a plan for the continuous monitoring of security control effectiveness that contains [Assignment: organization-defined level of detail].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(9) { \n" + 
			"  name: \"ACQUISITION PROCESS | FUNCTIONS / PORTS / PROTOCOLS / SERVICES IN USE \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to identify early in the system development life cycle, the functions, ports, protocols, and services intended for organizational use.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-4(10) { \n" + 
			"  name: \"ACQUISITION PROCESS | USE OF APPROVED PIV PRODUCTS \"\n" + 
			"  specification: \"The organization employs only information technology products on the FIPS 201-approved products list for Personal Identity Verification (PIV) capability implemented within organizational information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-5 { \n" + 
			"  name: \"INFORMATION SYSTEM DOCUMENTATION \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Obtains administrator documentation for the information system, system component, or information system service that describes:\n" + 
			"				1. Secure configuration, installation, and operation of the system, component, or service;\n" + 
			"				2. Effective use and maintenance of security functions/mechanisms; and\n" + 
			"				3. Known vulnerabilities regarding configuration and use of administrative (i.e., privileged) functions;\n" + 
			"			b. Obtains user documentation for the information system, system component, or information system service that describes:\n" + 
			"				1. User-accessible security functions/mechanisms and how to effectively use those security functions/mechanisms;\n" + 
			"				2. Methods for user interaction, which enables individuals to use the system, component, or service in a more secure manner; and\n" + 
			"				3. User responsibilities in maintaining the security of the system, component, or service;\n" + 
			"			c. Documents attempts to obtain information system, system component, or information system service documentation when such documentation is either unavailable or nonexistent and takes [Assignment: organization-defined actions] in response;\n" + 
			"			d. Protects documentation as required, in accordance with the risk management strategy; and\n" + 
			"			e. Distributes documentation to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-5(1) { \n" + 
			"  name: \"INFORMATION SYSTEM DOCUMENTATION | FUNCTIONAL PROPERTIES OF SECURITY CONTROLS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-4 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-5(2) { \n" + 
			"  name: \"INFORMATION SYSTEM DOCUMENTATION | SECURITY-RELEVANT EXTERNAL SYSTEM INTERFACES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-4 (2).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-5(3) { \n" + 
			"  name: \"INFORMATION SYSTEM DOCUMENTATION | HIGH-LEVEL DESIGN \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-4 (2).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-5(4) { \n" + 
			"  name: \"INFORMATION SYSTEM DOCUMENTATION | LOW-LEVEL DESIGN \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-4 (2).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-5(5) { \n" + 
			"  name: \"INFORMATION SYSTEM DOCUMENTATION | SOURCE CODE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-4 (2).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-6 { \n" + 
			"  name: \"SOFTWARE USAGE RESTRICTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-10 and SI-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-7 { \n" + 
			"  name: \"USER-INSTALLED SOFTWARE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into CM-11 and SI-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-8 { \n" + 
			"  name: \"SECURITY ENGINEERING PRINCIPLES \"\n" + 
			"  specification: \"The organization applies information system security engineering principles in the specification, design, development, implementation, and modification of the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-9 { \n" + 
			"  name: \"EXTERNAL INFORMATION SYSTEM SERVICES \"\n" + 
			"  specification: \"The organization: a. Requires that providers of external information system services comply with organizational information security requirements and employ [Assignment: organization-defined security controls] in accordance with applicable federal laws, Executive Orders, directives, policies, regulations, standards, and guidance; b. Defines and documents government oversight and user roles and responsibilities with regard to external information system services; and c. Employs [Assignment: organization-defined processes, methods, and techniques] to monitor security control compliance by external service providers on an ongoing basis.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-9(1) { \n" + 
			"  name: \"EXTERNAL INFORMATION SYSTEMS | RISK ASSESSMENTS / ORGANIZATIONAL APPROVALS \"\n" + 
			"  specification: \"The organization: (a) Conducts an organizational assessment of risk prior to the acquisition or outsourcing of dedicated information security services; and (b) Ensures that the acquisition or outsourcing of dedicated information security services is approved by [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-9(2) { \n" + 
			"  name: \"EXTERNAL INFORMATION SYSTEMS | IDENTIFICATION OF FUNCTIONS / PORTS / PROTOCOLS / SERVICES \"\n" + 
			"  specification: \"The organization requires providers of [Assignment: organization-defined external information system services] to identify the functions, ports, protocols, and other services required for the use of such services.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-9(3) { \n" + 
			"  name: \"EXTERNAL INFORMATION SYSTEMS | ESTABLISH / MAINTAIN TRUST RELATIONSHIP WITH PROVIDERS \"\n" + 
			"  specification: \"The organization establishes, documents, and maintains trust relationships with external service providers based on [Assignment: organization-defined security requirements, properties, factors, or conditions defining acceptable trust relationships].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-9(4) { \n" + 
			"  name: \"EXTERNAL INFORMATION SYSTEMS | CONSISTENT INTERESTS OF CONSUMERS AND PROVIDERS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to ensure that the interests of [Assignment: organization-defined external service providers] are consistent with and reflect organizational interests.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-9(5) { \n" + 
			"  name: \"EXTERNAL INFORMATION SYSTEMS | PROCESSING, STORAGE, AND SERVICE LOCATION \"\n" + 
			"  specification: \"The organization restricts the location of [Selection (one or more): information processing; information/data; information system services] to [Assignment: organization-defined locations] based on [Assignment: organization-defined requirements or conditions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10 { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to:\n" + 
			"			a. Perform configuration management during system, component, or service [Selection (one or more): design; development; implementation; operation];\n" + 
			"			b. Document, manage, and control the integrity of changes to [Assignment: organization-defined configuration items under configuration management];\n" + 
			"			c. Implement only organization-approved changes to the system, component, or service;\n" + 
			"			d. Document approved changes to the system, component, or service and the potential security impacts of such changes; and\n" + 
			"			e. Track security flaws and flaw resolution within the system, component, or service and report findings to [Assignment: organization-defined personnel].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10(1) { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT | SOFTWARE / FIRMWARE INTEGRITY VERIFICATION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to enable integrity verification of software and firmware components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10(2) { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT | ALTERNATIVE CONFIGURATION MANAGEMENT PROCESSES \"\n" + 
			"  specification: \"The organization provides an alternate configuration management process using organizational personnel in the absence of a dedicated developer configuration management team.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10(3) { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT | HARDWARE INTEGRITY VERIFICATION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to enable integrity verification of hardware components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10(4) { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT | TRUSTED GENERATION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to employ tools for comparing newly generated versions of security-relevant hardware descriptions and software/firmware source and object code with previous versions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10(5) { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT | MAPPING INTEGRITY FOR VERSION CONTROL \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to maintain the integrity of the mapping between the master build data (hardware drawings and software/firmware code) describing the current version of security-relevant hardware, software, and firmware and the on-site master copy of the data for the current version.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-10(6) { \n" + 
			"  name: \"DEVELOPER CONFIGURATION MANAGEMENT | TRUSTED DISTRIBUTION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to execute procedures for ensuring that security-relevant hardware, software, and firmware updates distributed to the organization are exactly as specified by the master copies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11 { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to:\n" + 
			"			a. Create and implement a security assessment plan;\n" + 
			"			b. Perform [Selection (one or more): unit; integration; system; regression] testing/evaluation at [Assignment: organization-defined depth and coverage];\n" + 
			"			c. Produce evidence of the execution of the security assessment plan and the results of the security testing/evaluation;\n" + 
			"			d. Implement a verifiable flaw remediation process; and\n" + 
			"			e. Correct flaws identified during security testing/evaluation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(1) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | STATIC CODE ANALYSIS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to employ static code analysis tools to identify common flaws and document the results of the analysis.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(2) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | THREAT AND VULNERABILITY ANALYSES \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to perform threat and vulnerability analyses and subsequent testing/evaluation of the as-built system, component, or service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(3) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | INDEPENDENT VERIFICATION OF ASSESSMENT PLANS / EVIDENCE \"\n" + 
			"  specification: \"The organization: (a) Requires an independent agent satisfying [Assignment: organization-defined independence criteria] to verify the correct implementation of the developer security assessment plan and the evidence produced during security testing/evaluation; and (b) Ensures that the independent agent is either provided with sufficient information to complete the verification process or granted the authority to obtain such information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(4) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | MANUAL CODE REVIEWS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to perform a manual code review of [Assignment: organization-defined specific code] using [Assignment: organization-defined processes, procedures, and/or techniques].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(5) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | PENETRATION TESTING \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to perform penetration testing at [Assignment: organization-defined breadth/depth] and with [Assignment: organization-defined constraints].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(6) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | ATTACK SURFACE REVIEWS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to perform attack surface reviews.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(7) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | VERIFY SCOPE OF TESTING / EVALUATION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to verify that the scope of security testing/evaluation provides complete coverage of required security controls at [Assignment: organization-defined depth of testing/evaluation].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-11(8) { \n" + 
			"  name: \"DEVELOPER SECURITY TESTING AND EVALUATION | DYNAMIC CODE ANALYSIS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to employ dynamic code analysis tools to identify common flaws and document the results of the analysis.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12 { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION \"\n" + 
			"  specification: \"The organization protects against supply chain threats to the information system, system component, or information system service by employing [Assignment: organization-defined security safeguards] as part of a comprehensive, defense-in-breadth information security strategy.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(1) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | ACQUISITION STRATEGIES / TOOLS / METHODS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined tailored acquisition strategies, contract tools, and procurement methods] for the purchase of the information system, system component, or information system service from suppliers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(2) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | SUPPLIER REVIEWS \"\n" + 
			"  specification: \"The organization conducts a supplier review prior to entering into a contractual agreement to acquire the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(3) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | TRUSTED SHIPPING AND WAREHOUSING \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-12 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(4) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | DIVERSITY OF SUPPLIERS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-12 (13).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(5) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | LIMITATION OF HARM \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to limit harm from potential adversaries identifying and targeting the organizational supply chain.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(6) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | MINIMIZING PROCUREMENT TIME \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-12 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(7) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | ASSESSMENTS PRIOR TO SELECTION / ACCEPTANCE / UPDATE \"\n" + 
			"  specification: \"The organization conducts an assessment of the information system, system component, or information system service prior to selection, acceptance, or update.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(8) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | USE OF ALL-SOURCE INTELLIGENCE \"\n" + 
			"  specification: \"The organization uses all-source intelligence analysis of suppliers and potential suppliers of the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(9) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | OPERATIONS SECURITY \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined Operations Security (OPSEC) safeguards] in accordance with classification guides to protect supply chain-related information for the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(10) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | VALIDATE AS GENUINE AND NOT ALTERED \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to validate that the information system or system component received is genuine and has not been altered.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(11) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | PENETRATION TESTING / ANALYSIS OF ELEMENTS, PROCESSES, AND ACTORS \"\n" + 
			"  specification: \"The organization employs [Selection (one or more): organizational analysis, independent third-party analysis, organizational penetration testing, independent third-party penetration testing] of [Assignment: organization-defined supply chain elements, processes, and actors] associated with the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(12) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | INTER-ORGANIZATIONAL AGREEMENTS \"\n" + 
			"  specification: \"The organization establishes inter-organizational agreements and procedures with entities involved in the supply chain for the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(13) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | CRITICAL INFORMATION SYSTEM COMPONENTS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to ensure an adequate supply of [Assignment: organization-defined critical information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(14) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | IDENTITY AND TRACEABILITY \"\n" + 
			"  specification: \"The organization establishes and retains unique identification of [Assignment: organization-defined supply chain elements, processes, and actors] for the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-12(15) { \n" + 
			"  name: \"SUPPLY CHAIN PROTECTION | PROCESSES TO ADDRESS WEAKNESSES OR DEFICIENCIES \"\n" + 
			"  specification: \"The organization establishes a process to address weaknesses or deficiencies in supply chain elements identified during independent or organizational assessments of such elements.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-13 { \n" + 
			"  name: \"TRUSTWORTHINESS \"\n" + 
			"  specification: \"The organization: a. Describes the trustworthiness required in the [Assignment: organization-defined information system, information system component, or information system service] supporting its critical missions/business functions; and b. Implements [Assignment: organization-defined assurance overlay] to achieve such trustworthiness.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-14 { \n" + 
			"  name: \"CRITICALITY ANALYSIS \"\n" + 
			"  specification: \"The organization identifies critical information system components and functions by performing a criticality analysis for [Assignment: organization-defined information systems, information system components, or information system services] at [Assignment: organization-defined decision points in the system development life cycle].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15 { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Requires the developer of the information system, system component, or information system service to follow a documented development process that:\n" + 
			"				1. Explicitly addresses security requirements;\n" + 
			"				2. Identifies the standards and tools used in the development process;\n" + 
			"				3. Documents the specific tool options and tool configurations used in the development process; and\n" + 
			"				4. Documents, manages, and ensures the integrity of changes to the process and/or tools used in development; and\n" + 
			"			b. Reviews the development process, standards, tools, and tool options/configurations [Assignment: organization-defined frequency] to determine if the process, standards, tools, and tool options/configurations selected and employed can satisfy [Assignment: organization-defined security requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(1) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | QUALITY METRICS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to: (a) Define quality metrics at the beginning of the development process; and (b) Provide evidence of meeting the quality metrics [Selection (one or more): [Assignment: organization-defined frequency]; [Assignment: organization-defined program review milestones]; upon delivery].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(2) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | SECURITY TRACKING TOOLS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to select and employ a security tracking tool for use during the development process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(3) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | CRITICALITY ANALYSIS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to perform a criticality analysis at [Assignment: organization-defined breadth/depth] and at [Assignment: organization-defined decision points in the system development life cycle].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(4) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | THREAT MODELING / VULNERABILITY ANALYSIS \"\n" + 
			"  specification: \"The organization requires that developers perform threat modeling and a vulnerability analysis for the information system at [Assignment: organization-defined breadth/depth] that: (a) Uses [Assignment: organization-defined information concerning impact, environment of operations, known or assumed threats, and acceptable risk levels]; (b) Employs [Assignment: organization-defined tools and methods]; and (c) Produces evidence that meets [Assignment: organization-defined acceptance criteria].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(5) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | ATTACK SURFACE REDUCTION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to reduce attack surfaces to [Assignment: organization-defined thresholds].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(6) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | CONTINUOUS IMPROVEMENT \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to implement an explicit process to continuously improve the development process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(7) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | AUTOMATED VULNERABILITY ANALYSIS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to: (a) Perform an automated vulnerability analysis using [Assignment: organization-defined tools]; (b) Determine the exploitation potential for discovered vulnerabilities; (c) Determine potential risk mitigations for delivered vulnerabilities; and (d) Deliver the outputs of the tools and results of the analysis to [Assignment: organization-defined personnel or roles].		\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(8) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | REUSE OF THREAT / VULNERABILITY INFORMATION \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to use threat modeling and vulnerability analyses from similar systems, components, or services to inform the current development process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(9) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | USE OF LIVE DATA \"\n" + 
			"  specification: \"The organization approves, documents, and controls the use of live data in development and test environments for the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(10) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | INCIDENT RESPONSE PLAN \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to provide an incident response plan.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-15(11) { \n" + 
			"  name: \"DEVELOPMENT PROCESS, STANDARDS, AND TOOLS | ARCHIVE INFORMATION SYSTEM / COMPONENT \"\n" + 
			"  specification: \"The organization requires the developer of the information system or system component to archive the system or component to be released or delivered together with the corresponding evidence supporting the final security review.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-16 { \n" + 
			"  name: \"DEVELOPER-PROVIDED TRAINING \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to provide [Assignment: organization-defined training] on the correct use and operation of the implemented security functions, controls, and/or mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17 { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to produce a design specification and security architecture that: a. Is consistent with and supportive of the organization’s security architecture which is established within and is an integrated part of the organization’s enterprise architecture; b. Accurately and completely describes the required security functionality, and the allocation of security controls among physical and logical components; and c. Expresses how individual security functions, mechanisms, and services work together to provide required security capabilities and a unified approach to protection.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(1) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | FORMAL POLICY MODEL \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to: (a) Produce, as an integral part of the development process, a formal policy model describing the [Assignment: organization-defined elements of organizational security policy] to be enforced; and (b) Prove that the formal policy model is internally consistent and sufficient to enforce the defined elements of the organizational security policy when implemented. \n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(2) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | SECURITY-RELEVANT COMPONENTS \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to: (a) Define security-relevant hardware, software, and firmware; and (b) Provide a rationale that the definition for security-relevant hardware, software, and firmware is complete.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(3) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | FORMAL CORRESPONDENCE \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to:\n" + 
			"			(a) Produce, as an integral part of the development process, a formal top-level specification that specifies the interfaces to security-relevant hardware, software, and firmware in terms of exceptions, error messages, and effects;\n" + 
			"			(b) Show via proof to the extent feasible with additional informal demonstration as necessary, that the formal top-level specification is consistent with the formal policy model;\n" + 
			"			(c) Show via informal demonstration, that the formal top-level specification completely covers the interfaces to security-relevant hardware, software, and firmware;\n" + 
			"			(d) Show that the formal top-level specification is an accurate description of the implemented security-relevant hardware, software, and firmware; and\n" + 
			"			(e) Describe the security-relevant hardware, software, and firmware mechanisms not addressed in the formal top-level specification but strictly internal to the security-relevant hardware, software, and firmware.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(4) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | INFORMAL CORRESPONDENCE \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to:(a) Produce, as an integral part of the development process, an informal descriptive top-level specification that specifies the interfaces to security-relevant hardware, software, and firmware in terms of exceptions, error messages, and effects;			(b) Show via [Selection: informal demonstration, convincing argument with formal methods as feasible] that the descriptive top-level specification is consistent with the formal policy model;			(c) Show via informal demonstration, that the descriptive top-level specification completely covers the interfaces to security-relevant hardware, software, and firmware;			(d) Show that the descriptive top-level specification is an accurate description of the interfaces to security-relevant hardware, software, and firmware; and			(e) Describe the security-relevant hardware, software, and firmware mechanisms not addressed in the descriptive top-level specification but strictly internal to the security-relevant hardware, software, and firmware.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(5) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | CONCEPTUALLY SIMPLE DESIGN \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to: (a) Design and structure the security-relevant hardware, software, and firmware to use a complete, conceptually simple protection mechanism with precisely defined semantics; and (b) Internally structure the security-relevant hardware, software, and firmware with specific regard for this mechanism.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(6) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | CONCEPTUALLY SIMPLE DESIGN \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to structure security-relevant hardware, software, and firmware to facilitate testing.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-17(7) { \n" + 
			"  name: \"DEVELOPER SECURITY ARCHITECTURE AND DESIGN | CONCEPTUALLY SIMPLE DESIGN \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service to structure security-relevant hardware, software, and firmware to facilitate controlling access with least privilege.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-18 { \n" + 
			"  name: \"TAMPER RESISTANCE AND DETECTION \"\n" + 
			"  specification: \"The organization implements a tamper protection program for the information system, system component, or information system service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-18(1) { \n" + 
			"  name: \"TAMPER RESISTANCE AND DETECTION | MULTIPLE PHASES OF SDLC \"\n" + 
			"  specification: \"The organization employs anti-tamper technologies and techniques during multiple phases in the system development life cycle including design, development, integration, operations, and maintenance.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-18(2) { \n" + 
			"  name: \"TAMPER RESISTANCE AND DETECTION | INSPECTION OF INFORMATION SYSTEMS, COMPONENTS, OR DEVICES \"\n" + 
			"  specification: \"The organization inspects [Assignment: organization-defined information systems, system components, or devices] [Selection (one or more): at random; at [Assignment: organization-defined frequency], upon [Assignment: organization-defined indications of need for inspection]] to detect tampering.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-19 { \n" + 
			"  name: \"COMPONENT AUTHENTICITY \"\n" + 
			"  specification: \"The organization: a. Develops and implements anti-counterfeit policy and procedures that include the means to detect and prevent counterfeit components from entering the information system; and b. Reports counterfeit information system components to [Selection (one or more): source of counterfeit component; [Assignment: organization-defined external reporting organizations]; [Assignment: organization-defined personnel or roles]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-19(1) { \n" + 
			"  name: \"COMPONENT AUTHENTICITY | ANTI-COUNTERFEIT TRAINING \"\n" + 
			"  specification: \"The organization trains [Assignment: organization-defined personnel or roles] to detect counterfeit information system components (including hardware, software, and firmware).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-19(2) { \n" + 
			"  name: \"COMPONENT AUTHENTICITY | CONFIGURATION CONTROL FOR COMPONENT SERVICE / REPAIR \"\n" + 
			"  specification: \"The organization maintains configuration control over [Assignment: organization-defined information system components] awaiting service/repair and serviced/repaired components awaiting return to service.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-19(3) { \n" + 
			"  name: \"COMPONENT AUTHENTICITY | COMPONENT DISPOSAL \"\n" + 
			"  specification: \"The organization disposes of information system components using [Assignment: organization-defined techniques and methods].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-19(4) { \n" + 
			"  name: \"COMPONENT AUTHENTICITY | ANTI-COUNTERFEIT SCANNING \"\n" + 
			"  specification: \"The organization scans for counterfeit information system components [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-20 { \n" + 
			"  name: \"CUSTOMIZED DEVELOPMENT OF CRITICAL COMPONENTS \"\n" + 
			"  specification: \"The organization re-implements or custom develops [Assignment: organization-defined critical information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-21 { \n" + 
			"  name: \"DEVELOPER SCREENING \"\n" + 
			"  specification: \"The organization requires that the developer of [Assignment: organization-defined information system, system component, or information system service]: a. Have appropriate access authorizations as determined by assigned [Assignment: organization-defined official government duties]; and b. Satisfy [Assignment: organization-defined additional personnel screening criteria].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-21(1) { \n" + 
			"  name: \"DEVELOPER SCREENING | VALIDATION OF SCREENING \"\n" + 
			"  specification: \"The organization requires the developer of the information system, system component, or information system service take [Assignment: organization-defined actions] to ensure that the required access authorizations and screening criteria are satisfied.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-22 { \n" + 
			"  name: \"UNSUPPORTED SYSTEM COMPONENTS \"\n" + 
			"  specification: \"The organization: a. Replaces information system components when support for the components is no longer available from the developer, vendor, or manufacturer; and b. Provides justification and documents approval for the continued use of unsupported system components required to satisfy mission/business needs.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SA-22(1) { \n" + 
			"  name: \"UNSUPPORTED SYSTEM COMPONENTS | ALTERNATIVE SOURCES FOR CONTINUED SUPPORT \"\n" + 
			"  specification: \"The organization provides [Selection (one or more): in-house support; [Assignment: organization-defined support from external providers]] for unsupported information system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SA \n" + 
			"  } \n" + 
			"  security control SC-1 { \n" + 
			"  name: \"SYSTEM AND COMMUNICATIONS PROTECTION POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A system and communications protection policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the system and communications protection policy and associated system and communications protection controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. System and communications protection policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. System and communications protection procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-2 { \n" + 
			"  name: \"APPLICATION PARTITIONING \"\n" + 
			"  specification: \"The information system separates user functionality (including user interface services) from information system management functionality.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-2(1) { \n" + 
			"  name: \"APPLICATION PARTITIONING | INTERFACES FOR NON-PRIVILEGED USERS \"\n" + 
			"  specification: \"The information system prevents the presentation of information system management-related functionality at an interface for non-privileged users.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-3 { \n" + 
			"  name: \"SECURITY FUNCTION ISOLATION \"\n" + 
			"  specification: \"The information system isolates security functions from nonsecurity functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-3(1) { \n" + 
			"  name: \"SECURITY FUNCTION ISOLATION | HARDWARE SEPARATION \"\n" + 
			"  specification: \"The information system utilizes underlying hardware separation mechanisms to implement security function isolation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-3(2) { \n" + 
			"  name: \"SECURITY FUNCTION ISOLATION | ACCESS / FLOW CONTROL FUNCTIONS \"\n" + 
			"  specification: \"The information system isolates security functions enforcing access and information flow control from nonsecurity functions and from other security functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-3(3) { \n" + 
			"  name: \"SECURITY FUNCTION ISOLATION | MINIMIZE NONSECURITY FUNCTIONALITY \"\n" + 
			"  specification: \"The organization minimizes the number of nonsecurity functions included within the isolation boundary containing security functions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-3(4) { \n" + 
			"  name: \"SECURITY FUNCTION ISOLATION | MODULE COUPLING AND COHESIVENESS \"\n" + 
			"  specification: \"The organization implements security functions as largely independent modules that maximize internal cohesiveness within modules and minimize coupling between modules.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-3(5) { \n" + 
			"  name: \"SECURITY FUNCTION ISOLATION | LAYERED STRUCTURES \"\n" + 
			"  specification: \"The organization implements security functions as a layered structure minimizing interactions between layers of the design and avoiding any dependence by lower layers on the functionality or correctness of higher layers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-4 { \n" + 
			"  name: \"INFORMATION IN SHARED RESOURCES \"\n" + 
			"  specification: \"The information system prevents unauthorized and unintended information transfer via shared system resources.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-4(1) { \n" + 
			"  name: \"INFORMATION IN SHARED RESOURCES | SECURITY LEVELS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-4(2) { \n" + 
			"  name: \"INFORMATION IN SHARED RESOURCES | PERIODS PROCESSING \"\n" + 
			"  specification: \"The information system prevents unauthorized information transfer via shared resources in accordance with [Assignment: organization-defined procedures] when system processing explicitly switches between different information classification levels or security categories.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-5 { \n" + 
			"  name: \"DENIAL OF SERVICE PROTECTION \"\n" + 
			"  specification: \"The information system protects against or limits the effects of the following types of denial of service attacks: [Assignment: organization-defined types of denial of service attacks or references to sources for such information] by employing [Assignment: organization-defined security safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-5(1) { \n" + 
			"  name: \"DENIAL OF SERVICE PROTECTION | RESTRICT INTERNAL USERS \"\n" + 
			"  specification: \"The information system restricts the ability of individuals to launch [Assignment: organization-defined denial of service attacks] against other information systems.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-5(2) { \n" + 
			"  name: \"DENIAL OF SERVICE PROTECTION | EXCESS CAPACITY / BANDWIDTH / REDUNDANCY \"\n" + 
			"  specification: \"The information system manages excess capacity, bandwidth, or other redundancy to limit the effects of information flooding denial of service attacks.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-5(3) { \n" + 
			"  name: \"DENIAL OF SERVICE PROTECTION | DETECTION / MONITORING \"\n" + 
			"  specification: \"The organization: (a) Employs [Assignment: organization-defined monitoring tools] to detect indicators of denial of service attacks against the information system; and (b) Monitors [Assignment: organization-defined information system resources] to determine if sufficient resources exist to prevent effective denial of service attacks.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-6 { \n" + 
			"  name: \"RESOURCE AVAILABILITY \"\n" + 
			"  specification: \"The information system protects the availability of resources by allocating [Assignment: organization-defined resources] by [Selection (one or more); priority; quota; [Assignment: organization-defined security safeguards]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7 { \n" + 
			"  name: \"BOUNDARY PROTECTION \"\n" + 
			"  specification: \"The information system: a. Monitors and controls communications at the external boundary of the system and at key internal boundaries within the system; b. Implements subnetworks for publicly accessible system components that are [Selection: physically; logically] separated from internal organizational networks; and c. Connects to external networks or information systems only through managed interfaces consisting of boundary protection devices arranged in accordance with an organizational security architecture. \n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(1) { \n" + 
			"  name: \"BOUNDARY PROTECTION | PHYSICALLY SEPARATED SUBNETWORKS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(2) { \n" + 
			"  name: \"BOUNDARY PROTECTION | PUBLIC ACCESS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(3) { \n" + 
			"  name: \"BOUNDARY PROTECTION | ACCESS POINTS \"\n" + 
			"  specification: \"The organization limits the number of external network connections to the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(4) { \n" + 
			"  name: \"BOUNDARY PROTECTION | EXTERNAL TELECOMMUNICATIONS SERVICES \"\n" + 
			"  specification: \"The organization: (a) Implements a managed interface for each external telecommunication service; (b) Establishes a traffic flow policy for each managed interface; (c) Protects the confidentiality and integrity of the information being transmitted across each interface; (d) Documents each exception to the traffic flow policy with a supporting mission/business need and duration of that need; and (e) Reviews exceptions to the traffic flow policy [Assignment: organization-defined frequency] and removes exceptions that are no longer supported by an explicit mission/business need.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(5) { \n" + 
			"  name: \"BOUNDARY PROTECTION | DENY BY DEFAULT / ALLOW BY EXCEPTION \"\n" + 
			"  specification: \"The information system at managed interfaces denies network communications traffic by default and allows network communications traffic by exception (i.e., deny all, permit by exception).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(6) { \n" + 
			"  name: \"BOUNDARY PROTECTION | RESPONSE TO RECOGNIZED FAILURES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-7 (18).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(7) { \n" + 
			"  name: \"BOUNDARY PROTECTION | PREVENT SPLIT TUNNELING FOR REMOTE DEVICES \"\n" + 
			"  specification: \"The information system, in conjunction with a remote device, prevents the device from simultaneously establishing non-remote connections with the system and communicating via some other connection to resources in external networks.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(8) { \n" + 
			"  name: \"BOUNDARY PROTECTION | ROUTE TRAFFIC TO AUTHENTICATED PROXY SERVERS \"\n" + 
			"  specification: \"The information system routes [Assignment: organization-defined internal communications traffic] to [Assignment: organization-defined external networks] through authenticated proxy servers at managed interfaces.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(9) { \n" + 
			"  name: \"BOUNDARY PROTECTION | RESTRICT THREATENING OUTGOING COMMUNICATIONS TRAFFIC \"\n" + 
			"  specification: \"The information system: (a) Detects and denies outgoing communications traffic posing a threat to external information systems; and (b) Audits the identity of internal users associated with denied communications.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(10) { \n" + 
			"  name: \"BOUNDARY PROTECTION | PREVENT UNAUTHORIZED EXFILTRATION \"\n" + 
			"  specification: \"The organization prevents the unauthorized exfiltration of information across managed interfaces.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(11) { \n" + 
			"  name: \"BOUNDARY PROTECTION | RESTRICT INCOMING COMMUNICATIONS TRAFFIC \"\n" + 
			"  specification: \"The information system only allows incoming communications from [Assignment: organization-defined authorized sources] to be routed to [Assignment: organization-defined authorized destinations].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(12) { \n" + 
			"  name: \"BOUNDARY PROTECTION | HOST-BASED PROTECTION \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined host-based boundary protection mechanisms] at [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(13) { \n" + 
			"  name: \"BOUNDARY PROTECTION | ISOLATION OF SECURITY TOOLS / MECHANISMS / SUPPORT COMPONENTS \"\n" + 
			"  specification: \"The organization isolates [Assignment: organization-defined information security tools, mechanisms, and support components] from other internal information system components by implementing physically separate subnetworks with managed interfaces to other components of the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(14) { \n" + 
			"  name: \"BOUNDARY PROTECTION | PROTECTS AGAINST UNAUTHORIZED PHYSICAL CONNECTIONS \"\n" + 
			"  specification: \"The organization protects against unauthorized physical connections at [Assignment: organization-defined managed interfaces].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(15) { \n" + 
			"  name: \"BOUNDARY PROTECTION | ROUTE PRIVILEGED NETWORK ACCESSES \"\n" + 
			"  specification: \"The information system routes all networked, privileged accesses through a dedicated, managed interface for purposes of access control and auditing.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(16) { \n" + 
			"  name: \"BOUNDARY PROTECTION | PREVENT DISCOVERY OF COMPONENTS / DEVICES \"\n" + 
			"  specification: \"The information system prevents discovery of specific system components composing a managed interface.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(17) { \n" + 
			"  name: \"BOUNDARY PROTECTION | AUTOMATED ENFORCEMENT OF PROTOCOL FORMATS \"\n" + 
			"  specification: \"The information system enforces adherence to protocol formats.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(18) { \n" + 
			"  name: \"BOUNDARY PROTECTION | FAIL SECURE \"\n" + 
			"  specification: \"The information system fails securely in the event of an operational failure of a boundary protection device.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(19) { \n" + 
			"  name: \"BOUNDARY PROTECTION | BLOCKS COMMUNICATION FROM NON-ORGANIZATIONALLY CONFIGURED HOSTS \"\n" + 
			"  specification: \"The information system blocks both inbound and outbound communications traffic between [Assignment: organization-defined communication clients] that are independently configured by end users and external service providers.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(20) { \n" + 
			"  name: \"BOUNDARY PROTECTION | DYNAMIC ISOLATION / SEGREGATION \"\n" + 
			"  specification: \"The information system provides the capability to dynamically isolate/segregate [Assignment: organization-defined information system components] from other components of the system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(21) { \n" + 
			"  name: \"BOUNDARY PROTECTION | ISOLATION OF INFORMATION SYSTEM COMPONENTS \"\n" + 
			"  specification: \"The organization employs boundary protection mechanisms to separate [Assignment: organization-defined information system components] supporting [Assignment: organization-defined missions and/or business functions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(22) { \n" + 
			"  name: \"BOUNDARY PROTECTION | SEPARATE SUBNETS FOR CONNECTING TO DIFFERENT SECURITY DOMAINS \"\n" + 
			"  specification: \"The information system implements separate network addresses (i.e., different subnets) to connect to systems in different security domains.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-7(23) { \n" + 
			"  name: \"BOUNDARY PROTECTION | DISABLE SENDER FEEDBACK ON PROTOCOL VALIDATION FAILURE \"\n" + 
			"  specification: \"The information system disables feedback to senders on protocol format validation failure.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-8 { \n" + 
			"  name: \"TRANSMISSION CONFIDENTIALITY AND INTEGRITY \"\n" + 
			"  specification: \"The information system protects the [Selection (one or more): confidentiality; integrity] of transmitted information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-8(1) { \n" + 
			"  name: \"TRANSMISSION CONFIDENTIALITY AND INTEGRITY | CRYPTOGRAPHIC OR ALTERNATE PHYSICAL PROTECTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to [Selection (one or more): prevent unauthorized disclosure of information; detect changes to information] during transmission unless otherwise protected by [Assignment: organization-defined alternative physical safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-8(2) { \n" + 
			"  name: \"TRANSMISSION CONFIDENTIALITY AND INTEGRITY | PRE / POST TRANSMISSION HANDLING \"\n" + 
			"  specification: \"The information system maintains the [Selection (one or more): confidentiality; integrity] of information during preparation for transmission and during reception.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-8(3) { \n" + 
			"  name: \"TRANSMISSION CONFIDENTIALITY AND INTEGRITY | CRYPTOGRAPHIC PROTECTION FOR MESSAGE EXTERNALS \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to protect message externals unless otherwise protected by [Assignment: organization-defined alternative physical safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-8(4) { \n" + 
			"  name: \"TRANSMISSION CONFIDENTIALITY AND INTEGRITY | CONCEAL / RANDOMIZE COMMUNICATIONS \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to conceal or randomize communication patterns unless otherwise protected by [Assignment: organization-defined alternative physical safeguards].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-9 { \n" + 
			"  name: \"TRANSMISSION CONFIDENTIALITY \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-8.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-10 { \n" + 
			"  name: \"NETWORK DISCONNECT \"\n" + 
			"  specification: \"The information system terminates the network connection associated with a communications session at the end of the session or after [Assignment: organization-defined time period] of inactivity.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-11 { \n" + 
			"  name: \"TRUSTED PATH \"\n" + 
			"  specification: \"The information system establishes a trusted communications path between the user and the following security functions of the system: [Assignment: organization-defined security functions to include at a minimum, information system authentication and re-authentication].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-11(1) { \n" + 
			"  name: \"TRUSTED PATH | LOGICAL ISOLATION \"\n" + 
			"  specification: \"The information system provides a trusted communications path that is logically isolated and distinguishable from other paths.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-12 { \n" + 
			"  name: \"CRYPTOGRAPHIC KEY ESTABLISHMENT AND MANAGEMENT \"\n" + 
			"  specification: \"The organization establishes and manages cryptographic keys for required cryptography employed within the information system in accordance with [Assignment: organization-defined requirements for key generation, distribution, storage, access, and destruction].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-12(1) { \n" + 
			"  name: \"CRYPTOGRAPHIC KEY ESTABLISHMENT AND MANAGEMENT | AVAILABILITY \"\n" + 
			"  specification: \"The organization maintains availability of information in the event of the loss of cryptographic keys by users.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-12(2) { \n" + 
			"  name: \"CRYPTOGRAPHIC KEY ESTABLISHMENT AND MANAGEMENT | SYMMETRIC KEYS \"\n" + 
			"  specification: \"The organization produces, controls, and distributes symmetric cryptographic keys using [Selection: NIST FIPS-compliant; NSA-approved] key management technology and processes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-12(3) { \n" + 
			"  name: \"CRYPTOGRAPHIC KEY ESTABLISHMENT AND MANAGEMENT | ASYMMETRIC KEYS \"\n" + 
			"  specification: \"The organization produces, controls, and distributes asymmetric cryptographic keys using [Selection: NSA-approved key management technology and processes; approved PKI Class 3 certificates or prepositioned keying material; approved PKI Class 3 or Class 4 certificates and hardware security tokens that protect the user’s private key].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-12(4) { \n" + 
			"  name: \"CRYPTOGRAPHIC KEY ESTABLISHMENT AND MANAGEMENT | PKI CERTIFICATES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-12.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-12(5) { \n" + 
			"  name: \"CRYPTOGRAPHIC KEY ESTABLISHMENT AND MANAGEMENT | PKI CERTIFICATES / HARDWARE TOKENS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-12.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-13 { \n" + 
			"  name: \"CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined cryptographic uses and type of cryptography required for each use] in accordance with applicable federal laws, Executive Orders, directives, policies, regulations, and standards.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-13(1) { \n" + 
			"  name: \"CRYPTOGRAPHIC PROTECTION | FIPS-VALIDATED CRYPTOGRAPHY \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-13.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-13(2) { \n" + 
			"  name: \"CRYPTOGRAPHIC PROTECTION | NSA-APPROVED CRYPTOGRAPHY \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-13.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-13(3) { \n" + 
			"  name: \"CRYPTOGRAPHIC PROTECTION | INDIVIDUALS WITHOUT FORMAL ACCESS APPROVALS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-13.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-13(4) { \n" + 
			"  name: \"CRYPTOGRAPHIC PROTECTION | DIGITAL SIGNATURES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-13.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-14 { \n" + 
			"  name: \"PUBLIC ACCESS PROTECTIONS \"\n" + 
			"  specification: \"Withdrawn: Capability provided by AC-2, AC-3, AC-5, AC-6, SI-3, SI-4, SI-5, SI-7, SI-10.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-15 { \n" + 
			"  name: \"COLLABORATIVE COMPUTING DEVICES \"\n" + 
			"  specification: \"The information system: a. Prohibits remote activation of collaborative computing devices with the following exceptions: [Assignment: organization-defined exceptions where remote activation is to be allowed]; and b. Provides an explicit indication of use to users physically present at the devices.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-15(1) { \n" + 
			"  name: \"COLLABORATIVE COMPUTING DEVICES | PHYSICAL DISCONNECT \"\n" + 
			"  specification: \"The information system provides physical disconnect of collaborative computing devices in a manner that supports ease of use.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-15(2) { \n" + 
			"  name: \"COLLABORATIVE COMPUTING DEVICES | BLOCKING INBOUND / OUTBOUND COMMUNICATIONS TRAFFIC \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-15(3) { \n" + 
			"  name: \"COLLABORATIVE COMPUTING DEVICES | DISABLING / REMOVAL IN SECURE WORK AREAS \"\n" + 
			"  specification: \"The organization disables or removes collaborative computing devices from [Assignment: organization-defined information systems or information system components] in [Assignment: organization-defined secure work areas].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-15(4) { \n" + 
			"  name: \"COLLABORATIVE COMPUTING DEVICES | EXPLICITLY INDICATE CURRENT PARTICIPANTS \"\n" + 
			"  specification: \"The information system provides an explicit indication of current participants in [Assignment: organization-defined online meetings and teleconferences].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-16 { \n" + 
			"  name: \"TRANSMISSION OF SECURITY ATTRIBUTES \"\n" + 
			"  specification: \"The information system associates [Assignment: organization-defined security attributes] with information exchanged between information systems and between system components.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-16(1) { \n" + 
			"  name: \"TRANSMISSION OF SECURITY ATTRIBUTES | INTEGRITY VALIDATION \"\n" + 
			"  specification: \"The information system validates the integrity of transmitted security attributes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-17 { \n" + 
			"  name: \"PUBLIC KEY INFRASTRUCTURE CERTIFICATES \"\n" + 
			"  specification: \"The organization issues public key certificates under an [Assignment: organization-defined certificate policy] or obtains public key certificates from an approved service provider.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-18 { \n" + 
			"  name: \"MOBILE CODE \"\n" + 
			"  specification: \"The organization: a. Defines acceptable and unacceptable mobile code and mobile code technologies; b. Establishes usage restrictions and implementation guidance for acceptable mobile code and mobile code technologies; and c. Authorizes, monitors, and controls the use of mobile code within the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-18(1) { \n" + 
			"  name: \"MOBILE CODE | IDENTIFY UNACCEPTABLE CODE / TAKE CORRECTIVE ACTIONS \"\n" + 
			"  specification: \"The information system identifies [Assignment: organization-defined unacceptable mobile code] and takes [Assignment: organization-defined corrective actions].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-18(2) { \n" + 
			"  name: \"MOBILE CODE | ACQUISITION / DEVELOPMENT / USE \"\n" + 
			"  specification: \"The organization ensures that the acquisition, development, and use of mobile code to be deployed in the information system meets [Assignment: organization-defined mobile code requirements].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-18(3) { \n" + 
			"  name: \"MOBILE CODE | PREVENT DOWNLOADING / EXECUTION \"\n" + 
			"  specification: \"The information system prevents the download and execution of [Assignment: organization-defined unacceptable mobile code].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-18(4) { \n" + 
			"  name: \"MOBILE CODE | PREVENT AUTOMATIC EXECUTION \"\n" + 
			"  specification: \"The information system prevents the automatic execution of mobile code in [Assignment: organization-defined software applications] and enforces [Assignment: organization-defined actions] prior to executing the code.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-18(5) { \n" + 
			"  name: \"MOBILE CODE | ALLOW EXECUTION ONLY IN CONFINED ENVIRONMENTS \"\n" + 
			"  specification: \"The organization allows execution of permitted mobile code only in confined virtual machine environments.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-19 { \n" + 
			"  name: \"VOICE OVER INTERNET PROTOCOL \"\n" + 
			"  specification: \"The organization: a. Establishes usage restrictions and implementation guidance for Voice over Internet Protocol (VoIP) technologies based on the potential to cause damage to the information system if used maliciously; and b. Authorizes, monitors, and controls the use of VoIP within the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-20 { \n" + 
			"  name: \"SECURE NAME / ADDRESS RESOLUTION SERVICE (AUTHORITATIVE SOURCE) \"\n" + 
			"  specification: \"The information system: a. Provides additional data origin authentication and integrity verification artifacts along with the authoritative name resolution data the system returns in response to external name/address resolution queries; and b. Provides the means to indicate the security status of child zones and (if the child supports secure resolution services) to enable verification of a chain of trust among parent and child domains, when operating as part of a distributed, hierarchical namespace.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-20(1) { \n" + 
			"  name: \"SECURE NAME / ADDRESS RESOLUTION SERVICE (AUTHORITATIVE SOURCE) | CHILD SUBSPACES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-20.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-20(2) { \n" + 
			"  name: \"SECURE NAME / ADDRESS RESOLUTION SERVICE (AUTHORITATIVE SOURCE) | DATA ORIGIN / INTEGRITY \"\n" + 
			"  specification: \"The information system provides data origin and integrity protection artifacts for internal name/address resolution queries.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-21 { \n" + 
			"  name: \"SECURE NAME / ADDRESS RESOLUTION SERVICE (RECURSIVE OR CACHING RESOLVER) \"\n" + 
			"  specification: \"The information system requests and performs data origin authentication and data integrity verification on the name/address resolution responses the system receives from authoritative sources.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-21(1) { \n" + 
			"  name: \"SECURE NAME / ADDRESS RESOLUTION SERVICE (RECURSIVE OR CACHING RESOLVER) | DATA ORIGIN / INTEGRITY \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-21.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-22 { \n" + 
			"  name: \"ARCHITECTURE AND PROVISIONING FOR NAME / ADDRESS RESOLUTION SERVICE \"\n" + 
			"  specification: \"The information systems that collectively provide name/address resolution service for an organization are fault-tolerant and implement internal/external role separation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-23 { \n" + 
			"  name: \"SESSION AUTHENTICITY \"\n" + 
			"  specification: \"The information system protects the authenticity of communications sessions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-23(1) { \n" + 
			"  name: \"SESSION AUTHENTICITY | INVALIDATE SESSION IDENTIFIERS AT LOGOUT \"\n" + 
			"  specification: \"The information system invalidates session identifiers upon user logout or other session termination.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-23(2) { \n" + 
			"  name: \"SESSION AUTHENTICITY | USER-INITIATED LOGOUTS / MESSAGE DISPLAYS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-12 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-23(3) { \n" + 
			"  name: \" \"\n" + 
			"  specification: \"The information system generates a unique session identifier for each session with [Assignment: organization-defined randomness requirements] and recognizes only session identifiers that are system-generated.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-23(4) { \n" + 
			"  name: \"SESSION AUTHENTICITY | UNIQUE SESSION IDENTIFIERS WITH RANDOMIZATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-23 (3).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-23(5) { \n" + 
			"  name: \"SESSION AUTHENTICITY | ALLOWED CERTIFICATE AUTHORITIES \"\n" + 
			"  specification: \"The information system only allows the use of [Assignment: organization-defined certificate authorities] for verification of the establishment of protected sessions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-24 { \n" + 
			"  name: \"FAIL IN KNOWN STATE \"\n" + 
			"  specification: \"The information system fails to a [Assignment: organization-defined known-state] for [Assignment: organization-defined types of failures] preserving [Assignment: organization-defined system state information] in failure.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-25 { \n" + 
			"  name: \"THIN NODES \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined information system components] with minimal functionality and information storage.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-26 { \n" + 
			"  name: \"HONEYPOTS \"\n" + 
			"  specification: \"The information system includes components specifically designed to be the target of malicious attacks for the purpose of detecting, deflecting, and analyzing such attacks.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-26(1) { \n" + 
			"  name: \"HONEYPOTS | DETECTION OF MALICIOUS CODE \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-35.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-27 { \n" + 
			"  name: \"PLATFORM-INDEPENDENT APPLICATIONS \"\n" + 
			"  specification: \"The information system includes: [Assignment: organization-defined platform-independent applications].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-28 { \n" + 
			"  name: \"PROTECTION OF INFORMATION AT REST \"\n" + 
			"  specification: \"The information system protects the [Selection (one or more): confidentiality; integrity] of [Assignment: organization-defined information at rest].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-28(1) { \n" + 
			"  name: \"PROTECTION OF INFORMATION AT REST | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to prevent unauthorized disclosure and modification of [Assignment: organization-defined information] on [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-28(2) { \n" + 
			"  name: \"PROTECTION OF INFORMATION AT REST | OFF-LINE STORAGE \"\n" + 
			"  specification: \"The organization removes from online storage and stores off-line in a secure location [Assignment: organization-defined information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-29 { \n" + 
			"  name: \"HETEROGENEITY \"\n" + 
			"  specification: \"The organization employs a diverse set of information technologies for [Assignment: organization-defined information system components] in the implementation of the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-29(1) { \n" + 
			"  name: \"HETEROGENEITY | VIRTUALIZATION TECHNIQUES \"\n" + 
			"  specification: \"The organization employs virtualization techniques to support the deployment of a diversity of operating systems and applications that are changed [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-30 { \n" + 
			"  name: \"CONCEALMENT AND MISDIRECTION \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined concealment and misdirection techniques] for [Assignment: organization-defined information systems] at [Assignment: organization-defined time periods] to confuse and mislead adversaries.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-30(1) { \n" + 
			"  name: \"CONCEALMENT AND MISDIRECTION | VIRTUALIZATION TECHNIQUES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-29 (1).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-30(2) { \n" + 
			"  name: \"CONCEALMENT AND MISDIRECTION | RANDOMNESS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined techniques] to introduce randomness into organizational operations and assets.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-30(3) { \n" + 
			"  name: \"CONCEALMENT AND MISDIRECTION | CHANGE PROCESSING / STORAGE LOCATIONS \"\n" + 
			"  specification: \"The organization changes the location of [Assignment: organization-defined processing and/or storage] [Selection: [Assignment: organization-defined time frequency]; at random time intervals]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-30(4) { \n" + 
			"  name: \"CONCEALMENT AND MISDIRECTION | MISLEADING INFORMATION \"\n" + 
			"  specification: \"The organization employs realistic, but misleading information in [Assignment: organization-defined information system components] with regard to its security state or posture.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-30(5) { \n" + 
			"  name: \"CONCEALMENT AND MISDIRECTION | CONCEALMENT OF SYSTEM COMPONENTS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined techniques] to hide or conceal [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-31 { \n" + 
			"  name: \"COVERT CHANNEL ANALYSIS \"\n" + 
			"  specification: \"The organization: a. Performs a covert channel analysis to identify those aspects of communications within the information system that are potential avenues for covert [Selection (one or more): storage; timing] channels; and b. Estimates the maximum bandwidth of those channels.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-31(1) { \n" + 
			"  name: \"COVERT CHANNEL ANALYSIS | TEST COVERT CHANNELS FOR EXPLOITABILITY \"\n" + 
			"  specification: \"The organization tests a subset of the identified covert channels to determine which channels are exploitable.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-31(2) { \n" + 
			"  name: \"COVERT CHANNEL ANALYSIS | MAXIMUM BANDWIDTH \"\n" + 
			"  specification: \"The organization reduces the maximum bandwidth for identified covert [Selection (one or more); storage; timing] channels to [Assignment: organization-defined values].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-31(3) { \n" + 
			"  name: \"COVERT CHANNEL ANALYSIS | MEASURE BANDWIDTH IN OPERATIONAL ENVIRONMENTS \"\n" + 
			"  specification: \"The organization measures the bandwidth of [Assignment: organization-defined subset of identified covert channels] in the operational environment of the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-32 { \n" + 
			"  name: \"INFORMATION SYSTEM PARTITIONING \"\n" + 
			"  specification: \"The organization partitions the information system into [Assignment: organization-defined information system components] residing in separate physical domains or environments based on [Assignment: organization-defined circumstances for physical separation of components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-33 { \n" + 
			"  name: \"TRANSMISSION PREPARATION INTEGRITY \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SC-8.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-34 { \n" + 
			"  name: \"NON-MODIFIABLE EXECUTABLE PROGRAMS \"\n" + 
			"  specification: \"The information system at [Assignment: organization-defined information system components]: a. Loads and executes the operating environment from hardware-enforced, read-only media; and b. Loads and executes [Assignment: organization-defined applications] from hardware-enforced, read-only media.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-34(1) { \n" + 
			"  name: \"NON-MODIFIABLE EXECUTABLE PROGRAMS | NO WRITABLE STORAGE \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined information system components] with no writeable storage that is persistent across component restart or power on/off.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-34(2) { \n" + 
			"  name: \"NON-MODIFIABLE EXECUTABLE PROGRAMS | INTEGRITY PROTECTION / READ-ONLY MEDIA \"\n" + 
			"  specification: \"The organization protects the integrity of information prior to storage on read-only media and controls the media after such information has been recorded onto the media.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-34(3) { \n" + 
			"  name: \"NON-MODIFIABLE EXECUTABLE PROGRAMS | HARDWARE-BASED PROTECTION \"\n" + 
			"  specification: \"The organization: (a) Employs hardware-based, write-protect for [Assignment: organization-defined information system firmware components]; and (b) Implements specific procedures for [Assignment: organization-defined authorized individuals] to manually disable hardware write-protect for firmware modifications and re-enable the write-protect prior to returning to operational mode.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-35 { \n" + 
			"  name: \"HONEYCLIENTS \"\n" + 
			"  specification: \"The information system includes components that proactively seek to identify malicious websites and/or web-based malicious code.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-36 { \n" + 
			"  name: \"DISTRIBUTED PROCESSING AND STORAGE \"\n" + 
			"  specification: \"The organization distributes [Assignment: organization-defined processing and storage] across multiple physical locations.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-36(1) { \n" + 
			"  name: \"DISTRIBUTED PROCESSING AND STORAGE | POLLING TECHNIQUES \"\n" + 
			"  specification: \"The organization employs polling techniques to identify potential faults, errors, or compromises to [Assignment: organization-defined distributed processing and storage components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-37 { \n" + 
			"  name: \"OUT-OF-BAND CHANNELS \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined out-of-band channels] for the physical delivery or electronic transmission of [Assignment: organization-defined information, information system components, or devices] to [Assignment: organization-defined individuals or information systems].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-37(1) { \n" + 
			"  name: \"OUT-OF-BAND CHANNELS | ENSURE DELIVERY / TRANSMISSION \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to ensure that only [Assignment: organization-defined individuals or information systems] receive the [Assignment: organization-defined information, information system components, or devices].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-38 { \n" + 
			"  name: \"OPERATIONS SECURITY \"\n" + 
			"  specification: \"The organization employs [Assignment: organization-defined security safeguards] to ensure that only [Assignment: organization-defined individuals or information systems] receive the [Assignment: organization-defined information, information system components, or devices].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-39 { \n" + 
			"  name: \"PROCESS ISOLATION \"\n" + 
			"  specification: \"The information system maintains a separate execution domain for each executing process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-39(1) { \n" + 
			"  name: \"PROCESS ISOLATION | HARDWARE SEPARATION \"\n" + 
			"  specification: \"The information system implements underlying hardware separation mechanisms to facilitate process separation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-39(2) { \n" + 
			"  name: \"PROCESS ISOLATION | THREAD ISOLATION \"\n" + 
			"  specification: \"The information system maintains a separate execution domain for each thread in [Assignment: organization-defined multi-threaded processing].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-40 { \n" + 
			"  name: \"WIRELESS LINK PROTECTION \"\n" + 
			"  specification: \"The information system protects external and internal [Assignment: organization-defined wireless links] from [Assignment: organization-defined types of signal parameter attacks or references to sources for such attacks].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-40(1) { \n" + 
			"  name: \"WIRELESS LINK PROTECTION | ELECTROMAGNETIC INTERFERENCE \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms that achieve [Assignment: organization-defined level of protection] against the effects of intentional electromagnetic interference.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-40(2) { \n" + 
			"  name: \"WIRELESS LINK PROTECTION | REDUCE DETECTION POTENTIAL \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to reduce the detection potential of wireless links to [Assignment: organization-defined level of reduction].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-40(3) { \n" + 
			"  name: \"WIRELESS LINK PROTECTION | IMITATIVE OR MANIPULATIVE COMMUNICATIONS DECEPTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to identify and reject wireless transmissions that are deliberate attempts to achieve imitative or manipulative communications deception based on signal parameters.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-40(4) { \n" + 
			"  name: \"WIRELESS LINK PROTECTION | SIGNAL PARAMETER IDENTIFICATION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to prevent the identification of [Assignment: organization-defined wireless transmitters] by using the transmitter signal parameters.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-41 { \n" + 
			"  name: \"PORT AND I/O DEVICE ACCESS \"\n" + 
			"  specification: \"The organization physically disables or removes [Assignment: organization-defined connection ports or input/output devices] on [Assignment: organization-defined information systems or information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-42 { \n" + 
			"  name: \"SENSOR CAPABILITY AND DATA \"\n" + 
			"  specification: \"The information system: a. Prohibits the remote activation of environmental sensing capabilities with the following exceptions: [Assignment: organization-defined exceptions where remote activation of sensors is allowed]; andb. Provides an explicit indication of sensor use to [Assignment: organization-defined class of users].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-42(1) { \n" + 
			"  name: \"SENSOR CAPABILITY AND DATA | REPORTING TO AUTHORIZED INDIVIDUALS OR ROLES \"\n" + 
			"  specification: \"The organization ensures that the information system is configured so that data or information collected by the [Assignment: organization-defined sensors] is only reported to authorized individuals or roles.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-42(2) { \n" + 
			"  name: \"SENSOR CAPABILITY AND DATA | AUTHORIZED USE \"\n" + 
			"  specification: \"The organization employs the following measures: [Assignment: organization-defined measures], so that data or information collected by [Assignment: organization-defined sensors] is only used for authorized purposes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-42(3) { \n" + 
			"  name: \"SENSOR CAPABILITY AND DATA | PROHIBIT USE OF DEVICES \"\n" + 
			"  specification: \"The organization prohibits the use of devices possessing [Assignment: organization-defined environmental sensing capabilities] in [Assignment: organization-defined facilities, areas, or systems].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-43 { \n" + 
			"  name: \"USAGE RESTRICTIONS \"\n" + 
			"  specification: \"The organization: a.Establishes usage restrictions and implementation guidance for [Assignment: organization-defined information system components] based on the potential to cause damage to the information system if used maliciously; and b. Authorizes, monitors, and controls the use of such components within the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SC-44 { \n" + 
			"  name: \"DETONATION CHAMBERS \"\n" + 
			"  specification: \"The organization employs a detonation chamber capability within [Assignment: organization-defined information system, system component, or location].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SC \n" + 
			"  } \n" + 
			"  security control SI-1 { \n" + 
			"  name: \"SYSTEM AND INFORMATION INTEGRITY POLICY AND PROCEDURES \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Develops, documents, and disseminates to [Assignment: organization-defined personnel or roles]:\n" + 
			"				1. A system and information integrity policy that addresses purpose, scope, roles, responsibilities, management commitment, coordination among organizational entities, and compliance; and\n" + 
			"				2. Procedures to facilitate the implementation of the system and information integrity policy and associated system and information integrity controls; and\n" + 
			"			b. Reviews and updates the current:\n" + 
			"				1. System and information integrity policy [Assignment: organization-defined frequency]; and\n" + 
			"				2. System and information integrity procedures [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2 { \n" + 
			"  name: \"FLAW REMEDIATION \"\n" + 
			"  specification: \"The organization: a. Identifies, reports, and corrects information system flaws; b. Tests software and firmware updates related to flaw remediation for effectiveness and potential side effects before installation; c. Installs security-relevant software and firmware updates within [Assignment: organization-defined time period] of the release of the updates; and d. Incorporates flaw remediation into the organizational configuration management process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2(1) { \n" + 
			"  name: \"FLAW REMEDIATION | CENTRAL MANAGEMENT \"\n" + 
			"  specification: \"The organization centrally manages the flaw remediation process.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2(2) { \n" + 
			"  name: \"FLAW REMEDIATION | AUTOMATED FLAW REMEDIATION STATUS \"\n" + 
			"  specification: \"The organization employs automated mechanisms [Assignment: organization-defined frequency] to determine the state of information system components with regard to flaw remediation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2(3) { \n" + 
			"  name: \"FLAW REMEDIATION | TIME TO REMEDIATE FLAWS / BENCHMARKS FOR CORRECTIVE ACTIONS \"\n" + 
			"  specification: \"The organization: (a) Measures the time between flaw identification and flaw remediation; and (b) Establishes [Assignment: organization-defined benchmarks] for taking corrective actions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2(4) { \n" + 
			"  name: \"FLAW REMEDIATION | AUTOMATED PATCH MANAGEMENT TOOLS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-2.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2(5) { \n" + 
			"  name: \"FLAW REMEDIATION | AUTOMATIC SOFTWARE / FIRMWARE UPDATES \"\n" + 
			"  specification: \"The organization installs [Assignment: organization-defined security-relevant software and firmware updates] automatically to [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-2(6) { \n" + 
			"  name: \"FLAW REMEDIATION | REMOVAL OF PREVIOUS VERSIONS OF SOFTWARE / FIRMWARE \"\n" + 
			"  specification: \"The organization removes [Assignment: organization-defined software and firmware components] after updated versions have been installed.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3 { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Employs malicious code protection mechanisms at information system entry and exit points to detect and eradicate malicious code;\n" + 
			"			b. Updates malicious code protection mechanisms whenever new releases are available in accordance with organizational configuration management policy and procedures;\n" + 
			"			c. Configures malicious code protection mechanisms to:\n" + 
			"				1. Perform periodic scans of the information system [Assignment: organization-defined frequency] and real-time scans of files from external sources at [Selection (one or more); endpoint; network entry/exit points] as the files are downloaded, opened, or executed in accordance with organizational security policy; and\n" + 
			"				2. [Selection (one or more): block malicious code; quarantine malicious code; send alert to administrator; [Assignment: organization-defined action]] in response to malicious code detection; and\n" + 
			"			d. Addresses the receipt of false positives during malicious code detection and eradication and the resulting potential impact on the availability of the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(1) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | CENTRAL MANAGEMENT \"\n" + 
			"  specification: \"The organization centrally manages malicious code protection mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(2) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | AUTOMATIC UPDATES \"\n" + 
			"  specification: \"The information system automatically updates malicious code protection mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(3) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | NON-PRIVILEGED USERS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-6 (10).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(4) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | UPDATES ONLY BY PRIVILEGED USERS \"\n" + 
			"  specification: \"The information system updates malicious code protection mechanisms only when directed by a privileged user.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(5) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | PORTABLE STORAGE DEVICES \"\n" + 
			"  specification: \"Withdrawn: Incorporated into MP-7.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(6) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | TESTING / VERIFICATION \"\n" + 
			"  specification: \"The organization: (a) Tests malicious code protection mechanisms [Assignment: organization-defined frequency] by introducing a known benign, non-spreading test case into the information system; and (b) Verifies that both detection of the test case and associated incident reporting occur.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(7) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | NONSIGNATURE-BASED DETECTION \"\n" + 
			"  specification: \"The information system implements nonsignature-based malicious code detection mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(8) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | DETECT UNAUTHORIZED COMMANDS \"\n" + 
			"  specification: \"The information system detects [Assignment: organization-defined unauthorized operating system commands] through the kernel application programming interface at [Assignment: organization-defined information system hardware components] and [Selection (one or more): issues a warning; audits the command execution; prevents the execution of the command].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(9) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | AUTHENTICATE REMOTE COMMANDS \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined security safeguards] to authenticate [Assignment: organization-defined remote commands].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-3(10) { \n" + 
			"  name: \"MALICIOUS CODE PROTECTION | MALICIOUS CODE ANALYSIS \"\n" + 
			"  specification: \"The organization: (a) Employs [Assignment: organization-defined tools and techniques] to analyze the characteristics and behavior of malicious code; and (b) Incorporates the results from malicious code analysis into organizational incident response and flaw remediation processes.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4 { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING \"\n" + 
			"  specification: \"The organization:\n" + 
			"			a. Monitors the information system to detect:\n" + 
			"				1. Attacks and indicators of potential attacks in accordance with [Assignment: organization-defined monitoring objectives]; and\n" + 
			"				2. Unauthorized local, network, and remote connections;\n" + 
			"			b. Identifies unauthorized use of the information system through [Assignment: organization-defined techniques and methods];\n" + 
			"			c. Deploys monitoring devices:\n" + 
			"				1. Strategically within the information system to collect organization-determined essential information; and\n" + 
			"				2. At ad hoc locations within the system to track specific types of transactions of interest to the organization;\n" + 
			"			d. Protects information obtained from intrusion-monitoring tools from unauthorized access, modification, and deletion;\n" + 
			"			e. Heightens the level of information system monitoring activity whenever there is an indication of increased risk to organizational operations and assets, individuals, other organizations, or the Nation based on law enforcement information, intelligence information, or other credible sources of information;\n" + 
			"			f. Obtains legal opinion with regard to information system monitoring activities in accordance with applicable federal laws, Executive Orders, directives, policies, or regulations; and\n" + 
			"			g. Provides [Assignment: organization-defined information system monitoring information] to [Assignment: organization-defined personnel or roles] [Selection (one or more): as needed; [Assignment: organization-defined frequency]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(1) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | SYSTEM-WIDE INTRUSION DETECTION SYSTEM \"\n" + 
			"  specification: \"The organization connects and configures individual intrusion detection tools into an information system-wide intrusion detection system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(2) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | AUTOMATED TOOLS FOR REAL-TIME ANALYSIS \"\n" + 
			"  specification: \"The organization employs automated tools to support near real-time analysis of events.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(3) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | AUTOMATED TOOL INTEGRATION \"\n" + 
			"  specification: \"The organization employs automated tools to integrate intrusion detection tools into access control and flow control mechanisms for rapid response to attacks by enabling reconfiguration of these mechanisms in support of attack isolation and elimination.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(4) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | INBOUND AND OUTBOUND COMMUNICATIONS TRAFFIC \"\n" + 
			"  specification: \"The information system monitors inbound and outbound communications traffic [Assignment: organization-defined frequency] for unusual or unauthorized activities or conditions.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(5) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | SYSTEM-GENERATED ALERTS \"\n" + 
			"  specification: \"The information system alerts [Assignment: organization-defined personnel or roles] when the following indications of compromise or potential compromise occur: [Assignment: organization-defined compromise indicators].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(6) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | RESTRICT NON-PRIVILEGED USERS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-6 (10).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(7) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | AUTOMATED RESPONSE TO SUSPICIOUS EVENTS \"\n" + 
			"  specification: \"The information system notifies [Assignment: organization-defined incident response personnel (identified by name and/or by role)] of detected suspicious events and takes [Assignment: organization-defined least-disruptive actions to terminate suspicious events].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(8) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | PROTECTION OF MONITORING INFORMATION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-4.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(9) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | TESTING OF MONITORING TOOLS \"\n" + 
			"  specification: \"The organization tests intrusion-monitoring tools [Assignment: organization-defined frequency].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(10) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | VISIBILITY OF ENCRYPTED COMMUNICATIONS \"\n" + 
			"  specification: \"The organization makes provisions so that [Assignment: organization-defined encrypted communications traffic] is visible to [Assignment: organization-defined information system monitoring tools].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(11) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | ANALYZE COMMUNICATIONS TRAFFIC ANOMALIES \"\n" + 
			"  specification: \"The organization analyzes outbound communications traffic at the external boundary of the information system and selected [Assignment: organization-defined interior points within the system (e.g., subnetworks, subsystems)] to discover anomalies.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(12) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | AUTOMATED ALERTS \"\n" + 
			"  specification: \"The organization employs automated mechanisms to alert security personnel of the following inappropriate or unusual activities with security implications: [Assignment: organization-defined activities that trigger alerts].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(13) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | ANALYZE TRAFFIC / EVENT PATTERNS \"\n" + 
			"  specification: \"The organization: (a) Analyzes communications traffic/event patterns for the information system; (b) Develops profiles representing common traffic patterns and/or events; and (c) Uses the traffic/event profiles in tuning system-monitoring devices to reduce the number of false positives and the number of false negatives.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(14) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | WIRELESS INTRUSION DETECTION \"\n" + 
			"  specification: \"The organization employs a wireless intrusion detection system to identify rogue wireless devices and to detect attack attempts and potential compromises/breaches to the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(15) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | WIRELESS TO WIRELINE COMMUNICATIONS \"\n" + 
			"  specification: \"The organization employs an intrusion detection system to monitor wireless communications traffic as the traffic passes from wireless to wireline networks.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(16) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | CORRELATE MONITORING INFORMATION \"\n" + 
			"  specification: \"The organization correlates information from monitoring tools employed throughout the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(17) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | INTEGRATED SITUATIONAL AWARENESS \"\n" + 
			"  specification: \"The organization correlates information from monitoring physical, cyber, and supply chain activities to achieve integrated, organization-wide situational awareness.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(18) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | ANALYZE TRAFFIC / COVERT EXFILTRATION \"\n" + 
			"  specification: \"The organization analyzes outbound communications traffic at the external boundary of the information system (i.e., system perimeter) and at [Assignment: organization-defined interior points within the system (e.g., subsystems, subnetworks)] to detect covert exfiltration of information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(19) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | INDIVIDUALS POSING GREATER RISK \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined additional monitoring] of individuals who have been identified by [Assignment: organization-defined sources] as posing an increased level of risk.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(20) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | PRIVILEGED USERS \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined additional monitoring] of privileged users.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(21) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | PROBATIONARY PERIODS \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined additional monitoring] of individuals during [Assignment: organization-defined probationary period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(22) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | UNAUTHORIZED NETWORK SERVICES \"\n" + 
			"  specification: \"The information system detects network services that have not been authorized or approved by [Assignment: organization-defined authorization or approval processes] and [Selection (one or more): audits; alerts [Assignment: organization-defined personnel or roles]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(23) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | HOST-BASED DEVICES \"\n" + 
			"  specification: \"The organization implements [Assignment: organization-defined host-based monitoring mechanisms] at [Assignment: organization-defined information system components].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-4(24) { \n" + 
			"  name: \"INFORMATION SYSTEM MONITORING | INDICATORS OF COMPROMISE \"\n" + 
			"  specification: \"The information system discovers, collects, distributes, and uses indicators of compromise.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-5 { \n" + 
			"  name: \"SECURITY ALERTS, ADVISORIES, AND DIRECTIVES \"\n" + 
			"  specification: \"The organization: a. Receives information system security alerts, advisories, and directives from [Assignment: organization-defined external organizations] on an ongoing basis; b. Generates internal security alerts, advisories, and directives as deemed necessary; c. Disseminates security alerts, advisories, and directives to: [Selection (one or more): [Assignment: organization-defined personnel or roles]; [Assignment: organization-defined elements within the organization]; [Assignment: organization-defined external organizations]]; and d. Implements security directives in accordance with established time frames, or notifies the issuing organization of the degree of noncompliance.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-5(1) { \n" + 
			"  name: \"SECURITY ALERTS, ADVISORIES, AND DIRECTIVES | AUTOMATED ALERTS AND ADVISORIES \"\n" + 
			"  specification: \"The organization employs automated mechanisms to make security alert and advisory information available throughout the organization.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-6 { \n" + 
			"  name: \"SECURITY FUNCTION VERIFICATION \"\n" + 
			"  specification: \"The information system: a. Verifies the correct operation of [Assignment: organization-defined security functions]; b. Performs this verification [Selection (one or more): [Assignment: organization-defined system transitional states]; upon command by user with appropriate privilege; [Assignment: organization-defined frequency]]; c. Notifies [Assignment: organization-defined personnel or roles] of failed security verification tests; and d. [Selection (one or more): shuts the information system down; restarts the information system; [Assignment: organization-defined alternative action(s)]] when anomalies are discovered.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-6(1) { \n" + 
			"  name: \"SECURITY FUNCTION VERIFICATION | NOTIFICATION OF FAILED SECURITY TESTS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-6(2) { \n" + 
			"  name: \"SECURITY FUNCTION VERIFICATION | AUTOMATION SUPPORT FOR DISTRIBUTED TESTING \"\n" + 
			"  specification: \"The information system implements automated mechanisms to support the management of distributed security testing.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-6(3) { \n" + 
			"  name: \"SECURITY FUNCTION VERIFICATION | REPORT VERIFICATION RESULTS \"\n" + 
			"  specification: \"The organization reports the results of security function verification to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7 { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY \"\n" + 
			"  specification: \"The organization employs integrity verification tools to detect unauthorized changes to [Assignment: organization-defined software, firmware, and information].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(1) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | INTEGRITY CHECKS \"\n" + 
			"  specification: \"The information system performs an integrity check of [Assignment: organization-defined software, firmware, and information] [Selection (one or more): at startup; at [Assignment: organization-defined transitional states or security-relevant events]; [Assignment: organization-defined frequency]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(2) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | AUTOMATED NOTIFICATIONS OF INTEGRITY VIOLATIONS \"\n" + 
			"  specification: \"The organization employs automated tools that provide notification to [Assignment: organization-defined personnel or roles] upon discovering discrepancies during integrity verification.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(3) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | CENTRALLY-MANAGED INTEGRITY TOOLS \"\n" + 
			"  specification: \"The organization employs centrally managed integrity verification tools.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(4) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | TAMPER-EVIDENT PACKAGING \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SA-12.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(5) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | AUTOMATED RESPONSE TO INTEGRITY VIOLATIONS \"\n" + 
			"  specification: \"The information system automatically [Selection (one or more): shuts the information system down; restarts the information system; implements [Assignment: organization-defined security safeguards]] when integrity violations are discovered.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(6) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | CRYPTOGRAPHIC PROTECTION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to detect unauthorized changes to software, firmware, and information.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(7) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | INTEGRATION OF DETECTION AND RESPONSE \"\n" + 
			"  specification: \"The organization incorporates the detection of unauthorized [Assignment: organization-defined security-relevant changes to the information system] into the organizational incident response capability.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(8) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | AUDITING CAPABILITY FOR SIGNIFICANT EVENTS \"\n" + 
			"  specification: \"The information system, upon detection of a potential integrity violation, provides the capability to audit the event and initiates the following actions: [Selection (one or more): generates an audit record; alerts current user; alerts [Assignment: organization-defined personnel or roles]; [Assignment: organization-defined other actions]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(9) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | VERIFY BOOT PROCESS \"\n" + 
			"  specification: \"The information system verifies the integrity of the boot process of [Assignment: organization-defined devices].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(10) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | PROTECTION OF BOOT FIRMWARE \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined security safeguards] to protect the integrity of boot firmware in [Assignment: organization-defined devices].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(11) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | CONFINED ENVIRONMENTS WITH LIMITED PRIVILEGES \"\n" + 
			"  specification: \"The organization requires that [Assignment: organization-defined user-installed software] execute in a confined physical or virtual machine environment with limited privileges.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(12) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | CONFINED ENVIRONMENTS WITH LIMITED PRIVILEGES \"\n" + 
			"  specification: \"The organization requires that the integrity of [Assignment: organization-defined user-installed software] be verified prior to execution.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(13) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | CODE EXECUTION IN PROTECTED ENVIRONMENTS \"\n" + 
			"  specification: \"The organization allows execution of binary or machine-executable code obtained from sources with limited or no warranty and without the provision of source code only in confined physical or virtual machine environments and with the explicit approval of [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(14) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | BINARY OR MACHINE EXECUTABLE CODE \"\n" + 
			"  specification: \"The organization: (a) Prohibits the use of binary or machine-executable code from sources with limited or no warranty and without the provision of source code; and (b) Provides exceptions to the source code requirement only for compelling mission/operational requirements and with the approval of the authorizing official.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(15) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | CODE AUTHENTICATION \"\n" + 
			"  specification: \"The information system implements cryptographic mechanisms to authenticate [Assignment: organization-defined software or firmware components] prior to installation.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-7(16) { \n" + 
			"  name: \"SOFTWARE, FIRMWARE, AND INFORMATION INTEGRITY | TIME LIMIT ON PROCESS EXECUTION W/O SUPERVISION \"\n" + 
			"  specification: \"The organization does not allow processes to execute without supervision for more than [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-8 { \n" + 
			"  name: \"SPAM PROTECTION \"\n" + 
			"  specification: \"The organization: a. Employs spam protection mechanisms at information system entry and exit points to detect and take action on unsolicited messages; and b. Updates spam protection mechanisms when new releases are available in accordance with organizational configuration management policy and procedures.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-8(1) { \n" + 
			"  name: \"SPAM PROTECTION | CENTRAL MANAGEMENT \"\n" + 
			"  specification: \"The organization centrally manages spam protection mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-8(2) { \n" + 
			"  name: \"SPAM PROTECTION | AUTOMATIC UPDATES \"\n" + 
			"  specification: \"The information system automatically updates spam protection mechanisms.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-8(3) { \n" + 
			"  name: \"SPAM PROTECTION | CONTINUOUS LEARNING CAPABILITY \"\n" + 
			"  specification: \"The information system implements spam protection mechanisms with a learning capability to more effectively identify legitimate communications traffic.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-9 { \n" + 
			"  name: \"INFORMATION INPUT RESTRICTIONS \"\n" + 
			"  specification: \"Withdrawn: Incorporated into AC-2, AC-3, AC-5, AC-6.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-10 { \n" + 
			"  name: \"INFORMATION INPUT VALIDATION \"\n" + 
			"  specification: \"The information system checks the validity of [Assignment: organization-defined information inputs].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-10(1) { \n" + 
			"  name: \"INFORMATION INPUT VALIDATION | MANUAL OVERRIDE CAPABILITY \"\n" + 
			"  specification: \"The information system: (a) Provides a manual override capability for input validation of [Assignment: organization-defined inputs]; (b) Restricts the use of the manual override capability to only [Assignment: organization-defined authorized individuals]; and (c) Audits the use of the manual override capability.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-10(2) { \n" + 
			"  name: \"INFORMATION INPUT VALIDATION | REVIEW / RESOLUTION OF ERRORS \"\n" + 
			"  specification: \"The organization ensures that input validation errors are reviewed and resolved within [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-10(3) { \n" + 
			"  name: \"INFORMATION INPUT VALIDATION | PREDICTABLE BEHAVIOR \"\n" + 
			"  specification: \"The information system behaves in a predictable and documented manner that reflects organizational and system objectives when invalid inputs are received.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-10(4) { \n" + 
			"  name: \"INFORMATION INPUT VALIDATION | REVIEW / TIMING INTERACTIONS \"\n" + 
			"  specification: \"The organization accounts for timing interactions among information system components in determining appropriate responses for invalid inputs.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-10(5) { \n" + 
			"  name: \"INFORMATION INPUT VALIDATION | RESTRICT INPUTS TO TRUSTED SOURCES AND APPROVED FORMATS \"\n" + 
			"  specification: \"The organization restricts the use of information inputs to [Assignment: organization-defined trusted sources] and/or [Assignment: organization-defined formats].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-11 { \n" + 
			"  name: \"ERROR HANDLING \"\n" + 
			"  specification: \"The information system: a. Generates error messages that provide information necessary for corrective actions without revealing information that could be exploited by adversaries; and b. Reveals error messages only to [Assignment: organization-defined personnel or roles].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-12 { \n" + 
			"  name: \"INFORMATION HANDLING AND RETENTION \"\n" + 
			"  specification: \"The organization handles and retains information within the information system and information output from the system in accordance with applicable federal laws, Executive Orders, directives, policies, regulations, standards, and operational requirements.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-13 { \n" + 
			"  name: \"PREDICTABLE FAILURE PREVENTION \"\n" + 
			"  specification: \"The organization: a. Determines mean time to failure (MTTF) for [Assignment: organization-defined information system components] in specific environments of operation; and b. Provides substitute information system components and a means to exchange active and standby components at [Assignment: organization-defined MTTF substitution criteria].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-13(1) { \n" + 
			"  name: \"PREDICTABLE FAILURE PREVENTION | TRANSFERRING COMPONENT RESPONSIBILITIES \"\n" + 
			"  specification: \"The organization takes information system components out of service by transferring component responsibilities to substitute components no later than [Assignment: organization-defined fraction or percentage] of mean time to failure.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-13(2) { \n" + 
			"  name: \"PREDICTABLE FAILURE PREVENTION | TIME LIMIT ON PROCESS EXECUTION WITHOUT SUPERVISION \"\n" + 
			"  specification: \"Withdrawn: Incorporated into SI-7 (16).\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-13(3) { \n" + 
			"  name: \"PREDICTABLE FAILURE PREVENTION | MANUAL TRANSFER BETWEEN COMPONENTS \"\n" + 
			"  specification: \"The organization manually initiates transfers between active and standby information system components [Assignment: organization-defined frequency] if the mean time to failure exceeds [Assignment: organization-defined time period].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-13(4) { \n" + 
			"  name: \"PREDICTABLE FAILURE PREVENTION | STANDBY COMPONENT INSTALLATION / NOTIFICATION \"\n" + 
			"  specification: \"The organization, if information system component failures are detected: (a) Ensures that the standby components are successfully and transparently installed within [Assignment: organization-defined time period]; and (b) [Selection (one or more): activates [Assignment: organization-defined alarm]; automatically shuts down the information system].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-13(5) { \n" + 
			"  name: \"PREDICTABLE FAILURE PREVENTION | FAILOVER CAPABILITY \"\n" + 
			"  specification: \"The organization provides [Selection: real-time; near real-time] [Assignment: organization-defined failover capability] for the information system.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-14 { \n" + 
			"  name: \"NON-PERSISTENCE \"\n" + 
			"  specification: \"The organization implements non-persistent [Assignment: organization-defined information system components and services] that are initiated in a known state and terminated [Selection (one or more): upon end of session of use; periodically at [Assignment: organization-defined frequency]].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-14(1) { \n" + 
			"  name: \"NON-PERSISTENCE | REFRESH FROM TRUSTED SOURCES \"\n" + 
			"  specification: \"The organization ensures that software and data employed during information system component and service refreshes are obtained from [Assignment: organization-defined trusted sources].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-15 { \n" + 
			"  name: \"INFORMATION OUTPUT FILTERING \"\n" + 
			"  specification: \"The information system validates information output from [Assignment: organization-defined software programs and/or applications] to ensure that the information is consistent with the expected content.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-16 { \n" + 
			"  name: \"MEMORY PROTECTION \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined security safeguards] to protect its memory from unauthorized code execution.\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"  security control SI-17 { \n" + 
			"  name: \"FAIL-SAFE PROCEDURES \"\n" + 
			"  specification: \"The information system implements [Assignment: organization-defined fail-safe procedures] when [Assignment: organization-defined failure conditions occur].\n" + 
			"		 \"\n" + 
			"  domain: MUSASEC.SI \n" + 
			"  } \n" + 
			"\n" + 
			"\n" + 
			"}}\n" + 
			"";
}



