package eu.musa.deployer.parsers;


import org.junit.Test;

import eu.musa.deployer.TestConfigurator;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class TestParser {
	
	@Test
	public void generatePlanFromCamel() {
		try {
			String token = TestConfigurator.configure();
			Parser parser = new Parser("771", token);
			Plan plan = parser.parse();
			System.out.println(plan.toString());
			assertTrue(plan != null);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
}
