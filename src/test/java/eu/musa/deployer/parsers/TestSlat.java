package eu.musa.deployer.parsers;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.w3c.dom.Element;

public class TestSlat {
	
	@Test
	public void test(){
		Slat slat;
		try{
			Slat.mustSiMULATE=false;
			// SLA 771, currently invalid because there is no serviceResources
			slat = new Slat("771", "http://framework.musa-project.eu/sla-manager/cloud-sla/slas/5A8ADA332CDC0A07EEB107B1", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6Im9wZXJhdG9yMUBtdXNhLXByb2plY3QuZXUiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXBwX21ldGFkYXRhIjp7InJvbGVzIjoib3BlcmF0b3IiLCJncm91cHMiOlsidGVzdGluZyIsImZsaWdodF9zY2hlZHVsaW5nIiwic21hcnRfbW9iaWxpdHkiXSwib3JnYW5pemF0aW9uIjoibXVzYSJ9LCJpc3MiOiJodHRwczovL211c2EtcHJvamVjdC5ldS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTk1MjYzMjFhM2U0Mzc2ZmViZDQwNmQ0IiwiYXVkIjoiUVV2Z2Rhak53VEJYdzlaeWF5VFJLU2VsR2lpdWRvMEoiLCJpYXQiOjE1MTkwNjMzODksImV4cCI6MTUxOTA5OTM4OX0.zr7x8MU7-a0z1Uzisqoc3HQhgA1JtZEslDcGs-jbWBY");
			Element iaas = slat.getIaaS();
			 assertNotNull(iaas);
			Element vm = slat.getVM();
			assertNotNull(vm);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
}
