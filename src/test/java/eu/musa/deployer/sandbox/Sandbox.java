package eu.musa.deployer.sandbox;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.musa.deployer.framework.ClientSession;
import eu.musa.deployer.web.rest.RESTClient;

public class Sandbox {

	public static void main(String[] args) throws Exception {
		getComponentsFromSession("http://framework.musa-project.eu/session/", "120","eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6Im1hbmFnZXIxQG11c2EtcHJvamVjdC5ldSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhcHBfbWV0YWRhdGEiOnsicm9sZXMiOiJtYW5hZ2VyIiwiZ3JvdXBzIjpbInRlc3RpbmciLCJmbGlnaHRfc2NoZWR1bGluZyIsInNtYXJ0X21vYmlsaXR5Il19LCJpc3MiOiJodHRwczovL211c2EtcHJvamVjdC5ldS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTk1MjYzNmIzYmU1MzU0YjZhNzVmZjQ1IiwiYXVkIjoiUVV2Z2Rhak53VEJYdzlaeWF5VFJLU2VsR2lpdWRvMEoiLCJleHAiOjE1MDYwMTg4OTEsImlhdCI6MTUwNTk4Mjg5MX0.jtcZoI3nwMtv2Nod1ODjZVB3RX336koRhKsFwwSzaPk");
	}
	
	private static HashMap<String, Object> getSession(String url,String id, String token) throws Exception {
		Map<String,String> headers = new HashMap<>();
		headers.put(ClientSession.KEY_SecurityHeader, token);
		String data = RESTClient.GET(url + id, headers);
		ObjectMapper mapper = new ObjectMapper();
		return (HashMap<String, Object>) mapper.readValue(data, new TypeReference<HashMap<String, Object>>(){});
	}
	
	private static Map<String, String> getComponentsFromSession(String url, String id, String token) throws Exception {
		HashMap<String, Object> session = getSession(url,id, token);
		Map<String, String> components = new HashMap<String, String>(); 
		List<Map<String,Object>> columns = (List<Map<String,Object>>) session.get("columns");
		for (Map<String,Object> column : columns) {
			List<Map<String,Object>> items = (List<Map<String,Object>> ) column.get("items");
			for (Map<String,Object> item : items) {
				String name = item.get("text").toString();
				String cid =  item.get("cid").toString();
				System.out.println(name + "     " + cid);
			}
	
		}
		
		return null;
	}
}
